python -m pip install --upgrade pip
pip install opencv_python-4.4.0-cp37-cp37m-win_amd64.whl
pip install pandas-1.2.1-cp37-cp37m-win_amd64.whl
pip install PySide2
pip install matplotlib
pip install numpy
pip install Pyvisa==1.8
pip install IPython
pip install pylef