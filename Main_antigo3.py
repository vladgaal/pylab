print("Aguarde, carregando PyLab...")
import sys
#from win32com.shell import shell, shellcon
import cv2
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from PySide.QtGui import *
from PySide.QtCore import *
from PySide import QtCore, QtGui
from ui_files import mainDialogV4 as mainDialog
from ui_files import sobre
from ui_files import tutorial
from ui_files import duvida
import pylef    # pylef para controlar instrumentos
import visa
import numpy as np   # Numpy para lidar com matrizes
import pandas as pd
import os   # module for general OS manipulation
import time
import copy
import csv
import webbrowser
import random

__appname__ = "PyLab"
__version__ = "0.6.0"
__modulo__ = 'M'
__videoURL__ = "https://www.youtube.com/watch?v=FevnHbuhJjI"


class Oprograma(QDialog, sobre.Ui_Dialog):

    def __init__(self, parent=None):
        super(Oprograma, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)
        self.setWindowTitle(__appname__ + " - Sobre o programa")
        self.okBt.clicked.connect(self.close)
        self.label_3.setText(QtGui.QApplication.translate("Dialog", "V " + __version__, None, QtGui.QApplication.UnicodeUTF8))


class Duvida(QDialog, duvida.Ui_Dialog):

    def __init__(self, parent=None, indice=0):
        super(Duvida, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)
        self.setWindowTitle(__appname__ + " - Instruções para utilização")
        self.okBt.clicked.connect(self.close)
        self.tabWidget.setCurrentIndex(indice)


class Tutorial(QDialog, tutorial.Ui_Dialog):

    def __init__(self, parent=None):
        super(Tutorial, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)
        self.setWindowTitle(__appname__ + " - Tutorial de configuração dos Osciloscópio")
        self.okBt.clicked.connect(self.close)
        self.videoBt.clicked.connect(self.chamaVideo)
        self.label_3.setText(
            QtGui.QApplication.translate("Dialog", "V " + __version__, None, QtGui.QApplication.UnicodeUTF8))

    def chamaVideo(self):
        webbrowser.open_new_tab(__videoURL__)


class MySignal(QObject):
    status = Signal(str)
    falha = Signal(str)
    medida = Signal(float, float, float, float, int)
    terminado = Signal(str)
    terminadoTela = Signal(str)
    terminadoFft = Signal(str)
    tela = Signal(object, object, object)
    fftSignal = Signal(object, object)
    resultado = Signal(int, int, int)


class GraphThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.tempo = 1500
        self.dadosX = []
        self.dadosY1 = []
        self.dadosY2 = []
        self.signal = MySignal()
        self.graficoBranco = False
        self.telaBranca = False
        self.varreduraFreq = False
        self.tela = False
        self.fft = False
        self.fftBranco = False
        self.escalaCH1 = 1
        self.escalaCH2 = 1
        self.ch1 = False
        self.ch2 = False

    def run(self):
        __nome__ = __modulo__ + '07'

        if self.varreduraFreq:
            mensagem = 'Gerando grafico Freq'
            print('{};{}'.format(__nome__, mensagem))
            if len(self.dadosY1) >= 1:
                npt = len(self.dadosY1)
                fig, ax = plt.subplots(2, sharex=True, figsize=(7.5, 4.2))

                ax[0] = plt.subplot(211)  # define um eixo
                ax[0].plot(self.dadosX[0:npt], self.dadosY1[0:npt], 'go')  # plota a transmissão
                ax[0].set_xscale('log')
                ax[0].set_ylabel('Transmissão (dB)')  # seta escala do eixo y
                ax[0].grid(True)
                ax[0].axhline(y=-3, c="R", linewidth=1)
                ax[0].set_ylim((min(self.dadosY1)-0.2*abs(min(self.dadosY1)), max(self.dadosY1)+0.2*abs(max(self.dadosY1))))
                ax[0].set_xlim((min(self.dadosX), max(self.dadosX)))

                ax[1] = plt.subplot(212)  # define um eixo
                ax[1].plot(self.dadosX[0:npt], self.dadosY2[0:npt], 'bo')  # plota a transmissão
                ax[1].set_xscale('log')  # seta a escala de x para logaritmica
                # Por que não usamos escala log no eixo y também?
                ax[1].set_xlabel('frequência (Hz)')  # seta escala do eixo x
                ax[1].set_ylabel('Fase (graus)')  # seta escala do eixo y
                ax[1].grid(True)
                ax[1].axhline(y=90, c="R", linewidth=1)
                ax[1].axhline(y=-90, c="R", linewidth=1)
                ax[1].axhline(y=0, c="R", linewidth=1)
                ax[1].axhline(y=-180, c="R", linewidth=1)
                ax[1].axhline(y=180, c="R", linewidth=1)
                ax[1].set_ylim((min(self.dadosY2)-0.2*abs(min(self.dadosY2)), max(self.dadosY2)+0.2*abs(max(self.dadosY2))))
                ax[1].set_xlim((min(self.dadosX), max(self.dadosX)))

                fig.tight_layout()
                fig.savefig('varreduraFreq.png', format='png', bbox_inches='tight')
                plt.clf()
                plt.close()

                self.signal.terminado.emit('ok')

            time.sleep(0.001 * self.tempo)
            self.varreduraFreq = False

        if self.graficoBranco:
            mensagem = 'Gerando grafico branco'
            print('{};{}'.format(__nome__, mensagem))

            fig, ax = plt.subplots(2, sharex=True, figsize=(8, 4.2))

            ax[0] = plt.subplot(211)  # define um eixo
            ax[0].plot([1], [1], 'ro')
            ax[0].set_xscale('log')
            ax[0].set_ylabel('Transmissão (dB)')  # seta escala do eixo y
            ax[0].grid(True)
            ax[0].set_ylim(-5., 1.)
            ax[0].set_xlim(10., 100000.)

            ax[1] = plt.subplot(212)  # define um eixo
            ax[1].plot([1], [360], 'bo')
            ax[1].set_xscale('log')  # seta a escala de x para logaritmica
            # Por que não usamos escala log no eixo y também?
            ax[1].set_xlabel('frequência (Hz)')  # seta escala do eixo x
            ax[1].set_ylabel('Fase (graus)')  # seta escala do eixo y
            ax[1].grid(True)
            ax[1].set_ylim(-180, 180)
            ax[1].set_xlim(10, 100000)

            fig.savefig('varreduraFreq.png', bbox_inches='tight')
            plt.clf()
            plt.close()

            self.signal.terminado.emit('ok')
            self.graficoBranco = False

        if self.tela:
            mensagem = 'Gerando grafico tela em VT'
            print('{};{}'.format(__nome__, mensagem))
            x = np.array(self.dadosX)

            if self.ch1:
                y1 = np.array(self.dadosY1)
            else:
                y1 = np.zeros(len(self.dadosX))
            if self.ch2:
                y2 = np.array(self.dadosY2)
            else:
                y2 = np.zeros(len(self.dadosX))

            y3 = y1 - y2

            #if self.ch1 and self.ch2:
            #else:
             #   y3 = np.zeros(len(self.dadosX))

            if self.ch1 and self.ch2:
                plt.style.use('dark_background')
                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(x, y1, 'y-')
                ax1.set_xlabel('tempo (s)')
                # Make the y-axis label, ticks and tick labels match the line color.
                ax1.set_ylabel('tensão CH1 (V)', color='y')
                ax1.tick_params('y', colors='y')
                if abs(max(y1) - min(y1)) > 0.025:
                    ax1.axis([min(x), max(x), min(y1), max(y1)])
                else:
                    ax1.axis([min(x), max(x), -1, 1])
                ax1.grid(True)

                ax2 = ax1.twinx()
                ax2.plot(x, y2, linestyle='-', color='cyan')
                ax2.set_ylabel('tensão CH2 (V)', color='cyan')
                ax2.tick_params('y', colors='cyan')
                if abs(max(y2) - min(y2)) > 0.025:
                    ax2.axis([min(x), max(x), min(y2), max(y2)])
                else:
                    ax2.axis([min(x), max(x), -1, 1])
                ax2.grid(True)

                fig.tight_layout()
                fig.savefig('telaVT.png', bbox_inches='tight')
                plt.clf()
                plt.close()

                mensagem = 'Gerando grafico tela em IV'
                print('{};{}'.format(__nome__, mensagem))

                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(y1, y2, 'ro')
                ax1.set_xlabel('tensão CH1 (V)')
                # Make the y-axis label, ticks and tick labels match the line color.
                ax1.set_ylabel('tensão CH2 (V)')
                ax1.grid(True)

                fig.tight_layout()
                fig.savefig('telaIV.png', bbox_inches='tight')
                plt.clf()
                plt.close()

                mensagem = 'Gerando grafico tela Ch1 - Ch2 x Ch2'
                print('{};{}'.format(__nome__, mensagem))

                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(y2, y3, 'ro')
                ax1.set_ylabel('tensão CH1 - CH2 (V)')
                ax1.set_xlabel('tensão CH2 (V)')
                ax1.grid(True)

                fig.tight_layout()
                fig.savefig('telaCh122.png', bbox_inches='tight')
                plt.clf()
                plt.close()

            elif self.ch1 and self.ch2 is False:
                plt.style.use('dark_background')

                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(x, y1, 'y-')
                ax1.set_xlabel('tempo (s)')
                # Make the y-axis label, ticks and tick labels match the line color.
                ax1.set_ylabel('tensão CH1 (V)', color='y')
                ax1.tick_params('y', colors='y')
                if abs(max(y1) - min(y1)) > 0.025:
                    ax1.axis([min(x), max(x), min(y1), max(y1)])
                else:
                    ax1.axis([min(x), max(x), -1, 1])
                ax1.grid(True)

                fig.tight_layout()
                fig.savefig('telaVT.png', bbox_inches='tight')
                plt.clf()
                plt.close()

                mensagem = 'Gerando grafico tela em IV'
                print('{};{}'.format(__nome__, mensagem))

                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(y1, y2, 'ro')
                ax1.set_xlabel('tensão CH1 (V)')
                # Make the y-axis label, ticks and tick labels match the line color.
                ax1.set_ylabel('tensão CH2 (V)')
                ax1.grid(True)

                fig.tight_layout()
                fig.savefig('telaIV.png', bbox_inches='tight')
                plt.clf()
                plt.close()

                mensagem = 'Gerando grafico tela Ch1 - Ch2 x Ch2'
                print('{};{}'.format(__nome__, mensagem))

                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(y2, y3, 'ro')
                ax1.set_ylabel('tensão CH1 - CH2 (V)')
                ax1.set_xlabel('tensão CH2 (V)')
                ax1.grid(True)

                fig.tight_layout()
                fig.savefig('telaCh122.png', bbox_inches='tight')
                plt.clf()
                plt.close()

            elif self.ch2 and self.ch1 is False:
                plt.style.use('dark_background')

                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(x, y2, linestyle='-', color='cyan')
                ax1.set_xlabel('tempo (s)')
                # Make the y-axis label, ticks and tick labels match the line color.
                ax1.set_ylabel('tensão CH2 (V)', color='cyan')
                ax1.tick_params('y', colors='cyan')
                if abs(max(y2) - min(y2)) > 0.025:
                    ax1.axis([min(x), max(x), min(y2), max(y2)])
                else:
                    ax1.axis([min(x), max(x), -1, 1])
                ax1.grid(True)

                fig.tight_layout()
                fig.savefig('telaVT.png', bbox_inches='tight')
                plt.clf()
                plt.close()

                mensagem = 'Gerando grafico tela em IV'
                print('{};{}'.format(__nome__, mensagem))

                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(y1, y2, 'ro')
                ax1.set_xlabel('tensão CH1 (V)')
                # Make the y-axis label, ticks and tick labels match the line color.
                ax1.set_ylabel('tensão CH2 (V)')
                ax1.grid(True)

                fig.tight_layout()
                fig.savefig('telaIV.png', bbox_inches='tight')
                plt.clf()
                plt.close()

                mensagem = 'Gerando grafico tela Ch1 - Ch2 x Ch2'
                print('{};{}'.format(__nome__, mensagem))

                fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
                ax1.plot(y2, y3, 'ro')
                ax1.set_ylabel('tensão CH1 - CH2 (V)')
                ax1.set_xlabel('tensão CH2 (V)')
                ax1.grid(True)

                fig.tight_layout()
                fig.savefig('telaCh122.png', bbox_inches='tight')
                plt.clf()
                plt.close()

            self.signal.terminadoTela.emit('ok')
            self.tela = False

        if self.telaBranca:
            plt.style.use('dark_background')
            mensagem = 'Gerando Tela em branco VT'
            print('{};{}'.format(__nome__, mensagem))
            x = np.arange(1024)
            x = x / 1023
            x = x - max(x) / 2
            y1 = np.zeros(1024)
            y2 = np.zeros(1024)
            y3 = np.zeros(1024)

            fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
            ax1.plot(x, y1, 'y-')
            ax1.set_xlabel('tempo (s)')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('tensão CH1 (V)', color='y')
            ax1.tick_params('y', colors='y')
            ax1.grid(True)
            ax1.set_ylim(-1, 1)
            ax1.set_xlim(min(x), max(x))

            ax2 = ax1.twinx()
            ax2.plot(x, y2, linestyle='-', color='cyan')
            ax2.set_ylabel('tensão CH2 (V)', color='cyan')
            ax2.tick_params('y', colors='cyan')
            ax2.grid(True)
            ax2.set_ylim(-1, 1)

            fig.tight_layout()
            fig.savefig('telaVT.png', bbox_inches='tight')
            plt.clf()
            plt.close()

            mensagem = 'Gerando Tela em branco IV'
            print('{};{}'.format(__nome__, mensagem))

            fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
            ax1.plot(y1, y2, 'ro')
            ax1.set_xlabel('tensão CH1 (V)')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('tensão CH2 (V)')
            ax1.grid(True)

            fig.tight_layout()
            fig.savefig('telaIV.png', bbox_inches='tight')
            plt.clf()
            plt.close()

            mensagem = 'Gerando grafico tela em branco Ch1 - Ch2 x Ch2'
            print('{};{}'.format(__nome__, mensagem))

            fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
            ax1.plot(y2, y3, 'ro')
            ax1.set_ylabel('tensão CH1 - CH2 (V)')
            ax1.set_xlabel('tensão CH2 (V)')
            ax1.grid(True)

            fig.tight_layout()
            fig.savefig('telaCh122.png', bbox_inches='tight')
            plt.clf()

            self.signal.terminadoTela.emit('ok')
            self.telaBranca = False

        if self.fft:
            mensagem = 'Gerando grafico FFT'
            print('{};{}'.format(__nome__, mensagem))
            x = np.array(self.dadosX)
            if len(self.dadosY1) > 0:
                y1 = np.array(self.dadosY1)
            else:
                y1 = np.zeros(len(self.dadosX))

            fig, ax1 = plt.subplots(figsize=(8.5, 4.2))
            #ax1.plot(x, y1, 'r-')
            #ax1.semilogx(x, y1, 'r')  # plotting the spectrum
            ax1.stem(x, y1, linefmt='r-', markerfmt='ro', basefmt='')
            ax1.set_xscale("log")
            ax1.set_xlabel('Frequência (Hz)')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('|Y(freq)|')
            ax1.axis([1e1, max(x), 0, max(y1)])
            ax1.grid(True)

            fig.tight_layout()
            fig.savefig('fft.png', bbox_inches='tight')
            plt.show()

            self.signal.terminadoFft.emit('ok')
            self.fft = False

        if self.fftBranco:
            mensagem = 'Gerando grafico FFT em branco'
            print('{};{}'.format(__nome__, mensagem))
            x = np.linspace(10, 1e6, 1000)
            y1 = np.zeros(len(x))

            fig, ax1 = plt.subplots(figsize=(8.5, 4.2))
            ax1.semilogx(x, y1, 'r')  # plotting the spectrum
            ax1.set_xlabel('Frequência (Hz)')
            ax1.set_ylabel('|Y(freq)|')
            ax1.axis([1e1, max(x), 0, 1])
            ax1.grid(True)

            fig.tight_layout()
            fig.savefig('fft.png', bbox_inches='tight')
            plt.show()
            self.fftBranco = False
            self.signal.terminadoFft.emit('ok')


class EquipamentosThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.conectarScope = False
        self.conectarGerador = False
        self.desconectarScope = False
        self.desconectarGerador = False
        self.varrerFreq = False
        self.capturarTela = False
        self.scopeConectado = False
        self.geradorConectado = False
        self.interromperFreq = False
        self.fft = False
        self.embaralharGerador = False
        self.embaralharOsciloscopio = False
        self.verificarResposta = False
        self.dificuldade = 0
        self.freqInicial = 0
        self.freqFinal = 0
        self.quantidadePontos = 0
        self.medias = 4
        self.signal = MySignal()
        self.leitura = 0
        self.trigger = 'CH1'
        self.ch1 = True
        self.ch2 = True
        self.tempo = True
        self.tensaoVetNum = [1, 2, 5]
        self.tensaoVetExp = [-1, 0, -2]
        self.freqVetNum = [1, 2, 5]
        self.freqVetExp = [0, 1, 2, 3, 4, 5, 6]
        self.ondaVet = ["SINE", "SQUARE", "RAMP"]
        self.trigVet = ["CH1", "CH2", "AC LINE"]
        self.scalaTensaoVet = [5, 1, 0.1, 0.01, 0.001]
        self.scalaTempoVetNum = [1, 2, 5]
        self.scalaTempoVetExp = [-1, -3, -6, -9, -5, -7]
        self.tensaoMultpVet = [1, 10, 100]
        self.tensaoPosVet = [-5, -1, 0, 1, 5, -0.1, 0.1]
        self.trigPosVet = [-5, -1, 0, 1, 5]
        self.tensaoAtual = 0
        self.freqAtual = 0
        self.ondaAtual = ""
        self.userRespTensao = 0
        self.userRespFreq = 0
        self.userRespOnda = ""

    def run(self):
        __nome__ = __modulo__ + '04'
        if self.conectarScope:
            try:
                mensagem = 'Tentando conectar Scope'
                print('{};{}'.format(__nome__, mensagem))
                self.signal.status.emit('Conectando Scope')
                visa.ResourceManager().list_resources()
                self.scope = pylef.TektronixTBS1062()
                self.scope.start_acquisition()
                self.scope.set_sample()
                self.scope.ch1.set_probe(1)
                self.scope.ch2.set_probe(1)
                self.scope.ch1.set_position(0)
                self.scope.ch2.set_position(0)
                self.scope.ch1.turn_on()
                self.scope.ch2.turn_on()
                self.scope.ch1.set_scale(2)
                self.scope.ch2.set_scale(2)
                self.scopeConectado = True
                self.signal.status.emit('Scope conectado')

            except ValueError:
                self.signal.falha.emit('Scope')
                self.scopeConectado = False
                self.signal.status.emit('Scope desconectado')
                mensagem = 'Problema ao conectar Scope'
                print('{};{}'.format(__nome__, mensagem))
            self.conectarScope = False

        if self.desconectarScope:
            if self.scopeConectado:
                mensagem = 'Desconectar Scope'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.close()
                self.scopeConectado = False
                self.signal.status.emit('Scope desconectado')
            self.desconectarScope = False

        if self.conectarGerador:
            try:
                mensagem = 'Tentando conectar Gerador'
                print('{};{}'.format(__nome__, mensagem))
                self.signal.status.emit('Conectando Gerador')
                visa.ResourceManager().list_resources()
                self.gerador = pylef.BK4052()
                self.geradorConectado = True
                self.signal.status.emit('Gerador conectado')

            except ValueError:
                self.signal.falha.emit('Gerador')
                self.geradorConectado = False
                self.signal.status.emit('Gerador desconectado')
                mensagem = 'Problema ao conectar Gerador'
                print('{};{}'.format(__nome__, mensagem))
            self.conectarGerador = False

        if self.desconectarGerador:
            if self.geradorConectado:
                mensagem = 'Desconectar Gerador'
                print('{};{}'.format(__nome__, mensagem))
                self.gerador.close()
                self.geradorConectado = False
                self.signal.status.emit('Gerador desconectado')
            self.desconectarGerador = False

        if self.varrerFreq:
            self.freq = np.logspace(np.log10(self.freqInicial), np.log10(self.freqFinal), self.quantidadePontos,
                                    endpoint=True)  # varredura logaritmica
            self.scope.set_average_number(self.medias)  # ajusta o número de médias
            self.scope.set_average()  # turn average ON
            # -----------------
            self.Vpp1 = 0
            self.Vpp2 = 0  # listas para guardar as variáveis
            self.phase = 0  # listas para guardar as variáveis
            ### aquisição de dados no gerador com varredura de frequência
            self.scope.write('MEASUREment:MEAS1:TYPE NONE')
            self.scope.write('MEASUREment:MEAS2:TYPE NONE')
            self.scope.write('MEASUREment:MEAS3:TYPE NONE')
            self.scope.write('MEASUREment:MEAS4:TYPE NONE')
            self.scope.write('MEASUREment:MEAS5:TYPE NONE')
            self.scope.ch1.turn_on()
            self.scope.ch2.turn_on()
            self.scope.ch1.set_ac()
            self.scope.ch2.set_ac()
            self.scope.ch1.set_position(0)
            self.scope.ch2.set_position(0)
            self.gerador.ch1.turn_on()
            #Ligar o gerador
            for m, freqP in enumerate(list(self.freq)):  # loop de aquisição
                mensagem = 'Ponto de medida ' + str(m) + ': ' + str(freqP) + 'Hz'
                print('{};{}'.format(__nome__, mensagem))
                ### ajuste dos instrumentos
                self.gerador.ch1.set_frequency(freqP)  # muda a frequência
                periodP = 1. / freqP  # período da onda
                self.scope.set_horizontal_scale(periodP / 4.)  # escala horizontal = 1/4 período (2.5 oscilações em tela)
                self.scope.ch1.set_smart_scale()  # rescala o canal 1
                self.scope.ch2.set_smart_scale()  # rescala o canal 2
                ### aquisição de dados
                self.Vpp1 = (self.scope.ch1.measure.Vpp())  # acumula a medida do Vpp no canal 1
                self.phase = (-self.scope.ch1.measure.phase())  # acumula a medida da fase no canal 1
                self.Vpp2 = (self.scope.ch2.measure.Vpp())  # acumula a medida do Vpp no canal 2
                self.signal.medida.emit(self.Vpp1, self.Vpp2, self.phase, freqP, m)
                if self.interromperFreq:
                    break
            self.signal.terminado.emit('OK')
            self.varrerFreq = False

        if self.capturarTela:
            mensagem = 'Lendo canais'
            print('{};{}'.format(__nome__, mensagem))
            canal1 = []
            canal2 = []
            tempo = []

            if self.leitura == 0:
                self.scope.set_sample()
            else:
                self.scope.set_average_number(self.leitura)  # ajusta o número de médias

            self.scope.trigger.set_source(self.trigger)
            self.scope.trigger.set_to_50()

            if self.ch1:
                mensagem = 'Lendo canal 1'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.ch1.turn_on()
                time.sleep(0.5)
                tempo, canal1 = self.scope.ch1.read_channel()
            else:
                canal1 = np.zeros(2048)

            if self.ch2:
                mensagem = 'Lendo canal 2'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.ch2.turn_on()
                time.sleep(0.5)
                tempo, canal2 = self.scope.ch2.read_channel()
            else:
                canal2 = np.zeros(2048)

            if self.ch1 is False and self.ch2 is False:
                tempo = np.zeros(2048)


            self.signal.tela.emit(canal1, canal2, tempo)
            self.capturarTela = False

        if self.fft:
            mensagem = 'Lendo canal'
            print('{};{}'.format(__nome__, mensagem))

            if self.ch1:
                tempo, canal1 = self.scope.ch1.read_channel()
                escala = self.scope.horizontal_scale()
                tempoReal = np.linspace(0, escala*10, len(tempo))
                self.signal.fftSignal.emit(tempo, canal1)

            if self.ch2:
                tempo, canal2 = self.scope.ch2.read_channel()
                self.signal.fftSignal.emit(tempo, canal2)

            self.fft = False

        if self.embaralharGerador:
            self.tensaoAtual = self.tensaoVetNum[random.randint(0, len(self.tensaoVetNum)-1)] * \
                               10 ** self.tensaoVetExp[random.randint(0, len(self.tensaoVetExp)-1)]
            self.freqAtual = self.freqVetNum[random.randint(0, len(self.freqVetNum)-1)] * \
                               10 ** self.freqVetExp[random.randint(0, len(self.freqVetExp)-1)]
            self.ondaAtual = self.ondaVet[random.randint(0, len(self.ondaVet)-1)]

            if random.randint(0, 1):
                self.gerador.ch1.turn_on()
            else:
                self.gerador.ch1.turn_off()

            self.gerador.ch1.set_frequency(self.freqAtual)
            self.gerador.ch1.set_function(self.ondaAtual)
            self.gerador.ch1.set_Vpp(self.tensaoAtual)

            self.embaralharGerador = False

        if self.embaralharOsciloscopio:
            if random.randint(0, 1):
                self.scope.ch1.turn_on()
            else:
                self.scope.ch1.turn_off()

            self.scope.trigger.set_source(self.trigVet[random.randint(0, len(self.trigVet)-1)])

            self.scope.ch1.set_scale(self.scalaTensaoVet[random.randint(0, len(self.scalaTensaoVet)-1)])

            self.scope.set_horizontal_scale(self.scalaTempoVetNum[random.randint(0, len(self.scalaTempoVetNum) - 1)] *
                                            10 ** self.scalaTempoVetExp[random.randint(0, len(self.scalaTempoVetExp) - 1)])

            if self.dificuldade > 0:
                teste = random.randint(0, 2)
                if teste == 0:
                    self.scope.ch1.set_ac()
                elif teste == 1:
                    self.scope.ch1.set_dc()
                else:
                    self.scope.ch1.set_ground()

            self.scope.ch1.set_position(self.tensaoPosVet[random.randint(0, len(self.tensaoPosVet)-1)])

            if self.dificuldade > 1:
                self.scope.trigger.set_level(self.trigPosVet[random.randint(0, len(self.trigPosVet) - 1)])

            self.embaralharOsciloscopio = False

        if self.verificarResposta:
            if self.ondaAtual == self.userRespOnda:
                resp1 = 1
            else:
                resp1 = 0

            if self.userRespTensao >= (self.tensaoAtual - self.tensaoAtual * 0.05) and self.userRespTensao <= (self.tensaoAtual + self.tensaoAtual * 0.05):
                resp2 = 1
            else:
                resp2 = 0

            if self.userRespFreq >= (self.freqAtual - self.freqAtual * 0.05) and self.userRespFreq <= (self.freqAtual + self.freqAtual * 0.05):
                resp3 = 1
            else:
                resp3 = 0

            self.signal.resultado.emit(resp1, resp2, resp3)
            self.verificarResposta = False


class MainDialog(QDialog, mainDialog.Ui_Dialog):

    #scopeConectado = False
    geradorConectado = False
    average = 4
    tensao1 = []
    tensao2 = []
    fase = []
    frequencia = []
    transmitancia = []
    transmitanciaDb = []
    ch1Tela = []
    ch2Tela = []
    tempoTela = []
    diretorio = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
    #diretorio = '*'
    nota = 0.


    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)

        self.iconConectar = QtGui.QIcon()
        self.iconConectar.addPixmap(QtGui.QPixmap(":/Main/icons/conectar.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.iconDesconectar = QtGui.QIcon()
        self.iconDesconectar.addPixmap(QtGui.QPixmap(":/Main/icons/desconectar.png"), QtGui.QIcon.Normal,
                                       QtGui.QIcon.Off)
        self.iconCerto = QtGui.QIcon()
        self.iconCerto.addPixmap(QtGui.QPixmap(":/Main/icons/correto.png"), QtGui.QIcon.Normal,
                                       QtGui.QIcon.Off)
        self.iconErrado = QtGui.QIcon()
        self.iconErrado.addPixmap(QtGui.QPixmap(":/Main/icons/errado.png"), QtGui.QIcon.Normal,
                                 QtGui.QIcon.Off)

        self.conectarBtOsc.setIcon(self.iconConectar)
        self.conectarBtGer.setIcon(self.iconConectar)

        self.setWindowTitle(__appname__ + ' V' + __version__)
        self.stackedWidget.setCurrentIndex(1)
        self.modelosGerCombo.addItems(['', 'BK 4052'])
        self.modelosOscCombo.addItems(['', 'TBS1062'])
        self.modelosOscCombo.setCurrentIndex(1)
        self.modelosGerCombo.setCurrentIndex(1)
        self.freqInicialBox.addItems(['Hz', 'KHz', 'MHz'])
        self.freqFinalBox.addItems(['Hz', 'KHz', 'MHz'])
        self.mediaBox.addItems(['4', '16', '64', '128'])
        self.progressBar.setRange(0, 100)
        self.progressBar.setValue(0)
        self.vtRBt.setChecked(True)

        self.userRespFreqExp.addItems(['Hz', 'KHz', 'MHz'])
        self.userRespAmpExp.addItems(['V', 'mV'])
        self.userRespOnda.addItems(['', 'Senoidal', 'Quadrada', 'Triangular'])
        self.userRespOnda.setCurrentIndex(0)
        self.userRespAmpExp.setCurrentIndex(0)
        self.userRespFreqExp.setCurrentIndex(0)
        self.userRespAmpNum.setInputMask('9999')
        self.userRespAmpNum.setText('10')
        self.userRespFreqNum.setInputMask('9999')
        self.userRespFreqNum.setText('10')
        self.notaLabel.setText('--')
        self.notaLabel.setStyleSheet("QLabel#notaLabel {color: black; font: bold 18pt; " + "font-family: Tahoma;}")
        self.embGerBt.clicked.connect(self.embaralharGerador)
        self.embOscBt.clicked.connect(self.embaralharOsciloscopio)
        self.respostaBt.clicked.connect(self.verificaResp)
        self.respostaBt.setEnabled(False)
        self.userRespOndaVerif.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
        self.userRespAmpVerif.setStyleSheet("background-color: rgba(255, 255, 255, 0);")
        self.userRespFreqVerif.setStyleSheet("background-color: rgba(255, 255, 255, 0);")

        self.graph_thread = GraphThread()
        self.graph_thread.signal.terminado.connect(self.atualizaGraficoFreq)
        self.graph_thread.signal.terminadoTela.connect(self.atualizaGraficoTela)
        self.graph_thread.signal.terminadoFft.connect(self.atualizaGraficoFft)
        self.equipamentos_thread = EquipamentosThread()
        self.equipamentos_thread.start()
        self.equipamentos_thread.signal.status.connect(self.atualizaStatus)
        self.equipamentos_thread.signal.falha.connect(self.falha)
        self.equipamentos_thread.signal.medida.connect(self.recebendoMedidaFreq)
        self.equipamentos_thread.signal.terminado.connect(self.varreduraFreqTerminada)
        self.equipamentos_thread.signal.tela.connect(self.recebendoTela)
        self.equipamentos_thread.signal.fftSignal.connect(self.calculoFft)
        self.equipamentos_thread.signal.resultado.connect(self.gerarNota)

        #self.timer = QTimer()
        #self.timer.timeout.connect(self.atualizaStatus)
        #self.timer.start(500)

        self.habilitarCH1()
        self.habilitarCH2()
        self.canalUmCheck.toggled.connect(self.habilitarCH1)
        self.canalDoisCheck.toggled.connect(self.habilitarCH2)
        self.amostraRBt.toggled.connect(self.modoLeitura)
        self.mediaRBt.toggled.connect(self.modoLeitura)
        self.ivRBt.toggled.connect(self.atualizaGraficoTela)
        self.vtRBt.toggled.connect(self.atualizaGraficoTela)
        self.ch12ch2RBt.toggled.connect(self.atualizaGraficoTela)

        self.freqInicialLine.setInputMask('9999')
        self.freqInicialLine.setText('10')
        self.freqFinalLine.setInputMask('9999')
        self.freqFinalLine.setText('100')
        self.freqInicialBox.setCurrentIndex(0)
        self.freqFinalBox.setCurrentIndex(1)
        self.numeroPontos.setInputMask('9999')
        self.numeroPontos.setText('10')

        self.exercicio1Bt.clicked.connect(self.telaExecercicio1)
        self.fftBt.clicked.connect(self.telaFft)
        self.freqBt.clicked.connect(self.teleFrequencia)
        self.ondaBt.clicked.connect(self.telaOnda)

        self.sairBt.clicked.connect(self.close)
        self.questBt.clicked.connect(self.telaDuvida)
        self.tutorialBt.clicked.connect(self.telaTutorial)
        self.sobreBt.clicked.connect(self.telaPrograma)
        self.conectarBtOsc.clicked.connect(self.conectarScope)
        self.conectarBtGer.clicked.connect(self.conectarGerador)

        self.startBt_Freq.clicked.connect(self.varreduraFreq)
        self.stopBt_Freq.clicked.connect(self.interromperFreq)
        self.novoBt_Freq.clicked.connect(self.limparVarreduraFreq)
        self.salvarBt_Freq.clicked.connect(self.salvarFreq2)

        self.capturarBt_Cap.clicked.connect(self.capturaTela)
        self.salvarBt_Cap.clicked.connect(self.salvarTela)

        self.startBt_Fft.clicked.connect(self.iniciaFft)
        self.salvarBt_Fft.clicked.connect(self.salvarFft)

        self.atualizaStatus('Scope desconectado')
        self.atualizaStatus('Gerador desconectado')
        self.stopBt_Freq.setEnabled(False)
        self.salvarBt_Freq.setEnabled(False)
        self.novoBt_Freq.setEnabled(False)
        self.salvarBt_Cap.setEnabled(False)
        self.salvarBt_Fft.setEnabled(False)
        self.statusFreq.setText("Em espera")
        self.statusFreq.setStyleSheet("QLabel#statusFreq {color: blue; font: bold 12pt; " +
                                     "font-family: Tahoma;}")

        self.graficoBranco()
        self.graficoBrancoTela()
        self.graficoBrancoFft()
        self.modoLeitura()

    def gerarNota(self, resp1, resp2, resp3):
        self.nota = round(10 * (resp1 + resp2 + resp3) / 3, 1)
        self.notaLabel.setText(str(self.nota))
        if resp1:
            self.userRespOndaVerif.setIcon(self.iconCerto)
        else:
            self.userRespOndaVerif.setIcon(self.iconErrado)
        if resp2:
            self.userRespAmpVerif.setIcon(self.iconCerto)
        else:
            self.userRespAmpVerif.setIcon(self.iconErrado)
        if resp3:
            self.userRespFreqVerif.setIcon(self.iconCerto)
        else:
            self.userRespFreqVerif.setIcon(self.iconErrado)

        if self.nota >= 7:
            self.notaLabel.setStyleSheet("QLabel#notaLabel {color: blue; font: bold 18pt; " +
                                         "font-family: Tahoma;}")
        else:
            self.notaLabel.setStyleSheet("QLabel#notaLabel {color: red; font: bold 18pt; " +
                                         "font-family: Tahoma;}")

        if self.equipamentos_thread.ondaAtual == "SINE":
            self.respOnda.setText("senoidal")
        elif self.equipamentos_thread.ondaAtual == "SQUARE":
            self.respOnda.setText("quadrada")
        elif self.equipamentos_thread.ondaAtual == "RAMP":
            self.respOnda.setText("triangular")

        self.respAmp.setText(str(self.equipamentos_thread.tensaoAtual) + " V")
        self.respFreq.setText(str(self.equipamentos_thread.freqAtual) + " Hz")

    def verificaResp(self):
        if self.userRespOnda.currentIndex() == 1:
            self.equipamentos_thread.userRespOnda = "SINE"
        elif self.userRespOnda.currentIndex() == 2:
            self.equipamentos_thread.userRespOnda = "SQUARE"
        elif self.userRespOnda.currentIndex() == 3:
            self.equipamentos_thread.userRespOnda = "RAMP"

        self.equipamentos_thread.userRespFreq = float(self.userRespFreqNum.text()) * \
                                                10 ** (self.userRespFreqExp.currentIndex() * 3)
        self.equipamentos_thread.userRespTensao = float(self.userRespAmpNum.text()) * \
                                                  10 ** (self.userRespAmpExp.currentIndex() * -3)

        self.equipamentos_thread.verificarResposta = True
        self.equipamentos_thread.start()
        self.respostaBt.setEnabled(False)

    def embaralharOsciloscopio(self):
        if self.equipamentos_thread.scopeConectado and self.equipamentos_thread.geradorConectado:
            if self.facilBt.isChecked():
                self.equipamentos_thread.dificuldade = 0
            elif self.medioBt.isChecked():
                self.equipamentos_thread.dificuldade = 1
            elif self.dificilBt.isChecked():
                self.equipamentos_thread.dificuldade = 2

            self.equipamentos_thread.embaralharOsciloscopio = True
            self.equipamentos_thread.start()
            self.respostaBt.setEnabled(True)
        else:
            QMessageBox.warning(self, "Falha", "Conecte os equipamentos "
                                               "\npara iniciar o exercício.", QMessageBox.Ok)

    def embaralharGerador(self):
        if self.equipamentos_thread.scopeConectado and self.equipamentos_thread.geradorConectado:
            self.equipamentos_thread.embaralharGerador = True
            self.equipamentos_thread.start()
            self.respostaBt.setEnabled(True)
            self.respOnda.setText("")
            self.respAmp.setText("")
            self.respFreq.setText("")
        else:
            QMessageBox.warning(self, "Falha", "Conecte os equipamentos "
                                               "\npara iniciar o exercício.", QMessageBox.Ok)

    def telaExecercicio1(self):
        self.stackedWidget.setCurrentIndex(0)

    def teleFrequencia(self):
        self.stackedWidget.setCurrentIndex(1)

    def telaOnda(self):
        self.stackedWidget.setCurrentIndex(2)

    def telaFft(self):
        self.stackedWidget.setCurrentIndex(3)

    def limparVarreduraFreq(self):
        __nome__ = __modulo__ + '06'
        result = QMessageBox.question(self, __appname__, "Tem certeza que deseja apagar os dados?",
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        if result == QMessageBox.Yes:
            mensagem = 'Limpando dados de varredura em Freq'
            print('{};{}'.format(__nome__, mensagem))
            self.graficoBranco()
            self.atualizaGraficoFreq()
            self.salvarBt_Freq.setEnabled(False)
            self.novoBt_Freq.setEnabled(False)
            del self.tensao1[:]
            del self.tensao2[:]
            del self.fase[:]
            del self.frequencia[:]
            del self.transmitancia[:]
            del self.transmitanciaDb[:]
            self.graph_thread.dadosX = copy.deepcopy(self.frequencia)
            self.graph_thread.dadosY1 = copy.deepcopy(self.transmitanciaDb)
            self.graph_thread.dadosY2 = copy.deepcopy(self.fase)

    def graficoBranco(self):
        __nome__ = __modulo__ + '05'
        mensagem = 'Gerando grafico em branco'
        #print('{};{}'.format(__nome__, mensagem))
        self.graph_thread.graficoBranco = True
        self.graph_thread.start()

    def graficoBrancoTela(self):
        __nome__ = __modulo__ + '12'
        mensagem = 'Gerando Tela em branco'
        #print('{};{}'.format(__nome__, mensagem))
        self.graph_thread.telaBranca = True
        self.graph_thread.start()

    def graficoBrancoFft(self):
        __nome__ = __modulo__ + '18'
        mensagem = 'Gerando FFT em branco'
        #print('{};{}'.format(__nome__, mensagem))
        self.graph_thread.fftBranco = True
        self.graph_thread.start()

    def interromperFreq(self):
        __nome__ = __modulo__ + '02'
        mensagem = 'Varredura em freq interrompida manualmente'
        print('{};{}'.format(__nome__, mensagem))
        self.equipamentos_thread.varrerFreq = False
        self.equipamentos_thread.interromperFreq = True
        self.startBt_Freq.setEnabled(True)

    def varreduraFreqTerminada(self):
        time.sleep(1)
        __nome__ = __modulo__ + '03'
        mensagem = 'Varredura em freq terminada'
        print('{};{}'.format(__nome__, mensagem))
        self.dadosFreq = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosFreq['frequencia (Hz)'] = self.frequencia
        self.dadosFreq['Vpp1 (V)'], self.dadosFreq['Vpp2 (V)'] = self.tensao1, self.tensao2
        self.dadosFreq['fase (Ch2-Ch1) (graus)'] = self.fase
        self.dadosFreq['T'], self.dadosFreq['T_dB'] = self.transmitancia, self.transmitanciaDb
        mensagem = 'Banco de dados Freq criado'
        print('{};{}'.format(__nome__, mensagem))
        self.progressBar.setValue(100)
        self.salvarBt_Freq.setEnabled(True)
        self.startBt_Freq.setEnabled(True)
        self.stopBt_Freq.setEnabled(False)
        self.novoBt_Freq.setEnabled(True)
        self.statusFreq.setText("Em espera")
        self.statusFreq.setStyleSheet("QLabel#statusFreq {color: blue; font: bold 12pt; " +
                                      "font-family: Tahoma;}")

    def recebendoMedidaFreq(self, vpp1, vpp2, phase, freq, ponto):
        __nome__ = __modulo__ + '03'
        self.progressBar.setValue(100*ponto/self.equipamentos_thread.quantidadePontos)
        self.tensao1.append(vpp1)
        self.tensao2.append(vpp2)
        self.fase.append(phase)
        self.frequencia.append(freq)
        T = (vpp2 / vpp1) ** 2  # cálculo da transmissão
        T_dB = 10 * np.log10(T)  # transmissão em dB
        self.transmitancia.append(T)
        self.transmitanciaDb.append(T_dB)
        mensagem = 'Recebido V1:' + str(vpp1) + ' V2:' + str(vpp2) + ' fase:' + str(phase) + ' freq:' + str(freq)
        print('{};{}'.format(__nome__, mensagem))

        self.graph_thread.dadosX = copy.deepcopy(self.frequencia)
        self.graph_thread.dadosY1 = copy.deepcopy(self.transmitanciaDb)
        self.graph_thread.dadosY2 = copy.deepcopy(self.fase)

        self.graph_thread.varreduraFreq = True
        self.graph_thread.start()

    def varreduraFreq(self):
        __nome__ = __modulo__ + '01'
        mensagem = 'Iniciando varredura em freq'
        print('{};{}'.format(__nome__, mensagem))
        self.equipamentos_thread.freqInicial = float(self.freqInicialLine.text())*10**(3*self.freqInicialBox.currentIndex())
        self.equipamentos_thread.freqFinal = float(self.freqFinalLine.text())*10**(3*self.freqFinalBox.currentIndex())
        self.equipamentos_thread.quantidadePontos = int(self.numeroPontos.text())
        self.equipamentos_thread.interromperFreq = False
        self.progressBar.setValue(0)
        self.graficoBranco()
        if self.equipamentos_thread.scopeConectado and self.equipamentos_thread.geradorConectado:
            del self.tensao1[:]
            del self.tensao2[:]
            del self.fase[:]
            del self.frequencia[:]
            del self.transmitancia[:]
            del self.transmitanciaDb[:]
            self.statusFreq.setText("Varrendo...")
            self.statusFreq.setStyleSheet("QLabel#statusFreq {color: red; font: bold 12pt; " +
                                          "font-family: Tahoma;}")
            self.startBt_Freq.setEnabled(False)
            self.stopBt_Freq.setEnabled(True)
            self.graph_thread.varreduraFreq = True
            self.equipamentos_thread.varrerFreq = True
            self.equipamentos_thread.start()
            self.graph_thread.start()
        else:
            QMessageBox.warning(self, "Falha", "Conecte os equipamentos "
                                               "\npara iniciar as medidas.", QMessageBox.Ok)

    def salvarFreq2(self):
        __nome__ = __modulo__ + '09'
        dir = self.diretorio
        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        mensagem = 'Janela de salvar Freq aberta'
        print('{};{}'.format(__nome__, mensagem))

        if self.outFile[0] != '':
            self.dadosFreq.to_csv(self.outFile[0][:len(self.outFile[0])-4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0])-4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))

            im = cv2.imread('varreduraFreq.png')
            cv2.imwrite(self.outFile[0][:len(self.outFile[0])-4]+'_figura.png', im)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png'
            print('{};{}'.format(__nome__, mensagem))

            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'Nenhum arquivo selecionado'
            print('{};{}'.format(__nome__, mensagem))

    def capturaTela(self):
        __nome__ = __modulo__ + '10'
        if self.equipamentos_thread.scopeConectado:
            mensagem = 'Iniciando captura de tela'
            print('{};{}'.format(__nome__, mensagem))

            if self.canalUmCheck.isChecked():
                self.equipamentos_thread.ch1 = True
            else:
                self.equipamentos_thread.ch1 = False

            if self.canalDoisCheck.isChecked():
                self.equipamentos_thread.ch2 = True
            else:
                self.equipamentos_thread.ch2 = False

            if self.amostraRBt.isChecked():
                self.equipamentos_thread.leitura = 0
            elif self.mediaRBt.isChecked():
                if self.mediaBox.currentIndex() == 0:
                    self.equipamentos_thread.leitura = 4
                if self.mediaBox.currentIndex() == 1:
                    self.equipamentos_thread.leitura = 16
                if self.mediaBox.currentIndex() == 2:
                    self.equipamentos_thread.leitura = 64
                if self.mediaBox.currentIndex() == 3:
                    self.equipamentos_thread.leitura = 128

            if self.triggerCh1.isChecked():
                self.equipamentos_thread.trigger = 'CH1'
            elif self.triggerCh2.isChecked():
                self.equipamentos_thread.trigger = 'CH2'
            elif self.triggerExt.isChecked():
                self.equipamentos_thread.trigger = 'EXT'
            elif self.triggerLinha.isChecked():
                self.equipamentos_thread.trigger = 'AC LINE'

            self.equipamentos_thread.capturarTela = True
            self.equipamentos_thread.start()
            self.salvarBt_Cap.setEnabled(True)
        else:
            QMessageBox.warning(self, "Falha", "Conecte o osciloscópio "
                                               "\npara iniciar as medidas.", QMessageBox.Ok)

    def recebendoTela(self, ch1, ch2, tempo):
        __nome__ = __modulo__ + '13'
        self.ch1Tela = ch1
        self.ch2Tela = ch2
        self.tempoTela = tempo
        self.dadosTela = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosTela['Tempo (s)'] = self.tempoTela
        mensagem = 'Banco de dados Tela criado'
        print('{};{}'.format(__nome__, mensagem))
        self.graph_thread.tela = True
        if self.canalUmCheck.isChecked():
            self.dadosTela['V1 (V)'] = self.ch1Tela
            self.graph_thread.ch1 = True
        else:
            self.graph_thread.ch1 = False

        if self.canalDoisCheck.isChecked():
            self.dadosTela['V2 (V)'] = self.ch2Tela
            self.graph_thread.ch2 = True
        else:
            self.graph_thread.ch2 = False

        if self.canalDoisCheck.isChecked() and self.canalUmCheck.isChecked():
            y1 = np.array(self.ch1Tela)
            y2 = np.array(self.ch2Tela)
            y3 = y1 - y2
            self.dadosTela['V1 - V2 (V)'] = y3

        self.graph_thread.dadosX = copy.deepcopy(self.tempoTela)
        self.graph_thread.dadosY1 = copy.deepcopy(self.ch1Tela)
        self.graph_thread.dadosY2 = copy.deepcopy(self.ch2Tela)
        self.graph_thread.start()

    def salvarTela(self):
        __nome__ = __modulo__ + '14'
        dir = self.diretorio

        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        mensagem = 'Janela de salvar Tela aberta'
        print('{};{}'.format(__nome__, mensagem))

        if self.outFile[0] != '':
            self.dadosTela.to_csv(self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))

            im = cv2.imread('telaVT.png')
            cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoVT.png', im)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoVT.png'
            print('{};{}'.format(__nome__, mensagem))

            im = cv2.imread('telaIV.png')
            cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoIV.png', im)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoIV.png'
            print('{};{}'.format(__nome__, mensagem))

            im = cv2.imread('telaCh122.png')
            cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoCh1-Ch2xCh2.png', im)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoCh1-Ch2xCh2.png'

            print('{};{}'.format(__nome__, mensagem))
            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'Nenhum arquivo selecionado'
            print('{};{}'.format(__nome__, mensagem))

    def iniciaFft(self):
        __nome__ = __modulo__ + '15'
        mensagem = 'Iniciando leitura para FFT'
        print('{};{}'.format(__nome__, mensagem))
        if self.equipamentos_thread.scopeConectado:
            if self.CH1_Fft.isChecked():
                self.equipamentos_thread.ch1 = True
                self.equipamentos_thread.ch2 = False

            if self.CH2_Fft.isChecked():
                self.equipamentos_thread.ch1 = False
                self.equipamentos_thread.ch2 = True

            self.equipamentos_thread.fft = True
            self.equipamentos_thread.start()
            self.salvarBt_Fft.setEnabled(True)

        else:
            mensagem = 'Osciloscópio não conectado'
            print('{};{}'.format(__nome__, mensagem))
            QMessageBox.warning(self, "Falha", "Conecte o osciloscópio"
                                               "\npara iniciar as medidas.", QMessageBox.Ok)

    def calculoFft(self, tempo, canal):
        __nome__ = __modulo__ + '16'
        mensagem = 'Iniciando calculo do FFT'
        print('{};{}'.format(__nome__, mensagem))
        delta = tempo[1] - tempo[0]  # time step\n",
        tempo = tempo - tempo[0] + delta
        Ts = float(tempo[0])
        n = len(tempo)  # length of the signal
        k = np.arange(n)
        T = n * Ts
        frq = k / T
        fft = np.fft.fft(canal) / n  # fft computing and normalization

        self.graph_thread.fft = True
        self.graph_thread.dadosX = copy.deepcopy(frq[:int(len(frq)/2)])
        self.graph_thread.dadosY1 = copy.deepcopy(4*abs(fft[:int(len(fft)/2)]))
        self.graph_thread.start()
        self.dadosFft = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosFft['Frequencia (Hz)'] = frq
        self.dadosFft['Amplitude'] = abs(fft)

    def salvarFft(self):
        __nome__ = __modulo__ + '19'
        dir = self.diretorio

        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        mensagem = 'Janela de salvar FFT aberta'
        print('{};{}'.format(__nome__, mensagem))

        if self.outFile[0] != '':
            self.dadosFft.to_csv(self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))

            im = cv2.imread('fft.png')
            cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png', im)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png'
            print('{};{}'.format(__nome__, mensagem))

            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'Nenhum arquivo selecionado'
            print('{};{}'.format(__nome__, mensagem))

    def atualizaGraficoFreq(self):
        __nome__ = __modulo__ + '08'
        mensagem = 'Atualizando grafico Freq'
        print('{};{}'.format(__nome__, mensagem))
        self.pixmap = QPixmap("varreduraFreq.png")
        tamanho = self.pixmap.size()
        self.grafico_Freq.setGeometry(QtCore.QRect(209, 50, tamanho.width(), tamanho.height()))
        self.grafico_Freq.setPixmap(self.pixmap)

    def atualizaGraficoTela(self):
        __nome__ = __modulo__ + '11'
        mensagem = 'Atualizando grafico Tela'
        print('{};{}'.format(__nome__, mensagem))
        if self.vtRBt.isChecked():
            self.pixmap = QPixmap("telaVT.png")
        if self.ivRBt.isChecked():
            self.pixmap = QPixmap("telaIV.png")
        if self.ch12ch2RBt.isChecked():
            self.pixmap = QPixmap("telaCh122.png")
        tamanho = self.pixmap.size()
        self.grafico_Cap.setGeometry(QtCore.QRect(180, 40, tamanho.width(), tamanho.height()))
        self.grafico_Cap.setPixmap(self.pixmap)

    def atualizaGraficoFft(self):
        __nome__ = __modulo__ + '17'
        mensagem = 'Atualizando grafico FFT'
        print('{};{}'.format(__nome__, mensagem))
        self.pixmap = QPixmap("fft.png")
        tamanho = self.pixmap.size()
        self.grafico_Fft.setGeometry(QtCore.QRect(110, 50, tamanho.width(), tamanho.height()))
        self.grafico_Fft.setPixmap(self.pixmap)

    def habilitarCH1(self):
        if self.canalUmCheck.isChecked():
            self.canalUmCheck.setToolTip(
                 QtGui.QApplication.translate("Dialog", "Captura do canal 1 habilitada", None,
                                             QtGui.QApplication.UnicodeUTF8))

        else:
            self.canalUmCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Captura do canal 1 desabilitada", None,
                                             QtGui.QApplication.UnicodeUTF8))

    def habilitarCH2(self):
        if self.canalDoisCheck.isChecked():
            self.canalDoisCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Captura do canal 2 habilitada", None,
                                             QtGui.QApplication.UnicodeUTF8))

        else:
            self.canalDoisCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Captura do canal 2 desabilitada", None,
                                             QtGui.QApplication.UnicodeUTF8))

    def modoLeitura(self):
        if self.amostraRBt.isChecked():
            self.mediaBox.setEnabled(False)

        elif self.mediaRBt.isChecked():
            self.mediaBox.setEnabled(True)

    def telaDuvida(self):
        if self.stackedWidget.currentIndex() == 0:
            prog = Duvida(indice=0)
        if self.stackedWidget.currentIndex() == 1:
            prog = Duvida(indice=2)
        if self.stackedWidget.currentIndex() == 2:
            prog = Duvida(indice=3)
        if self.stackedWidget.currentIndex() == 3:
            prog = Duvida(indice=1)
        prog.exec_()

    def telaTutorial(self):
        prog = Tutorial()
        prog.exec_()

    def telaPrograma(self):
        prog = Oprograma()
        prog.exec_()

    def conectarScope(self):
        if self.equipamentos_thread.scopeConectado:
            self.equipamentos_thread.desconectarScope = True
            self.equipamentos_thread.start()
        else:
            if self.modelosOscCombo.currentIndex() == 1:
                self.equipamentos_thread.conectarScope = True
                self.equipamentos_thread.start()
            else:
                QMessageBox.warning(self, "Falha ao conectar", "Selecione um osciloscópio válido.", QMessageBox.Ok)

    def conectarGerador(self):
        if self.equipamentos_thread.geradorConectado:
            self.equipamentos_thread.desconectarGerador = True
            self.equipamentos_thread.start()
        else:
            if self.modelosGerCombo.currentIndex() == 1:
                self.equipamentos_thread.conectarGerador = True
                self.equipamentos_thread.start()
            else:
                QMessageBox.warning(self, "Falha ao conectar", "Selecione um gerador válido.", QMessageBox.Ok)

    def atualizaStatus(self, data):

        if data == 'Conectando Scope':
            self.osciloscopioStatus.setText("Conectando...")
            self.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: blue; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
        elif data == 'Scope conectado':
            self.osciloscopioStatus.setText("Conectado")
            self.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: green; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
            self.conectarBtOsc.setIcon(self.iconDesconectar)
            self.conectarBtOsc.setToolTip(
                QtGui.QApplication.translate("Dialog", "Desconecta o osciloscópio", None,
                                             QtGui.QApplication.UnicodeUTF8))


        elif data == 'Scope desconectado':
            self.osciloscopioStatus.setText("Desconectado")
            self.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: red; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
            self.conectarBtOsc.setIcon(self.iconConectar)
            self.conectarBtOsc.setToolTip(
                QtGui.QApplication.translate("Dialog", "Conecta o osciloscópio", None,
                                             QtGui.QApplication.UnicodeUTF8))

        elif data == 'Conectando Gerador':
            self.geradorStatus.setText("Conectando...")
            self.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: blue; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
        elif data == 'Gerador conectado':
            self.geradorStatus.setText("Conectado")
            self.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: green; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
            self.conectarBtGer.setIcon(self.iconDesconectar)
            self.conectarBtGer.setToolTip(
                QtGui.QApplication.translate("Dialog", "Desconecta o gerador", None,
                                             QtGui.QApplication.UnicodeUTF8))


        elif data == 'Gerador desconectado':
            self.geradorStatus.setText("Desconectado")
            self.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: red; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
            self.conectarBtGer.setIcon(self.iconConectar)
            self.conectarBtGer.setToolTip(
                QtGui.QApplication.translate("Dialog", "Conecta o gerador", None,
                                             QtGui.QApplication.UnicodeUTF8))

    def falha(self, data):
        if data == 'Scope':
            QMessageBox.warning(self, "Falha ao conectar", "Osciloscópio não encontrado, verifique as conexões.",
                                QMessageBox.Ok)
        if data == 'Gerador':
            QMessageBox.warning(self, "Falha ao conectar", "Gerador não encontrado, verifique as conexões.",
                                QMessageBox.Ok)

    def salvarFreq(self):
        dir = "."
        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        self.dadosFreq.to_csv(self.outFile[0] + '_dados.csv', sep='\t', index=False)
        im = cv2.imread('varreduraFreq.png')
        cv2.imwrite(self.outFile[0]+'_grafico.png', im)

    def closeEvent(self, event, *args, **kwargs):
        """Ritual de encerramento"""

        result = QMessageBox.question(self, __appname__, "Tem certeza que deseja sair?",
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)

        if result == QMessageBox.Yes:
            """Colocar aqui tudo que tiver que ser feito antes de sair"""
            self.equipamentos_thread.desconectarGerador = True
            self.equipamentos_thread.desconectarScope = True
            self.equipamentos_thread.interromperFreq = True
            self.equipamentos_thread.varrerFreq = False
            self.equipamentos_thread.start()
            time.sleep(1)
            event.accept()
        else:
            event.ignore()


def escreve_dicionario():
    __nome__ = __modulo__ + '10'
    mensagem = 'Escrevendo dicionario'
    print('{};{}'.format(__nome__, mensagem))

    with open('Dicionario.csv', 'w', newline='') as g:
        spamwriter = csv.writer(g, delimiter=';', dialect='excel')
        spamwriter.writerow(['M00', 'Main', 'Main'])
        spamwriter.writerow(['M01', 'MainDialog', 'varreduraFreq'])
        spamwriter.writerow(['M02', 'MainDialog', 'interromperFreq'])
        spamwriter.writerow(['M03', 'MainDialog', 'varreduraFreqTerminada'])
        spamwriter.writerow(['M04', 'EquipamentosThread', 'run'])
        spamwriter.writerow(['M05', 'MainDialog', 'graficoBranco'])
        spamwriter.writerow(['M06', 'MainDialog', 'limparVarreduraFreq'])
        spamwriter.writerow(['M07', 'GraphThread', 'run'])
        spamwriter.writerow(['M08', 'MainDialog', 'atualizaGraficoFreq'])
        spamwriter.writerow(['M09', 'MainDialog', 'salvarFreq'])
        spamwriter.writerow(['M10', 'MainDialog', 'capturaTela'])
        spamwriter.writerow(['M11', 'MainDialog', 'atualizaGraficoTela'])
        spamwriter.writerow(['M12', 'MainDialog', 'graficoBrancoTela'])
        spamwriter.writerow(['M13', 'MainDialog', 'recebendoTela'])
        spamwriter.writerow(['M14', 'MainDialog', 'salvarTela'])
        spamwriter.writerow(['M15', 'MainDialog', 'FFT'])
        spamwriter.writerow(['M16', 'MainDialog', 'Cálculo FFT'])
        spamwriter.writerow(['M17', 'MainDialog', 'atualizaGraficoFft'])
        spamwriter.writerow(['M18', 'MainDialog', 'graficoBrancoFft'])
        spamwriter.writerow(['M19', 'MainDialog', 'salvarFft'])


if __name__ == '__main__':
    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("IFGW Unicamp")
    QCoreApplication.setOrganizationDomain("portal.ifi.unicamp.br")

    app = QApplication(sys.argv)
    form = MainDialog()
    form.show()
    app.exec_()
