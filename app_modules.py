################################################################################
##
## BY: WANDERSON M.PIMENTA
## PROJECT MADE WITH: Qt Designer and PySide2
## V: 1.0.0
##
## This project can be used freely for all uses, as long as they maintain the
## respective credits only in the Python scripts, any information in the visual
## interface (GUI) can be modified without any implication.
##
## There are limitations on Qt licenses if you want to use your products
## commercially, I recommend reading them on the official website:
## https://doc.qt.io/qtforpython/licenses.html
##
################################################################################

# GUI FILE
from ui_files.mainDialogV6 import Ui_MainWindow
from ui_files.tutorialV2 import Ui_Dialog

# IMPORT QSS CUSTOM
from ui_files.ui_styles import Style

# IMPORT FUNCTIONS
from ui_files.ui_functions import *

## ==> APP FUNCTIONS
from app_functions import *

# GUI SPLASH SCREEN
from ui_files.splash_screen import Ui_SplashScreen

# THREADS
import threads

# COMMUNICATIONS
import pylef
import visa

# MATHS
import numpy as np
import pandas as pd
import time
import random
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib import collections as matcoll
import copy
import os
import webbrowser