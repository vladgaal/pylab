# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'splash_screen.ui'
#
# Created: Wed Jan 13 21:43:32 2021
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_SplashScreen(object):
    def setupUi(self, SplashScreen):
        SplashScreen.setObjectName("SplashScreen")
        SplashScreen.resize(680, 400)
        self.centralwidget = QtGui.QWidget(SplashScreen)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setContentsMargins(10, 10, 10, 10)
        self.verticalLayout.setObjectName("verticalLayout")
        self.dropShadowFrame = QtGui.QFrame(self.centralwidget)
        self.dropShadowFrame.setStyleSheet("QFrame {    \n"
"    background-color: rgb(56, 58, 89);    \n"
"    color: rgb(220, 220, 220);\n"
"    border-radius: 10px;\n"
"}")
        self.dropShadowFrame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.dropShadowFrame.setFrameShadow(QtGui.QFrame.Raised)
        self.dropShadowFrame.setObjectName("dropShadowFrame")
        self.label_title = QtGui.QLabel(self.dropShadowFrame)
        self.label_title.setGeometry(QtCore.QRect(0, 50, 661, 101))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(50)
        self.label_title.setFont(font)
        self.label_title.setStyleSheet("color: rgb(254, 121, 199);")
        self.label_title.setAlignment(QtCore.Qt.AlignCenter)
        self.label_title.setObjectName("label_title")
        self.label_description = QtGui.QLabel(self.dropShadowFrame)
        self.label_description.setGeometry(QtCore.QRect(0, 150, 661, 31))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(14)
        self.label_description.setFont(font)
        self.label_description.setStyleSheet("color: rgb(98, 114, 164);")
        self.label_description.setAlignment(QtCore.Qt.AlignCenter)
        self.label_description.setObjectName("label_description")
        self.label_loading = QtGui.QLabel(self.dropShadowFrame)
        self.label_loading.setGeometry(QtCore.QRect(230, 230, 431, 111))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(37)
        self.label_loading.setFont(font)
        self.label_loading.setStyleSheet("color: rgb(98, 114, 164);")
        self.label_loading.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_loading.setObjectName("label_loading")
        self.label_credits = QtGui.QLabel(self.dropShadowFrame)
        self.label_credits.setGeometry(QtCore.QRect(20, 350, 621, 21))
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        font.setPointSize(10)
        self.label_credits.setFont(font)
        self.label_credits.setStyleSheet("color: rgb(98, 114, 164);")
        self.label_credits.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_credits.setObjectName("label_credits")
        self.verticalLayout.addWidget(self.dropShadowFrame)
        SplashScreen.setCentralWidget(self.centralwidget)

        self.retranslateUi(SplashScreen)
        QtCore.QMetaObject.connectSlotsByName(SplashScreen)

    def retranslateUi(self, SplashScreen):
        SplashScreen.setWindowTitle(QtGui.QApplication.translate("SplashScreen", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.label_title.setText(QtGui.QApplication.translate("SplashScreen", "<strong>YOUR</strong> APP NAME", None, QtGui.QApplication.UnicodeUTF8))
        self.label_description.setText(QtGui.QApplication.translate("SplashScreen", "<strong>APP</strong> DESCRIPTION", None, QtGui.QApplication.UnicodeUTF8))
        self.label_loading.setText(QtGui.QApplication.translate("SplashScreen", "loading.", None, QtGui.QApplication.UnicodeUTF8))
        self.label_credits.setText(QtGui.QApplication.translate("SplashScreen", "<strong>Created</strong>: Vladimir Gaal", None, QtGui.QApplication.UnicodeUTF8))

