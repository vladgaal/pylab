# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainDialogV2.ui'
#
# Created: Sat Jan 23 09:30:50 2021
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1072, 620)
        Dialog.setMinimumSize(QtCore.QSize(1072, 620))
        Dialog.setMaximumSize(QtCore.QSize(1072, 620))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/Main/icons/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("QProgressBar {\n"
"    border: 2px solid grey;\n"
"    border-radius: 5px;\n"
"    text-align: center;\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: qlineargradient(spread:pad, x1:0.988545, y1:1, x2:1, y2:0.455, stop:0 rgba(255, 0, 0, 255), stop:1 rgba(204, 0, 0, 255));\n"
"    width: 10px;\n"
"    margin: 0.5px;\n"
"}")
        self.frame = QtGui.QFrame(Dialog)
        self.frame.setGeometry(QtCore.QRect(0, 0, 111, 461))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.exercicio1Bt = QtGui.QToolButton(self.frame)
        self.exercicio1Bt.setGeometry(QtCore.QRect(10, 20, 91, 101))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.exercicio1Bt.setFont(font)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/Main/icons/exercicios.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.exercicio1Bt.setIcon(icon1)
        self.exercicio1Bt.setIconSize(QtCore.QSize(64, 64))
        self.exercicio1Bt.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.exercicio1Bt.setObjectName("exercicio1Bt")
        self.freqBt = QtGui.QToolButton(self.frame)
        self.freqBt.setGeometry(QtCore.QRect(10, 240, 91, 101))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.freqBt.setFont(font)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/Main/icons/plot.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.freqBt.setIcon(icon2)
        self.freqBt.setIconSize(QtCore.QSize(64, 64))
        self.freqBt.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.freqBt.setObjectName("freqBt")
        self.ondaBt = QtGui.QToolButton(self.frame)
        self.ondaBt.setGeometry(QtCore.QRect(10, 350, 91, 101))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.ondaBt.setFont(font)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/Main/icons/onda.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ondaBt.setIcon(icon3)
        self.ondaBt.setIconSize(QtCore.QSize(64, 64))
        self.ondaBt.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.ondaBt.setObjectName("ondaBt")
        self.fftBt = QtGui.QToolButton(self.frame)
        self.fftBt.setGeometry(QtCore.QRect(10, 130, 91, 101))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.fftBt.setFont(font)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/Main/icons/fft.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.fftBt.setIcon(icon4)
        self.fftBt.setIconSize(QtCore.QSize(64, 64))
        self.fftBt.setAutoRepeatInterval(100)
        self.fftBt.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.fftBt.setObjectName("fftBt")
        self.stackedWidget = QtGui.QStackedWidget(Dialog)
        self.stackedWidget.setGeometry(QtCore.QRect(110, 0, 961, 461))
        self.stackedWidget.setCursor(QtCore.Qt.ArrowCursor)
        self.stackedWidget.setMouseTracking(False)
        self.stackedWidget.setFocusPolicy(QtCore.Qt.NoFocus)
        self.stackedWidget.setObjectName("stackedWidget")
        self.page = QtGui.QWidget()
        self.page.setObjectName("page")
        self.label = QtGui.QLabel(self.page)
        self.label.setGeometry(QtCore.QRect(330, 170, 151, 91))
        self.label.setObjectName("label")
        self.stackedWidget.addWidget(self.page)
        self.page_2 = QtGui.QWidget()
        self.page_2.setObjectName("page_2")
        self.label_2 = QtGui.QLabel(self.page_2)
        self.label_2.setGeometry(QtCore.QRect(310, 0, 331, 41))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.grafico_Freq = QtGui.QLabel(self.page_2)
        self.grafico_Freq.setGeometry(QtCore.QRect(209, 50, 731, 400))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.grafico_Freq.setFont(font)
        self.grafico_Freq.setFrameShape(QtGui.QFrame.Box)
        self.grafico_Freq.setAlignment(QtCore.Qt.AlignCenter)
        self.grafico_Freq.setObjectName("grafico_Freq")
        self.line = QtGui.QFrame(self.page_2)
        self.line.setGeometry(QtCore.QRect(190, 50, 11, 390))
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.label_8 = QtGui.QLabel(self.page_2)
        self.label_8.setGeometry(QtCore.QRect(10, 40, 141, 22))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.label_9 = QtGui.QLabel(self.page_2)
        self.label_9.setGeometry(QtCore.QRect(10, 100, 51, 22))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.label_9.setFont(font)
        self.label_9.setObjectName("label_9")
        self.label_10 = QtGui.QLabel(self.page_2)
        self.label_10.setGeometry(QtCore.QRect(10, 150, 141, 22))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.label_10.setFont(font)
        self.label_10.setObjectName("label_10")
        self.freqInicialLine = QtGui.QLineEdit(self.page_2)
        self.freqInicialLine.setGeometry(QtCore.QRect(70, 70, 51, 22))
        self.freqInicialLine.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.freqInicialLine.setObjectName("freqInicialLine")
        self.freqFinalLine = QtGui.QLineEdit(self.page_2)
        self.freqFinalLine.setGeometry(QtCore.QRect(70, 100, 51, 22))
        self.freqFinalLine.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.freqFinalLine.setObjectName("freqFinalLine")
        self.numeroPontos = QtGui.QLineEdit(self.page_2)
        self.numeroPontos.setGeometry(QtCore.QRect(140, 150, 41, 22))
        self.numeroPontos.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.numeroPontos.setCursorPosition(0)
        self.numeroPontos.setObjectName("numeroPontos")
        self.freqInicialBox = QtGui.QComboBox(self.page_2)
        self.freqInicialBox.setGeometry(QtCore.QRect(130, 70, 51, 22))
        self.freqInicialBox.setObjectName("freqInicialBox")
        self.freqFinalBox = QtGui.QComboBox(self.page_2)
        self.freqFinalBox.setGeometry(QtCore.QRect(130, 100, 51, 22))
        self.freqFinalBox.setObjectName("freqFinalBox")
        self.line_2 = QtGui.QFrame(self.page_2)
        self.line_2.setGeometry(QtCore.QRect(10, 170, 171, 21))
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.startBt_Freq = QtGui.QToolButton(self.page_2)
        self.startBt_Freq.setGeometry(QtCore.QRect(10, 190, 81, 91))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.startBt_Freq.setFont(font)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/Main/icons/start.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.startBt_Freq.setIcon(icon5)
        self.startBt_Freq.setIconSize(QtCore.QSize(64, 64))
        self.startBt_Freq.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.startBt_Freq.setObjectName("startBt_Freq")
        self.stopBt_Freq = QtGui.QToolButton(self.page_2)
        self.stopBt_Freq.setGeometry(QtCore.QRect(100, 190, 81, 91))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.stopBt_Freq.setFont(font)
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/Main/icons/stop.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.stopBt_Freq.setIcon(icon6)
        self.stopBt_Freq.setIconSize(QtCore.QSize(64, 64))
        self.stopBt_Freq.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.stopBt_Freq.setObjectName("stopBt_Freq")
        self.salvarBt_Freq = QtGui.QToolButton(self.page_2)
        self.salvarBt_Freq.setGeometry(QtCore.QRect(10, 360, 81, 91))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.salvarBt_Freq.setFont(font)
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(":/Main/icons/salvar.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.salvarBt_Freq.setIcon(icon7)
        self.salvarBt_Freq.setIconSize(QtCore.QSize(64, 64))
        self.salvarBt_Freq.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.salvarBt_Freq.setObjectName("salvarBt_Freq")
        self.novoBt_Freq = QtGui.QToolButton(self.page_2)
        self.novoBt_Freq.setGeometry(QtCore.QRect(100, 360, 81, 91))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.novoBt_Freq.setFont(font)
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(":/Main/icons/novo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.novoBt_Freq.setIcon(icon8)
        self.novoBt_Freq.setIconSize(QtCore.QSize(64, 64))
        self.novoBt_Freq.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.novoBt_Freq.setObjectName("novoBt_Freq")
        self.label_11 = QtGui.QLabel(self.page_2)
        self.label_11.setGeometry(QtCore.QRect(10, 70, 51, 22))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.label_11.setFont(font)
        self.label_11.setObjectName("label_11")
        self.line_3 = QtGui.QFrame(self.page_2)
        self.line_3.setGeometry(QtCore.QRect(10, 130, 171, 21))
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.progressBar = QtGui.QProgressBar(self.page_2)
        self.progressBar.setGeometry(QtCore.QRect(10, 320, 171, 21))
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.line_4 = QtGui.QFrame(self.page_2)
        self.line_4.setGeometry(QtCore.QRect(10, 340, 171, 21))
        self.line_4.setFrameShape(QtGui.QFrame.HLine)
        self.line_4.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.statusFreq = QtGui.QLabel(self.page_2)
        self.statusFreq.setGeometry(QtCore.QRect(10, 280, 171, 41))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.statusFreq.setFont(font)
        self.statusFreq.setObjectName("statusFreq")
        self.stackedWidget.addWidget(self.page_2)
        self.page_3 = QtGui.QWidget()
        self.page_3.setObjectName("page_3")
        self.label_3 = QtGui.QLabel(self.page_3)
        self.label_3.setGeometry(QtCore.QRect(200, 0, 741, 41))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        self.label_3.setFont(font)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.grafico_Cap = QtGui.QLabel(self.page_3)
        self.grafico_Cap.setGeometry(QtCore.QRect(180, 40, 771, 411))
        self.grafico_Cap.setFrameShape(QtGui.QFrame.Box)
        self.grafico_Cap.setAlignment(QtCore.Qt.AlignCenter)
        self.grafico_Cap.setObjectName("grafico_Cap")
        self.capturarBt_Cap = QtGui.QToolButton(self.page_3)
        self.capturarBt_Cap.setGeometry(QtCore.QRect(10, 370, 81, 81))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.capturarBt_Cap.setFont(font)
        self.capturarBt_Cap.setIcon(icon5)
        self.capturarBt_Cap.setIconSize(QtCore.QSize(50, 50))
        self.capturarBt_Cap.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.capturarBt_Cap.setObjectName("capturarBt_Cap")
        self.salvarBt_Cap = QtGui.QToolButton(self.page_3)
        self.salvarBt_Cap.setGeometry(QtCore.QRect(90, 370, 81, 81))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.salvarBt_Cap.setFont(font)
        self.salvarBt_Cap.setIcon(icon7)
        self.salvarBt_Cap.setIconSize(QtCore.QSize(50, 50))
        self.salvarBt_Cap.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.salvarBt_Cap.setObjectName("salvarBt_Cap")
        self.groupBox = QtGui.QGroupBox(self.page_3)
        self.groupBox.setGeometry(QtCore.QRect(10, 30, 161, 51))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(10)
        self.groupBox.setFont(font)
        self.groupBox.setObjectName("groupBox")
        self.canalUmCheck = QtGui.QCheckBox(self.groupBox)
        self.canalUmCheck.setGeometry(QtCore.QRect(10, 20, 141, 21))
        self.canalUmCheck.setChecked(True)
        self.canalUmCheck.setObjectName("canalUmCheck")
        self.groupBox_2 = QtGui.QGroupBox(self.page_3)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 90, 161, 51))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(10)
        self.groupBox_2.setFont(font)
        self.groupBox_2.setObjectName("groupBox_2")
        self.canalDoisCheck = QtGui.QCheckBox(self.groupBox_2)
        self.canalDoisCheck.setGeometry(QtCore.QRect(10, 20, 141, 21))
        self.canalDoisCheck.setChecked(True)
        self.canalDoisCheck.setObjectName("canalDoisCheck")
        self.groupBox_3 = QtGui.QGroupBox(self.page_3)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 300, 161, 61))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(10)
        self.groupBox_3.setFont(font)
        self.groupBox_3.setObjectName("groupBox_3")
        self.triggerCh1 = QtGui.QRadioButton(self.groupBox_3)
        self.triggerCh1.setGeometry(QtCore.QRect(10, 20, 51, 20))
        self.triggerCh1.setChecked(True)
        self.triggerCh1.setAutoExclusive(True)
        self.triggerCh1.setObjectName("triggerCh1")
        self.triggerExt = QtGui.QRadioButton(self.groupBox_3)
        self.triggerExt.setGeometry(QtCore.QRect(60, 20, 81, 20))
        self.triggerExt.setAutoExclusive(True)
        self.triggerExt.setObjectName("triggerExt")
        self.triggerLinha = QtGui.QRadioButton(self.groupBox_3)
        self.triggerLinha.setGeometry(QtCore.QRect(60, 40, 91, 20))
        self.triggerLinha.setAutoExclusive(True)
        self.triggerLinha.setObjectName("triggerLinha")
        self.triggerCh2 = QtGui.QRadioButton(self.groupBox_3)
        self.triggerCh2.setGeometry(QtCore.QRect(10, 40, 51, 20))
        self.triggerCh2.setAutoExclusive(True)
        self.triggerCh2.setObjectName("triggerCh2")
        self.groupBox_5 = QtGui.QGroupBox(self.page_3)
        self.groupBox_5.setGeometry(QtCore.QRect(10, 150, 161, 61))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(10)
        self.groupBox_5.setFont(font)
        self.groupBox_5.setObjectName("groupBox_5")
        self.vtRBt = QtGui.QRadioButton(self.groupBox_5)
        self.vtRBt.setGeometry(QtCore.QRect(10, 20, 81, 20))
        self.vtRBt.setChecked(False)
        self.vtRBt.setAutoExclusive(True)
        self.vtRBt.setObjectName("vtRBt")
        self.ch12ch2RBt = QtGui.QRadioButton(self.groupBox_5)
        self.ch12ch2RBt.setGeometry(QtCore.QRect(10, 40, 141, 20))
        self.ch12ch2RBt.setAutoExclusive(True)
        self.ch12ch2RBt.setObjectName("ch12ch2RBt")
        self.ivRBt = QtGui.QRadioButton(self.groupBox_5)
        self.ivRBt.setGeometry(QtCore.QRect(80, 20, 81, 20))
        self.ivRBt.setChecked(True)
        self.ivRBt.setAutoExclusive(True)
        self.ivRBt.setObjectName("ivRBt")
        self.groupBox_6 = QtGui.QGroupBox(self.page_3)
        self.groupBox_6.setGeometry(QtCore.QRect(10, 220, 161, 71))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(10)
        self.groupBox_6.setFont(font)
        self.groupBox_6.setObjectName("groupBox_6")
        self.amostraRBt = QtGui.QRadioButton(self.groupBox_6)
        self.amostraRBt.setGeometry(QtCore.QRect(10, 20, 131, 20))
        self.amostraRBt.setChecked(True)
        self.amostraRBt.setAutoExclusive(True)
        self.amostraRBt.setObjectName("amostraRBt")
        self.mediaRBt = QtGui.QRadioButton(self.groupBox_6)
        self.mediaRBt.setGeometry(QtCore.QRect(10, 40, 61, 20))
        self.mediaRBt.setAutoExclusive(True)
        self.mediaRBt.setObjectName("mediaRBt")
        self.mediaBox = QtGui.QComboBox(self.groupBox_6)
        self.mediaBox.setGeometry(QtCore.QRect(80, 40, 69, 22))
        self.mediaBox.setObjectName("mediaBox")
        self.stackedWidget.addWidget(self.page_3)
        self.page_4 = QtGui.QWidget()
        self.page_4.setObjectName("page_4")
        self.label_4 = QtGui.QLabel(self.page_4)
        self.label_4.setGeometry(QtCore.QRect(110, 0, 841, 41))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        self.label_4.setFont(font)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.grafico_Fft = QtGui.QLabel(self.page_4)
        self.grafico_Fft.setGeometry(QtCore.QRect(110, 50, 841, 400))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.grafico_Fft.setFont(font)
        self.grafico_Fft.setFrameShape(QtGui.QFrame.Box)
        self.grafico_Fft.setAlignment(QtCore.Qt.AlignCenter)
        self.grafico_Fft.setObjectName("grafico_Fft")
        self.startBt_Fft = QtGui.QToolButton(self.page_4)
        self.startBt_Fft.setGeometry(QtCore.QRect(10, 260, 81, 91))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.startBt_Fft.setFont(font)
        self.startBt_Fft.setIcon(icon5)
        self.startBt_Fft.setIconSize(QtCore.QSize(64, 64))
        self.startBt_Fft.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.startBt_Fft.setObjectName("startBt_Fft")
        self.salvarBt_Fft = QtGui.QToolButton(self.page_4)
        self.salvarBt_Fft.setGeometry(QtCore.QRect(10, 360, 81, 91))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.salvarBt_Fft.setFont(font)
        self.salvarBt_Fft.setIcon(icon7)
        self.salvarBt_Fft.setIconSize(QtCore.QSize(64, 64))
        self.salvarBt_Fft.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.salvarBt_Fft.setObjectName("salvarBt_Fft")
        self.groupBox_4 = QtGui.QGroupBox(self.page_4)
        self.groupBox_4.setGeometry(QtCore.QRect(10, 180, 81, 71))
        self.groupBox_4.setObjectName("groupBox_4")
        self.CH1_Fft = QtGui.QRadioButton(self.groupBox_4)
        self.CH1_Fft.setGeometry(QtCore.QRect(10, 20, 51, 21))
        self.CH1_Fft.setChecked(True)
        self.CH1_Fft.setAutoExclusive(True)
        self.CH1_Fft.setObjectName("CH1_Fft")
        self.CH2_Fft = QtGui.QRadioButton(self.groupBox_4)
        self.CH2_Fft.setGeometry(QtCore.QRect(10, 40, 51, 21))
        self.CH2_Fft.setAutoExclusive(True)
        self.CH2_Fft.setObjectName("CH2_Fft")
        self.stackedWidget.addWidget(self.page_4)
        self.frame_2 = QtGui.QFrame(Dialog)
        self.frame_2.setGeometry(QtCore.QRect(0, 460, 1071, 131))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.sairBt = QtGui.QToolButton(self.frame_2)
        self.sairBt.setGeometry(QtCore.QRect(960, 15, 101, 105))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        font.setWeight(50)
        font.setItalic(False)
        font.setUnderline(False)
        font.setStrikeOut(False)
        font.setBold(False)
        self.sairBt.setFont(font)
        self.sairBt.setAcceptDrops(False)
        self.sairBt.setStatusTip("")
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(":/Main/icons/sair.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sairBt.setIcon(icon9)
        self.sairBt.setIconSize(QtCore.QSize(64, 64))
        self.sairBt.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.sairBt.setArrowType(QtCore.Qt.NoArrow)
        self.sairBt.setObjectName("sairBt")
        self.osciloscopioBox = QtGui.QGroupBox(self.frame_2)
        self.osciloscopioBox.setGeometry(QtCore.QRect(10, 10, 281, 111))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.osciloscopioBox.setFont(font)
        self.osciloscopioBox.setObjectName("osciloscopioBox")
        self.conectarBtOsc = QtGui.QToolButton(self.osciloscopioBox)
        self.conectarBtOsc.setGeometry(QtCore.QRect(190, 20, 81, 81))
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap(":/Main/icons/conectar.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.conectarBtOsc.setIcon(icon10)
        self.conectarBtOsc.setIconSize(QtCore.QSize(64, 64))
        self.conectarBtOsc.setObjectName("conectarBtOsc")
        self.label_5 = QtGui.QLabel(self.osciloscopioBox)
        self.label_5.setGeometry(QtCore.QRect(20, 30, 60, 20))
        self.label_5.setObjectName("label_5")
        self.modelosOscCombo = QtGui.QComboBox(self.osciloscopioBox)
        self.modelosOscCombo.setGeometry(QtCore.QRect(20, 50, 161, 22))
        self.modelosOscCombo.setObjectName("modelosOscCombo")
        self.osciloscopioStatus = QtGui.QLabel(self.osciloscopioBox)
        self.osciloscopioStatus.setGeometry(QtCore.QRect(20, 70, 121, 31))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.osciloscopioStatus.setFont(font)
        self.osciloscopioStatus.setObjectName("osciloscopioStatus")
        self.geradorBox = QtGui.QGroupBox(self.frame_2)
        self.geradorBox.setGeometry(QtCore.QRect(330, 10, 281, 111))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        self.geradorBox.setFont(font)
        self.geradorBox.setObjectName("geradorBox")
        self.conectarBtGer = QtGui.QToolButton(self.geradorBox)
        self.conectarBtGer.setGeometry(QtCore.QRect(190, 20, 81, 81))
        self.conectarBtGer.setIcon(icon10)
        self.conectarBtGer.setIconSize(QtCore.QSize(64, 64))
        self.conectarBtGer.setObjectName("conectarBtGer")
        self.label_6 = QtGui.QLabel(self.geradorBox)
        self.label_6.setGeometry(QtCore.QRect(20, 30, 60, 20))
        self.label_6.setObjectName("label_6")
        self.modelosGerCombo = QtGui.QComboBox(self.geradorBox)
        self.modelosGerCombo.setGeometry(QtCore.QRect(20, 50, 161, 22))
        self.modelosGerCombo.setObjectName("modelosGerCombo")
        self.geradorStatus = QtGui.QLabel(self.geradorBox)
        self.geradorStatus.setGeometry(QtCore.QRect(20, 70, 121, 31))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.geradorStatus.setFont(font)
        self.geradorStatus.setObjectName("geradorStatus")
        self.sobreBt = QtGui.QToolButton(self.frame_2)
        self.sobreBt.setGeometry(QtCore.QRect(850, 15, 101, 105))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        font.setWeight(50)
        font.setItalic(False)
        font.setUnderline(False)
        font.setStrikeOut(False)
        font.setBold(False)
        self.sobreBt.setFont(font)
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap(":/Main/icons/sobre.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sobreBt.setIcon(icon11)
        self.sobreBt.setIconSize(QtCore.QSize(64, 64))
        self.sobreBt.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.sobreBt.setArrowType(QtCore.Qt.NoArrow)
        self.sobreBt.setObjectName("sobreBt")
        self.questBt = QtGui.QToolButton(self.frame_2)
        self.questBt.setGeometry(QtCore.QRect(740, 15, 101, 105))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        font.setWeight(50)
        font.setItalic(False)
        font.setUnderline(False)
        font.setStrikeOut(False)
        font.setBold(False)
        self.questBt.setFont(font)
        icon12 = QtGui.QIcon()
        icon12.addPixmap(QtGui.QPixmap(":/Main/icons/question.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.questBt.setIcon(icon12)
        self.questBt.setIconSize(QtCore.QSize(64, 64))
        self.questBt.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.questBt.setArrowType(QtCore.Qt.NoArrow)
        self.questBt.setObjectName("questBt")
        self.sobreBt_2 = QtGui.QToolButton(self.frame_2)
        self.sobreBt_2.setGeometry(QtCore.QRect(630, 15, 101, 105))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        font.setWeight(50)
        font.setItalic(False)
        font.setUnderline(False)
        font.setStrikeOut(False)
        font.setBold(False)
        self.sobreBt_2.setFont(font)
        self.sobreBt_2.setIcon(icon11)
        self.sobreBt_2.setIconSize(QtCore.QSize(64, 64))
        self.sobreBt_2.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.sobreBt_2.setArrowType(QtCore.Qt.NoArrow)
        self.sobreBt_2.setObjectName("sobreBt_2")
        self.line_5 = QtGui.QFrame(Dialog)
        self.line_5.setGeometry(QtCore.QRect(0, 590, 1072, 20))
        self.line_5.setFrameShape(QtGui.QFrame.HLine)
        self.line_5.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.label_7 = QtGui.QLabel(Dialog)
        self.label_7.setGeometry(QtCore.QRect(10, 600, 991, 16))
        self.label_7.setObjectName("label_7")

        self.retranslateUi(Dialog)
        self.stackedWidget.setCurrentIndex(2)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.exercicio1Bt.setToolTip(QtGui.QApplication.translate("Dialog", "Rotina de exercício 1", None, QtGui.QApplication.UnicodeUTF8))
        self.exercicio1Bt.setText(QtGui.QApplication.translate("Dialog", "Exercício", None, QtGui.QApplication.UnicodeUTF8))
        self.freqBt.setToolTip(QtGui.QApplication.translate("Dialog", "Rotina de experimento de varredura em frequência", None, QtGui.QApplication.UnicodeUTF8))
        self.freqBt.setText(QtGui.QApplication.translate("Dialog", "Varredura", None, QtGui.QApplication.UnicodeUTF8))
        self.ondaBt.setToolTip(QtGui.QApplication.translate("Dialog", "Rotina de captura da tela do osciloscópio", None, QtGui.QApplication.UnicodeUTF8))
        self.ondaBt.setText(QtGui.QApplication.translate("Dialog", "Captura", None, QtGui.QApplication.UnicodeUTF8))
        self.fftBt.setToolTip(QtGui.QApplication.translate("Dialog", "Rotina de exercício 2", None, QtGui.QApplication.UnicodeUTF8))
        self.fftBt.setText(QtGui.QApplication.translate("Dialog", "FFT", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog", "Exercício 1", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Dialog", "Varredura em frequência", None, QtGui.QApplication.UnicodeUTF8))
        self.grafico_Freq.setText(QtGui.QApplication.translate("Dialog", "Grafico", None, QtGui.QApplication.UnicodeUTF8))
        self.label_8.setText(QtGui.QApplication.translate("Dialog", "Frequência", None, QtGui.QApplication.UnicodeUTF8))
        self.label_9.setText(QtGui.QApplication.translate("Dialog", "final:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_10.setText(QtGui.QApplication.translate("Dialog", "Pontos amostrais:", None, QtGui.QApplication.UnicodeUTF8))
        self.startBt_Freq.setToolTip(QtGui.QApplication.translate("Dialog", "Inicia a varredura em frequência", None, QtGui.QApplication.UnicodeUTF8))
        self.startBt_Freq.setText(QtGui.QApplication.translate("Dialog", "Iniciar", None, QtGui.QApplication.UnicodeUTF8))
        self.stopBt_Freq.setToolTip(QtGui.QApplication.translate("Dialog", "Interrompe a varredura", None, QtGui.QApplication.UnicodeUTF8))
        self.stopBt_Freq.setText(QtGui.QApplication.translate("Dialog", "Parar", None, QtGui.QApplication.UnicodeUTF8))
        self.salvarBt_Freq.setToolTip(QtGui.QApplication.translate("Dialog", "Salva os dados adquiridos em CSV e PNG", None, QtGui.QApplication.UnicodeUTF8))
        self.salvarBt_Freq.setText(QtGui.QApplication.translate("Dialog", "Salvar", None, QtGui.QApplication.UnicodeUTF8))
        self.novoBt_Freq.setToolTip(QtGui.QApplication.translate("Dialog", "Limpa os dados adquiridos", None, QtGui.QApplication.UnicodeUTF8))
        self.novoBt_Freq.setText(QtGui.QApplication.translate("Dialog", "Novo", None, QtGui.QApplication.UnicodeUTF8))
        self.label_11.setText(QtGui.QApplication.translate("Dialog", "Inicial:", None, QtGui.QApplication.UnicodeUTF8))
        self.statusFreq.setText(QtGui.QApplication.translate("Dialog", "Em espera", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Dialog", "Captura de tela", None, QtGui.QApplication.UnicodeUTF8))
        self.grafico_Cap.setText(QtGui.QApplication.translate("Dialog", "Gráfico", None, QtGui.QApplication.UnicodeUTF8))
        self.capturarBt_Cap.setToolTip(QtGui.QApplication.translate("Dialog", "Inicia a captura da tela do osciloscópio", None, QtGui.QApplication.UnicodeUTF8))
        self.capturarBt_Cap.setText(QtGui.QApplication.translate("Dialog", "Capturar", None, QtGui.QApplication.UnicodeUTF8))
        self.salvarBt_Cap.setToolTip(QtGui.QApplication.translate("Dialog", "Salva a tela capturada em CSV e PNG", None, QtGui.QApplication.UnicodeUTF8))
        self.salvarBt_Cap.setText(QtGui.QApplication.translate("Dialog", "Salvar", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("Dialog", "Canal 1", None, QtGui.QApplication.UnicodeUTF8))
        self.canalUmCheck.setToolTip(QtGui.QApplication.translate("Dialog", "Habilita Desabilita a captura do canal 1", None, QtGui.QApplication.UnicodeUTF8))
        self.canalUmCheck.setText(QtGui.QApplication.translate("Dialog", "Capturar canal", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("Dialog", "Canal 2", None, QtGui.QApplication.UnicodeUTF8))
        self.canalDoisCheck.setToolTip(QtGui.QApplication.translate("Dialog", "Habilita Desabilita a captura do canal 2", None, QtGui.QApplication.UnicodeUTF8))
        self.canalDoisCheck.setText(QtGui.QApplication.translate("Dialog", "Capturar canal", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_3.setTitle(QtGui.QApplication.translate("Dialog", "Trigger", None, QtGui.QApplication.UnicodeUTF8))
        self.triggerCh1.setText(QtGui.QApplication.translate("Dialog", "Ch1", None, QtGui.QApplication.UnicodeUTF8))
        self.triggerExt.setText(QtGui.QApplication.translate("Dialog", "Externo", None, QtGui.QApplication.UnicodeUTF8))
        self.triggerLinha.setText(QtGui.QApplication.translate("Dialog", "Linha (60Hz)", None, QtGui.QApplication.UnicodeUTF8))
        self.triggerCh2.setText(QtGui.QApplication.translate("Dialog", "Ch2", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_5.setTitle(QtGui.QApplication.translate("Dialog", "Plot", None, QtGui.QApplication.UnicodeUTF8))
        self.vtRBt.setText(QtGui.QApplication.translate("Dialog", "Vxtempo", None, QtGui.QApplication.UnicodeUTF8))
        self.ch12ch2RBt.setText(QtGui.QApplication.translate("Dialog", "(Ch1 - Ch2) x Ch2", None, QtGui.QApplication.UnicodeUTF8))
        self.ivRBt.setText(QtGui.QApplication.translate("Dialog", "Ch1 x Ch2", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_6.setTitle(QtGui.QApplication.translate("Dialog", "Leitura", None, QtGui.QApplication.UnicodeUTF8))
        self.amostraRBt.setText(QtGui.QApplication.translate("Dialog", "Amostra", None, QtGui.QApplication.UnicodeUTF8))
        self.mediaRBt.setText(QtGui.QApplication.translate("Dialog", "Média:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Dialog", "Cálculo do FFT do sinal", None, QtGui.QApplication.UnicodeUTF8))
        self.grafico_Fft.setText(QtGui.QApplication.translate("Dialog", "Grafico", None, QtGui.QApplication.UnicodeUTF8))
        self.startBt_Fft.setToolTip(QtGui.QApplication.translate("Dialog", "Inicia o cálculo do FFT", None, QtGui.QApplication.UnicodeUTF8))
        self.startBt_Fft.setText(QtGui.QApplication.translate("Dialog", "Iniciar", None, QtGui.QApplication.UnicodeUTF8))
        self.salvarBt_Fft.setToolTip(QtGui.QApplication.translate("Dialog", "Salva os dados adquiridos em CSV e PNG", None, QtGui.QApplication.UnicodeUTF8))
        self.salvarBt_Fft.setText(QtGui.QApplication.translate("Dialog", "Salvar", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_4.setTitle(QtGui.QApplication.translate("Dialog", "Fonte", None, QtGui.QApplication.UnicodeUTF8))
        self.CH1_Fft.setText(QtGui.QApplication.translate("Dialog", "CH1", None, QtGui.QApplication.UnicodeUTF8))
        self.CH2_Fft.setText(QtGui.QApplication.translate("Dialog", "CH2", None, QtGui.QApplication.UnicodeUTF8))
        self.sairBt.setToolTip(QtGui.QApplication.translate("Dialog", "Sair do programa", None, QtGui.QApplication.UnicodeUTF8))
        self.sairBt.setWhatsThis(QtGui.QApplication.translate("Dialog", "Desconecta os equipamentos, encerra os processos e desliga o programa.", None, QtGui.QApplication.UnicodeUTF8))
        self.sairBt.setAccessibleName(QtGui.QApplication.translate("Dialog", "Sair", None, QtGui.QApplication.UnicodeUTF8))
        self.sairBt.setAccessibleDescription(QtGui.QApplication.translate("Dialog", "Sair do programa", None, QtGui.QApplication.UnicodeUTF8))
        self.sairBt.setText(QtGui.QApplication.translate("Dialog", "Sair", None, QtGui.QApplication.UnicodeUTF8))
        self.osciloscopioBox.setTitle(QtGui.QApplication.translate("Dialog", "Osciloscópio", None, QtGui.QApplication.UnicodeUTF8))
        self.conectarBtOsc.setToolTip(QtGui.QApplication.translate("Dialog", "Conecta ou desconecta o osciloscópio", None, QtGui.QApplication.UnicodeUTF8))
        self.conectarBtOsc.setText(QtGui.QApplication.translate("Dialog", "Conectar", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("Dialog", "Modelo", None, QtGui.QApplication.UnicodeUTF8))
        self.osciloscopioStatus.setText(QtGui.QApplication.translate("Dialog", "Desconectado", None, QtGui.QApplication.UnicodeUTF8))
        self.geradorBox.setTitle(QtGui.QApplication.translate("Dialog", "Gerador", None, QtGui.QApplication.UnicodeUTF8))
        self.conectarBtGer.setToolTip(QtGui.QApplication.translate("Dialog", "Conecta ou desconecta o gerador", None, QtGui.QApplication.UnicodeUTF8))
        self.conectarBtGer.setText(QtGui.QApplication.translate("Dialog", "Conectar", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("Dialog", "Modelo", None, QtGui.QApplication.UnicodeUTF8))
        self.geradorStatus.setText(QtGui.QApplication.translate("Dialog", "Desconectado", None, QtGui.QApplication.UnicodeUTF8))
        self.sobreBt.setToolTip(QtGui.QApplication.translate("Dialog", "Informações sobre o programa", None, QtGui.QApplication.UnicodeUTF8))
        self.sobreBt.setWhatsThis(QtGui.QApplication.translate("Dialog", "Abre janela com informações sobre o programa", None, QtGui.QApplication.UnicodeUTF8))
        self.sobreBt.setAccessibleName(QtGui.QApplication.translate("Dialog", "Sobre", None, QtGui.QApplication.UnicodeUTF8))
        self.sobreBt.setAccessibleDescription(QtGui.QApplication.translate("Dialog", "Informações sobre o programa", None, QtGui.QApplication.UnicodeUTF8))
        self.sobreBt.setText(QtGui.QApplication.translate("Dialog", "Sobre", None, QtGui.QApplication.UnicodeUTF8))
        self.questBt.setToolTip(QtGui.QApplication.translate("Dialog", "Informações sobre os experimentos", None, QtGui.QApplication.UnicodeUTF8))
        self.questBt.setWhatsThis(QtGui.QApplication.translate("Dialog", "Abre janela com informações sobre os experimentos", None, QtGui.QApplication.UnicodeUTF8))
        self.questBt.setAccessibleName(QtGui.QApplication.translate("Dialog", "Dúvidas", None, QtGui.QApplication.UnicodeUTF8))
        self.questBt.setAccessibleDescription(QtGui.QApplication.translate("Dialog", "Informações sobre os experimentos", None, QtGui.QApplication.UnicodeUTF8))
        self.questBt.setText(QtGui.QApplication.translate("Dialog", "Dúvidas", None, QtGui.QApplication.UnicodeUTF8))
        self.sobreBt_2.setText(QtGui.QApplication.translate("Dialog", "Outro", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(QtGui.QApplication.translate("Dialog", "Escrito e compilado por Vladimir Gaal - Março 2018 - Agradecimento ao site icons8.com por fornecer gratuitamente alguns dos ícones utilizados", None, QtGui.QApplication.UnicodeUTF8))

import icones_rc
