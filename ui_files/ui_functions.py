################################################################################
##
## BY: WANDERSON M.PIMENTA
## PROJECT MADE WITH: Qt Designer and PySide2
## V: 1.0.0
##
## This project can be used freely for all uses, as long as they maintain the
## respective credits only in the Python scripts, any information in the visual
## interface (GUI) can be modified without any implication.
##
## There are limitations on Qt licenses if you want to use your products
## commercially, I recommend reading them on the official website:
## https://doc.qt.io/qtforpython/licenses.html
##
################################################################################

## ==> GUI FILE
from main import *
import matplotlib
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure


## ==> GLOBALS
GLOBAL_STATE = 0
GLOBAL_TITLE_BAR = True

## ==> COUT INITIAL MENU
count = 1

class UIFunctions(MainWindow):

    ## ==> GLOBALS
    GLOBAL_STATE = 0
    GLOBAL_TITLE_BAR = True

    ########################################################################
    ## START - GUI FUNCTIONS
    ########################################################################

    ## ==> MAXIMIZE/RESTORE
    ########################################################################
    def maximize_restore(self):
        global GLOBAL_STATE
        status = GLOBAL_STATE
        if status == 0:
            self.showMaximized()
            GLOBAL_STATE = 1
            self.ui.horizontalLayout.setContentsMargins(0, 0, 0, 0)
            self.ui.btn_maximize_restore.setToolTip("Restore")
            self.ui.btn_maximize_restore.setIcon(QtGui.QIcon(u":/16x16/icons/16x16/cil-window-restore.png"))
            self.ui.frame_top_btns.setStyleSheet("QPushButton { background-color: rgb(19, 20, 26); ") ##27, 29, 35
            self.ui.frame_size_grip.hide()
        else:
            GLOBAL_STATE = 0
            self.showNormal()
            self.resize(self.width()+1, self.height()+1)
            self.ui.horizontalLayout.setContentsMargins(10, 10, 10, 10)
            self.ui.btn_maximize_restore.setToolTip("Maximize")
            self.ui.btn_maximize_restore.setIcon(QtGui.QIcon(u":/16x16/icons/16x16/cil-window-maximize.png"))
            self.ui.frame_top_btns.setStyleSheet("QPushButton { background-color: rgb(19, 20, 26); ")
            self.ui.frame_size_grip.show()

    ## ==> RETURN STATUS
    def returStatus():
        return GLOBAL_STATE

    ## ==> SET STATUS
    def setStatus(status):
        global GLOBAL_STATE
        GLOBAL_STATE = status

    ## ==> ENABLE MAXIMUM SIZE
    ########################################################################
    def enableMaximumSize(self, width, height):
        if width != '' and height != '':
            self.setMaximumSize(QSize(width, height))
            self.ui.frame_size_grip.hide()
            self.ui.btn_maximize_restore.hide()


    ## ==> TOGGLE MENU
    ########################################################################
    def toggleMenu(self, maxWidth, enable):
        if enable:
            # GET WIDTH
            width = self.ui.frame_left_menu.width()
            maxExtend = maxWidth
            standard = 70

            # SET MAX WIDTH
            if width == 70:
                widthExtended = maxExtend
            else:
                widthExtended = standard

            # ANIMATION
            self.animation = QPropertyAnimation(self.ui.frame_left_menu, b"minimumWidth")
            self.animation.setDuration(300)
            self.animation.setStartValue(width)
            self.animation.setEndValue(widthExtended)
            self.animation.setEasingCurve(QtCore.QEasingCurve.InOutQuart)
            self.animation.start()

    ## ==> SET TITLE BAR
    ########################################################################
    def removeTitleBar(status):
        global GLOBAL_TITLE_BAR
        GLOBAL_TITLE_BAR = status

    ## ==> HEADER TEXTS
    ########################################################################
    # LABEL TITLE
    def labelTitle(self, text):
        self.ui.label_title_bar_top.setText(text)

    # LABEL DESCRIPTION
    def labelDescription(self, text):
        self.ui.label_top_info_1.setText(text)

    def addComboItem(self, type, name):
        if type == 'gen':
            self.ui.modelosGerCombo.addItems([name])
        elif type == 'scope':
            self.ui.modelosOscCombo.addItems([name])
        elif type == 'freq_init':
            self.ui.freqInicialBox.addItems([name])
        elif type == 'freq_end':
            self.ui.freqFinalBox.addItems([name])
        elif type == 'amp':
            self.ui.mediaBox.addItems([name])

    def setIndexCombo(self, type, item):
        if type == 'gen':
            self.ui.modelosGerCombo.setCurrentIndex(item)
        elif type == 'scope':
            self.ui.modelosOscCombo.setCurrentIndex(item)

    ## ==> DYNAMIC MENUS
    ########################################################################
    def addNewMenu(self, name, objName, icon, isTopMenu):
        font = QFont()
        font.setFamily(u"Segoe UI")
        font.setPointSizeF(12)
        button = QPushButton(str(count),self)
        button.setObjectName(objName)
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(button.sizePolicy().hasHeightForWidth())
        button.setSizePolicy(sizePolicy3)
        button.setMinimumSize(QSize(0, 80))
        button.setLayoutDirection(Qt.LeftToRight)
        button.setFont(font)
        teste = Style.style_bt_standard.replace('ICON_REPLACE', icon)
        button.setStyleSheet(Style.style_bt_standard.replace('ICON_REPLACE', icon)\
                                                    .replace("background-color: rgb(27, 29, 35);",
                                                             "background-color: rgb(19, 20, 26); ")\
                                                    .replace("border-left: 28px solid rgb(27, 29, 35);",
                                                             "border-left: 28px solid rgb(19, 20, 26);"))
        button.setText(name)
        button.setToolTip(name)
        button.clicked.connect(self.Button)

        if isTopMenu:
            self.ui.layout_menus.addWidget(button)
        else:
            self.ui.layout_menu_bottom.addWidget(button)

    ## ==> SELECT/DESELECT MENU
    ########################################################################
    ## ==> SELECT
    def selectMenu(getStyle):
        select = getStyle + ("QPushButton { border-right: 7px solid rgb(71, 78, 97); }") +\
                  ("QPushButton { background-color: rgb(19, 20, 26); }"
                   "QPushButton { border-left: 28px solid rgb(19, 20, 26);}")

        #44, 49, 60)
        return select

    ## ==> DESELECT
    def deselectMenu(getStyle):
        deselect = getStyle.replace("QPushButton { border-right: 7px solid rgb(71, 78, 97); }"
                                    "QPushButton { background-color: rgb(19, 20, 26); }"
                                    "QPushButton { border-left: 28px solid rgb(19, 20, 26);}",
                                    "QPushButton { background-color: rgb(19, 20, 26); }"
                                    "QPushButton { border-left: 28px solid rgb(19, 20, 26);}")
        return deselect

    ## ==> START SELECTION
    def selectStandardMenu(self, widget):
        for w in self.ui.frame_left_menu.findChildren(QPushButton):
            if w.objectName() == widget:
                w.setStyleSheet(UIFunctions.selectMenu(w.styleSheet()))


    ## ==> RESET SELECTION
    def resetStyle(self, widget):
        for w in self.ui.frame_left_menu.findChildren(QPushButton):
            if w.objectName() != widget:
                w.setStyleSheet(UIFunctions.deselectMenu(w.styleSheet()))

    ## ==> CHANGE PAGE LABEL TEXT
    def labelPage(self, text):
        newText = '| ' + text.upper()
        self.ui.label_top_info_2.setText(newText)

    ## ==> USER ICON
    ########################################################################
    def userIcon(self, initialsTooltip, icon, showHide):
        if showHide:
            # SET TEXT
            self.ui.label_user_icon.setText(initialsTooltip)

            # SET ICON
            if icon:
                style = self.ui.label_user_icon.styleSheet()
                setIcon = "QLabel { background-image: " + icon + "; }"
                self.ui.label_user_icon.setStyleSheet(style + setIcon)
                self.ui.label_user_icon.setText('')
                self.ui.label_user_icon.setToolTip(initialsTooltip)
        else:
            self.ui.label_user_icon.hide()

    def setIcons(self):
        self.iconConectar = QtGui.QIcon()
        self.iconConectar.addPixmap(QtGui.QPixmap(":/old/icons/conectar.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.iconDesconectar = QtGui.QIcon()
        self.iconDesconectar.addPixmap(QtGui.QPixmap(":/old/icons/desconectar.png"), QtGui.QIcon.Normal,
                                       QtGui.QIcon.Off)
        self.iconRedLight = QtGui.QIcon()
        self.iconRedLight.addPixmap(QtGui.QPixmap(":/old/icons/red_light.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.iconGreenLight = QtGui.QIcon()
        self.iconGreenLight.addPixmap(QtGui.QPixmap(":/old/icons/green_light.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.iconCerto = QtGui.QIcon()
        self.iconCerto.addPixmap(QtGui.QPixmap(":/old/icons/correto.png"), QtGui.QIcon.Normal,
                                       QtGui.QIcon.Off)
        self.iconErrado = QtGui.QIcon()
        self.iconErrado.addPixmap(QtGui.QPixmap(":/old/icons/errado.png"), QtGui.QIcon.Normal,
                                 QtGui.QIcon.Off)
    ########################################################################
    ## END - GUI FUNCTIONS
    ########################################################################

    ########################################################################
    ## START - PLOT SETTINGS
    ########################################################################
    def setPlots(self):
        ## COMMON
        color1 = 'white'
        font_mid = {'family': 'Segoe UI', 'size': 16}
        font_big = {'family': 'Segoe UI', 'weight': 'bold', 'size': 22}
        matplotlib.rc('font', **font_mid)

        ## FREQ PLOT
        fig_freq = Figure(figsize=(7, 5), dpi=65, frameon=False)
        fig_freq.patch.set_alpha(0)
        fig_freq.subplots_adjust(left=0.12, bottom=0.12, right=0.98, top=0.98)
        self.canvas_freq = FigureCanvas(fig_freq)
        self.ui.plot_area_freq.addWidget(self.canvas_freq)
        self.ax1_freq = fig_freq.add_subplot(211)
        self.ax1_freq.patch.set_alpha(0)
        for spine in self.ax1_freq.spines.values():
            spine.set_edgecolor(color1)
        self.ax1_freq.xaxis.label.set_color(color1)
        self.ax1_freq.yaxis.label.set_color(color1)
        self.ax1_freq.set_xscale('log')
        self.ax1_freq.grid(True)
        self.ax1_freq.tick_params(axis='both', colors=color1)
        # self.ax1_freq.set_xlabel("Transmittance", **font_big)
        self.ax1_freq.set_ylabel("Transmittance (dB)", **font_big)

        self.ax2_freq = fig_freq.add_subplot(212)
        self.ax2_freq.patch.set_alpha(0)
        for spine in self.ax2_freq.spines.values():
            spine.set_edgecolor(color1)
        self.ax2_freq.set_xscale('log')
        self.ax2_freq.xaxis.label.set_color(color1)
        self.ax2_freq.yaxis.label.set_color(color1)
        self.ax2_freq.grid(True)
        self.ax2_freq.tick_params(axis='both', colors=color1)
        self.ax2_freq.set_xlabel("Frequency (Hz)", **font_big)
        self.ax2_freq.set_ylabel("Phase (degrees)", **font_big)

        ## FFT PLOT
        fig_fft = Figure(figsize=(7, 5), dpi=65, frameon=True)
        fig_fft.patch.set_alpha(0)
        fig_fft.subplots_adjust(left=0.12, bottom=0.12, right=0.98, top=0.98)
        self.ax_fft = fig_fft.add_subplot(111)
        self.ax_fft.patch.set_alpha(0)
        for spine in self.ax_fft.spines.values():
            spine.set_edgecolor(color1)
        self.ax_fft.xaxis.label.set_color(color1)
        self.ax_fft.yaxis.label.set_color(color1)
        self.ax_fft.tick_params(axis='both', colors=color1)
        self.ax_fft.grid(True)
        self.ax_fft.set_xlabel("Frequency (Hz)", **font_big)
        self.ax_fft.set_ylabel("|Y(freq)|", **font_big)
        self.canvas_fft = FigureCanvas(fig_fft)
        self.ui.plot_area_fft.addWidget(self.canvas_fft)

        ## TIME PLOT
        fig_time = Figure(figsize=(7, 5), dpi=65, frameon=True)
        fig_time.patch.set_alpha(0)
        fig_time.subplots_adjust(left=0.12, bottom=0.12, right=0.88, top=0.98)
        self.ax1_time = fig_time.add_subplot(111)
        self.ax1_time.patch.set_alpha(0)
        for spine in self.ax1_time.spines.values():
            spine.set_edgecolor(color1)
        self.ax1_time.xaxis.label.set_color(color1)
        self.ax1_time.yaxis.label.set_color(color1)
        self.ax1_time.tick_params(axis='x', colors=color1)
        self.ax1_time.tick_params(axis='y', colors='y')
        self.ax1_time.grid(True)
        self.ax1_time.set_xlabel("Time (s)", **font_big)
        self.ax1_time.set_ylabel("CH1 amplitude (V)", **font_big, color='y')
        self.canvas_time = FigureCanvas(fig_time)
        self.ui.plot_area_time.addWidget(self.canvas_time)

        self.ax2_time = self.ax1_time.twinx()
        self.ax2_time.set_ylabel('CH2 amplitude (V)', **font_big, color='cyan')
        self.ax2_time.tick_params(axis='y', colors='cyan')
        self.ax2_time.grid(True)

        ## VOLT PLOT
        fig_volt = Figure(figsize=(7, 5), dpi=65, frameon=True)
        fig_volt.patch.set_alpha(0)
        fig_volt.subplots_adjust(left=0.12, bottom=0.12, right=0.88, top=0.98)
        self.ax1_volt = fig_volt.add_subplot(111)
        self.ax1_volt.patch.set_alpha(0)
        for spine in self.ax1_volt.spines.values():
            spine.set_edgecolor(color1)
        self.ax1_volt.xaxis.label.set_color(color1)
        self.ax1_volt.yaxis.label.set_color(color1)
        self.ax1_volt.tick_params(axis='x', colors=color1)
        self.ax1_volt.tick_params(axis='y', colors='y')
        self.ax1_volt.grid(True)
        self.ax1_volt.set_xlabel("CH1 amplitude (V)", **font_big)
        self.ax1_volt.set_ylabel("CH2 amplitude (V)", **font_big)
        self.canvas_volt = FigureCanvas(fig_volt)
        self.ui.plot_area_volt.addWidget(self.canvas_volt)

        ## CH122 PLOT
        fig_ch122 = Figure(figsize=(7, 5), dpi=65, frameon=True)
        fig_ch122.patch.set_alpha(0)
        fig_ch122.subplots_adjust(left=0.12, bottom=0.12, right=0.88, top=0.98)
        self.ax1_ch122 = fig_ch122.add_subplot(111)
        self.ax1_ch122.patch.set_alpha(0)
        for spine in self.ax1_ch122.spines.values():
            spine.set_edgecolor(color1)
        self.ax1_ch122.xaxis.label.set_color(color1)
        self.ax1_ch122.yaxis.label.set_color(color1)
        self.ax1_ch122.tick_params(axis='x', colors=color1)
        self.ax1_ch122.tick_params(axis='y', colors='y')
        self.ax1_ch122.grid(True)
        self.ax1_ch122.set_xlabel("CH1 amplitude (V)", **font_big)
        self.ax1_ch122.set_ylabel("CH1 - CH2 amplitude (V)", **font_big)
        self.canvas_ch122 = FigureCanvas(fig_ch122)
        self.ui.plot_area_ch122.addWidget(self.canvas_ch122)



    ########################################################################
    ## END - GUI PLOT SETTINGS
    ########################################################################

    ########################################################################
    ## START - GUI DEFINITIONS
    ########################################################################

    ## ==> UI DEFINITIONS
    ########################################################################
    def uiDefinitions(self):
        def dobleClickMaximizeRestore(event):
            # IF DOUBLE CLICK CHANGE STATUS
            if event.type() == QtCore.QEvent.MouseButtonDblClick:
                QtCore.QTimer.singleShot(250, lambda: UIFunctions.maximize_restore(self))

        ## REMOVE ==> STANDARD TITLE BAR
        if GLOBAL_TITLE_BAR:
            self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
            self.ui.frame_label_top_btns.mouseDoubleClickEvent = dobleClickMaximizeRestore
        else:
            self.ui.horizontalLayout.setContentsMargins(0, 0, 0, 0)
            self.ui.frame_label_top_btns.setContentsMargins(8, 0, 0, 5)
            self.ui.frame_label_top_btns.setMinimumHeight(42)
            self.ui.frame_icon_top_bar.hide()
            self.ui.frame_btns_right.hide()
            self.ui.frame_size_grip.hide()


        ## SHOW ==> DROP SHADOW
        self.shadow = QGraphicsDropShadowEffect(self)
        self.shadow.setBlurRadius(17)
        self.shadow.setXOffset(0)
        self.shadow.setYOffset(0)
        self.shadow.setColor(QColor(0, 0, 0, 150))
        self.ui.frame_main.setGraphicsEffect(self.shadow)

        ## ==> RESIZE WINDOW
        self.sizegrip = QSizeGrip(self.ui.frame_size_grip)
        self.sizegrip.setStyleSheet("width: 20px; height: 20px; margin 0px; padding: 0px;")

        ### ==> MINIMIZE
        self.ui.btn_minimize.clicked.connect(lambda: self.showMinimized())

        ## ==> MAXIMIZE/RESTORE
        self.ui.btn_maximize_restore.clicked.connect(lambda: UIFunctions.maximize_restore(self))

        ## SHOW ==> CLOSE APPLICATION
        self.ui.btn_close.clicked.connect(lambda: self.close())

        ## ==> FFT TAB
        self.ui.salvarBt_Fft.setEnabled(False)
        self.ui.ch1_Fft.setChecked(True)

        ## ==> SETTINGS TAB
        self.ui.modelosGerCombo.addItems(['', 'BK 4052'])
        self.ui.modelosOscCombo.addItems(['', 'TBS1062'])
        self.ui.modelosOscCombo.setCurrentIndex(1)
        self.ui.modelosGerCombo.setCurrentIndex(1)

        ## ==> FREQ TAB
        self.ui.freqInicialBox.addItems(['Hz', 'KHz', 'MHz'])
        self.ui.freqFinalBox.addItems(['Hz', 'KHz', 'MHz'])
        self.ui.progressBar.setRange(0, 100)
        self.ui.progressBar.setValue(0)
        self.ui.freqInicialLine.setInputMask('9999')
        self.ui.freqInicialLine.setText('10')
        self.ui.freqFinalLine.setInputMask('9999')
        self.ui.freqFinalLine.setText('100')
        self.ui.freqInicialBox.setCurrentIndex(0)
        self.ui.freqFinalBox.setCurrentIndex(1)
        self.ui.numeroPontos.setInputMask('9999')
        self.ui.numeroPontos.setText('10')
        self.ui.stopBt_Freq.setEnabled(False)
        self.ui.salvarBt_Freq.setEnabled(False)
        self.ui.novoBt_Freq.setEnabled(False)
        self.ui.statusFreq.setText("Holding")
        self.ui.statusFreq.setStyleSheet("QLabel#statusFreq {color: white; font: bold 12pt; " +
                                     "font-family: Segoe UI;}")

        ## ==> TIME TAB
        self.ui.mediaBox.addItems(['4', '16', '64', '128'])
        self.ui.vtRBt.setChecked(True)
        self.ui.salvarBt_Cap.setEnabled(False)
        self.ui.canalUmCheck.setChecked(True)
        self.ui.canalDoisCheck.setChecked(True)
        self.ui.amostraRBt.setChecked(True)
        self.ui.triggerCh1.setChecked(True)

        ## ==> EXERCISE TAB
        self.ui.userRespFreqExp.addItems(['Hz', 'KHz', 'MHz'])
        self.ui.userRespAmpExp.addItems(['V', 'mV'])
        self.ui.userRespOnda.addItems(['', 'Sinus', 'Square', 'Ramp'])
        self.ui.userRespOnda.setCurrentIndex(0)
        self.ui.userRespAmpExp.setCurrentIndex(0)
        self.ui.userRespFreqExp.setCurrentIndex(0)
        self.ui.userRespAmpNum.setInputMask('9999')
        self.ui.userRespFreqNum.setInputMask('9999')
        self.ui.notaLabel.setText('--')
        self.ui.notaLabel.setStyleSheet("QLabel#notaLabel {color: white; font: bold 18pt; " + "font-family: Segoe UI;}")
        self.ui.respostaBt.setEnabled(False)
        self.ui.dificilBt.setChecked(True)
        self.ui.userRespAmpNum.setText('0')
        self.ui.userRespFreqNum.setText('0')
        self.ui.userRespFreqNum.setCursorPosition(0)
        self.ui.userRespAmpNum.setCursorPosition(0)


    ########################################################################
    ## END - GUI DEFINITIONS
    ########################################################################
