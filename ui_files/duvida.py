# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'duvida.ui'
#
# Created: Fri Feb 28 15:14:52 2020
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(558, 650)
        Dialog.setMinimumSize(QtCore.QSize(558, 650))
        Dialog.setMaximumSize(QtCore.QSize(558, 650))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/geral/icons/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("QGroupBox {\n"
"    background-color:transparent;\n"
"    border: 2px solid rgb(0, 0, 0);\n"
"    border-radius: 5px;\n"
"    margin-top: 1ex; \n"
"font-weight: bold;\n"
"font-size: 15px\n"
"}\n"
"\n"
"QGroupBox::title{\n"
"subcontrol-origin: margin;\n"
"subcontrol-position: top left;\n"
"padding: 0 5px;\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"\n"
"QPushButton{\n"
"background-color: Transparent; \n"
"border-style: outset;\n"
"    border-width: 2px;\n"
"    border-radius: 0px;\n"
"    border-color: rgb(127, 127, 127);\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"border-style: inset;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"background-color:qradialgradient(spread:pad, cx:0.131182, cy:0.108, radius:1.262, fx:0.13, fy:0.142, stop:0.357955 rgba(0, 0, 0, 0), stop:1 rgba(0, 0, 0, 14));\n"
"}")
        self.tabWidget = QtGui.QTabWidget(Dialog)
        self.tabWidget.setGeometry(QtCore.QRect(10, 10, 541, 601))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(12)
        font.setWeight(75)
        font.setBold(True)
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.exerciciosTab = QtGui.QWidget()
        self.exerciciosTab.setObjectName("exerciciosTab")
        self.groupBox = QtGui.QGroupBox(self.exerciciosTab)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 511, 191))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox.setFont(font)
        self.groupBox.setObjectName("groupBox")
        self.textBrowser = QtGui.QTextBrowser(self.groupBox)
        self.textBrowser.setGeometry(QtCore.QRect(10, 20, 491, 161))
        self.textBrowser.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser.setObjectName("textBrowser")
        self.groupBox_14 = QtGui.QGroupBox(self.exerciciosTab)
        self.groupBox_14.setGeometry(QtCore.QRect(10, 210, 511, 231))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_14.setFont(font)
        self.groupBox_14.setObjectName("groupBox_14")
        self.textBrowser_14 = QtGui.QTextBrowser(self.groupBox_14)
        self.textBrowser_14.setGeometry(QtCore.QRect(10, 20, 491, 201))
        self.textBrowser_14.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_14.setObjectName("textBrowser_14")
        self.tabWidget.addTab(self.exerciciosTab, "")
        self.fftTab = QtGui.QWidget()
        self.fftTab.setObjectName("fftTab")
        self.groupBox_2 = QtGui.QGroupBox(self.fftTab)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 10, 511, 121))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_2.setFont(font)
        self.groupBox_2.setObjectName("groupBox_2")
        self.textBrowser_2 = QtGui.QTextBrowser(self.groupBox_2)
        self.textBrowser_2.setGeometry(QtCore.QRect(10, 20, 491, 91))
        self.textBrowser_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_2.setObjectName("textBrowser_2")
        self.groupBox_3 = QtGui.QGroupBox(self.fftTab)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 140, 511, 81))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_3.setFont(font)
        self.groupBox_3.setObjectName("groupBox_3")
        self.textBrowser_3 = QtGui.QTextBrowser(self.groupBox_3)
        self.textBrowser_3.setGeometry(QtCore.QRect(10, 20, 491, 51))
        self.textBrowser_3.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_3.setObjectName("textBrowser_3")
        self.groupBox_5 = QtGui.QGroupBox(self.fftTab)
        self.groupBox_5.setGeometry(QtCore.QRect(10, 230, 511, 171))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_5.setFont(font)
        self.groupBox_5.setObjectName("groupBox_5")
        self.textBrowser_5 = QtGui.QTextBrowser(self.groupBox_5)
        self.textBrowser_5.setGeometry(QtCore.QRect(10, 20, 491, 121))
        self.textBrowser_5.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_5.setObjectName("textBrowser_5")
        self.groupBox_4 = QtGui.QGroupBox(self.fftTab)
        self.groupBox_4.setGeometry(QtCore.QRect(10, 410, 511, 151))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_4.setFont(font)
        self.groupBox_4.setObjectName("groupBox_4")
        self.textBrowser_4 = QtGui.QTextBrowser(self.groupBox_4)
        self.textBrowser_4.setGeometry(QtCore.QRect(10, 20, 491, 121))
        self.textBrowser_4.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_4.setObjectName("textBrowser_4")
        self.tabWidget.addTab(self.fftTab, "")
        self.varreduraTab = QtGui.QWidget()
        self.varreduraTab.setObjectName("varreduraTab")
        self.groupBox_6 = QtGui.QGroupBox(self.varreduraTab)
        self.groupBox_6.setGeometry(QtCore.QRect(10, 250, 511, 151))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_6.setFont(font)
        self.groupBox_6.setObjectName("groupBox_6")
        self.textBrowser_6 = QtGui.QTextBrowser(self.groupBox_6)
        self.textBrowser_6.setGeometry(QtCore.QRect(10, 20, 491, 121))
        self.textBrowser_6.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_6.setObjectName("textBrowser_6")
        self.groupBox_7 = QtGui.QGroupBox(self.varreduraTab)
        self.groupBox_7.setGeometry(QtCore.QRect(10, 140, 511, 101))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_7.setFont(font)
        self.groupBox_7.setObjectName("groupBox_7")
        self.textBrowser_7 = QtGui.QTextBrowser(self.groupBox_7)
        self.textBrowser_7.setGeometry(QtCore.QRect(10, 20, 491, 71))
        self.textBrowser_7.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_7.setObjectName("textBrowser_7")
        self.groupBox_8 = QtGui.QGroupBox(self.varreduraTab)
        self.groupBox_8.setGeometry(QtCore.QRect(10, 10, 511, 121))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_8.setFont(font)
        self.groupBox_8.setObjectName("groupBox_8")
        self.textBrowser_8 = QtGui.QTextBrowser(self.groupBox_8)
        self.textBrowser_8.setGeometry(QtCore.QRect(10, 20, 491, 91))
        self.textBrowser_8.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_8.setObjectName("textBrowser_8")
        self.groupBox_9 = QtGui.QGroupBox(self.varreduraTab)
        self.groupBox_9.setGeometry(QtCore.QRect(10, 410, 511, 151))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_9.setFont(font)
        self.groupBox_9.setObjectName("groupBox_9")
        self.textBrowser_9 = QtGui.QTextBrowser(self.groupBox_9)
        self.textBrowser_9.setGeometry(QtCore.QRect(10, 20, 491, 121))
        self.textBrowser_9.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_9.setObjectName("textBrowser_9")
        self.tabWidget.addTab(self.varreduraTab, "")
        self.capturaTab = QtGui.QWidget()
        self.capturaTab.setObjectName("capturaTab")
        self.groupBox_10 = QtGui.QGroupBox(self.capturaTab)
        self.groupBox_10.setGeometry(QtCore.QRect(10, 80, 511, 161))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_10.setFont(font)
        self.groupBox_10.setObjectName("groupBox_10")
        self.textBrowser_10 = QtGui.QTextBrowser(self.groupBox_10)
        self.textBrowser_10.setGeometry(QtCore.QRect(10, 20, 491, 131))
        self.textBrowser_10.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_10.setObjectName("textBrowser_10")
        self.groupBox_11 = QtGui.QGroupBox(self.capturaTab)
        self.groupBox_11.setGeometry(QtCore.QRect(10, 410, 511, 151))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_11.setFont(font)
        self.groupBox_11.setObjectName("groupBox_11")
        self.textBrowser_11 = QtGui.QTextBrowser(self.groupBox_11)
        self.textBrowser_11.setGeometry(QtCore.QRect(10, 20, 491, 121))
        self.textBrowser_11.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_11.setObjectName("textBrowser_11")
        self.groupBox_12 = QtGui.QGroupBox(self.capturaTab)
        self.groupBox_12.setGeometry(QtCore.QRect(10, 250, 511, 151))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_12.setFont(font)
        self.groupBox_12.setObjectName("groupBox_12")
        self.textBrowser_12 = QtGui.QTextBrowser(self.groupBox_12)
        self.textBrowser_12.setGeometry(QtCore.QRect(10, 20, 491, 121))
        self.textBrowser_12.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_12.setObjectName("textBrowser_12")
        self.groupBox_13 = QtGui.QGroupBox(self.capturaTab)
        self.groupBox_13.setGeometry(QtCore.QRect(10, 10, 511, 61))
        font = QtGui.QFont()
        font.setPointSize(-1)
        font.setWeight(75)
        font.setBold(True)
        self.groupBox_13.setFont(font)
        self.groupBox_13.setObjectName("groupBox_13")
        self.textBrowser_13 = QtGui.QTextBrowser(self.groupBox_13)
        self.textBrowser_13.setGeometry(QtCore.QRect(10, 20, 491, 31))
        self.textBrowser_13.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser_13.setObjectName("textBrowser_13")
        self.tabWidget.addTab(self.capturaTab, "")
        self.okBt = QtGui.QPushButton(Dialog)
        self.okBt.setGeometry(QtCore.QRect(10, 612, 541, 31))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setWeight(75)
        font.setBold(True)
        self.okBt.setFont(font)
        self.okBt.setObjectName("okBt")

        self.retranslateUi(Dialog)
        self.tabWidget.setCurrentIndex(3)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("Dialog", "Geral", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">O objetivo do exercício embaralhar as configurações do osciloscópio e propor ao aluno que acerte os parâmetros para fazer a leitura.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma\'; font-size:12pt;\">Conecte com um cabo BNC-BNC o CH1 do gerador de sinais ao CH1 do osciloscópio. Siga as orientações na tela.</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_14.setTitle(QtGui.QApplication.translate("Dialog", "Fontes de problema", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_14.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Caso não esteja conseguindo de forma alguma encontrar o sinal na tela, verifique: se o CH1 do gerador está ligado; se o CH1 do osciloscópio está habilitado; se o cabo ligando os equipamentos está em bom estado; </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.exerciciosTab), QtGui.QApplication.translate("Dialog", "Exercícios", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("Dialog", "Geral", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_2.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Rotina para aquisição do sinal no plano de frequências. Esta rotina captura a leitura do osciloscópio, calcula a transformada rápida de Fourier</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> (FFT) e apresenta o resultado na tela. </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_3.setTitle(QtGui.QApplication.translate("Dialog", "Opções de input", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_3.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma\'; font-size:12pt;\">É possivel selecionar entre aquisição do canal 1 ou 2 do osciloscópio. </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_5.setTitle(QtGui.QApplication.translate("Dialog", "Opções de output", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_5.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Serão salvos na pasta selecionada pelo usuário e com o nome selecionado pelo usuário os seguintes arquivos:</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">‘_dados.csv’ – arquivo de texto contendo as intensidades de cada frequência;</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">‘_grafico.png’ – arquivo de imagem com o mesmo gráfico apresentado na tela do PyLab;</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_4.setTitle(QtGui.QApplication.translate("Dialog", "Fontes de problema", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_4.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">A escala de tempo no osciloscópio afeta significativamente o resultado do cálculo do FFT. Caso o resultado não seja o esperado verifique outras escalas.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Os arquivos serão sobrescritos se estiverem com o mesmo nome.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Aquisições não salvas serão perdidas definitivamente.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.fftTab), QtGui.QApplication.translate("Dialog", "FFT", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_6.setTitle(QtGui.QApplication.translate("Dialog", "Opções de output", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_6.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Serão salvos na pasta selecionada pelo usuário e com o nome selecionado pelo usuário os seguintes arquivos:</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">‘_dados.csv’ – arquivo de texto contendo as tensões de pico-a-pico dos dois canais do osciloscópio, a fase entre os sinais e a transmitância em db de cada frequência amostrada;</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">‘_grafico.png’ – arquivo de imagem com o mesmo gráfico apresentado na tela do PyLab;</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_7.setTitle(QtGui.QApplication.translate("Dialog", "Opções de input", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_7.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">É possível selecionar a frequência inicial, final e numero de pontos amostrais. A tensão do gerador não é alterada pelo programa e deve ser configurada pelo operador.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_8.setTitle(QtGui.QApplication.translate("Dialog", "Geral", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_8.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif,serif\'; font-size:12pt;\">Rotina para análise do circuito em função da frequência aplicada. Esta rotina aplica diferentes frequências de sinal de tensão senoidal e captura a leitura do osciloscópio nos canais 1 e 2 (respectivamente entrada e saída do circuito), calcula a transmitância e a diferença de fase entre os sinais capturados e apresenta na tela o “diagrama de Bode” (transmitância em db em função da frequência) e também a fase em função da frequência.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_9.setTitle(QtGui.QApplication.translate("Dialog", "Fontes de problema", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_9.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">O PyLab considera  o canal 1 do osciloscópio como entrada do circuito e o canal 2 como saída. A inversão destes canais na montagem pode atrapalhar a aquisição.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">O usuário deve utilizar o canal 1 do gerador de sinais. O usuário deve também selecionar a tensão apropriada e habilitar a saída do gerador manualmente.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Os arquivos serão sobrescritos se estiverem com o mesmo nome.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Aquisições não salvas serão perdidas definitivamente.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.varreduraTab), QtGui.QApplication.translate("Dialog", "Varredura", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_10.setTitle(QtGui.QApplication.translate("Dialog", "Opções de input", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_10.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">É possível escolher entre ler ambos os canais ou apenas o canal 1 ou 2. Existe a possibilidade leitura por amostra ou médias predefinidas. O triger pode ser feito por algum dos canais, externamente ou usando os 60Hz da rede elétrica. O plot em tensão por tempo ou Ch1 X Ch2 é feito com base na ultima captura e pode ser alternado sem necessidade de uma nova captura.</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_11.setTitle(QtGui.QApplication.translate("Dialog", "Fontes de problema", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_11.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma\'; font-size:12pt;\">O PyLab apresentará o mesmo sinal mostrado na tela do osciloscópio porém com a escala maximizada na tela, por isso os gráficos da tela do osciloscópio e da tela do PyLab podem ser ligeiramente diferentes.</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma\'; font-size:12pt;\">O plot de Ch1 X Ch2 é feito matematicamente com a leitura dos dois canais, portanto a captura deve de ser feita com ambos os canais habilitados.</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma\'; font-size:12pt;\">O arquivo de saída com os pontos amostrais tem três colunas: Tempo, Tensão no Ch1 e Tensão no Ch2. Cabe ao usuário quando for posteriormente reconstruir o gráfico escolher apropriadamente os eixos. </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_12.setTitle(QtGui.QApplication.translate("Dialog", "Opções de output", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_12.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">Serão salvos na pasta selecionada pelo usuário e com o nome selecionado pelo usuário os seguintes arquivos:</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">‘_dados.csv’ – arquivo de texto contendo as tensões em função do tempo dos dois canais do osciloscópio;</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">‘_graficoVT.png’ – arquivo de imagem com o gráfico de tensão por tempo apresentado na tela do PyLab;</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">‘_graficoIV.png’ – arquivo de imagem com o gráfico de Ch1 X Ch2 apresentado na tela do PyLab;</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma,sans-serif\'; font-size:12pt;\">‘_graficoCh1-Ch2xCh2.png’ – arquivo de imagem com o gráfico de (Ch1 – Ch2) X Ch2 apresentado na tela do PyLab;</span><span style=\" font-family:\'Tahoma\'; font-size:12pt;\"> </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_13.setTitle(QtGui.QApplication.translate("Dialog", "Geral", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_13.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Tahoma\'; font-size:12pt;\">Rotina para captura do sinal lido pelo osciloscópio. </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.capturaTab), QtGui.QApplication.translate("Dialog", "Captura", None, QtGui.QApplication.UnicodeUTF8))
        self.okBt.setText(QtGui.QApplication.translate("Dialog", "Ok", None, QtGui.QApplication.UnicodeUTF8))

import iconesDiag_rc
