# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainDialogV6.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import files_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1000, 720)
        MainWindow.setMinimumSize(QSize(1000, 720))
        palette = QPalette()
        brush = QBrush(QColor(255, 255, 255, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(0, 0, 0, 0))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        brush2 = QBrush(QColor(66, 73, 90, 255))
        brush2.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Light, brush2)
        brush3 = QBrush(QColor(55, 61, 75, 255))
        brush3.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Midlight, brush3)
        brush4 = QBrush(QColor(22, 24, 30, 255))
        brush4.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Dark, brush4)
        brush5 = QBrush(QColor(29, 32, 40, 255))
        brush5.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Mid, brush5)
        brush6 = QBrush(QColor(210, 210, 210, 255))
        brush6.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Text, brush6)
        palette.setBrush(QPalette.Active, QPalette.BrightText, brush)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush1)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
        brush7 = QBrush(QColor(0, 0, 0, 255))
        brush7.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Shadow, brush7)
        brush8 = QBrush(QColor(85, 170, 255, 255))
        brush8.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Highlight, brush8)
        palette.setBrush(QPalette.Active, QPalette.Link, brush8)
        brush9 = QBrush(QColor(255, 0, 127, 255))
        brush9.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.LinkVisited, brush9)
        palette.setBrush(QPalette.Active, QPalette.AlternateBase, brush4)
        brush10 = QBrush(QColor(44, 49, 60, 255))
        brush10.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.ToolTipBase, brush10)
        palette.setBrush(QPalette.Active, QPalette.ToolTipText, brush6)
        brush11 = QBrush(QColor(210, 210, 210, 128))
        brush11.setStyle(Qt.NoBrush)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Active, QPalette.PlaceholderText, brush11)
#endif
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Light, brush2)
        palette.setBrush(QPalette.Inactive, QPalette.Midlight, brush3)
        palette.setBrush(QPalette.Inactive, QPalette.Dark, brush4)
        palette.setBrush(QPalette.Inactive, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush6)
        palette.setBrush(QPalette.Inactive, QPalette.BrightText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Shadow, brush7)
        palette.setBrush(QPalette.Inactive, QPalette.Highlight, brush8)
        palette.setBrush(QPalette.Inactive, QPalette.Link, brush8)
        palette.setBrush(QPalette.Inactive, QPalette.LinkVisited, brush9)
        palette.setBrush(QPalette.Inactive, QPalette.AlternateBase, brush4)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipBase, brush10)
        palette.setBrush(QPalette.Inactive, QPalette.ToolTipText, brush6)
        brush12 = QBrush(QColor(210, 210, 210, 128))
        brush12.setStyle(Qt.NoBrush)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Inactive, QPalette.PlaceholderText, brush12)
#endif
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Light, brush2)
        palette.setBrush(QPalette.Disabled, QPalette.Midlight, brush3)
        palette.setBrush(QPalette.Disabled, QPalette.Dark, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Mid, brush5)
        palette.setBrush(QPalette.Disabled, QPalette.Text, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.BrightText, brush)
        palette.setBrush(QPalette.Disabled, QPalette.ButtonText, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Shadow, brush7)
        brush13 = QBrush(QColor(51, 153, 255, 255))
        brush13.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Disabled, QPalette.Highlight, brush13)
        palette.setBrush(QPalette.Disabled, QPalette.Link, brush8)
        palette.setBrush(QPalette.Disabled, QPalette.LinkVisited, brush9)
        palette.setBrush(QPalette.Disabled, QPalette.AlternateBase, brush10)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipBase, brush10)
        palette.setBrush(QPalette.Disabled, QPalette.ToolTipText, brush6)
        brush14 = QBrush(QColor(210, 210, 210, 128))
        brush14.setStyle(Qt.NoBrush)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Disabled, QPalette.PlaceholderText, brush14)
#endif
        MainWindow.setPalette(palette)
        font = QFont()
        font.setFamily(u"Segoe UI")
        font.setPointSize(10)
        MainWindow.setFont(font)
        MainWindow.setStyleSheet(u"QMainWindow {background: transparent; }\n"
"QToolTip {\n"
"	color: #ffffff;\n"
"	background-color: rgba(27, 29, 35, 160);\n"
"	border: 1px solid rgb(40, 40, 40);\n"
"	border-radius: 2px;\n"
"}")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setStyleSheet(u"background: transparent;\n"
"color: rgb(210, 210, 210);")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(10, 10, 10, 10)
        self.frame_main = QFrame(self.centralwidget)
        self.frame_main.setObjectName(u"frame_main")
        self.frame_main.setStyleSheet(u"/* LINE EDIT */\n"
"QLineEdit {\n"
"	background-color: rgb(27, 29, 35);\n"
"	border-radius: 5px;\n"
"	border: 2px solid rgb(27, 29, 35);\n"
"	padding-left: 10px;\n"
"}\n"
"QLineEdit:hover {\n"
"	border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"QLineEdit:focus {\n"
"	border: 2px solid rgb(91, 101, 124);\n"
"}\n"
"\n"
"/* SCROLL BARS */\n"
"QScrollBar:horizontal {\n"
"    border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    height: 14px;\n"
"    margin: 0px 21px 0 21px;\n"
"	border-radius: 0px;\n"
"}\n"
"QScrollBar::handle:horizontal {\n"
"    background: rgb(85, 170, 255);\n"
"    min-width: 25px;\n"
"	border-radius: 7px\n"
"}\n"
"QScrollBar::add-line:horizontal {\n"
"    border: none;\n"
"    background: rgb(55, 63, 77);\n"
"    width: 20px;\n"
"	border-top-right-radius: 7px;\n"
"    border-bottom-right-radius: 7px;\n"
"    subcontrol-position: right;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"QScrollBar::sub-line:horizontal {\n"
"    border: none;\n"
"    background: rgb(55, 63, 77);\n"
"    width: 20px;\n"
""
                        "	border-top-left-radius: 7px;\n"
"    border-bottom-left-radius: 7px;\n"
"    subcontrol-position: left;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"QScrollBar::up-arrow:horizontal, QScrollBar::down-arrow:horizontal\n"
"{\n"
"     background: none;\n"
"}\n"
"QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal\n"
"{\n"
"     background: none;\n"
"}\n"
" QScrollBar:vertical {\n"
"	border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    width: 14px;\n"
"    margin: 21px 0 21px 0;\n"
"	border-radius: 0px;\n"
" }\n"
" QScrollBar::handle:vertical {	\n"
"	background: rgb(85, 170, 255);\n"
"    min-height: 25px;\n"
"	border-radius: 7px\n"
" }\n"
" QScrollBar::add-line:vertical {\n"
"     border: none;\n"
"    background: rgb(55, 63, 77);\n"
"     height: 20px;\n"
"	border-bottom-left-radius: 7px;\n"
"    border-bottom-right-radius: 7px;\n"
"     subcontrol-position: bottom;\n"
"     subcontrol-origin: margin;\n"
" }\n"
" QScrollBar::sub-line:vertical {\n"
"	border: none;\n"
"    background: rgb(55, 63"
                        ", 77);\n"
"     height: 20px;\n"
"	border-top-left-radius: 7px;\n"
"    border-top-right-radius: 7px;\n"
"     subcontrol-position: top;\n"
"     subcontrol-origin: margin;\n"
" }\n"
" QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {\n"
"     background: none;\n"
" }\n"
"\n"
" QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {\n"
"     background: none;\n"
" }\n"
"\n"
"/* CHECKBOX */\n"
"QCheckBox::indicator {\n"
"    border: 3px solid rgb(52, 59, 72);\n"
"	width: 15px;\n"
"	height: 15px;\n"
"	border-radius: 10px;\n"
"    background: rgb(44, 49, 60);\n"
"}\n"
"QCheckBox::indicator:hover {\n"
"    border: 3px solid rgb(58, 66, 81);\n"
"}\n"
"QCheckBox::indicator:checked {\n"
"    background: 3px solid rgb(52, 59, 72);\n"
"	border: 3px solid rgb(52, 59, 72);	\n"
"	background-image: url(:/16x16/icons/16x16/cil-check-alt.png);\n"
"}\n"
"\n"
"/* RADIO BUTTON */\n"
"QRadioButton::indicator {\n"
"    border: 3px solid rgb(52, 59, 72);\n"
"	width: 15px;\n"
"	height: 15px;\n"
"	border-radius"
                        ": 10px;\n"
"    background: rgb(44, 49, 60);\n"
"}\n"
"QRadioButton::indicator:hover {\n"
"    border: 3px solid rgb(58, 66, 81);\n"
"}\n"
"QRadioButton::indicator:checked {\n"
"    background: 3px solid rgb(94, 106, 130);\n"
"	border: 3px solid rgb(52, 59, 72);	\n"
"}\n"
"\n"
"/* COMBOBOX */\n"
"QComboBox{\n"
"	background-color: rgb(27, 29, 35);\n"
"	border-radius: 5px;\n"
"	border: 2px solid rgb(27, 29, 35);\n"
"	padding: 5px;\n"
"	padding-left: 10px;\n"
"}\n"
"QComboBox:hover{\n"
"	border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"QComboBox::drop-down {\n"
"	subcontrol-origin: padding;\n"
"	subcontrol-position: top right;\n"
"	width: 25px; \n"
"	border-left-width: 3px;\n"
"	border-left-color: rgba(39, 44, 54, 150);\n"
"	border-left-style: solid;\n"
"	border-top-right-radius: 3px;\n"
"	border-bottom-right-radius: 3px;	\n"
"	background-image: url(:/16x16/icons/16x16/cil-arrow-bottom.png);\n"
"	background-position: center;\n"
"	background-repeat: no-reperat;\n"
" }\n"
"QComboBox QAbstractItemView {\n"
"	color: rgb("
                        "85, 170, 255);	\n"
"	background-color: rgb(27, 29, 35);\n"
"	padding: 10px;\n"
"	selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"\n"
"/* SLIDERS */\n"
"QSlider::groove:horizontal {\n"
"    border-radius: 9px;\n"
"    height: 18px;\n"
"	margin: 0px;\n"
"	background-color: rgb(52, 59, 72);\n"
"}\n"
"QSlider::groove:horizontal:hover {\n"
"	background-color: rgb(55, 62, 76);\n"
"}\n"
"QSlider::handle:horizontal {\n"
"    background-color: rgb(85, 170, 255);\n"
"    border: none;\n"
"    height: 18px;\n"
"    width: 18px;\n"
"    margin: 0px;\n"
"	border-radius: 9px;\n"
"}\n"
"QSlider::handle:horizontal:hover {\n"
"    background-color: rgb(105, 180, 255);\n"
"}\n"
"QSlider::handle:horizontal:pressed {\n"
"    background-color: rgb(65, 130, 195);\n"
"}\n"
"\n"
"QSlider::groove:vertical {\n"
"    border-radius: 9px;\n"
"    width: 18px;\n"
"    margin: 0px;\n"
"	background-color: rgb(52, 59, 72);\n"
"}\n"
"QSlider::groove:vertical:hover {\n"
"	background-color: rgb(55, 62, 76);\n"
"}\n"
"QSlider::handle:verti"
                        "cal {\n"
"    background-color: rgb(85, 170, 255);\n"
"	border: none;\n"
"    height: 18px;\n"
"    width: 18px;\n"
"    margin: 0px;\n"
"	border-radius: 9px;\n"
"}\n"
"QSlider::handle:vertical:hover {\n"
"    background-color: rgb(105, 180, 255);\n"
"}\n"
"QSlider::handle:vertical:pressed {\n"
"    background-color: rgb(65, 130, 195);\n"
"}\n"
"\n"
"")
        self.frame_main.setFrameShape(QFrame.NoFrame)
        self.frame_main.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame_main)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame_top = QFrame(self.frame_main)
        self.frame_top.setObjectName(u"frame_top")
        self.frame_top.setMinimumSize(QSize(0, 70))
        self.frame_top.setMaximumSize(QSize(16777215, 65))
        self.frame_top.setStyleSheet(u"background-color: transparent;")
        self.frame_top.setFrameShape(QFrame.NoFrame)
        self.frame_top.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.frame_top)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.frame_toggle = QFrame(self.frame_top)
        self.frame_toggle.setObjectName(u"frame_toggle")
        self.frame_toggle.setMaximumSize(QSize(70, 16777215))
        self.frame_toggle.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_toggle.setFrameShape(QFrame.NoFrame)
        self.frame_toggle.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.frame_toggle)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.btn_toggle_menu = QPushButton(self.frame_toggle)
        self.btn_toggle_menu.setObjectName(u"btn_toggle_menu")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_toggle_menu.sizePolicy().hasHeightForWidth())
        self.btn_toggle_menu.setSizePolicy(sizePolicy)
        self.btn_toggle_menu.setStyleSheet(u"QPushButton {\n"
"	background-image: url(:/24x24/icons/24x24/cil-menu.png);\n"
"	background-position: center;\n"
"	background-repeat: no-reperat;\n"
"	border: none;\n"
"	background-color: rgb(19, 20, 26);\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(41, 45, 56);\n"
"}\n"
"QPushButton:pressed {	\n"
"	background-color: rgb(85, 170, 255);\n"
"}")

        self.verticalLayout_3.addWidget(self.btn_toggle_menu)


        self.horizontalLayout_3.addWidget(self.frame_toggle)

        self.frame_top_right = QFrame(self.frame_top)
        self.frame_top_right.setObjectName(u"frame_top_right")
        self.frame_top_right.setStyleSheet(u"background: transparent;")
        self.frame_top_right.setFrameShape(QFrame.NoFrame)
        self.frame_top_right.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_top_right)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_top_btns = QFrame(self.frame_top_right)
        self.frame_top_btns.setObjectName(u"frame_top_btns")
        self.frame_top_btns.setMaximumSize(QSize(16777215, 42))
        self.frame_top_btns.setStyleSheet(u"background-color: rgb(19, 20, 26)")
        self.frame_top_btns.setFrameShape(QFrame.NoFrame)
        self.frame_top_btns.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.frame_top_btns)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.frame_label_top_btns = QFrame(self.frame_top_btns)
        self.frame_label_top_btns.setObjectName(u"frame_label_top_btns")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.frame_label_top_btns.sizePolicy().hasHeightForWidth())
        self.frame_label_top_btns.setSizePolicy(sizePolicy1)
        self.frame_label_top_btns.setFrameShape(QFrame.NoFrame)
        self.frame_label_top_btns.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_10 = QHBoxLayout(self.frame_label_top_btns)
        self.horizontalLayout_10.setSpacing(0)
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.horizontalLayout_10.setContentsMargins(5, 0, 10, 0)
        self.frame_icon_top_bar = QFrame(self.frame_label_top_btns)
        self.frame_icon_top_bar.setObjectName(u"frame_icon_top_bar")
        self.frame_icon_top_bar.setMaximumSize(QSize(50, 30))
        self.frame_icon_top_bar.setStyleSheet(u"background: transparent;\n"
"background-image: url(:/old/icons/logo_low.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;")
        self.frame_icon_top_bar.setFrameShape(QFrame.StyledPanel)
        self.frame_icon_top_bar.setFrameShadow(QFrame.Raised)

        self.horizontalLayout_10.addWidget(self.frame_icon_top_bar)

        self.label_title_bar_top = QLabel(self.frame_label_top_btns)
        self.label_title_bar_top.setObjectName(u"label_title_bar_top")
        font1 = QFont()
        font1.setFamily(u"Segoe UI")
        font1.setPointSize(10)
        font1.setBold(True)
        font1.setWeight(75)
        self.label_title_bar_top.setFont(font1)
        self.label_title_bar_top.setStyleSheet(u"background: transparent;\n"
"")

        self.horizontalLayout_10.addWidget(self.label_title_bar_top)


        self.horizontalLayout_4.addWidget(self.frame_label_top_btns)

        self.frame_btns_right = QFrame(self.frame_top_btns)
        self.frame_btns_right.setObjectName(u"frame_btns_right")
        sizePolicy1.setHeightForWidth(self.frame_btns_right.sizePolicy().hasHeightForWidth())
        self.frame_btns_right.setSizePolicy(sizePolicy1)
        self.frame_btns_right.setMaximumSize(QSize(120, 16777215))
        self.frame_btns_right.setFrameShape(QFrame.NoFrame)
        self.frame_btns_right.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.frame_btns_right)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.btn_minimize = QPushButton(self.frame_btns_right)
        self.btn_minimize.setObjectName(u"btn_minimize")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.btn_minimize.sizePolicy().hasHeightForWidth())
        self.btn_minimize.setSizePolicy(sizePolicy2)
        self.btn_minimize.setMinimumSize(QSize(40, 0))
        self.btn_minimize.setMaximumSize(QSize(40, 16777215))
        self.btn_minimize.setStyleSheet(u"QPushButton {	\n"
"	border: none;\n"
"	background-color: transparent;\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(52, 59, 72);\n"
"}\n"
"QPushButton:pressed {	\n"
"	background-color: rgb(85, 170, 255);\n"
"}")
        icon = QIcon()
        icon.addFile(u":/16x16/icons/16x16/cil-window-minimize.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_minimize.setIcon(icon)

        self.horizontalLayout_5.addWidget(self.btn_minimize)

        self.btn_maximize_restore = QPushButton(self.frame_btns_right)
        self.btn_maximize_restore.setObjectName(u"btn_maximize_restore")
        sizePolicy2.setHeightForWidth(self.btn_maximize_restore.sizePolicy().hasHeightForWidth())
        self.btn_maximize_restore.setSizePolicy(sizePolicy2)
        self.btn_maximize_restore.setMinimumSize(QSize(40, 0))
        self.btn_maximize_restore.setMaximumSize(QSize(40, 16777215))
        self.btn_maximize_restore.setStyleSheet(u"QPushButton {	\n"
"	border: none;\n"
"	background-color: transparent;\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(52, 59, 72);\n"
"}\n"
"QPushButton:pressed {	\n"
"	background-color: rgb(85, 170, 255);\n"
"}")
        icon1 = QIcon()
        icon1.addFile(u":/16x16/icons/16x16/cil-window-maximize.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_maximize_restore.setIcon(icon1)

        self.horizontalLayout_5.addWidget(self.btn_maximize_restore)

        self.btn_close = QPushButton(self.frame_btns_right)
        self.btn_close.setObjectName(u"btn_close")
        sizePolicy2.setHeightForWidth(self.btn_close.sizePolicy().hasHeightForWidth())
        self.btn_close.setSizePolicy(sizePolicy2)
        self.btn_close.setMinimumSize(QSize(40, 0))
        self.btn_close.setMaximumSize(QSize(40, 16777215))
        self.btn_close.setStyleSheet(u"QPushButton {	\n"
"	border: none;\n"
"	background-color: transparent;\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(52, 59, 72);\n"
"}\n"
"QPushButton:pressed {	\n"
"	background-color: rgb(85, 170, 255);\n"
"}")
        icon2 = QIcon()
        icon2.addFile(u":/16x16/icons/16x16/cil-x.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_close.setIcon(icon2)

        self.horizontalLayout_5.addWidget(self.btn_close)


        self.horizontalLayout_4.addWidget(self.frame_btns_right, 0, Qt.AlignRight)


        self.verticalLayout_2.addWidget(self.frame_top_btns)

        self.frame_top_info = QFrame(self.frame_top_right)
        self.frame_top_info.setObjectName(u"frame_top_info")
        self.frame_top_info.setMinimumSize(QSize(0, 30))
        self.frame_top_info.setMaximumSize(QSize(16777215, 65))
        self.frame_top_info.setStyleSheet(u"background-color: rgb(32, 35, 43);")
        self.frame_top_info.setFrameShape(QFrame.NoFrame)
        self.frame_top_info.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_8 = QHBoxLayout(self.frame_top_info)
        self.horizontalLayout_8.setSpacing(0)
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.horizontalLayout_8.setContentsMargins(10, 0, 10, 0)
        self.frame_53 = QFrame(self.frame_top_info)
        self.frame_53.setObjectName(u"frame_53")
        self.frame_53.setFrameShape(QFrame.StyledPanel)
        self.frame_53.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_21 = QHBoxLayout(self.frame_53)
        self.horizontalLayout_21.setSpacing(0)
        self.horizontalLayout_21.setObjectName(u"horizontalLayout_21")
        self.horizontalLayout_21.setContentsMargins(0, 0, 0, 0)
        self.scope_ind_light = QLabel(self.frame_53)
        self.scope_ind_light.setObjectName(u"scope_ind_light")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.scope_ind_light.sizePolicy().hasHeightForWidth())
        self.scope_ind_light.setSizePolicy(sizePolicy3)
        self.scope_ind_light.setMinimumSize(QSize(25, 25))
        self.scope_ind_light.setMaximumSize(QSize(25, 25))
        font2 = QFont()
        font2.setFamily(u"Segoe UI")
        self.scope_ind_light.setFont(font2)
        self.scope_ind_light.setStyleSheet(u"")
        self.scope_ind_light.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_21.addWidget(self.scope_ind_light)

        self.scope_ind_label = QLabel(self.frame_53)
        self.scope_ind_label.setObjectName(u"scope_ind_label")
        sizePolicy3.setHeightForWidth(self.scope_ind_label.sizePolicy().hasHeightForWidth())
        self.scope_ind_label.setSizePolicy(sizePolicy3)
        self.scope_ind_label.setMinimumSize(QSize(150, 0))
        self.scope_ind_label.setMaximumSize(QSize(200, 16777215))
        self.scope_ind_label.setFont(font)
        self.scope_ind_label.setStyleSheet(u"color: rgb(210, 210, 210)")

        self.horizontalLayout_21.addWidget(self.scope_ind_label)

        self.gen_ind_light = QLabel(self.frame_53)
        self.gen_ind_light.setObjectName(u"gen_ind_light")
        sizePolicy3.setHeightForWidth(self.gen_ind_light.sizePolicy().hasHeightForWidth())
        self.gen_ind_light.setSizePolicy(sizePolicy3)
        self.gen_ind_light.setMinimumSize(QSize(25, 25))
        self.gen_ind_light.setMaximumSize(QSize(25, 25))
        self.gen_ind_light.setStyleSheet(u"")

        self.horizontalLayout_21.addWidget(self.gen_ind_light)

        self.gen_ind_label = QLabel(self.frame_53)
        self.gen_ind_label.setObjectName(u"gen_ind_label")
        sizePolicy3.setHeightForWidth(self.gen_ind_label.sizePolicy().hasHeightForWidth())
        self.gen_ind_label.setSizePolicy(sizePolicy3)
        self.gen_ind_label.setMinimumSize(QSize(150, 0))
        self.gen_ind_label.setMaximumSize(QSize(200, 16777215))
        self.gen_ind_label.setFont(font)
        self.gen_ind_label.setStyleSheet(u"color: rgb(210, 210, 210)")

        self.horizontalLayout_21.addWidget(self.gen_ind_label)


        self.horizontalLayout_8.addWidget(self.frame_53)

        self.label_top_info_2 = QLabel(self.frame_top_info)
        self.label_top_info_2.setObjectName(u"label_top_info_2")
        sizePolicy1.setHeightForWidth(self.label_top_info_2.sizePolicy().hasHeightForWidth())
        self.label_top_info_2.setSizePolicy(sizePolicy1)
        self.label_top_info_2.setMinimumSize(QSize(0, 0))
        self.label_top_info_2.setMaximumSize(QSize(16777215, 16777215))
        font3 = QFont()
        font3.setFamily(u"Segoe UI")
        font3.setBold(True)
        font3.setWeight(75)
        self.label_top_info_2.setFont(font3)
        self.label_top_info_2.setStyleSheet(u"color: rgb(98, 103, 111);")
        self.label_top_info_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_8.addWidget(self.label_top_info_2)


        self.verticalLayout_2.addWidget(self.frame_top_info)


        self.horizontalLayout_3.addWidget(self.frame_top_right)


        self.verticalLayout.addWidget(self.frame_top)

        self.frame_center = QFrame(self.frame_main)
        self.frame_center.setObjectName(u"frame_center")
        sizePolicy.setHeightForWidth(self.frame_center.sizePolicy().hasHeightForWidth())
        self.frame_center.setSizePolicy(sizePolicy)
        self.frame_center.setStyleSheet(u"background-color: rgb(40, 44, 52);")
        self.frame_center.setFrameShape(QFrame.NoFrame)
        self.frame_center.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame_center)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_left_menu = QFrame(self.frame_center)
        self.frame_left_menu.setObjectName(u"frame_left_menu")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.frame_left_menu.sizePolicy().hasHeightForWidth())
        self.frame_left_menu.setSizePolicy(sizePolicy4)
        self.frame_left_menu.setMinimumSize(QSize(70, 0))
        self.frame_left_menu.setMaximumSize(QSize(70, 16777215))
        self.frame_left_menu.setLayoutDirection(Qt.LeftToRight)
        self.frame_left_menu.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_left_menu.setFrameShape(QFrame.NoFrame)
        self.frame_left_menu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.frame_left_menu)
        self.verticalLayout_5.setSpacing(1)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.frame_menus = QFrame(self.frame_left_menu)
        self.frame_menus.setObjectName(u"frame_menus")
        self.frame_menus.setFrameShape(QFrame.NoFrame)
        self.frame_menus.setFrameShadow(QFrame.Raised)
        self.layout_menus = QVBoxLayout(self.frame_menus)
        self.layout_menus.setSpacing(0)
        self.layout_menus.setObjectName(u"layout_menus")
        self.layout_menus.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_5.addWidget(self.frame_menus, 0, Qt.AlignTop)

        self.frame_extra_menus = QFrame(self.frame_left_menu)
        self.frame_extra_menus.setObjectName(u"frame_extra_menus")
        sizePolicy4.setHeightForWidth(self.frame_extra_menus.sizePolicy().hasHeightForWidth())
        self.frame_extra_menus.setSizePolicy(sizePolicy4)
        self.frame_extra_menus.setFrameShape(QFrame.NoFrame)
        self.frame_extra_menus.setFrameShadow(QFrame.Raised)
        self.layout_menu_bottom = QVBoxLayout(self.frame_extra_menus)
        self.layout_menu_bottom.setSpacing(10)
        self.layout_menu_bottom.setObjectName(u"layout_menu_bottom")
        self.layout_menu_bottom.setContentsMargins(0, 0, 0, 25)
        self.label_user_icon = QLabel(self.frame_extra_menus)
        self.label_user_icon.setObjectName(u"label_user_icon")
        sizePolicy5 = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.label_user_icon.sizePolicy().hasHeightForWidth())
        self.label_user_icon.setSizePolicy(sizePolicy5)
        self.label_user_icon.setMinimumSize(QSize(60, 60))
        self.label_user_icon.setMaximumSize(QSize(60, 60))
        font4 = QFont()
        font4.setFamily(u"Segoe UI")
        font4.setPointSize(12)
        self.label_user_icon.setFont(font4)
        self.label_user_icon.setStyleSheet(u"QLabel {\n"
"	border-radius: 30px;\n"
"	background-color: rgb(44, 49, 60);\n"
"	border: 5px solid rgb(39, 44, 54);\n"
"	background-position: center;\n"
"	background-repeat: no-repeat;\n"
"}")
        self.label_user_icon.setAlignment(Qt.AlignCenter)

        self.layout_menu_bottom.addWidget(self.label_user_icon, 0, Qt.AlignHCenter)


        self.verticalLayout_5.addWidget(self.frame_extra_menus, 0, Qt.AlignBottom)


        self.horizontalLayout_2.addWidget(self.frame_left_menu)

        self.frame_content_right = QFrame(self.frame_center)
        self.frame_content_right.setObjectName(u"frame_content_right")
        self.frame_content_right.setStyleSheet(u"	background-color: rgb(71, 78, 97);")
        self.frame_content_right.setFrameShape(QFrame.NoFrame)
        self.frame_content_right.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frame_content_right)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.frame_content = QFrame(self.frame_content_right)
        self.frame_content.setObjectName(u"frame_content")
        self.frame_content.setFrameShape(QFrame.NoFrame)
        self.frame_content.setFrameShadow(QFrame.Raised)
        self.verticalLayout_9 = QVBoxLayout(self.frame_content)
        self.verticalLayout_9.setSpacing(0)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.verticalLayout_9.setContentsMargins(5, 5, 5, 5)
        self.stackedWidget = QStackedWidget(self.frame_content)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.stackedWidget.setStyleSheet(u"background: transparent;")
        self.page_exercises = QWidget()
        self.page_exercises.setObjectName(u"page_exercises")
        self.verticalLayout_10 = QVBoxLayout(self.page_exercises)
        self.verticalLayout_10.setSpacing(0)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.verticalLayout_10.setContentsMargins(0, 0, 0, 0)
        self.frame_86 = QFrame(self.page_exercises)
        self.frame_86.setObjectName(u"frame_86")
        self.frame_86.setFrameShape(QFrame.StyledPanel)
        self.frame_86.setFrameShadow(QFrame.Raised)
        self.gridLayout_10 = QGridLayout(self.frame_86)
        self.gridLayout_10.setSpacing(5)
        self.gridLayout_10.setObjectName(u"gridLayout_10")
        self.gridLayout_10.setContentsMargins(0, 0, 0, 0)
        self.frame_90 = QFrame(self.frame_86)
        self.frame_90.setObjectName(u"frame_90")
        self.frame_90.setMinimumSize(QSize(0, 100))
        self.frame_90.setMaximumSize(QSize(500, 5555555))
        self.frame_90.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_90.setFrameShape(QFrame.StyledPanel)
        self.frame_90.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_30 = QHBoxLayout(self.frame_90)
        self.horizontalLayout_30.setSpacing(5)
        self.horizontalLayout_30.setObjectName(u"horizontalLayout_30")
        self.horizontalLayout_30.setContentsMargins(5, 5, 5, 5)
        self.embGerBt = QToolButton(self.frame_90)
        self.embGerBt.setObjectName(u"embGerBt")
        sizePolicy4.setHeightForWidth(self.embGerBt.sizePolicy().hasHeightForWidth())
        self.embGerBt.setSizePolicy(sizePolicy4)
        self.embGerBt.setMinimumSize(QSize(0, 0))
        font5 = QFont()
        font5.setFamily(u"Segoe UI")
        font5.setPointSize(14)
        self.embGerBt.setFont(font5)
        self.embGerBt.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}")

        self.horizontalLayout_30.addWidget(self.embGerBt)

        self.embOscBt = QToolButton(self.frame_90)
        self.embOscBt.setObjectName(u"embOscBt")
        sizePolicy4.setHeightForWidth(self.embOscBt.sizePolicy().hasHeightForWidth())
        self.embOscBt.setSizePolicy(sizePolicy4)
        self.embOscBt.setMinimumSize(QSize(0, 0))
        self.embOscBt.setFont(font5)
        self.embOscBt.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}")

        self.horizontalLayout_30.addWidget(self.embOscBt)

        self.respostaBt = QToolButton(self.frame_90)
        self.respostaBt.setObjectName(u"respostaBt")
        sizePolicy4.setHeightForWidth(self.respostaBt.sizePolicy().hasHeightForWidth())
        self.respostaBt.setSizePolicy(sizePolicy4)
        self.respostaBt.setMinimumSize(QSize(0, 0))
        self.respostaBt.setFont(font5)
        self.respostaBt.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")

        self.horizontalLayout_30.addWidget(self.respostaBt)


        self.gridLayout_10.addWidget(self.frame_90, 1, 0, 1, 2)

        self.frame_92 = QFrame(self.frame_86)
        self.frame_92.setObjectName(u"frame_92")
        self.frame_92.setMaximumSize(QSize(150, 16777215))
        self.frame_92.setStyleSheet(u"\n"
"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"\n"
"")
        self.frame_92.setFrameShape(QFrame.StyledPanel)
        self.frame_92.setFrameShadow(QFrame.Raised)
        self.verticalLayout_91 = QVBoxLayout(self.frame_92)
        self.verticalLayout_91.setObjectName(u"verticalLayout_91")
        self.verticalLayout_91.setContentsMargins(0, 0, 0, 0)
        self.frame_93 = QFrame(self.frame_92)
        self.frame_93.setObjectName(u"frame_93")
        self.frame_93.setMinimumSize(QSize(0, 35))
        self.frame_93.setMaximumSize(QSize(16777215, 35))
        self.frame_93.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_93.setFrameShape(QFrame.StyledPanel)
        self.frame_93.setFrameShadow(QFrame.Raised)
        self.verticalLayout_85 = QVBoxLayout(self.frame_93)
        self.verticalLayout_85.setObjectName(u"verticalLayout_85")
        self.verticalLayout_85.setContentsMargins(0, 0, 0, 0)
        self.label_39 = QLabel(self.frame_93)
        self.label_39.setObjectName(u"label_39")
        self.label_39.setFont(font1)
        self.label_39.setAlignment(Qt.AlignCenter)

        self.verticalLayout_85.addWidget(self.label_39)


        self.verticalLayout_91.addWidget(self.frame_93)

        self.frame_104 = QFrame(self.frame_92)
        self.frame_104.setObjectName(u"frame_104")
        self.frame_104.setFrameShape(QFrame.StyledPanel)
        self.frame_104.setFrameShadow(QFrame.Raised)
        self.verticalLayout_98 = QVBoxLayout(self.frame_104)
        self.verticalLayout_98.setSpacing(5)
        self.verticalLayout_98.setObjectName(u"verticalLayout_98")
        self.verticalLayout_98.setContentsMargins(4, 0, 4, 4)
        self.label_47 = QLabel(self.frame_104)
        self.label_47.setObjectName(u"label_47")
        self.label_47.setFont(font)

        self.verticalLayout_98.addWidget(self.label_47)

        self.respOnda = QLineEdit(self.frame_104)
        self.respOnda.setObjectName(u"respOnda")
        self.respOnda.setEnabled(False)
        self.respOnda.setMinimumSize(QSize(0, 30))
        self.respOnda.setFont(font)

        self.verticalLayout_98.addWidget(self.respOnda)

        self.label_48 = QLabel(self.frame_104)
        self.label_48.setObjectName(u"label_48")
        self.label_48.setFont(font)

        self.verticalLayout_98.addWidget(self.label_48)

        self.respFreq = QLineEdit(self.frame_104)
        self.respFreq.setObjectName(u"respFreq")
        self.respFreq.setEnabled(False)
        self.respFreq.setMinimumSize(QSize(0, 30))
        self.respFreq.setFont(font)

        self.verticalLayout_98.addWidget(self.respFreq)

        self.label_49 = QLabel(self.frame_104)
        self.label_49.setObjectName(u"label_49")
        self.label_49.setFont(font)

        self.verticalLayout_98.addWidget(self.label_49)

        self.respAmp = QLineEdit(self.frame_104)
        self.respAmp.setObjectName(u"respAmp")
        self.respAmp.setEnabled(False)
        self.respAmp.setMinimumSize(QSize(0, 30))
        self.respAmp.setFont(font)

        self.verticalLayout_98.addWidget(self.respAmp)


        self.verticalLayout_91.addWidget(self.frame_104)


        self.gridLayout_10.addWidget(self.frame_92, 2, 1, 1, 1)

        self.frame_88 = QFrame(self.frame_86)
        self.frame_88.setObjectName(u"frame_88")
        self.frame_88.setMaximumSize(QSize(500, 100))
        self.frame_88.setStyleSheet(u"")
        self.frame_88.setFrameShape(QFrame.StyledPanel)
        self.frame_88.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_29 = QHBoxLayout(self.frame_88)
        self.horizontalLayout_29.setObjectName(u"horizontalLayout_29")
        self.horizontalLayout_29.setContentsMargins(0, 0, 0, 0)
        self.frame_96 = QFrame(self.frame_88)
        self.frame_96.setObjectName(u"frame_96")
        self.frame_96.setStyleSheet(u"\n"
"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"\n"
"")
        self.frame_96.setFrameShape(QFrame.StyledPanel)
        self.frame_96.setFrameShadow(QFrame.Raised)
        self.verticalLayout_92 = QVBoxLayout(self.frame_96)
        self.verticalLayout_92.setObjectName(u"verticalLayout_92")
        self.verticalLayout_92.setContentsMargins(0, 0, 0, 0)
        self.frame_99 = QFrame(self.frame_96)
        self.frame_99.setObjectName(u"frame_99")
        self.frame_99.setMinimumSize(QSize(0, 35))
        self.frame_99.setMaximumSize(QSize(16777215, 35))
        self.frame_99.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_99.setFrameShape(QFrame.StyledPanel)
        self.frame_99.setFrameShadow(QFrame.Raised)
        self.verticalLayout_87 = QVBoxLayout(self.frame_99)
        self.verticalLayout_87.setObjectName(u"verticalLayout_87")
        self.verticalLayout_87.setContentsMargins(0, 0, 0, 0)
        self.label_41 = QLabel(self.frame_99)
        self.label_41.setObjectName(u"label_41")
        self.label_41.setFont(font1)
        self.label_41.setAlignment(Qt.AlignCenter)

        self.verticalLayout_87.addWidget(self.label_41)


        self.verticalLayout_92.addWidget(self.frame_99)

        self.frame_101 = QFrame(self.frame_96)
        self.frame_101.setObjectName(u"frame_101")
        self.frame_101.setFrameShape(QFrame.StyledPanel)
        self.frame_101.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_31 = QHBoxLayout(self.frame_101)
        self.horizontalLayout_31.setObjectName(u"horizontalLayout_31")
        self.facilBt = QRadioButton(self.frame_101)
        self.facilBt.setObjectName(u"facilBt")
        self.facilBt.setFont(font)

        self.horizontalLayout_31.addWidget(self.facilBt)

        self.medioBt = QRadioButton(self.frame_101)
        self.medioBt.setObjectName(u"medioBt")
        self.medioBt.setFont(font)

        self.horizontalLayout_31.addWidget(self.medioBt)

        self.dificilBt = QRadioButton(self.frame_101)
        self.dificilBt.setObjectName(u"dificilBt")
        self.dificilBt.setFont(font)

        self.horizontalLayout_31.addWidget(self.dificilBt)


        self.verticalLayout_92.addWidget(self.frame_101)


        self.horizontalLayout_29.addWidget(self.frame_96)

        self.frame_97 = QFrame(self.frame_88)
        self.frame_97.setObjectName(u"frame_97")
        self.frame_97.setStyleSheet(u"\n"
"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"\n"
"")
        self.frame_97.setFrameShape(QFrame.StyledPanel)
        self.frame_97.setFrameShadow(QFrame.Raised)
        self.verticalLayout_93 = QVBoxLayout(self.frame_97)
        self.verticalLayout_93.setObjectName(u"verticalLayout_93")
        self.verticalLayout_93.setContentsMargins(0, 0, 0, 0)
        self.frame_100 = QFrame(self.frame_97)
        self.frame_100.setObjectName(u"frame_100")
        self.frame_100.setMaximumSize(QSize(16777215, 35))
        self.frame_100.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_100.setFrameShape(QFrame.StyledPanel)
        self.frame_100.setFrameShadow(QFrame.Raised)
        self.verticalLayout_88 = QVBoxLayout(self.frame_100)
        self.verticalLayout_88.setObjectName(u"verticalLayout_88")
        self.verticalLayout_88.setContentsMargins(0, 0, 0, 0)
        self.label_45 = QLabel(self.frame_100)
        self.label_45.setObjectName(u"label_45")
        self.label_45.setMinimumSize(QSize(0, 35))
        self.label_45.setFont(font1)
        self.label_45.setAlignment(Qt.AlignCenter)

        self.verticalLayout_88.addWidget(self.label_45)


        self.verticalLayout_93.addWidget(self.frame_100)

        self.frame_102 = QFrame(self.frame_97)
        self.frame_102.setObjectName(u"frame_102")
        self.frame_102.setFrameShape(QFrame.StyledPanel)
        self.frame_102.setFrameShadow(QFrame.Raised)
        self.verticalLayout_97 = QVBoxLayout(self.frame_102)
        self.verticalLayout_97.setObjectName(u"verticalLayout_97")
        self.notaLabel = QLabel(self.frame_102)
        self.notaLabel.setObjectName(u"notaLabel")
        font6 = QFont()
        font6.setFamily(u"Segoe UI")
        font6.setPointSize(12)
        font6.setBold(True)
        font6.setWeight(75)
        self.notaLabel.setFont(font6)
        self.notaLabel.setAlignment(Qt.AlignCenter)

        self.verticalLayout_97.addWidget(self.notaLabel)


        self.verticalLayout_93.addWidget(self.frame_102)


        self.horizontalLayout_29.addWidget(self.frame_97)

        self.frame_98 = QFrame(self.frame_88)
        self.frame_98.setObjectName(u"frame_98")
        self.frame_98.setMaximumSize(QSize(100, 16777215))
        self.frame_98.setStyleSheet(u"\n"
"background-color: rgb(41, 45, 56);\n"
"")
        self.frame_98.setFrameShape(QFrame.StyledPanel)
        self.frame_98.setFrameShadow(QFrame.Raised)
        self.verticalLayout_94 = QVBoxLayout(self.frame_98)
        self.verticalLayout_94.setObjectName(u"verticalLayout_94")
        self.verticalLayout_94.setContentsMargins(4, 4, 4, 4)
        self.tutorialBt = QToolButton(self.frame_98)
        self.tutorialBt.setObjectName(u"tutorialBt")
        sizePolicy4.setHeightForWidth(self.tutorialBt.sizePolicy().hasHeightForWidth())
        self.tutorialBt.setSizePolicy(sizePolicy4)
        self.tutorialBt.setFont(font)
        self.tutorialBt.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}")
        icon3 = QIcon()
        icon3.addFile(u":/old/icons/tutorial_inv.png", QSize(), QIcon.Normal, QIcon.Off)
        self.tutorialBt.setIcon(icon3)
        self.tutorialBt.setIconSize(QSize(36, 36))
        self.tutorialBt.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.verticalLayout_94.addWidget(self.tutorialBt)


        self.horizontalLayout_29.addWidget(self.frame_98)


        self.gridLayout_10.addWidget(self.frame_88, 0, 0, 1, 2)

        self.frame_91 = QFrame(self.frame_86)
        self.frame_91.setObjectName(u"frame_91")
        self.frame_91.setMinimumSize(QSize(0, 250))
        self.frame_91.setMaximumSize(QSize(245, 16777215))
        self.frame_91.setStyleSheet(u"\n"
"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"\n"
"")
        self.frame_91.setFrameShape(QFrame.StyledPanel)
        self.frame_91.setFrameShadow(QFrame.Raised)
        self.verticalLayout_89 = QVBoxLayout(self.frame_91)
        self.verticalLayout_89.setObjectName(u"verticalLayout_89")
        self.verticalLayout_89.setContentsMargins(0, 0, 0, 0)
        self.frame_95 = QFrame(self.frame_91)
        self.frame_95.setObjectName(u"frame_95")
        self.frame_95.setMinimumSize(QSize(0, 35))
        self.frame_95.setMaximumSize(QSize(16777215, 35))
        self.frame_95.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_95.setFrameShape(QFrame.StyledPanel)
        self.frame_95.setFrameShadow(QFrame.Raised)
        self.verticalLayout_86 = QVBoxLayout(self.frame_95)
        self.verticalLayout_86.setObjectName(u"verticalLayout_86")
        self.verticalLayout_86.setContentsMargins(0, 0, 0, 0)
        self.label_40 = QLabel(self.frame_95)
        self.label_40.setObjectName(u"label_40")
        self.label_40.setFont(font1)
        self.label_40.setAlignment(Qt.AlignCenter)

        self.verticalLayout_86.addWidget(self.label_40)


        self.verticalLayout_89.addWidget(self.frame_95)

        self.frame_103 = QFrame(self.frame_91)
        self.frame_103.setObjectName(u"frame_103")
        self.frame_103.setFrameShape(QFrame.StyledPanel)
        self.frame_103.setFrameShadow(QFrame.Raised)
        self.gridLayout_11 = QGridLayout(self.frame_103)
        self.gridLayout_11.setSpacing(5)
        self.gridLayout_11.setObjectName(u"gridLayout_11")
        self.gridLayout_11.setContentsMargins(4, 0, 4, 4)
        self.userRespFreqExp = QComboBox(self.frame_103)
        self.userRespFreqExp.setObjectName(u"userRespFreqExp")
        self.userRespFreqExp.setFont(font)

        self.gridLayout_11.addWidget(self.userRespFreqExp, 4, 1, 1, 1)

        self.userRespAmpExp = QComboBox(self.frame_103)
        self.userRespAmpExp.setObjectName(u"userRespAmpExp")
        self.userRespAmpExp.setFont(font)

        self.gridLayout_11.addWidget(self.userRespAmpExp, 6, 1, 1, 1)

        self.userRespFreqNum = QLineEdit(self.frame_103)
        self.userRespFreqNum.setObjectName(u"userRespFreqNum")
        self.userRespFreqNum.setMinimumSize(QSize(0, 30))
        self.userRespFreqNum.setFont(font)

        self.gridLayout_11.addWidget(self.userRespFreqNum, 4, 0, 1, 1)

        self.userRespAmpNum = QLineEdit(self.frame_103)
        self.userRespAmpNum.setObjectName(u"userRespAmpNum")
        self.userRespAmpNum.setMinimumSize(QSize(0, 30))
        self.userRespAmpNum.setFont(font)

        self.gridLayout_11.addWidget(self.userRespAmpNum, 6, 0, 1, 1)

        self.userRespOnda = QComboBox(self.frame_103)
        self.userRespOnda.setObjectName(u"userRespOnda")
        self.userRespOnda.setFont(font)

        self.gridLayout_11.addWidget(self.userRespOnda, 1, 0, 1, 2)

        self.userRespFreqVerif = QToolButton(self.frame_103)
        self.userRespFreqVerif.setObjectName(u"userRespFreqVerif")
        self.userRespFreqVerif.setEnabled(False)
        self.userRespFreqVerif.setMinimumSize(QSize(22, 22))

        self.gridLayout_11.addWidget(self.userRespFreqVerif, 4, 2, 1, 1)

        self.userRespOndaVerif = QToolButton(self.frame_103)
        self.userRespOndaVerif.setObjectName(u"userRespOndaVerif")
        self.userRespOndaVerif.setEnabled(False)
        self.userRespOndaVerif.setMinimumSize(QSize(22, 22))

        self.gridLayout_11.addWidget(self.userRespOndaVerif, 1, 2, 1, 1)

        self.userRespAmpVerif = QToolButton(self.frame_103)
        self.userRespAmpVerif.setObjectName(u"userRespAmpVerif")
        self.userRespAmpVerif.setEnabled(False)
        self.userRespAmpVerif.setMinimumSize(QSize(22, 22))

        self.gridLayout_11.addWidget(self.userRespAmpVerif, 6, 2, 1, 1)

        self.label_10 = QLabel(self.frame_103)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setFont(font)

        self.gridLayout_11.addWidget(self.label_10, 5, 0, 1, 3)

        self.label_7 = QLabel(self.frame_103)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font)

        self.gridLayout_11.addWidget(self.label_7, 2, 0, 1, 3)

        self.label_6 = QLabel(self.frame_103)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setFont(font)

        self.gridLayout_11.addWidget(self.label_6, 0, 0, 1, 3)


        self.verticalLayout_89.addWidget(self.frame_103)


        self.gridLayout_10.addWidget(self.frame_91, 2, 0, 1, 1)

        self.frame_87 = QFrame(self.frame_86)
        self.frame_87.setObjectName(u"frame_87")
        self.frame_87.setStyleSheet(u"\n"
"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"\n"
"")
        self.frame_87.setFrameShape(QFrame.StyledPanel)
        self.frame_87.setFrameShadow(QFrame.Raised)
        self.verticalLayout_96 = QVBoxLayout(self.frame_87)
        self.verticalLayout_96.setSpacing(0)
        self.verticalLayout_96.setObjectName(u"verticalLayout_96")
        self.verticalLayout_96.setContentsMargins(0, 0, 0, 0)
        self.frame_105 = QFrame(self.frame_87)
        self.frame_105.setObjectName(u"frame_105")
        self.frame_105.setMinimumSize(QSize(0, 35))
        self.frame_105.setMaximumSize(QSize(16777215, 35))
        self.frame_105.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_105.setFrameShape(QFrame.StyledPanel)
        self.frame_105.setFrameShadow(QFrame.Raised)
        self.verticalLayout_95 = QVBoxLayout(self.frame_105)
        self.verticalLayout_95.setObjectName(u"verticalLayout_95")
        self.verticalLayout_95.setContentsMargins(0, 0, 0, 0)
        self.label_46 = QLabel(self.frame_105)
        self.label_46.setObjectName(u"label_46")
        self.label_46.setFont(font6)
        self.label_46.setAlignment(Qt.AlignCenter)

        self.verticalLayout_95.addWidget(self.label_46)


        self.verticalLayout_96.addWidget(self.frame_105)

        self.textBrowser_24 = QTextBrowser(self.frame_87)
        self.textBrowser_24.setObjectName(u"textBrowser_24")
        self.textBrowser_24.setFont(font4)

        self.verticalLayout_96.addWidget(self.textBrowser_24)


        self.gridLayout_10.addWidget(self.frame_87, 0, 2, 4, 1)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_10.addItem(self.verticalSpacer_2, 3, 0, 1, 2)


        self.verticalLayout_10.addWidget(self.frame_86)

        self.stackedWidget.addWidget(self.page_exercises)
        self.page_freq_resp = QWidget()
        self.page_freq_resp.setObjectName(u"page_freq_resp")
        self.verticalLayout_66 = QVBoxLayout(self.page_freq_resp)
        self.verticalLayout_66.setSpacing(0)
        self.verticalLayout_66.setObjectName(u"verticalLayout_66")
        self.verticalLayout_66.setContentsMargins(0, 0, 0, 0)
        self.frame_54 = QFrame(self.page_freq_resp)
        self.frame_54.setObjectName(u"frame_54")
        self.frame_54.setFrameShape(QFrame.StyledPanel)
        self.frame_54.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(self.frame_54)
        self.gridLayout.setSpacing(6)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.frame_58 = QFrame(self.frame_54)
        self.frame_58.setObjectName(u"frame_58")
        self.frame_58.setMinimumSize(QSize(250, 200))
        self.frame_58.setMaximumSize(QSize(250, 16777215))
        self.frame_58.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_58.setFrameShape(QFrame.StyledPanel)
        self.frame_58.setFrameShadow(QFrame.Raised)
        self.verticalLayout_67 = QVBoxLayout(self.frame_58)
        self.verticalLayout_67.setSpacing(0)
        self.verticalLayout_67.setObjectName(u"verticalLayout_67")
        self.verticalLayout_67.setContentsMargins(0, 0, 0, 0)
        self.frame_59 = QFrame(self.frame_58)
        self.frame_59.setObjectName(u"frame_59")
        self.frame_59.setMinimumSize(QSize(0, 35))
        self.frame_59.setMaximumSize(QSize(16777215, 35))
        self.frame_59.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_59.setFrameShape(QFrame.StyledPanel)
        self.frame_59.setFrameShadow(QFrame.Raised)
        self.verticalLayout_68 = QVBoxLayout(self.frame_59)
        self.verticalLayout_68.setSpacing(0)
        self.verticalLayout_68.setObjectName(u"verticalLayout_68")
        self.verticalLayout_68.setContentsMargins(0, 0, 0, 0)
        self.label_3 = QLabel(self.frame_59)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setFont(font1)
        self.label_3.setAlignment(Qt.AlignCenter)

        self.verticalLayout_68.addWidget(self.label_3)


        self.verticalLayout_67.addWidget(self.frame_59)

        self.frame_60 = QFrame(self.frame_58)
        self.frame_60.setObjectName(u"frame_60")
        self.frame_60.setFrameShape(QFrame.StyledPanel)
        self.frame_60.setFrameShadow(QFrame.Raised)
        self.verticalLayout_69 = QVBoxLayout(self.frame_60)
        self.verticalLayout_69.setSpacing(0)
        self.verticalLayout_69.setObjectName(u"verticalLayout_69")
        self.verticalLayout_69.setContentsMargins(5, 0, 5, 0)
        self.label_5 = QLabel(self.frame_60)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font)
        self.label_5.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.verticalLayout_69.addWidget(self.label_5)


        self.verticalLayout_67.addWidget(self.frame_60)

        self.frame_61 = QFrame(self.frame_58)
        self.frame_61.setObjectName(u"frame_61")
        self.frame_61.setFrameShape(QFrame.StyledPanel)
        self.frame_61.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_22 = QHBoxLayout(self.frame_61)
        self.horizontalLayout_22.setSpacing(9)
        self.horizontalLayout_22.setObjectName(u"horizontalLayout_22")
        self.horizontalLayout_22.setContentsMargins(5, 0, 5, 0)
        self.label_8 = QLabel(self.frame_61)
        self.label_8.setObjectName(u"label_8")
        sizePolicy3.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy3)
        self.label_8.setMaximumSize(QSize(70, 16777215))
        self.label_8.setFont(font)

        self.horizontalLayout_22.addWidget(self.label_8)

        self.freqInicialLine = QLineEdit(self.frame_61)
        self.freqInicialLine.setObjectName(u"freqInicialLine")
        self.freqInicialLine.setMinimumSize(QSize(70, 30))
        self.freqInicialLine.setMaximumSize(QSize(16777215, 16777215))
        self.freqInicialLine.setFont(font)

        self.horizontalLayout_22.addWidget(self.freqInicialLine)

        self.freqInicialBox = QComboBox(self.frame_61)
        self.freqInicialBox.setObjectName(u"freqInicialBox")
        sizePolicy6 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.freqInicialBox.sizePolicy().hasHeightForWidth())
        self.freqInicialBox.setSizePolicy(sizePolicy6)
        self.freqInicialBox.setMinimumSize(QSize(80, 0))
        self.freqInicialBox.setMaximumSize(QSize(16777215, 16777215))
        self.freqInicialBox.setFont(font)

        self.horizontalLayout_22.addWidget(self.freqInicialBox)


        self.verticalLayout_67.addWidget(self.frame_61)

        self.frame_62 = QFrame(self.frame_58)
        self.frame_62.setObjectName(u"frame_62")
        self.frame_62.setFrameShape(QFrame.StyledPanel)
        self.frame_62.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_23 = QHBoxLayout(self.frame_62)
        self.horizontalLayout_23.setSpacing(9)
        self.horizontalLayout_23.setObjectName(u"horizontalLayout_23")
        self.horizontalLayout_23.setContentsMargins(5, 0, 5, 0)
        self.label_35 = QLabel(self.frame_62)
        self.label_35.setObjectName(u"label_35")
        sizePolicy3.setHeightForWidth(self.label_35.sizePolicy().hasHeightForWidth())
        self.label_35.setSizePolicy(sizePolicy3)
        self.label_35.setMaximumSize(QSize(60, 16777215))
        self.label_35.setFont(font)

        self.horizontalLayout_23.addWidget(self.label_35)

        self.freqFinalLine = QLineEdit(self.frame_62)
        self.freqFinalLine.setObjectName(u"freqFinalLine")
        self.freqFinalLine.setMinimumSize(QSize(70, 30))
        self.freqFinalLine.setMaximumSize(QSize(16777215, 16777215))
        self.freqFinalLine.setFont(font)

        self.horizontalLayout_23.addWidget(self.freqFinalLine)

        self.freqFinalBox = QComboBox(self.frame_62)
        self.freqFinalBox.setObjectName(u"freqFinalBox")
        sizePolicy6.setHeightForWidth(self.freqFinalBox.sizePolicy().hasHeightForWidth())
        self.freqFinalBox.setSizePolicy(sizePolicy6)
        self.freqFinalBox.setMinimumSize(QSize(80, 0))
        self.freqFinalBox.setMaximumSize(QSize(16777215, 16777215))
        self.freqFinalBox.setFont(font)

        self.horizontalLayout_23.addWidget(self.freqFinalBox)


        self.verticalLayout_67.addWidget(self.frame_62)

        self.frame_63 = QFrame(self.frame_58)
        self.frame_63.setObjectName(u"frame_63")
        self.frame_63.setFrameShape(QFrame.StyledPanel)
        self.frame_63.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_24 = QHBoxLayout(self.frame_63)
        self.horizontalLayout_24.setSpacing(9)
        self.horizontalLayout_24.setObjectName(u"horizontalLayout_24")
        self.horizontalLayout_24.setContentsMargins(5, 0, 5, 0)
        self.label_36 = QLabel(self.frame_63)
        self.label_36.setObjectName(u"label_36")
        sizePolicy3.setHeightForWidth(self.label_36.sizePolicy().hasHeightForWidth())
        self.label_36.setSizePolicy(sizePolicy3)
        self.label_36.setMaximumSize(QSize(60, 16777215))
        self.label_36.setFont(font)

        self.horizontalLayout_24.addWidget(self.label_36)

        self.numeroPontos = QLineEdit(self.frame_63)
        self.numeroPontos.setObjectName(u"numeroPontos")
        self.numeroPontos.setMinimumSize(QSize(70, 30))
        self.numeroPontos.setMaximumSize(QSize(16777215, 16777215))
        self.numeroPontos.setFont(font)

        self.horizontalLayout_24.addWidget(self.numeroPontos)

        self.horizontalSpacer = QSpacerItem(120, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_24.addItem(self.horizontalSpacer)


        self.verticalLayout_67.addWidget(self.frame_63)


        self.gridLayout.addWidget(self.frame_58, 0, 0, 1, 1)

        self.frame_56 = QFrame(self.frame_54)
        self.frame_56.setObjectName(u"frame_56")
        self.frame_56.setMinimumSize(QSize(250, 120))
        self.frame_56.setMaximumSize(QSize(250, 16777215))
        self.frame_56.setStyleSheet(u"\n"
"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"\n"
"")
        self.frame_56.setFrameShape(QFrame.StyledPanel)
        self.frame_56.setFrameShadow(QFrame.Raised)
        self.verticalLayout_70 = QVBoxLayout(self.frame_56)
        self.verticalLayout_70.setObjectName(u"verticalLayout_70")
        self.verticalLayout_70.setContentsMargins(0, 0, 0, 4)
        self.frame_64 = QFrame(self.frame_56)
        self.frame_64.setObjectName(u"frame_64")
        self.frame_64.setMinimumSize(QSize(0, 35))
        self.frame_64.setMaximumSize(QSize(16777215, 35))
        self.frame_64.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_64.setFrameShape(QFrame.StyledPanel)
        self.frame_64.setFrameShadow(QFrame.Raised)
        self.verticalLayout_71 = QVBoxLayout(self.frame_64)
        self.verticalLayout_71.setObjectName(u"verticalLayout_71")
        self.verticalLayout_71.setContentsMargins(0, 0, 0, 0)
        self.label_37 = QLabel(self.frame_64)
        self.label_37.setObjectName(u"label_37")
        self.label_37.setFont(font1)
        self.label_37.setAlignment(Qt.AlignCenter)

        self.verticalLayout_71.addWidget(self.label_37)


        self.verticalLayout_70.addWidget(self.frame_64)

        self.frame_65 = QFrame(self.frame_56)
        self.frame_65.setObjectName(u"frame_65")
        self.frame_65.setMaximumSize(QSize(16777215, 16777215))
        self.frame_65.setFrameShape(QFrame.StyledPanel)
        self.frame_65.setFrameShadow(QFrame.Raised)
        self.verticalLayout_72 = QVBoxLayout(self.frame_65)
        self.verticalLayout_72.setSpacing(0)
        self.verticalLayout_72.setObjectName(u"verticalLayout_72")
        self.verticalLayout_72.setContentsMargins(5, 0, 5, 0)
        self.statusFreq = QLabel(self.frame_65)
        self.statusFreq.setObjectName(u"statusFreq")
        self.statusFreq.setFont(font)
        self.statusFreq.setAlignment(Qt.AlignCenter)

        self.verticalLayout_72.addWidget(self.statusFreq)


        self.verticalLayout_70.addWidget(self.frame_65)

        self.frame_66 = QFrame(self.frame_56)
        self.frame_66.setObjectName(u"frame_66")
        self.frame_66.setMaximumSize(QSize(16777215, 16777215))
        self.frame_66.setFrameShape(QFrame.StyledPanel)
        self.frame_66.setFrameShadow(QFrame.Raised)
        self.verticalLayout_73 = QVBoxLayout(self.frame_66)
        self.verticalLayout_73.setSpacing(0)
        self.verticalLayout_73.setObjectName(u"verticalLayout_73")
        self.verticalLayout_73.setContentsMargins(5, 0, 5, 0)
        self.progressBar = QProgressBar(self.frame_66)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setMinimumSize(QSize(0, 30))
        self.progressBar.setFont(font)
        self.progressBar.setStyleSheet(u"QProgressBar {\n"
"	background-color: rgba(98, 114, 164, 0.3);\n"
"	color: rgb(210, 210, 210);\n"
"	border-style: none;\n"
"	border-radius: 10px;\n"
"	text-align: center;\n"
"}\n"
"QProgressBar::chunk{\n"
"	border-radius: 10px;\n"
"	background-color: qlineargradient(spread:pad, x1:0, y1:0.511364, x2:1, y2:0.523, stop:0 rgba(254, 121, 199, 255), stop:1 rgba(170, 85, 255, 255));\n"
"}")
        self.progressBar.setValue(24)

        self.verticalLayout_73.addWidget(self.progressBar)


        self.verticalLayout_70.addWidget(self.frame_66)


        self.gridLayout.addWidget(self.frame_56, 2, 0, 1, 1)

        self.frame_57 = QFrame(self.frame_54)
        self.frame_57.setObjectName(u"frame_57")
        self.frame_57.setMinimumSize(QSize(250, 200))
        self.frame_57.setMaximumSize(QSize(250, 16777215))
        self.frame_57.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_57.setFrameShape(QFrame.StyledPanel)
        self.frame_57.setFrameShadow(QFrame.Raised)
        self.gridLayout_2 = QGridLayout(self.frame_57)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(6, 6, 6, 6)
        self.startBt_Freq = QToolButton(self.frame_57)
        self.startBt_Freq.setObjectName(u"startBt_Freq")
        sizePolicy4.setHeightForWidth(self.startBt_Freq.sizePolicy().hasHeightForWidth())
        self.startBt_Freq.setSizePolicy(sizePolicy4)
        self.startBt_Freq.setFont(font)
        self.startBt_Freq.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")
        icon4 = QIcon()
        icon4.addFile(u":/old/icons/start_inv.png", QSize(), QIcon.Normal, QIcon.Off)
        self.startBt_Freq.setIcon(icon4)
        self.startBt_Freq.setIconSize(QSize(70, 70))
        self.startBt_Freq.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.gridLayout_2.addWidget(self.startBt_Freq, 1, 1, 1, 1)

        self.stopBt_Freq = QToolButton(self.frame_57)
        self.stopBt_Freq.setObjectName(u"stopBt_Freq")
        sizePolicy4.setHeightForWidth(self.stopBt_Freq.sizePolicy().hasHeightForWidth())
        self.stopBt_Freq.setSizePolicy(sizePolicy4)
        self.stopBt_Freq.setFont(font)
        self.stopBt_Freq.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")
        icon5 = QIcon()
        icon5.addFile(u":/old/icons/stop_inv.png", QSize(), QIcon.Normal, QIcon.Off)
        self.stopBt_Freq.setIcon(icon5)
        self.stopBt_Freq.setIconSize(QSize(70, 70))
        self.stopBt_Freq.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.gridLayout_2.addWidget(self.stopBt_Freq, 1, 2, 1, 1)

        self.novoBt_Freq = QToolButton(self.frame_57)
        self.novoBt_Freq.setObjectName(u"novoBt_Freq")
        sizePolicy4.setHeightForWidth(self.novoBt_Freq.sizePolicy().hasHeightForWidth())
        self.novoBt_Freq.setSizePolicy(sizePolicy4)
        self.novoBt_Freq.setFont(font)
        self.novoBt_Freq.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")
        icon6 = QIcon()
        icon6.addFile(u":/old/icons/novo_inv.png", QSize(), QIcon.Normal, QIcon.Off)
        self.novoBt_Freq.setIcon(icon6)
        self.novoBt_Freq.setIconSize(QSize(70, 70))
        self.novoBt_Freq.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.gridLayout_2.addWidget(self.novoBt_Freq, 2, 2, 1, 1)

        self.salvarBt_Freq = QToolButton(self.frame_57)
        self.salvarBt_Freq.setObjectName(u"salvarBt_Freq")
        sizePolicy4.setHeightForWidth(self.salvarBt_Freq.sizePolicy().hasHeightForWidth())
        self.salvarBt_Freq.setSizePolicy(sizePolicy4)
        self.salvarBt_Freq.setFont(font)
        self.salvarBt_Freq.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")
        icon7 = QIcon()
        icon7.addFile(u":/old/icons/salvar_inv.png", QSize(), QIcon.Normal, QIcon.Off)
        self.salvarBt_Freq.setIcon(icon7)
        self.salvarBt_Freq.setIconSize(QSize(70, 70))
        self.salvarBt_Freq.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.gridLayout_2.addWidget(self.salvarBt_Freq, 2, 1, 1, 1)


        self.gridLayout.addWidget(self.frame_57, 1, 0, 1, 1)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer_3, 3, 0, 1, 1)

        self.frame_55 = QFrame(self.frame_54)
        self.frame_55.setObjectName(u"frame_55")
        self.frame_55.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_55.setFrameShape(QFrame.StyledPanel)
        self.frame_55.setFrameShadow(QFrame.Raised)
        self.verticalLayout_100 = QVBoxLayout(self.frame_55)
        self.verticalLayout_100.setSpacing(0)
        self.verticalLayout_100.setObjectName(u"verticalLayout_100")
        self.verticalLayout_100.setContentsMargins(0, 0, 0, 0)
        self.plot_area_freq = QVBoxLayout()
        self.plot_area_freq.setObjectName(u"plot_area_freq")

        self.verticalLayout_100.addLayout(self.plot_area_freq)


        self.gridLayout.addWidget(self.frame_55, 0, 1, 4, 1)


        self.verticalLayout_66.addWidget(self.frame_54)

        self.stackedWidget.addWidget(self.page_freq_resp)
        self.page_time_resp = QWidget()
        self.page_time_resp.setObjectName(u"page_time_resp")
        self.verticalLayout_81 = QVBoxLayout(self.page_time_resp)
        self.verticalLayout_81.setSpacing(0)
        self.verticalLayout_81.setObjectName(u"verticalLayout_81")
        self.verticalLayout_81.setContentsMargins(0, 0, 0, 0)
        self.frame_67 = QFrame(self.page_time_resp)
        self.frame_67.setObjectName(u"frame_67")
        self.frame_67.setFrameShape(QFrame.StyledPanel)
        self.frame_67.setFrameShadow(QFrame.Raised)
        self.gridLayout_3 = QGridLayout(self.frame_67)
        self.gridLayout_3.setSpacing(6)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.frame_68 = QFrame(self.frame_67)
        self.frame_68.setObjectName(u"frame_68")
        self.frame_68.setMinimumSize(QSize(200, 80))
        self.frame_68.setMaximumSize(QSize(250, 16777215))
        self.frame_68.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_68.setFrameShape(QFrame.StyledPanel)
        self.frame_68.setFrameShadow(QFrame.Raised)
        self.verticalLayout_74 = QVBoxLayout(self.frame_68)
        self.verticalLayout_74.setSpacing(0)
        self.verticalLayout_74.setObjectName(u"verticalLayout_74")
        self.verticalLayout_74.setContentsMargins(0, 0, 0, 0)
        self.frame_69 = QFrame(self.frame_68)
        self.frame_69.setObjectName(u"frame_69")
        self.frame_69.setMinimumSize(QSize(0, 35))
        self.frame_69.setMaximumSize(QSize(16777215, 35))
        self.frame_69.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_69.setFrameShape(QFrame.StyledPanel)
        self.frame_69.setFrameShadow(QFrame.Raised)
        self.verticalLayout_75 = QVBoxLayout(self.frame_69)
        self.verticalLayout_75.setSpacing(0)
        self.verticalLayout_75.setObjectName(u"verticalLayout_75")
        self.verticalLayout_75.setContentsMargins(0, 0, 0, 0)
        self.label_9 = QLabel(self.frame_69)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font1)
        self.label_9.setAlignment(Qt.AlignCenter)

        self.verticalLayout_75.addWidget(self.label_9)


        self.verticalLayout_74.addWidget(self.frame_69)

        self.frame_70 = QFrame(self.frame_68)
        self.frame_70.setObjectName(u"frame_70")
        self.frame_70.setFrameShape(QFrame.StyledPanel)
        self.frame_70.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_25 = QHBoxLayout(self.frame_70)
        self.horizontalLayout_25.setSpacing(0)
        self.horizontalLayout_25.setObjectName(u"horizontalLayout_25")
        self.horizontalLayout_25.setContentsMargins(5, 0, 5, 0)
        self.canalUmCheck = QCheckBox(self.frame_70)
        self.canalUmCheck.setObjectName(u"canalUmCheck")
        self.canalUmCheck.setFont(font)

        self.horizontalLayout_25.addWidget(self.canalUmCheck)

        self.canalDoisCheck = QCheckBox(self.frame_70)
        self.canalDoisCheck.setObjectName(u"canalDoisCheck")
        self.canalDoisCheck.setFont(font)

        self.horizontalLayout_25.addWidget(self.canalDoisCheck)


        self.verticalLayout_74.addWidget(self.frame_70)


        self.gridLayout_3.addWidget(self.frame_68, 0, 0, 1, 1)

        self.frame_77 = QFrame(self.frame_67)
        self.frame_77.setObjectName(u"frame_77")
        self.frame_77.setMinimumSize(QSize(0, 120))
        self.frame_77.setMaximumSize(QSize(250, 16777215))
        self.frame_77.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_77.setFrameShape(QFrame.StyledPanel)
        self.frame_77.setFrameShadow(QFrame.Raised)
        self.verticalLayout_83 = QVBoxLayout(self.frame_77)
        self.verticalLayout_83.setSpacing(0)
        self.verticalLayout_83.setObjectName(u"verticalLayout_83")
        self.verticalLayout_83.setContentsMargins(0, 0, 0, 0)
        self.frame_81 = QFrame(self.frame_77)
        self.frame_81.setObjectName(u"frame_81")
        self.frame_81.setMinimumSize(QSize(0, 35))
        self.frame_81.setMaximumSize(QSize(16777215, 35))
        self.frame_81.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_81.setFrameShape(QFrame.StyledPanel)
        self.frame_81.setFrameShadow(QFrame.Raised)
        self.verticalLayout_79 = QVBoxLayout(self.frame_81)
        self.verticalLayout_79.setSpacing(0)
        self.verticalLayout_79.setObjectName(u"verticalLayout_79")
        self.verticalLayout_79.setContentsMargins(0, 0, 0, 0)
        self.label_44 = QLabel(self.frame_81)
        self.label_44.setObjectName(u"label_44")
        self.label_44.setFont(font1)
        self.label_44.setAlignment(Qt.AlignCenter)

        self.verticalLayout_79.addWidget(self.label_44)


        self.verticalLayout_83.addWidget(self.frame_81)

        self.frame_84 = QFrame(self.frame_77)
        self.frame_84.setObjectName(u"frame_84")
        self.frame_84.setFrameShape(QFrame.StyledPanel)
        self.frame_84.setFrameShadow(QFrame.Raised)
        self.gridLayout_7 = QGridLayout(self.frame_84)
        self.gridLayout_7.setSpacing(0)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_7.setContentsMargins(5, 0, 5, 0)
        self.triggerExt = QRadioButton(self.frame_84)
        self.triggerExt.setObjectName(u"triggerExt")
        self.triggerExt.setFont(font)

        self.gridLayout_7.addWidget(self.triggerExt, 0, 1, 1, 1)

        self.triggerCh1 = QRadioButton(self.frame_84)
        self.triggerCh1.setObjectName(u"triggerCh1")
        self.triggerCh1.setFont(font)

        self.gridLayout_7.addWidget(self.triggerCh1, 0, 0, 1, 1)

        self.triggerCh2 = QRadioButton(self.frame_84)
        self.triggerCh2.setObjectName(u"triggerCh2")
        self.triggerCh2.setFont(font)

        self.gridLayout_7.addWidget(self.triggerCh2, 1, 0, 1, 1)

        self.triggerLinha = QRadioButton(self.frame_84)
        self.triggerLinha.setObjectName(u"triggerLinha")
        self.triggerLinha.setFont(font)

        self.gridLayout_7.addWidget(self.triggerLinha, 1, 1, 1, 1)


        self.verticalLayout_83.addWidget(self.frame_84)


        self.gridLayout_3.addWidget(self.frame_77, 3, 0, 1, 1)

        self.frame_75 = QFrame(self.frame_67)
        self.frame_75.setObjectName(u"frame_75")
        self.frame_75.setMinimumSize(QSize(0, 120))
        self.frame_75.setMaximumSize(QSize(250, 16777215))
        self.frame_75.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_75.setFrameShape(QFrame.StyledPanel)
        self.frame_75.setFrameShadow(QFrame.Raised)
        self.verticalLayout_82 = QVBoxLayout(self.frame_75)
        self.verticalLayout_82.setSpacing(0)
        self.verticalLayout_82.setObjectName(u"verticalLayout_82")
        self.verticalLayout_82.setContentsMargins(0, 0, 0, 0)
        self.frame_80 = QFrame(self.frame_75)
        self.frame_80.setObjectName(u"frame_80")
        self.frame_80.setMinimumSize(QSize(0, 35))
        self.frame_80.setMaximumSize(QSize(16777215, 35))
        self.frame_80.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_80.setFrameShape(QFrame.StyledPanel)
        self.frame_80.setFrameShadow(QFrame.Raised)
        self.verticalLayout_78 = QVBoxLayout(self.frame_80)
        self.verticalLayout_78.setSpacing(0)
        self.verticalLayout_78.setObjectName(u"verticalLayout_78")
        self.verticalLayout_78.setContentsMargins(0, 0, 0, 0)
        self.label_43 = QLabel(self.frame_80)
        self.label_43.setObjectName(u"label_43")
        self.label_43.setFont(font1)
        self.label_43.setAlignment(Qt.AlignCenter)

        self.verticalLayout_78.addWidget(self.label_43)


        self.verticalLayout_82.addWidget(self.frame_80)

        self.frame_83 = QFrame(self.frame_75)
        self.frame_83.setObjectName(u"frame_83")
        self.frame_83.setFrameShape(QFrame.StyledPanel)
        self.frame_83.setFrameShadow(QFrame.Raised)
        self.gridLayout_8 = QGridLayout(self.frame_83)
        self.gridLayout_8.setSpacing(0)
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.gridLayout_8.setContentsMargins(5, 0, 5, 0)
        self.mediaRBt = QRadioButton(self.frame_83)
        self.mediaRBt.setObjectName(u"mediaRBt")
        self.mediaRBt.setFont(font)

        self.gridLayout_8.addWidget(self.mediaRBt, 1, 0, 1, 1)

        self.amostraRBt = QRadioButton(self.frame_83)
        self.amostraRBt.setObjectName(u"amostraRBt")
        self.amostraRBt.setFont(font)

        self.gridLayout_8.addWidget(self.amostraRBt, 0, 0, 1, 1)

        self.mediaBox = QComboBox(self.frame_83)
        self.mediaBox.setObjectName(u"mediaBox")
        self.mediaBox.setFont(font)

        self.gridLayout_8.addWidget(self.mediaBox, 1, 1, 1, 1)


        self.verticalLayout_82.addWidget(self.frame_83)


        self.gridLayout_3.addWidget(self.frame_75, 2, 0, 1, 1)

        self.frame_76 = QFrame(self.frame_67)
        self.frame_76.setObjectName(u"frame_76")
        self.frame_76.setMinimumSize(QSize(0, 120))
        self.frame_76.setMaximumSize(QSize(250, 16777215))
        self.frame_76.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_76.setFrameShape(QFrame.StyledPanel)
        self.frame_76.setFrameShadow(QFrame.Raised)
        self.verticalLayout_80 = QVBoxLayout(self.frame_76)
        self.verticalLayout_80.setSpacing(0)
        self.verticalLayout_80.setObjectName(u"verticalLayout_80")
        self.verticalLayout_80.setContentsMargins(0, 0, 0, 0)
        self.frame_78 = QFrame(self.frame_76)
        self.frame_78.setObjectName(u"frame_78")
        self.frame_78.setMinimumSize(QSize(0, 35))
        self.frame_78.setMaximumSize(QSize(16777215, 35))
        self.frame_78.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_78.setFrameShape(QFrame.StyledPanel)
        self.frame_78.setFrameShadow(QFrame.Raised)
        self.verticalLayout_77 = QVBoxLayout(self.frame_78)
        self.verticalLayout_77.setSpacing(0)
        self.verticalLayout_77.setObjectName(u"verticalLayout_77")
        self.verticalLayout_77.setContentsMargins(0, 0, 0, 0)
        self.label_42 = QLabel(self.frame_78)
        self.label_42.setObjectName(u"label_42")
        self.label_42.setFont(font1)
        self.label_42.setAlignment(Qt.AlignCenter)

        self.verticalLayout_77.addWidget(self.label_42)


        self.verticalLayout_80.addWidget(self.frame_78)

        self.frame_82 = QFrame(self.frame_76)
        self.frame_82.setObjectName(u"frame_82")
        self.frame_82.setFrameShape(QFrame.StyledPanel)
        self.frame_82.setFrameShadow(QFrame.Raised)
        self.gridLayout_6 = QGridLayout(self.frame_82)
        self.gridLayout_6.setSpacing(0)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.gridLayout_6.setContentsMargins(5, 0, 5, 0)
        self.vtRBt = QRadioButton(self.frame_82)
        self.vtRBt.setObjectName(u"vtRBt")
        self.vtRBt.setFont(font)

        self.gridLayout_6.addWidget(self.vtRBt, 0, 0, 1, 1)

        self.ivRBt = QRadioButton(self.frame_82)
        self.ivRBt.setObjectName(u"ivRBt")
        self.ivRBt.setFont(font)

        self.gridLayout_6.addWidget(self.ivRBt, 0, 1, 1, 1)

        self.ch12ch2RBt = QRadioButton(self.frame_82)
        self.ch12ch2RBt.setObjectName(u"ch12ch2RBt")
        self.ch12ch2RBt.setFont(font)

        self.gridLayout_6.addWidget(self.ch12ch2RBt, 1, 0, 1, 2)


        self.verticalLayout_80.addWidget(self.frame_82)


        self.gridLayout_3.addWidget(self.frame_76, 1, 0, 1, 1)

        self.frame_79 = QFrame(self.frame_67)
        self.frame_79.setObjectName(u"frame_79")
        self.frame_79.setMinimumSize(QSize(200, 120))
        self.frame_79.setMaximumSize(QSize(250, 16777215))
        self.frame_79.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"")
        self.frame_79.setFrameShape(QFrame.StyledPanel)
        self.frame_79.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_28 = QHBoxLayout(self.frame_79)
        self.horizontalLayout_28.setObjectName(u"horizontalLayout_28")
        self.horizontalLayout_28.setContentsMargins(6, 6, 6, 6)
        self.capturarBt_Cap = QToolButton(self.frame_79)
        self.capturarBt_Cap.setObjectName(u"capturarBt_Cap")
        sizePolicy4.setHeightForWidth(self.capturarBt_Cap.sizePolicy().hasHeightForWidth())
        self.capturarBt_Cap.setSizePolicy(sizePolicy4)
        self.capturarBt_Cap.setFont(font)
        self.capturarBt_Cap.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")
        self.capturarBt_Cap.setIcon(icon4)
        self.capturarBt_Cap.setIconSize(QSize(80, 80))
        self.capturarBt_Cap.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.horizontalLayout_28.addWidget(self.capturarBt_Cap)

        self.salvarBt_Cap = QToolButton(self.frame_79)
        self.salvarBt_Cap.setObjectName(u"salvarBt_Cap")
        sizePolicy4.setHeightForWidth(self.salvarBt_Cap.sizePolicy().hasHeightForWidth())
        self.salvarBt_Cap.setSizePolicy(sizePolicy4)
        self.salvarBt_Cap.setFont(font)
        self.salvarBt_Cap.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")
        self.salvarBt_Cap.setIcon(icon7)
        self.salvarBt_Cap.setIconSize(QSize(80, 80))
        self.salvarBt_Cap.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.horizontalLayout_28.addWidget(self.salvarBt_Cap)


        self.gridLayout_3.addWidget(self.frame_79, 4, 0, 1, 1)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_3.addItem(self.verticalSpacer_4, 5, 0, 1, 1)

        self.frame_74 = QFrame(self.frame_67)
        self.frame_74.setObjectName(u"frame_74")
        self.frame_74.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_74.setFrameShape(QFrame.StyledPanel)
        self.frame_74.setFrameShadow(QFrame.Raised)
        self.verticalLayout_102 = QVBoxLayout(self.frame_74)
        self.verticalLayout_102.setSpacing(0)
        self.verticalLayout_102.setObjectName(u"verticalLayout_102")
        self.verticalLayout_102.setContentsMargins(0, 0, 0, 0)
        self.stacked_plots = QStackedWidget(self.frame_74)
        self.stacked_plots.setObjectName(u"stacked_plots")
        self.time_page = QWidget()
        self.time_page.setObjectName(u"time_page")
        self.verticalLayout_103 = QVBoxLayout(self.time_page)
        self.verticalLayout_103.setSpacing(0)
        self.verticalLayout_103.setObjectName(u"verticalLayout_103")
        self.verticalLayout_103.setContentsMargins(0, 0, 0, 0)
        self.plot_area_time = QVBoxLayout()
        self.plot_area_time.setObjectName(u"plot_area_time")

        self.verticalLayout_103.addLayout(self.plot_area_time)

        self.stacked_plots.addWidget(self.time_page)
        self.volt_page = QWidget()
        self.volt_page.setObjectName(u"volt_page")
        self.verticalLayout_104 = QVBoxLayout(self.volt_page)
        self.verticalLayout_104.setSpacing(0)
        self.verticalLayout_104.setObjectName(u"verticalLayout_104")
        self.verticalLayout_104.setContentsMargins(0, 0, 0, 0)
        self.plot_area_volt = QVBoxLayout()
        self.plot_area_volt.setObjectName(u"plot_area_volt")

        self.verticalLayout_104.addLayout(self.plot_area_volt)

        self.stacked_plots.addWidget(self.volt_page)
        self.ch122_page = QWidget()
        self.ch122_page.setObjectName(u"ch122_page")
        self.verticalLayout_99 = QVBoxLayout(self.ch122_page)
        self.verticalLayout_99.setSpacing(0)
        self.verticalLayout_99.setObjectName(u"verticalLayout_99")
        self.verticalLayout_99.setContentsMargins(0, 0, 0, 0)
        self.plot_area_ch122 = QVBoxLayout()
        self.plot_area_ch122.setObjectName(u"plot_area_ch122")

        self.verticalLayout_99.addLayout(self.plot_area_ch122)

        self.stacked_plots.addWidget(self.ch122_page)

        self.verticalLayout_102.addWidget(self.stacked_plots)


        self.gridLayout_3.addWidget(self.frame_74, 0, 1, 6, 1)


        self.verticalLayout_81.addWidget(self.frame_67)

        self.stackedWidget.addWidget(self.page_time_resp)
        self.page_fft = QWidget()
        self.page_fft.setObjectName(u"page_fft")
        self.verticalLayout_90 = QVBoxLayout(self.page_fft)
        self.verticalLayout_90.setSpacing(0)
        self.verticalLayout_90.setObjectName(u"verticalLayout_90")
        self.verticalLayout_90.setContentsMargins(0, 0, 0, 0)
        self.frame_71 = QFrame(self.page_fft)
        self.frame_71.setObjectName(u"frame_71")
        self.frame_71.setFrameShape(QFrame.StyledPanel)
        self.frame_71.setFrameShadow(QFrame.Raised)
        self.gridLayout_9 = QGridLayout(self.frame_71)
        self.gridLayout_9.setSpacing(6)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.gridLayout_9.setContentsMargins(0, 0, 0, 0)
        self.frame_72 = QFrame(self.frame_71)
        self.frame_72.setObjectName(u"frame_72")
        self.frame_72.setMinimumSize(QSize(250, 80))
        self.frame_72.setMaximumSize(QSize(250, 55555))
        self.frame_72.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_72.setFrameShape(QFrame.StyledPanel)
        self.frame_72.setFrameShadow(QFrame.Raised)
        self.verticalLayout_76 = QVBoxLayout(self.frame_72)
        self.verticalLayout_76.setSpacing(0)
        self.verticalLayout_76.setObjectName(u"verticalLayout_76")
        self.verticalLayout_76.setContentsMargins(0, 0, 0, 0)
        self.frame_73 = QFrame(self.frame_72)
        self.frame_73.setObjectName(u"frame_73")
        self.frame_73.setMaximumSize(QSize(16777215, 35))
        self.frame_73.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_73.setFrameShape(QFrame.StyledPanel)
        self.frame_73.setFrameShadow(QFrame.Raised)
        self.verticalLayout_84 = QVBoxLayout(self.frame_73)
        self.verticalLayout_84.setSpacing(0)
        self.verticalLayout_84.setObjectName(u"verticalLayout_84")
        self.verticalLayout_84.setContentsMargins(0, 0, 0, 0)
        self.label_38 = QLabel(self.frame_73)
        self.label_38.setObjectName(u"label_38")
        self.label_38.setFont(font1)
        self.label_38.setAlignment(Qt.AlignCenter)

        self.verticalLayout_84.addWidget(self.label_38)


        self.verticalLayout_76.addWidget(self.frame_73)

        self.frame_85 = QFrame(self.frame_72)
        self.frame_85.setObjectName(u"frame_85")
        self.frame_85.setFrameShape(QFrame.StyledPanel)
        self.frame_85.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_26 = QHBoxLayout(self.frame_85)
        self.horizontalLayout_26.setSpacing(0)
        self.horizontalLayout_26.setObjectName(u"horizontalLayout_26")
        self.horizontalLayout_26.setContentsMargins(5, 0, 5, 0)
        self.ch1_Fft = QRadioButton(self.frame_85)
        self.ch1_Fft.setObjectName(u"ch1_Fft")
        self.ch1_Fft.setFont(font)

        self.horizontalLayout_26.addWidget(self.ch1_Fft)

        self.ch2_Fft = QRadioButton(self.frame_85)
        self.ch2_Fft.setObjectName(u"ch2_Fft")
        self.ch2_Fft.setFont(font)

        self.horizontalLayout_26.addWidget(self.ch2_Fft)


        self.verticalLayout_76.addWidget(self.frame_85)


        self.gridLayout_9.addWidget(self.frame_72, 0, 0, 1, 1)

        self.frame_89 = QFrame(self.frame_71)
        self.frame_89.setObjectName(u"frame_89")
        self.frame_89.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_89.setFrameShape(QFrame.StyledPanel)
        self.frame_89.setFrameShadow(QFrame.Raised)
        self.verticalLayout_101 = QVBoxLayout(self.frame_89)
        self.verticalLayout_101.setSpacing(0)
        self.verticalLayout_101.setObjectName(u"verticalLayout_101")
        self.verticalLayout_101.setContentsMargins(0, 0, 0, 0)
        self.plot_area_fft = QVBoxLayout()
        self.plot_area_fft.setObjectName(u"plot_area_fft")

        self.verticalLayout_101.addLayout(self.plot_area_fft)


        self.gridLayout_9.addWidget(self.frame_89, 0, 1, 3, 1)

        self.frame_94 = QFrame(self.frame_71)
        self.frame_94.setObjectName(u"frame_94")
        sizePolicy7 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.frame_94.sizePolicy().hasHeightForWidth())
        self.frame_94.setSizePolicy(sizePolicy7)
        self.frame_94.setMinimumSize(QSize(250, 120))
        self.frame_94.setMaximumSize(QSize(250, 16777215))
        self.frame_94.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"")
        self.frame_94.setFrameShape(QFrame.StyledPanel)
        self.frame_94.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_27 = QHBoxLayout(self.frame_94)
        self.horizontalLayout_27.setObjectName(u"horizontalLayout_27")
        self.horizontalLayout_27.setContentsMargins(6, 6, 6, 6)
        self.startBt_Fft = QToolButton(self.frame_94)
        self.startBt_Fft.setObjectName(u"startBt_Fft")
        sizePolicy7.setHeightForWidth(self.startBt_Fft.sizePolicy().hasHeightForWidth())
        self.startBt_Fft.setSizePolicy(sizePolicy7)
        self.startBt_Fft.setMinimumSize(QSize(100, 100))
        self.startBt_Fft.setFont(font)
        self.startBt_Fft.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")
        self.startBt_Fft.setIcon(icon4)
        self.startBt_Fft.setIconSize(QSize(80, 80))
        self.startBt_Fft.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.horizontalLayout_27.addWidget(self.startBt_Fft)

        self.salvarBt_Fft = QToolButton(self.frame_94)
        self.salvarBt_Fft.setObjectName(u"salvarBt_Fft")
        sizePolicy7.setHeightForWidth(self.salvarBt_Fft.sizePolicy().hasHeightForWidth())
        self.salvarBt_Fft.setSizePolicy(sizePolicy7)
        self.salvarBt_Fft.setMinimumSize(QSize(100, 100))
        self.salvarBt_Fft.setFont(font)
        self.salvarBt_Fft.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"QToolButton:disabled {	\n"
"background-image: url(:/old/icons/scratch.png);\n"
"background-size: contain;\n"
"background-position: center;\n"
"background-repeat: no-repeat;\n"
"}\n"
"\n"
"QToolButton:enabled {	\n"
"background-image: none;\n"
"}")
        self.salvarBt_Fft.setIcon(icon7)
        self.salvarBt_Fft.setIconSize(QSize(80, 80))
        self.salvarBt_Fft.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.horizontalLayout_27.addWidget(self.salvarBt_Fft)


        self.gridLayout_9.addWidget(self.frame_94, 1, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_9.addItem(self.verticalSpacer, 2, 0, 1, 1)


        self.verticalLayout_90.addWidget(self.frame_71)

        self.stackedWidget.addWidget(self.page_fft)
        self.page_settings = QWidget()
        self.page_settings.setObjectName(u"page_settings")
        self.verticalLayout_6 = QVBoxLayout(self.page_settings)
        self.verticalLayout_6.setSpacing(6)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.frame = QFrame(self.page_settings)
        self.frame.setObjectName(u"frame")
        self.frame.setMinimumSize(QSize(0, 150))
        self.frame.setStyleSheet(u"border-radius: 5px;")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_15 = QVBoxLayout(self.frame)
        self.verticalLayout_15.setSpacing(0)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.verticalLayout_15.setContentsMargins(0, 0, 0, 0)
        self.frame_div_content_1 = QFrame(self.frame)
        self.frame_div_content_1.setObjectName(u"frame_div_content_1")
        self.frame_div_content_1.setMinimumSize(QSize(0, 100))
        self.frame_div_content_1.setMaximumSize(QSize(16777215, 170))
        self.frame_div_content_1.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_div_content_1.setFrameShape(QFrame.NoFrame)
        self.frame_div_content_1.setFrameShadow(QFrame.Raised)
        self.verticalLayout_7 = QVBoxLayout(self.frame_div_content_1)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.frame_title_wid_1 = QFrame(self.frame_div_content_1)
        self.frame_title_wid_1.setObjectName(u"frame_title_wid_1")
        self.frame_title_wid_1.setMinimumSize(QSize(0, 35))
        self.frame_title_wid_1.setMaximumSize(QSize(16777215, 35))
        self.frame_title_wid_1.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_title_wid_1.setFrameShape(QFrame.StyledPanel)
        self.frame_title_wid_1.setFrameShadow(QFrame.Raised)
        self.verticalLayout_8 = QVBoxLayout(self.frame_title_wid_1)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(-1, 4, -1, 4)
        self.labelBoxBlenderInstalation = QLabel(self.frame_title_wid_1)
        self.labelBoxBlenderInstalation.setObjectName(u"labelBoxBlenderInstalation")
        font7 = QFont()
        font7.setFamily(u"Segoe UI")
        font7.setPointSize(15)
        font7.setBold(True)
        font7.setWeight(75)
        self.labelBoxBlenderInstalation.setFont(font7)
        self.labelBoxBlenderInstalation.setStyleSheet(u"")

        self.verticalLayout_8.addWidget(self.labelBoxBlenderInstalation)


        self.verticalLayout_7.addWidget(self.frame_title_wid_1)

        self.frame_content_wid_1 = QFrame(self.frame_div_content_1)
        self.frame_content_wid_1.setObjectName(u"frame_content_wid_1")
        self.frame_content_wid_1.setFrameShape(QFrame.NoFrame)
        self.frame_content_wid_1.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_9 = QHBoxLayout(self.frame_content_wid_1)
        self.horizontalLayout_9.setSpacing(7)
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.horizontalLayout_9.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_15 = QHBoxLayout()
        self.horizontalLayout_15.setSpacing(2)
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.verticalLayout_11 = QVBoxLayout()
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.label_2 = QLabel(self.frame_content_wid_1)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font)
        self.label_2.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.label_2)

        self.modelosOscCombo = QComboBox(self.frame_content_wid_1)
        self.modelosOscCombo.setObjectName(u"modelosOscCombo")
        self.modelosOscCombo.setFont(font)
        self.modelosOscCombo.setStyleSheet(u"QComboBox {\n"
"	background-color: rgb(27, 29, 35);\n"
"	border-radius: 5px;\n"
"	border: 2px solid rgb(27, 29, 35);\n"
"	padding-left: 10px;\n"
"}\n"
"QComboBox:hover {\n"
"	border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"QComboBox:focus {\n"
"	border: 2px solid rgb(91, 101, 124);\n"
"}")

        self.verticalLayout_11.addWidget(self.modelosOscCombo)

        self.osciloscopioStatus = QLabel(self.frame_content_wid_1)
        self.osciloscopioStatus.setObjectName(u"osciloscopioStatus")
        self.osciloscopioStatus.setFont(font2)
        self.osciloscopioStatus.setAlignment(Qt.AlignCenter)

        self.verticalLayout_11.addWidget(self.osciloscopioStatus)


        self.horizontalLayout_15.addLayout(self.verticalLayout_11)

        self.conectarBtOsc = QToolButton(self.frame_content_wid_1)
        self.conectarBtOsc.setObjectName(u"conectarBtOsc")
        self.conectarBtOsc.setMinimumSize(QSize(100, 100))
        self.conectarBtOsc.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}")
        icon8 = QIcon()
        icon8.addFile(u":/old/icons/conectar.png", QSize(), QIcon.Normal, QIcon.Off)
        self.conectarBtOsc.setIcon(icon8)
        self.conectarBtOsc.setIconSize(QSize(100, 100))

        self.horizontalLayout_15.addWidget(self.conectarBtOsc)


        self.horizontalLayout_9.addLayout(self.horizontalLayout_15)

        self.frame_10 = QFrame(self.frame_content_wid_1)
        self.frame_10.setObjectName(u"frame_10")
        self.frame_10.setMinimumSize(QSize(5, 0))
        self.frame_10.setMaximumSize(QSize(5, 5555555))
        self.frame_10.setStyleSheet(u"background-color: rgba(255, 255, 255,0.5);\n"
"border-radius: 2px;\n"
"")
        self.frame_10.setFrameShape(QFrame.StyledPanel)
        self.frame_10.setFrameShadow(QFrame.Raised)

        self.horizontalLayout_9.addWidget(self.frame_10)

        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setSpacing(2)
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.verticalLayout_16 = QVBoxLayout()
        self.verticalLayout_16.setObjectName(u"verticalLayout_16")
        self.label_4 = QLabel(self.frame_content_wid_1)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setFont(font)
        self.label_4.setAlignment(Qt.AlignCenter)

        self.verticalLayout_16.addWidget(self.label_4)

        self.modelosGerCombo = QComboBox(self.frame_content_wid_1)
        self.modelosGerCombo.setObjectName(u"modelosGerCombo")
        self.modelosGerCombo.setFont(font)
        self.modelosGerCombo.setStyleSheet(u"QComboBox {\n"
"	background-color: rgb(27, 29, 35);\n"
"	border-radius: 5px;\n"
"	border: 2px solid rgb(27, 29, 35);\n"
"	padding-left: 10px;\n"
"}\n"
"QComboBox:hover {\n"
"	border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"QComboBox:focus {\n"
"	border: 2px solid rgb(91, 101, 124);\n"
"}")

        self.verticalLayout_16.addWidget(self.modelosGerCombo)

        self.geradorStatus = QLabel(self.frame_content_wid_1)
        self.geradorStatus.setObjectName(u"geradorStatus")
        self.geradorStatus.setFont(font2)
        self.geradorStatus.setAlignment(Qt.AlignCenter)

        self.verticalLayout_16.addWidget(self.geradorStatus)


        self.horizontalLayout_11.addLayout(self.verticalLayout_16)

        self.conectarBtGer = QToolButton(self.frame_content_wid_1)
        self.conectarBtGer.setObjectName(u"conectarBtGer")
        self.conectarBtGer.setMinimumSize(QSize(100, 100))
        self.conectarBtGer.setStyleSheet(u"/* TOOL BUTTON*/\n"
"QToolButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 10px;	\n"
"	background-color: rgb(32, 35, 43);\n"
"}\n"
"QToolButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QToolButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}")
        self.conectarBtGer.setIcon(icon8)
        self.conectarBtGer.setIconSize(QSize(100, 100))

        self.horizontalLayout_11.addWidget(self.conectarBtGer)


        self.horizontalLayout_9.addLayout(self.horizontalLayout_11)


        self.verticalLayout_7.addWidget(self.frame_content_wid_1)


        self.verticalLayout_15.addWidget(self.frame_div_content_1)


        self.verticalLayout_6.addWidget(self.frame)

        self.frame_3 = QFrame(self.page_settings)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setMinimumSize(QSize(0, 300))
        self.frame_3.setStyleSheet(u"border-radius: 5px;\n"
"")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.verticalLayout_12 = QVBoxLayout(self.frame_3)
        self.verticalLayout_12.setSpacing(0)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.verticalLayout_12.setContentsMargins(0, 0, 0, 0)
        self.frame_11 = QFrame(self.frame_3)
        self.frame_11.setObjectName(u"frame_11")
        self.frame_11.setMinimumSize(QSize(0, 35))
        self.frame_11.setMaximumSize(QSize(16777215, 35))
        self.frame_11.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_11.setFrameShape(QFrame.StyledPanel)
        self.frame_11.setFrameShadow(QFrame.Raised)
        self.verticalLayout_13 = QVBoxLayout(self.frame_11)
        self.verticalLayout_13.setSpacing(6)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.verticalLayout_13.setContentsMargins(9, 4, 9, 4)
        self.label_14 = QLabel(self.frame_11)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setFont(font7)

        self.verticalLayout_13.addWidget(self.label_14)


        self.verticalLayout_12.addWidget(self.frame_11)

        self.frame_12 = QFrame(self.frame_3)
        self.frame_12.setObjectName(u"frame_12")
        self.frame_12.setStyleSheet(u"background-color: rgb(44, 49, 60);\n"
"border-radius: 5px;\n"
"")
        self.frame_12.setFrameShape(QFrame.StyledPanel)
        self.frame_12.setFrameShadow(QFrame.Raised)
        self.verticalLayout_25 = QVBoxLayout(self.frame_12)
        self.verticalLayout_25.setSpacing(0)
        self.verticalLayout_25.setObjectName(u"verticalLayout_25")
        self.verticalLayout_25.setContentsMargins(0, 4, 0, 0)
        self.tabWidget = QTabWidget(self.frame_12)
        self.tabWidget.setObjectName(u"tabWidget")
        font8 = QFont()
        font8.setPointSize(5)
        self.tabWidget.setFont(font8)
        self.tabWidget.setStyleSheet(u"QTabWidget::pane { /* The tab widget frame */\n"
"	border: none;\n"
"}\n"
"\n"
"QTabWidget::tab-bar {\n"
"    alignment: center;\n"
"}\n"
"\n"
"/* Style the tab using the tab sub-control. Note that\n"
"    it reads QTabBar _not_ QTabWidget */\n"
"QTabBar::tab {\n"
"	color: rgb(255, 255, 255);\n"
"	font: 15px Segoe UI;\n"
"	font-weight: bold;\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 rgba(225, 225, 225, 0.5), stop: 0.4 rgba(221, 221, 221, 0.5),\n"
"                                stop: 0.5 rgba(216, 216, 216, 0.5), stop: 1.0 rgba(211, 211, 211, 0.5));\n"
"	opacity: 40%; \n"
"    border: none;\n"
"	border-radius: 5px;\n"
"    min-width: 41ex;\n"
"	min-height: 30px;\n"
"    margin-left:5px;\n"
"    margin-right:5px;\n"
"\n"
"}\n"
"\n"
"QTabBar::tab:selected, QTabBar::tab:hover {\n"
"    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                stop: 0 #fafafa, stop: 0.4 #f4f4f4,\n"
"                                "
                        "stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);\n"
"	\n"
"	color: rgb(31, 35, 43);\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"    border-color: #9B9B9B;\n"
"    border-bottom-color: #C2C7CB; /* same as pane color */\n"
"}\n"
"")
        self.tab_exerc = QWidget()
        self.tab_exerc.setObjectName(u"tab_exerc")
        self.horizontalLayout_12 = QHBoxLayout(self.tab_exerc)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.horizontalLayout_12.setContentsMargins(0, 4, 0, 0)
        self.frame_13 = QFrame(self.tab_exerc)
        self.frame_13.setObjectName(u"frame_13")
        self.frame_13.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_13.setFrameShape(QFrame.StyledPanel)
        self.frame_13.setFrameShadow(QFrame.Raised)
        self.verticalLayout_26 = QVBoxLayout(self.frame_13)
        self.verticalLayout_26.setSpacing(0)
        self.verticalLayout_26.setObjectName(u"verticalLayout_26")
        self.verticalLayout_26.setContentsMargins(0, 0, 0, 0)
        self.frame_16 = QFrame(self.frame_13)
        self.frame_16.setObjectName(u"frame_16")
        self.frame_16.setMinimumSize(QSize(0, 25))
        self.frame_16.setMaximumSize(QSize(16777215, 25))
        self.frame_16.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_16.setFrameShape(QFrame.StyledPanel)
        self.frame_16.setFrameShadow(QFrame.Raised)
        self.verticalLayout_27 = QVBoxLayout(self.frame_16)
        self.verticalLayout_27.setObjectName(u"verticalLayout_27")
        self.verticalLayout_27.setContentsMargins(-1, 4, -1, 4)
        self.label_15 = QLabel(self.frame_16)
        self.label_15.setObjectName(u"label_15")
        font9 = QFont()
        font9.setFamily(u"Segoe UI")
        font9.setPointSize(10)
        font9.setBold(False)
        font9.setWeight(50)
        self.label_15.setFont(font9)
        self.label_15.setAlignment(Qt.AlignCenter)

        self.verticalLayout_27.addWidget(self.label_15)


        self.verticalLayout_26.addWidget(self.frame_16)

        self.textBrowser_4 = QTextBrowser(self.frame_13)
        self.textBrowser_4.setObjectName(u"textBrowser_4")
        self.textBrowser_4.setMaximumSize(QSize(16777215, 16777215))

        self.verticalLayout_26.addWidget(self.textBrowser_4)


        self.horizontalLayout_12.addWidget(self.frame_13)

        self.frame_14 = QFrame(self.tab_exerc)
        self.frame_14.setObjectName(u"frame_14")
        self.frame_14.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_14.setFrameShape(QFrame.StyledPanel)
        self.frame_14.setFrameShadow(QFrame.Raised)
        self.verticalLayout_28 = QVBoxLayout(self.frame_14)
        self.verticalLayout_28.setSpacing(0)
        self.verticalLayout_28.setObjectName(u"verticalLayout_28")
        self.verticalLayout_28.setContentsMargins(0, 0, 0, 0)
        self.frame_17 = QFrame(self.frame_14)
        self.frame_17.setObjectName(u"frame_17")
        self.frame_17.setMinimumSize(QSize(0, 25))
        self.frame_17.setMaximumSize(QSize(16777215, 25))
        self.frame_17.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_17.setFrameShape(QFrame.StyledPanel)
        self.frame_17.setFrameShadow(QFrame.Raised)
        self.verticalLayout_30 = QVBoxLayout(self.frame_17)
        self.verticalLayout_30.setObjectName(u"verticalLayout_30")
        self.verticalLayout_30.setContentsMargins(-1, 4, -1, 4)
        self.label_16 = QLabel(self.frame_17)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setFont(font9)
        self.label_16.setAlignment(Qt.AlignCenter)

        self.verticalLayout_30.addWidget(self.label_16)


        self.verticalLayout_28.addWidget(self.frame_17)

        self.textBrowser_5 = QTextBrowser(self.frame_14)
        self.textBrowser_5.setObjectName(u"textBrowser_5")

        self.verticalLayout_28.addWidget(self.textBrowser_5)


        self.horizontalLayout_12.addWidget(self.frame_14)

        self.frame_15 = QFrame(self.tab_exerc)
        self.frame_15.setObjectName(u"frame_15")
        self.frame_15.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_15.setFrameShape(QFrame.StyledPanel)
        self.frame_15.setFrameShadow(QFrame.Raised)
        self.verticalLayout_29 = QVBoxLayout(self.frame_15)
        self.verticalLayout_29.setSpacing(0)
        self.verticalLayout_29.setObjectName(u"verticalLayout_29")
        self.verticalLayout_29.setContentsMargins(0, 0, 0, 0)
        self.frame_18 = QFrame(self.frame_15)
        self.frame_18.setObjectName(u"frame_18")
        self.frame_18.setMinimumSize(QSize(0, 25))
        self.frame_18.setMaximumSize(QSize(16777215, 25))
        self.frame_18.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_18.setFrameShape(QFrame.StyledPanel)
        self.frame_18.setFrameShadow(QFrame.Raised)
        self.verticalLayout_31 = QVBoxLayout(self.frame_18)
        self.verticalLayout_31.setObjectName(u"verticalLayout_31")
        self.verticalLayout_31.setContentsMargins(-1, 4, -1, 4)
        self.label_17 = QLabel(self.frame_18)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setFont(font9)
        self.label_17.setAlignment(Qt.AlignCenter)

        self.verticalLayout_31.addWidget(self.label_17)


        self.verticalLayout_29.addWidget(self.frame_18)

        self.textBrowser_6 = QTextBrowser(self.frame_15)
        self.textBrowser_6.setObjectName(u"textBrowser_6")

        self.verticalLayout_29.addWidget(self.textBrowser_6)


        self.horizontalLayout_12.addWidget(self.frame_15)

        self.frame_19 = QFrame(self.tab_exerc)
        self.frame_19.setObjectName(u"frame_19")
        self.frame_19.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_19.setFrameShape(QFrame.StyledPanel)
        self.frame_19.setFrameShadow(QFrame.Raised)
        self.verticalLayout_32 = QVBoxLayout(self.frame_19)
        self.verticalLayout_32.setSpacing(0)
        self.verticalLayout_32.setObjectName(u"verticalLayout_32")
        self.verticalLayout_32.setContentsMargins(0, 0, 0, 0)
        self.frame_20 = QFrame(self.frame_19)
        self.frame_20.setObjectName(u"frame_20")
        self.frame_20.setMinimumSize(QSize(0, 25))
        self.frame_20.setMaximumSize(QSize(16777215, 25))
        self.frame_20.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_20.setFrameShape(QFrame.StyledPanel)
        self.frame_20.setFrameShadow(QFrame.Raised)
        self.verticalLayout_33 = QVBoxLayout(self.frame_20)
        self.verticalLayout_33.setObjectName(u"verticalLayout_33")
        self.verticalLayout_33.setContentsMargins(-1, 4, -1, 4)
        self.label_18 = QLabel(self.frame_20)
        self.label_18.setObjectName(u"label_18")
        self.label_18.setFont(font9)
        self.label_18.setAlignment(Qt.AlignCenter)

        self.verticalLayout_33.addWidget(self.label_18)


        self.verticalLayout_32.addWidget(self.frame_20)

        self.textBrowser_7 = QTextBrowser(self.frame_19)
        self.textBrowser_7.setObjectName(u"textBrowser_7")

        self.verticalLayout_32.addWidget(self.textBrowser_7)


        self.horizontalLayout_12.addWidget(self.frame_19)

        self.tabWidget.addTab(self.tab_exerc, "")
        self.tab_fft = QWidget()
        self.tab_fft.setObjectName(u"tab_fft")
        self.horizontalLayout_17 = QHBoxLayout(self.tab_fft)
        self.horizontalLayout_17.setObjectName(u"horizontalLayout_17")
        self.horizontalLayout_17.setContentsMargins(0, 4, 0, 0)
        self.frame_23 = QFrame(self.tab_fft)
        self.frame_23.setObjectName(u"frame_23")
        self.frame_23.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_23.setFrameShape(QFrame.StyledPanel)
        self.frame_23.setFrameShadow(QFrame.Raised)
        self.verticalLayout_36 = QVBoxLayout(self.frame_23)
        self.verticalLayout_36.setSpacing(0)
        self.verticalLayout_36.setObjectName(u"verticalLayout_36")
        self.verticalLayout_36.setContentsMargins(0, 0, 0, 0)
        self.frame_24 = QFrame(self.frame_23)
        self.frame_24.setObjectName(u"frame_24")
        self.frame_24.setMinimumSize(QSize(0, 25))
        self.frame_24.setMaximumSize(QSize(16777215, 25))
        self.frame_24.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_24.setFrameShape(QFrame.StyledPanel)
        self.frame_24.setFrameShadow(QFrame.Raised)
        self.verticalLayout_37 = QVBoxLayout(self.frame_24)
        self.verticalLayout_37.setObjectName(u"verticalLayout_37")
        self.verticalLayout_37.setContentsMargins(-1, 4, -1, 4)
        self.label_20 = QLabel(self.frame_24)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setFont(font9)
        self.label_20.setAlignment(Qt.AlignCenter)

        self.verticalLayout_37.addWidget(self.label_20)


        self.verticalLayout_36.addWidget(self.frame_24)

        self.textBrowser_9 = QTextBrowser(self.frame_23)
        self.textBrowser_9.setObjectName(u"textBrowser_9")
        self.textBrowser_9.setMaximumSize(QSize(16777215, 16777215))

        self.verticalLayout_36.addWidget(self.textBrowser_9)


        self.horizontalLayout_17.addWidget(self.frame_23)

        self.frame_21 = QFrame(self.tab_fft)
        self.frame_21.setObjectName(u"frame_21")
        self.frame_21.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_21.setFrameShape(QFrame.StyledPanel)
        self.frame_21.setFrameShadow(QFrame.Raised)
        self.verticalLayout_34 = QVBoxLayout(self.frame_21)
        self.verticalLayout_34.setSpacing(0)
        self.verticalLayout_34.setObjectName(u"verticalLayout_34")
        self.verticalLayout_34.setContentsMargins(0, 0, 0, 0)
        self.frame_22 = QFrame(self.frame_21)
        self.frame_22.setObjectName(u"frame_22")
        self.frame_22.setMinimumSize(QSize(0, 25))
        self.frame_22.setMaximumSize(QSize(16777215, 25))
        self.frame_22.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_22.setFrameShape(QFrame.StyledPanel)
        self.frame_22.setFrameShadow(QFrame.Raised)
        self.verticalLayout_35 = QVBoxLayout(self.frame_22)
        self.verticalLayout_35.setObjectName(u"verticalLayout_35")
        self.verticalLayout_35.setContentsMargins(-1, 4, -1, 4)
        self.label_19 = QLabel(self.frame_22)
        self.label_19.setObjectName(u"label_19")
        self.label_19.setFont(font9)
        self.label_19.setAlignment(Qt.AlignCenter)

        self.verticalLayout_35.addWidget(self.label_19)


        self.verticalLayout_34.addWidget(self.frame_22)

        self.textBrowser_8 = QTextBrowser(self.frame_21)
        self.textBrowser_8.setObjectName(u"textBrowser_8")

        self.verticalLayout_34.addWidget(self.textBrowser_8)


        self.horizontalLayout_17.addWidget(self.frame_21)

        self.frame_27 = QFrame(self.tab_fft)
        self.frame_27.setObjectName(u"frame_27")
        self.frame_27.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_27.setFrameShape(QFrame.StyledPanel)
        self.frame_27.setFrameShadow(QFrame.Raised)
        self.verticalLayout_40 = QVBoxLayout(self.frame_27)
        self.verticalLayout_40.setSpacing(0)
        self.verticalLayout_40.setObjectName(u"verticalLayout_40")
        self.verticalLayout_40.setContentsMargins(0, 0, 0, 0)
        self.frame_28 = QFrame(self.frame_27)
        self.frame_28.setObjectName(u"frame_28")
        self.frame_28.setMinimumSize(QSize(0, 25))
        self.frame_28.setMaximumSize(QSize(16777215, 25))
        self.frame_28.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_28.setFrameShape(QFrame.StyledPanel)
        self.frame_28.setFrameShadow(QFrame.Raised)
        self.verticalLayout_41 = QVBoxLayout(self.frame_28)
        self.verticalLayout_41.setObjectName(u"verticalLayout_41")
        self.verticalLayout_41.setContentsMargins(-1, 4, -1, 4)
        self.label_22 = QLabel(self.frame_28)
        self.label_22.setObjectName(u"label_22")
        self.label_22.setFont(font9)
        self.label_22.setAlignment(Qt.AlignCenter)

        self.verticalLayout_41.addWidget(self.label_22)


        self.verticalLayout_40.addWidget(self.frame_28)

        self.textBrowser_11 = QTextBrowser(self.frame_27)
        self.textBrowser_11.setObjectName(u"textBrowser_11")

        self.verticalLayout_40.addWidget(self.textBrowser_11)


        self.horizontalLayout_17.addWidget(self.frame_27)

        self.frame_25 = QFrame(self.tab_fft)
        self.frame_25.setObjectName(u"frame_25")
        self.frame_25.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_25.setFrameShape(QFrame.StyledPanel)
        self.frame_25.setFrameShadow(QFrame.Raised)
        self.verticalLayout_38 = QVBoxLayout(self.frame_25)
        self.verticalLayout_38.setSpacing(0)
        self.verticalLayout_38.setObjectName(u"verticalLayout_38")
        self.verticalLayout_38.setContentsMargins(0, 0, 0, 0)
        self.frame_26 = QFrame(self.frame_25)
        self.frame_26.setObjectName(u"frame_26")
        self.frame_26.setMinimumSize(QSize(0, 25))
        self.frame_26.setMaximumSize(QSize(16777215, 25))
        self.frame_26.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_26.setFrameShape(QFrame.StyledPanel)
        self.frame_26.setFrameShadow(QFrame.Raised)
        self.verticalLayout_39 = QVBoxLayout(self.frame_26)
        self.verticalLayout_39.setObjectName(u"verticalLayout_39")
        self.verticalLayout_39.setContentsMargins(-1, 4, -1, 4)
        self.label_21 = QLabel(self.frame_26)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setFont(font9)
        self.label_21.setAlignment(Qt.AlignCenter)

        self.verticalLayout_39.addWidget(self.label_21)


        self.verticalLayout_38.addWidget(self.frame_26)

        self.textBrowser_10 = QTextBrowser(self.frame_25)
        self.textBrowser_10.setObjectName(u"textBrowser_10")

        self.verticalLayout_38.addWidget(self.textBrowser_10)


        self.horizontalLayout_17.addWidget(self.frame_25)

        self.tabWidget.addTab(self.tab_fft, "")
        self.tab_freq = QWidget()
        self.tab_freq.setObjectName(u"tab_freq")
        self.horizontalLayout_18 = QHBoxLayout(self.tab_freq)
        self.horizontalLayout_18.setObjectName(u"horizontalLayout_18")
        self.horizontalLayout_18.setContentsMargins(0, 4, 0, 0)
        self.frame_31 = QFrame(self.tab_freq)
        self.frame_31.setObjectName(u"frame_31")
        self.frame_31.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_31.setFrameShape(QFrame.StyledPanel)
        self.frame_31.setFrameShadow(QFrame.Raised)
        self.verticalLayout_44 = QVBoxLayout(self.frame_31)
        self.verticalLayout_44.setSpacing(0)
        self.verticalLayout_44.setObjectName(u"verticalLayout_44")
        self.verticalLayout_44.setContentsMargins(0, 0, 0, 0)
        self.frame_32 = QFrame(self.frame_31)
        self.frame_32.setObjectName(u"frame_32")
        self.frame_32.setMinimumSize(QSize(0, 25))
        self.frame_32.setMaximumSize(QSize(16777215, 25))
        self.frame_32.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_32.setFrameShape(QFrame.StyledPanel)
        self.frame_32.setFrameShadow(QFrame.Raised)
        self.verticalLayout_45 = QVBoxLayout(self.frame_32)
        self.verticalLayout_45.setObjectName(u"verticalLayout_45")
        self.verticalLayout_45.setContentsMargins(-1, 4, -1, 4)
        self.label_24 = QLabel(self.frame_32)
        self.label_24.setObjectName(u"label_24")
        self.label_24.setFont(font9)
        self.label_24.setAlignment(Qt.AlignCenter)

        self.verticalLayout_45.addWidget(self.label_24)


        self.verticalLayout_44.addWidget(self.frame_32)

        self.textBrowser_13 = QTextBrowser(self.frame_31)
        self.textBrowser_13.setObjectName(u"textBrowser_13")
        self.textBrowser_13.setMaximumSize(QSize(16777215, 16777215))
        self.textBrowser_13.setFont(font)

        self.verticalLayout_44.addWidget(self.textBrowser_13)


        self.horizontalLayout_18.addWidget(self.frame_31)

        self.frame_29 = QFrame(self.tab_freq)
        self.frame_29.setObjectName(u"frame_29")
        self.frame_29.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_29.setFrameShape(QFrame.StyledPanel)
        self.frame_29.setFrameShadow(QFrame.Raised)
        self.verticalLayout_42 = QVBoxLayout(self.frame_29)
        self.verticalLayout_42.setSpacing(0)
        self.verticalLayout_42.setObjectName(u"verticalLayout_42")
        self.verticalLayout_42.setContentsMargins(0, 0, 0, 0)
        self.frame_30 = QFrame(self.frame_29)
        self.frame_30.setObjectName(u"frame_30")
        self.frame_30.setMinimumSize(QSize(0, 25))
        self.frame_30.setMaximumSize(QSize(16777215, 25))
        self.frame_30.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_30.setFrameShape(QFrame.StyledPanel)
        self.frame_30.setFrameShadow(QFrame.Raised)
        self.verticalLayout_43 = QVBoxLayout(self.frame_30)
        self.verticalLayout_43.setObjectName(u"verticalLayout_43")
        self.verticalLayout_43.setContentsMargins(-1, 4, -1, 4)
        self.label_23 = QLabel(self.frame_30)
        self.label_23.setObjectName(u"label_23")
        self.label_23.setFont(font9)
        self.label_23.setAlignment(Qt.AlignCenter)

        self.verticalLayout_43.addWidget(self.label_23)


        self.verticalLayout_42.addWidget(self.frame_30)

        self.textBrowser_12 = QTextBrowser(self.frame_29)
        self.textBrowser_12.setObjectName(u"textBrowser_12")
        self.textBrowser_12.setFont(font)

        self.verticalLayout_42.addWidget(self.textBrowser_12)


        self.horizontalLayout_18.addWidget(self.frame_29)

        self.frame_35 = QFrame(self.tab_freq)
        self.frame_35.setObjectName(u"frame_35")
        self.frame_35.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_35.setFrameShape(QFrame.StyledPanel)
        self.frame_35.setFrameShadow(QFrame.Raised)
        self.verticalLayout_48 = QVBoxLayout(self.frame_35)
        self.verticalLayout_48.setSpacing(0)
        self.verticalLayout_48.setObjectName(u"verticalLayout_48")
        self.verticalLayout_48.setContentsMargins(0, 0, 0, 0)
        self.frame_36 = QFrame(self.frame_35)
        self.frame_36.setObjectName(u"frame_36")
        self.frame_36.setMinimumSize(QSize(0, 25))
        self.frame_36.setMaximumSize(QSize(16777215, 25))
        self.frame_36.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_36.setFrameShape(QFrame.StyledPanel)
        self.frame_36.setFrameShadow(QFrame.Raised)
        self.verticalLayout_49 = QVBoxLayout(self.frame_36)
        self.verticalLayout_49.setObjectName(u"verticalLayout_49")
        self.verticalLayout_49.setContentsMargins(-1, 4, -1, 4)
        self.label_26 = QLabel(self.frame_36)
        self.label_26.setObjectName(u"label_26")
        self.label_26.setFont(font9)
        self.label_26.setAlignment(Qt.AlignCenter)

        self.verticalLayout_49.addWidget(self.label_26)


        self.verticalLayout_48.addWidget(self.frame_36)

        self.textBrowser_15 = QTextBrowser(self.frame_35)
        self.textBrowser_15.setObjectName(u"textBrowser_15")
        self.textBrowser_15.setFont(font)

        self.verticalLayout_48.addWidget(self.textBrowser_15)


        self.horizontalLayout_18.addWidget(self.frame_35)

        self.frame_33 = QFrame(self.tab_freq)
        self.frame_33.setObjectName(u"frame_33")
        self.frame_33.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_33.setFrameShape(QFrame.StyledPanel)
        self.frame_33.setFrameShadow(QFrame.Raised)
        self.verticalLayout_46 = QVBoxLayout(self.frame_33)
        self.verticalLayout_46.setSpacing(0)
        self.verticalLayout_46.setObjectName(u"verticalLayout_46")
        self.verticalLayout_46.setContentsMargins(0, 0, 0, 0)
        self.frame_34 = QFrame(self.frame_33)
        self.frame_34.setObjectName(u"frame_34")
        self.frame_34.setMinimumSize(QSize(0, 25))
        self.frame_34.setMaximumSize(QSize(16777215, 25))
        self.frame_34.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_34.setFrameShape(QFrame.StyledPanel)
        self.frame_34.setFrameShadow(QFrame.Raised)
        self.verticalLayout_47 = QVBoxLayout(self.frame_34)
        self.verticalLayout_47.setObjectName(u"verticalLayout_47")
        self.verticalLayout_47.setContentsMargins(-1, 4, -1, 4)
        self.label_25 = QLabel(self.frame_34)
        self.label_25.setObjectName(u"label_25")
        self.label_25.setFont(font9)
        self.label_25.setAlignment(Qt.AlignCenter)

        self.verticalLayout_47.addWidget(self.label_25)


        self.verticalLayout_46.addWidget(self.frame_34)

        self.textBrowser_14 = QTextBrowser(self.frame_33)
        self.textBrowser_14.setObjectName(u"textBrowser_14")
        self.textBrowser_14.setFont(font)

        self.verticalLayout_46.addWidget(self.textBrowser_14)


        self.horizontalLayout_18.addWidget(self.frame_33)

        self.tabWidget.addTab(self.tab_freq, "")
        self.tab_time = QWidget()
        self.tab_time.setObjectName(u"tab_time")
        self.horizontalLayout_19 = QHBoxLayout(self.tab_time)
        self.horizontalLayout_19.setObjectName(u"horizontalLayout_19")
        self.horizontalLayout_19.setContentsMargins(0, 4, 0, 0)
        self.frame_39 = QFrame(self.tab_time)
        self.frame_39.setObjectName(u"frame_39")
        self.frame_39.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_39.setFrameShape(QFrame.StyledPanel)
        self.frame_39.setFrameShadow(QFrame.Raised)
        self.verticalLayout_52 = QVBoxLayout(self.frame_39)
        self.verticalLayout_52.setSpacing(0)
        self.verticalLayout_52.setObjectName(u"verticalLayout_52")
        self.verticalLayout_52.setContentsMargins(0, 0, 0, 0)
        self.frame_40 = QFrame(self.frame_39)
        self.frame_40.setObjectName(u"frame_40")
        self.frame_40.setMinimumSize(QSize(0, 25))
        self.frame_40.setMaximumSize(QSize(16777215, 25))
        self.frame_40.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_40.setFrameShape(QFrame.StyledPanel)
        self.frame_40.setFrameShadow(QFrame.Raised)
        self.verticalLayout_53 = QVBoxLayout(self.frame_40)
        self.verticalLayout_53.setObjectName(u"verticalLayout_53")
        self.verticalLayout_53.setContentsMargins(-1, 4, -1, 4)
        self.label_28 = QLabel(self.frame_40)
        self.label_28.setObjectName(u"label_28")
        self.label_28.setFont(font9)
        self.label_28.setAlignment(Qt.AlignCenter)

        self.verticalLayout_53.addWidget(self.label_28)


        self.verticalLayout_52.addWidget(self.frame_40)

        self.textBrowser_17 = QTextBrowser(self.frame_39)
        self.textBrowser_17.setObjectName(u"textBrowser_17")
        self.textBrowser_17.setMaximumSize(QSize(16777215, 16777215))

        self.verticalLayout_52.addWidget(self.textBrowser_17)


        self.horizontalLayout_19.addWidget(self.frame_39)

        self.frame_37 = QFrame(self.tab_time)
        self.frame_37.setObjectName(u"frame_37")
        self.frame_37.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_37.setFrameShape(QFrame.StyledPanel)
        self.frame_37.setFrameShadow(QFrame.Raised)
        self.verticalLayout_50 = QVBoxLayout(self.frame_37)
        self.verticalLayout_50.setSpacing(0)
        self.verticalLayout_50.setObjectName(u"verticalLayout_50")
        self.verticalLayout_50.setContentsMargins(0, 0, 0, 0)
        self.frame_38 = QFrame(self.frame_37)
        self.frame_38.setObjectName(u"frame_38")
        self.frame_38.setMinimumSize(QSize(0, 25))
        self.frame_38.setMaximumSize(QSize(16777215, 25))
        self.frame_38.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_38.setFrameShape(QFrame.StyledPanel)
        self.frame_38.setFrameShadow(QFrame.Raised)
        self.verticalLayout_51 = QVBoxLayout(self.frame_38)
        self.verticalLayout_51.setObjectName(u"verticalLayout_51")
        self.verticalLayout_51.setContentsMargins(-1, 4, -1, 4)
        self.label_27 = QLabel(self.frame_38)
        self.label_27.setObjectName(u"label_27")
        self.label_27.setFont(font9)
        self.label_27.setAlignment(Qt.AlignCenter)

        self.verticalLayout_51.addWidget(self.label_27)


        self.verticalLayout_50.addWidget(self.frame_38)

        self.textBrowser_16 = QTextBrowser(self.frame_37)
        self.textBrowser_16.setObjectName(u"textBrowser_16")

        self.verticalLayout_50.addWidget(self.textBrowser_16)


        self.horizontalLayout_19.addWidget(self.frame_37)

        self.frame_43 = QFrame(self.tab_time)
        self.frame_43.setObjectName(u"frame_43")
        self.frame_43.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_43.setFrameShape(QFrame.StyledPanel)
        self.frame_43.setFrameShadow(QFrame.Raised)
        self.verticalLayout_56 = QVBoxLayout(self.frame_43)
        self.verticalLayout_56.setSpacing(0)
        self.verticalLayout_56.setObjectName(u"verticalLayout_56")
        self.verticalLayout_56.setContentsMargins(0, 0, 0, 0)
        self.frame_44 = QFrame(self.frame_43)
        self.frame_44.setObjectName(u"frame_44")
        self.frame_44.setMinimumSize(QSize(0, 25))
        self.frame_44.setMaximumSize(QSize(16777215, 25))
        self.frame_44.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_44.setFrameShape(QFrame.StyledPanel)
        self.frame_44.setFrameShadow(QFrame.Raised)
        self.verticalLayout_57 = QVBoxLayout(self.frame_44)
        self.verticalLayout_57.setObjectName(u"verticalLayout_57")
        self.verticalLayout_57.setContentsMargins(-1, 4, -1, 4)
        self.label_30 = QLabel(self.frame_44)
        self.label_30.setObjectName(u"label_30")
        self.label_30.setFont(font9)
        self.label_30.setAlignment(Qt.AlignCenter)

        self.verticalLayout_57.addWidget(self.label_30)


        self.verticalLayout_56.addWidget(self.frame_44)

        self.textBrowser_19 = QTextBrowser(self.frame_43)
        self.textBrowser_19.setObjectName(u"textBrowser_19")

        self.verticalLayout_56.addWidget(self.textBrowser_19)


        self.horizontalLayout_19.addWidget(self.frame_43)

        self.frame_41 = QFrame(self.tab_time)
        self.frame_41.setObjectName(u"frame_41")
        self.frame_41.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_41.setFrameShape(QFrame.StyledPanel)
        self.frame_41.setFrameShadow(QFrame.Raised)
        self.verticalLayout_54 = QVBoxLayout(self.frame_41)
        self.verticalLayout_54.setSpacing(0)
        self.verticalLayout_54.setObjectName(u"verticalLayout_54")
        self.verticalLayout_54.setContentsMargins(0, 0, 0, 0)
        self.frame_42 = QFrame(self.frame_41)
        self.frame_42.setObjectName(u"frame_42")
        self.frame_42.setMinimumSize(QSize(0, 25))
        self.frame_42.setMaximumSize(QSize(16777215, 25))
        self.frame_42.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_42.setFrameShape(QFrame.StyledPanel)
        self.frame_42.setFrameShadow(QFrame.Raised)
        self.verticalLayout_55 = QVBoxLayout(self.frame_42)
        self.verticalLayout_55.setObjectName(u"verticalLayout_55")
        self.verticalLayout_55.setContentsMargins(-1, 4, -1, 4)
        self.label_29 = QLabel(self.frame_42)
        self.label_29.setObjectName(u"label_29")
        self.label_29.setFont(font9)
        self.label_29.setAlignment(Qt.AlignCenter)

        self.verticalLayout_55.addWidget(self.label_29)


        self.verticalLayout_54.addWidget(self.frame_42)

        self.textBrowser_18 = QTextBrowser(self.frame_41)
        self.textBrowser_18.setObjectName(u"textBrowser_18")

        self.verticalLayout_54.addWidget(self.textBrowser_18)


        self.horizontalLayout_19.addWidget(self.frame_41)

        self.tabWidget.addTab(self.tab_time, "")
        self.tab_set = QWidget()
        self.tab_set.setObjectName(u"tab_set")
        self.horizontalLayout_20 = QHBoxLayout(self.tab_set)
        self.horizontalLayout_20.setObjectName(u"horizontalLayout_20")
        self.horizontalLayout_20.setContentsMargins(0, 4, 0, 0)
        self.frame_47 = QFrame(self.tab_set)
        self.frame_47.setObjectName(u"frame_47")
        self.frame_47.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_47.setFrameShape(QFrame.StyledPanel)
        self.frame_47.setFrameShadow(QFrame.Raised)
        self.verticalLayout_60 = QVBoxLayout(self.frame_47)
        self.verticalLayout_60.setSpacing(0)
        self.verticalLayout_60.setObjectName(u"verticalLayout_60")
        self.verticalLayout_60.setContentsMargins(0, 0, 0, 0)
        self.frame_48 = QFrame(self.frame_47)
        self.frame_48.setObjectName(u"frame_48")
        self.frame_48.setMinimumSize(QSize(0, 25))
        self.frame_48.setMaximumSize(QSize(16777215, 25))
        self.frame_48.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_48.setFrameShape(QFrame.StyledPanel)
        self.frame_48.setFrameShadow(QFrame.Raised)
        self.verticalLayout_61 = QVBoxLayout(self.frame_48)
        self.verticalLayout_61.setObjectName(u"verticalLayout_61")
        self.verticalLayout_61.setContentsMargins(-1, 4, -1, 4)
        self.label_32 = QLabel(self.frame_48)
        self.label_32.setObjectName(u"label_32")
        self.label_32.setFont(font9)
        self.label_32.setAlignment(Qt.AlignCenter)

        self.verticalLayout_61.addWidget(self.label_32)


        self.verticalLayout_60.addWidget(self.frame_48)

        self.textBrowser_21 = QTextBrowser(self.frame_47)
        self.textBrowser_21.setObjectName(u"textBrowser_21")
        self.textBrowser_21.setMaximumSize(QSize(16777215, 16777215))
        self.textBrowser_21.setFont(font)

        self.verticalLayout_60.addWidget(self.textBrowser_21)


        self.horizontalLayout_20.addWidget(self.frame_47)

        self.frame_45 = QFrame(self.tab_set)
        self.frame_45.setObjectName(u"frame_45")
        self.frame_45.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_45.setFrameShape(QFrame.StyledPanel)
        self.frame_45.setFrameShadow(QFrame.Raised)
        self.verticalLayout_58 = QVBoxLayout(self.frame_45)
        self.verticalLayout_58.setSpacing(0)
        self.verticalLayout_58.setObjectName(u"verticalLayout_58")
        self.verticalLayout_58.setContentsMargins(0, 0, 0, 0)
        self.frame_46 = QFrame(self.frame_45)
        self.frame_46.setObjectName(u"frame_46")
        self.frame_46.setMinimumSize(QSize(0, 25))
        self.frame_46.setMaximumSize(QSize(16777215, 25))
        self.frame_46.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_46.setFrameShape(QFrame.StyledPanel)
        self.frame_46.setFrameShadow(QFrame.Raised)
        self.verticalLayout_59 = QVBoxLayout(self.frame_46)
        self.verticalLayout_59.setObjectName(u"verticalLayout_59")
        self.verticalLayout_59.setContentsMargins(-1, 4, -1, 4)
        self.label_31 = QLabel(self.frame_46)
        self.label_31.setObjectName(u"label_31")
        self.label_31.setFont(font9)
        self.label_31.setAlignment(Qt.AlignCenter)

        self.verticalLayout_59.addWidget(self.label_31)


        self.verticalLayout_58.addWidget(self.frame_46)

        self.textBrowser_20 = QTextBrowser(self.frame_45)
        self.textBrowser_20.setObjectName(u"textBrowser_20")

        self.verticalLayout_58.addWidget(self.textBrowser_20)


        self.horizontalLayout_20.addWidget(self.frame_45)

        self.frame_51 = QFrame(self.tab_set)
        self.frame_51.setObjectName(u"frame_51")
        self.frame_51.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_51.setFrameShape(QFrame.StyledPanel)
        self.frame_51.setFrameShadow(QFrame.Raised)
        self.verticalLayout_64 = QVBoxLayout(self.frame_51)
        self.verticalLayout_64.setSpacing(0)
        self.verticalLayout_64.setObjectName(u"verticalLayout_64")
        self.verticalLayout_64.setContentsMargins(0, 0, 0, 0)
        self.frame_52 = QFrame(self.frame_51)
        self.frame_52.setObjectName(u"frame_52")
        self.frame_52.setMinimumSize(QSize(0, 25))
        self.frame_52.setMaximumSize(QSize(16777215, 25))
        self.frame_52.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_52.setFrameShape(QFrame.StyledPanel)
        self.frame_52.setFrameShadow(QFrame.Raised)
        self.verticalLayout_65 = QVBoxLayout(self.frame_52)
        self.verticalLayout_65.setObjectName(u"verticalLayout_65")
        self.verticalLayout_65.setContentsMargins(-1, 4, -1, 4)
        self.label_34 = QLabel(self.frame_52)
        self.label_34.setObjectName(u"label_34")
        self.label_34.setFont(font9)
        self.label_34.setAlignment(Qt.AlignCenter)

        self.verticalLayout_65.addWidget(self.label_34)


        self.verticalLayout_64.addWidget(self.frame_52)

        self.textBrowser_23 = QTextBrowser(self.frame_51)
        self.textBrowser_23.setObjectName(u"textBrowser_23")

        self.verticalLayout_64.addWidget(self.textBrowser_23)


        self.horizontalLayout_20.addWidget(self.frame_51)

        self.frame_49 = QFrame(self.tab_set)
        self.frame_49.setObjectName(u"frame_49")
        self.frame_49.setStyleSheet(u"background-color: rgb(41, 45, 56);")
        self.frame_49.setFrameShape(QFrame.StyledPanel)
        self.frame_49.setFrameShadow(QFrame.Raised)
        self.verticalLayout_62 = QVBoxLayout(self.frame_49)
        self.verticalLayout_62.setSpacing(0)
        self.verticalLayout_62.setObjectName(u"verticalLayout_62")
        self.verticalLayout_62.setContentsMargins(0, 0, 0, 0)
        self.frame_50 = QFrame(self.frame_49)
        self.frame_50.setObjectName(u"frame_50")
        self.frame_50.setMinimumSize(QSize(0, 25))
        self.frame_50.setMaximumSize(QSize(16777215, 25))
        self.frame_50.setStyleSheet(u"background-color: rgb(31, 35, 43);")
        self.frame_50.setFrameShape(QFrame.StyledPanel)
        self.frame_50.setFrameShadow(QFrame.Raised)
        self.verticalLayout_63 = QVBoxLayout(self.frame_50)
        self.verticalLayout_63.setObjectName(u"verticalLayout_63")
        self.verticalLayout_63.setContentsMargins(-1, 4, -1, 4)
        self.label_33 = QLabel(self.frame_50)
        self.label_33.setObjectName(u"label_33")
        self.label_33.setFont(font9)
        self.label_33.setAlignment(Qt.AlignCenter)

        self.verticalLayout_63.addWidget(self.label_33)


        self.verticalLayout_62.addWidget(self.frame_50)

        self.textBrowser_22 = QTextBrowser(self.frame_49)
        self.textBrowser_22.setObjectName(u"textBrowser_22")
        self.textBrowser_22.setFont(font)

        self.verticalLayout_62.addWidget(self.textBrowser_22)


        self.horizontalLayout_20.addWidget(self.frame_49)

        self.tabWidget.addTab(self.tab_set, "")

        self.verticalLayout_25.addWidget(self.tabWidget)


        self.verticalLayout_12.addWidget(self.frame_12)


        self.verticalLayout_6.addWidget(self.frame_3)

        self.frame_2 = QFrame(self.page_settings)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setStyleSheet(u"")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_14 = QHBoxLayout(self.frame_2)
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.horizontalLayout_14.setContentsMargins(0, 0, 0, 0)
        self.frame_5 = QFrame(self.frame_2)
        self.frame_5.setObjectName(u"frame_5")
        self.frame_5.setMaximumSize(QSize(555555, 55555))
        self.frame_5.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_5.setFrameShape(QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QFrame.Raised)
        self.verticalLayout_17 = QVBoxLayout(self.frame_5)
        self.verticalLayout_17.setSpacing(0)
        self.verticalLayout_17.setObjectName(u"verticalLayout_17")
        self.verticalLayout_17.setContentsMargins(0, 0, 0, 0)
        self.frame_8 = QFrame(self.frame_5)
        self.frame_8.setObjectName(u"frame_8")
        self.frame_8.setMinimumSize(QSize(0, 35))
        self.frame_8.setMaximumSize(QSize(16777215, 35))
        self.frame_8.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_8.setFrameShape(QFrame.StyledPanel)
        self.frame_8.setFrameShadow(QFrame.Raised)
        self.verticalLayout_20 = QVBoxLayout(self.frame_8)
        self.verticalLayout_20.setObjectName(u"verticalLayout_20")
        self.verticalLayout_20.setContentsMargins(-1, 4, -1, 4)
        self.label_11 = QLabel(self.frame_8)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setFont(font7)

        self.verticalLayout_20.addWidget(self.label_11)


        self.verticalLayout_17.addWidget(self.frame_8)

        self.textBrowser = QTextBrowser(self.frame_5)
        self.textBrowser.setObjectName(u"textBrowser")
        self.textBrowser.setMaximumSize(QSize(55555, 55555))
        font10 = QFont()
        font10.setFamily(u"Segoe UI Light")
        font10.setPointSize(10)
        self.textBrowser.setFont(font10)

        self.verticalLayout_17.addWidget(self.textBrowser)


        self.horizontalLayout_14.addWidget(self.frame_5)

        self.frame_6 = QFrame(self.frame_2)
        self.frame_6.setObjectName(u"frame_6")
        self.frame_6.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_6.setFrameShape(QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QFrame.Raised)
        self.verticalLayout_18 = QVBoxLayout(self.frame_6)
        self.verticalLayout_18.setSpacing(0)
        self.verticalLayout_18.setObjectName(u"verticalLayout_18")
        self.verticalLayout_18.setContentsMargins(0, 0, 0, 0)
        self.frame_9 = QFrame(self.frame_6)
        self.frame_9.setObjectName(u"frame_9")
        self.frame_9.setMinimumSize(QSize(0, 35))
        self.frame_9.setMaximumSize(QSize(16777215, 35))
        self.frame_9.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.frame_9.setFrameShape(QFrame.StyledPanel)
        self.frame_9.setFrameShadow(QFrame.Raised)
        self.verticalLayout_21 = QVBoxLayout(self.frame_9)
        self.verticalLayout_21.setObjectName(u"verticalLayout_21")
        self.verticalLayout_21.setContentsMargins(-1, 4, -1, 4)
        self.label_12 = QLabel(self.frame_9)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setFont(font7)

        self.verticalLayout_21.addWidget(self.label_12)


        self.verticalLayout_18.addWidget(self.frame_9)

        self.textBrowser_2 = QTextBrowser(self.frame_6)
        self.textBrowser_2.setObjectName(u"textBrowser_2")
        self.textBrowser_2.setFont(font)

        self.verticalLayout_18.addWidget(self.textBrowser_2)


        self.horizontalLayout_14.addWidget(self.frame_6)

        self.frame_7 = QFrame(self.frame_2)
        self.frame_7.setObjectName(u"frame_7")
        self.frame_7.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_7.setFrameShape(QFrame.StyledPanel)
        self.frame_7.setFrameShadow(QFrame.Raised)
        self.verticalLayout_19 = QVBoxLayout(self.frame_7)
        self.verticalLayout_19.setSpacing(0)
        self.verticalLayout_19.setObjectName(u"verticalLayout_19")
        self.verticalLayout_19.setContentsMargins(0, 0, 0, 0)
        self.widget = QWidget(self.frame_7)
        self.widget.setObjectName(u"widget")
        self.widget.setMinimumSize(QSize(0, 35))
        self.widget.setMaximumSize(QSize(16777215, 35))
        self.widget.setStyleSheet(u"background-color: rgb(19, 20, 26);")
        self.verticalLayout_22 = QVBoxLayout(self.widget)
        self.verticalLayout_22.setObjectName(u"verticalLayout_22")
        self.verticalLayout_22.setContentsMargins(-1, 4, -1, 4)
        self.label_13 = QLabel(self.widget)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setFont(font7)

        self.verticalLayout_22.addWidget(self.label_13)


        self.verticalLayout_19.addWidget(self.widget)

        self.textBrowser_3 = QTextBrowser(self.frame_7)
        self.textBrowser_3.setObjectName(u"textBrowser_3")

        self.verticalLayout_19.addWidget(self.textBrowser_3)


        self.horizontalLayout_14.addWidget(self.frame_7)


        self.verticalLayout_6.addWidget(self.frame_2)

        self.stackedWidget.addWidget(self.page_settings)
        self.page_backup = QWidget()
        self.page_backup.setObjectName(u"page_backup")
        self.frame_4 = QFrame(self.page_backup)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setGeometry(QRect(10, 130, 882, 190))
        self.frame_4.setMinimumSize(QSize(0, 150))
        self.frame_4.setStyleSheet(u"background-color: rgb(39, 44, 54);\n"
"border-radius: 5px;")
        self.frame_4.setFrameShape(QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QFrame.Raised)
        self.verticalLayout_14 = QVBoxLayout(self.frame_4)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.checkBox_2 = QCheckBox(self.frame_4)
        self.checkBox_2.setObjectName(u"checkBox_2")
        self.checkBox_2.setAutoFillBackground(False)
        self.checkBox_2.setStyleSheet(u"")

        self.gridLayout_4.addWidget(self.checkBox_2, 0, 0, 1, 1)

        self.radioButton_2 = QRadioButton(self.frame_4)
        self.radioButton_2.setObjectName(u"radioButton_2")
        self.radioButton_2.setStyleSheet(u"")

        self.gridLayout_4.addWidget(self.radioButton_2, 0, 1, 1, 1)

        self.verticalSlider_2 = QSlider(self.frame_4)
        self.verticalSlider_2.setObjectName(u"verticalSlider_2")
        self.verticalSlider_2.setStyleSheet(u"")
        self.verticalSlider_2.setOrientation(Qt.Vertical)

        self.gridLayout_4.addWidget(self.verticalSlider_2, 0, 2, 3, 1)

        self.verticalScrollBar_2 = QScrollBar(self.frame_4)
        self.verticalScrollBar_2.setObjectName(u"verticalScrollBar_2")
        self.verticalScrollBar_2.setStyleSheet(u" QScrollBar:vertical {\n"
"	border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    width: 14px;\n"
"    margin: 21px 0 21px 0;\n"
"	border-radius: 0px;\n"
" }")
        self.verticalScrollBar_2.setOrientation(Qt.Vertical)

        self.gridLayout_4.addWidget(self.verticalScrollBar_2, 0, 4, 3, 1)

        self.scrollArea_2 = QScrollArea(self.frame_4)
        self.scrollArea_2.setObjectName(u"scrollArea_2")
        self.scrollArea_2.setStyleSheet(u"QScrollArea {\n"
"	border: none;\n"
"	border-radius: 0px;\n"
"}\n"
"QScrollBar:horizontal {\n"
"    border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    height: 14px;\n"
"    margin: 0px 21px 0 21px;\n"
"	border-radius: 0px;\n"
"}\n"
" QScrollBar:vertical {\n"
"	border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    width: 14px;\n"
"    margin: 21px 0 21px 0;\n"
"	border-radius: 0px;\n"
" }\n"
"")
        self.scrollArea_2.setFrameShape(QFrame.NoFrame)
        self.scrollArea_2.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea_2.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scrollArea_2.setWidgetResizable(True)
        self.scrollAreaWidgetContents_2 = QWidget()
        self.scrollAreaWidgetContents_2.setObjectName(u"scrollAreaWidgetContents_2")
        self.scrollAreaWidgetContents_2.setGeometry(QRect(0, 0, 218, 218))
        self.horizontalLayout_13 = QHBoxLayout(self.scrollAreaWidgetContents_2)
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.plainTextEdit_2 = QPlainTextEdit(self.scrollAreaWidgetContents_2)
        self.plainTextEdit_2.setObjectName(u"plainTextEdit_2")
        self.plainTextEdit_2.setMinimumSize(QSize(200, 200))
        self.plainTextEdit_2.setStyleSheet(u"QPlainTextEdit {\n"
"	background-color: rgb(27, 29, 35);\n"
"	border-radius: 5px;\n"
"	padding: 10px;\n"
"}\n"
"QPlainTextEdit:hover {\n"
"	border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"QPlainTextEdit:focus {\n"
"	border: 2px solid rgb(91, 101, 124);\n"
"}")

        self.horizontalLayout_13.addWidget(self.plainTextEdit_2)

        self.scrollArea_2.setWidget(self.scrollAreaWidgetContents_2)

        self.gridLayout_4.addWidget(self.scrollArea_2, 0, 5, 3, 1)

        self.comboBox_2 = QComboBox(self.frame_4)
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.addItem("")
        self.comboBox_2.setObjectName(u"comboBox_2")
        font11 = QFont()
        font11.setFamily(u"Segoe UI")
        font11.setPointSize(9)
        self.comboBox_2.setFont(font11)
        self.comboBox_2.setAutoFillBackground(False)
        self.comboBox_2.setStyleSheet(u"QComboBox{\n"
"	background-color: rgb(27, 29, 35);\n"
"	border-radius: 5px;\n"
"	border: 2px solid rgb(27, 29, 35);\n"
"	padding: 5px;\n"
"	padding-left: 10px;\n"
"}\n"
"QComboBox:hover{\n"
"	border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"	color: rgb(85, 170, 255);	\n"
"	background-color: rgb(27, 29, 35);\n"
"	padding: 10px;\n"
"	selection-background-color: rgb(39, 44, 54);\n"
"}")
        self.comboBox_2.setIconSize(QSize(16, 16))
        self.comboBox_2.setFrame(True)

        self.gridLayout_4.addWidget(self.comboBox_2, 1, 0, 1, 2)

        self.horizontalScrollBar_2 = QScrollBar(self.frame_4)
        self.horizontalScrollBar_2.setObjectName(u"horizontalScrollBar_2")
        sizePolicy8 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy8.setHorizontalStretch(0)
        sizePolicy8.setVerticalStretch(0)
        sizePolicy8.setHeightForWidth(self.horizontalScrollBar_2.sizePolicy().hasHeightForWidth())
        self.horizontalScrollBar_2.setSizePolicy(sizePolicy8)
        self.horizontalScrollBar_2.setStyleSheet(u"QScrollBar:horizontal {\n"
"    border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    height: 14px;\n"
"    margin: 0px 21px 0 21px;\n"
"	border-radius: 0px;\n"
"}\n"
"")
        self.horizontalScrollBar_2.setOrientation(Qt.Horizontal)

        self.gridLayout_4.addWidget(self.horizontalScrollBar_2, 1, 3, 1, 1)

        self.commandLinkButton_2 = QCommandLinkButton(self.frame_4)
        self.commandLinkButton_2.setObjectName(u"commandLinkButton_2")
        self.commandLinkButton_2.setStyleSheet(u"QCommandLinkButton {	\n"
"	color: rgb(85, 170, 255);\n"
"	border-radius: 5px;\n"
"	padding: 5px;\n"
"}\n"
"QCommandLinkButton:hover {	\n"
"	color: rgb(210, 210, 210);\n"
"	background-color: rgb(44, 49, 60);\n"
"}\n"
"QCommandLinkButton:pressed {	\n"
"	color: rgb(210, 210, 210);\n"
"	background-color: rgb(52, 58, 71);\n"
"}")
        icon9 = QIcon()
        icon9.addFile(u":/16x16/icons/16x16/cil-link.png", QSize(), QIcon.Normal, QIcon.Off)
        self.commandLinkButton_2.setIcon(icon9)

        self.gridLayout_4.addWidget(self.commandLinkButton_2, 1, 6, 1, 1)

        self.horizontalSlider_2 = QSlider(self.frame_4)
        self.horizontalSlider_2.setObjectName(u"horizontalSlider_2")
        self.horizontalSlider_2.setStyleSheet(u"")
        self.horizontalSlider_2.setOrientation(Qt.Horizontal)

        self.gridLayout_4.addWidget(self.horizontalSlider_2, 2, 0, 1, 2)


        self.verticalLayout_14.addLayout(self.gridLayout_4)

        self.frame_div_content_3 = QFrame(self.page_backup)
        self.frame_div_content_3.setObjectName(u"frame_div_content_3")
        self.frame_div_content_3.setGeometry(QRect(20, 370, 882, 110))
        self.frame_div_content_3.setMinimumSize(QSize(0, 110))
        self.frame_div_content_3.setMaximumSize(QSize(16777215, 110))
        self.frame_div_content_3.setStyleSheet(u"background-color: rgb(41, 45, 56);\n"
"border-radius: 5px;\n"
"")
        self.frame_div_content_3.setFrameShape(QFrame.NoFrame)
        self.frame_div_content_3.setFrameShadow(QFrame.Raised)
        self.verticalLayout_23 = QVBoxLayout(self.frame_div_content_3)
        self.verticalLayout_23.setSpacing(0)
        self.verticalLayout_23.setObjectName(u"verticalLayout_23")
        self.verticalLayout_23.setContentsMargins(0, 0, 0, 0)
        self.frame_title_wid_3 = QFrame(self.frame_div_content_3)
        self.frame_title_wid_3.setObjectName(u"frame_title_wid_3")
        self.frame_title_wid_3.setMaximumSize(QSize(16777215, 35))
        self.frame_title_wid_3.setStyleSheet(u"background-color: rgb(39, 44, 54);")
        self.frame_title_wid_3.setFrameShape(QFrame.StyledPanel)
        self.frame_title_wid_3.setFrameShadow(QFrame.Raised)
        self.verticalLayout_24 = QVBoxLayout(self.frame_title_wid_3)
        self.verticalLayout_24.setObjectName(u"verticalLayout_24")
        self.labelBoxBlenderInstalation_3 = QLabel(self.frame_title_wid_3)
        self.labelBoxBlenderInstalation_3.setObjectName(u"labelBoxBlenderInstalation_3")
        self.labelBoxBlenderInstalation_3.setFont(font1)
        self.labelBoxBlenderInstalation_3.setStyleSheet(u"")

        self.verticalLayout_24.addWidget(self.labelBoxBlenderInstalation_3)


        self.verticalLayout_23.addWidget(self.frame_title_wid_3)

        self.frame_content_wid_3 = QFrame(self.frame_div_content_3)
        self.frame_content_wid_3.setObjectName(u"frame_content_wid_3")
        self.frame_content_wid_3.setFrameShape(QFrame.NoFrame)
        self.frame_content_wid_3.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_16 = QHBoxLayout(self.frame_content_wid_3)
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.gridLayout_5 = QGridLayout()
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.gridLayout_5.setContentsMargins(-1, -1, -1, 0)
        self.lineEdit_3 = QLineEdit(self.frame_content_wid_3)
        self.lineEdit_3.setObjectName(u"lineEdit_3")
        self.lineEdit_3.setMinimumSize(QSize(0, 30))
        self.lineEdit_3.setStyleSheet(u"QLineEdit {\n"
"	background-color: rgb(27, 29, 35);\n"
"	border-radius: 5px;\n"
"	border: 2px solid rgb(27, 29, 35);\n"
"	padding-left: 10px;\n"
"}\n"
"QLineEdit:hover {\n"
"	border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"QLineEdit:focus {\n"
"	border: 2px solid rgb(91, 101, 124);\n"
"}")

        self.gridLayout_5.addWidget(self.lineEdit_3, 0, 0, 1, 1)

        self.pushButton_3 = QPushButton(self.frame_content_wid_3)
        self.pushButton_3.setObjectName(u"pushButton_3")
        self.pushButton_3.setMinimumSize(QSize(150, 30))
        self.pushButton_3.setFont(font11)
        self.pushButton_3.setStyleSheet(u"QPushButton {\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	border-radius: 5px;	\n"
"	background-color: rgb(52, 59, 72);\n"
"}\n"
"QPushButton:hover {\n"
"	background-color: rgb(57, 65, 80);\n"
"	border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"QPushButton:pressed {	\n"
"	background-color: rgb(35, 40, 49);\n"
"	border: 2px solid rgb(43, 50, 61);\n"
"}")
        icon10 = QIcon()
        icon10.addFile(u":/16x16/icons/16x16/cil-folder-open.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_3.setIcon(icon10)

        self.gridLayout_5.addWidget(self.pushButton_3, 0, 1, 1, 1)

        self.labelVersion_5 = QLabel(self.frame_content_wid_3)
        self.labelVersion_5.setObjectName(u"labelVersion_5")
        self.labelVersion_5.setStyleSheet(u"color: rgb(98, 103, 111);")
        self.labelVersion_5.setLineWidth(1)
        self.labelVersion_5.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout_5.addWidget(self.labelVersion_5, 1, 0, 1, 2)


        self.horizontalLayout_16.addLayout(self.gridLayout_5)


        self.verticalLayout_23.addWidget(self.frame_content_wid_3)

        self.tabWidget_2 = QTabWidget(self.page_backup)
        self.tabWidget_2.setObjectName(u"tabWidget_2")
        self.tabWidget_2.setGeometry(QRect(260, 50, 521, 371))
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.tabWidget_2.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.tabWidget_2.addTab(self.tab_2, "")
        self.stackedWidget.addWidget(self.page_backup)

        self.verticalLayout_9.addWidget(self.stackedWidget)


        self.verticalLayout_4.addWidget(self.frame_content)

        self.frame_grip = QFrame(self.frame_content_right)
        self.frame_grip.setObjectName(u"frame_grip")
        self.frame_grip.setMinimumSize(QSize(0, 25))
        self.frame_grip.setMaximumSize(QSize(16777215, 25))
        self.frame_grip.setStyleSheet(u"background-color: rgb(32, 35, 43);")
        self.frame_grip.setFrameShape(QFrame.NoFrame)
        self.frame_grip.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_6 = QHBoxLayout(self.frame_grip)
        self.horizontalLayout_6.setSpacing(0)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(0, 0, 2, 0)
        self.frame_label_bottom = QFrame(self.frame_grip)
        self.frame_label_bottom.setObjectName(u"frame_label_bottom")
        self.frame_label_bottom.setFrameShape(QFrame.NoFrame)
        self.frame_label_bottom.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_7 = QHBoxLayout(self.frame_label_bottom)
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalLayout_7.setContentsMargins(10, 0, 10, 0)
        self.label_credits = QLabel(self.frame_label_bottom)
        self.label_credits.setObjectName(u"label_credits")
        self.label_credits.setFont(font2)
        self.label_credits.setStyleSheet(u"color: rgb(98, 103, 111);")

        self.horizontalLayout_7.addWidget(self.label_credits)

        self.label_version = QLabel(self.frame_label_bottom)
        self.label_version.setObjectName(u"label_version")
        self.label_version.setMaximumSize(QSize(100, 16777215))
        self.label_version.setFont(font2)
        self.label_version.setStyleSheet(u"color: rgb(98, 103, 111);")
        self.label_version.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_7.addWidget(self.label_version)


        self.horizontalLayout_6.addWidget(self.frame_label_bottom)

        self.frame_size_grip = QFrame(self.frame_grip)
        self.frame_size_grip.setObjectName(u"frame_size_grip")
        self.frame_size_grip.setMaximumSize(QSize(20, 20))
        self.frame_size_grip.setStyleSheet(u"QSizeGrip {\n"
"	background-image: url(:/16x16/icons/16x16/cil-size-grip.png);\n"
"	background-position: center;\n"
"	background-repeat: no-reperat;\n"
"}")
        self.frame_size_grip.setFrameShape(QFrame.NoFrame)
        self.frame_size_grip.setFrameShadow(QFrame.Raised)

        self.horizontalLayout_6.addWidget(self.frame_size_grip)


        self.verticalLayout_4.addWidget(self.frame_grip)


        self.horizontalLayout_2.addWidget(self.frame_content_right)


        self.verticalLayout.addWidget(self.frame_center)


        self.horizontalLayout.addWidget(self.frame_main)

        MainWindow.setCentralWidget(self.centralwidget)
        QWidget.setTabOrder(self.btn_minimize, self.btn_maximize_restore)
        QWidget.setTabOrder(self.btn_maximize_restore, self.btn_close)
        QWidget.setTabOrder(self.btn_close, self.btn_toggle_menu)

        self.retranslateUi(MainWindow)

        self.stackedWidget.setCurrentIndex(1)
        self.tabWidget.setCurrentIndex(4)
        self.tabWidget_2.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.btn_toggle_menu.setText("")
        self.label_title_bar_top.setText(QCoreApplication.translate("MainWindow", u"Main Window - Base", None))
#if QT_CONFIG(tooltip)
        self.btn_minimize.setToolTip(QCoreApplication.translate("MainWindow", u"Minimize", None))
#endif // QT_CONFIG(tooltip)
        self.btn_minimize.setText("")
#if QT_CONFIG(tooltip)
        self.btn_maximize_restore.setToolTip(QCoreApplication.translate("MainWindow", u"Maximize", None))
#endif // QT_CONFIG(tooltip)
        self.btn_maximize_restore.setText("")
#if QT_CONFIG(tooltip)
        self.btn_close.setToolTip(QCoreApplication.translate("MainWindow", u"Close", None))
#endif // QT_CONFIG(tooltip)
        self.btn_close.setText("")
        self.scope_ind_light.setText("")
        self.scope_ind_label.setText(QCoreApplication.translate("MainWindow", u"Scope desconectado", None))
        self.gen_ind_light.setText("")
        self.gen_ind_label.setText(QCoreApplication.translate("MainWindow", u"Gerador desconectado", None))
        self.label_top_info_2.setText(QCoreApplication.translate("MainWindow", u"| SETTINGS", None))
        self.label_user_icon.setText(QCoreApplication.translate("MainWindow", u"WM", None))
        self.embGerBt.setText(QCoreApplication.translate("MainWindow", u"SHUFFLE\n"
"GENERATOR", None))
        self.embOscBt.setText(QCoreApplication.translate("MainWindow", u"SHUFFLE\n"
"OSCILLOSCOPE", None))
        self.respostaBt.setText(QCoreApplication.translate("MainWindow", u"ANSWER\n"
"CHECK", None))
        self.label_39.setText(QCoreApplication.translate("MainWindow", u"RIGHT ANSWER", None))
        self.label_47.setText(QCoreApplication.translate("MainWindow", u"Waveform", None))
        self.label_48.setText(QCoreApplication.translate("MainWindow", u"Frequency", None))
        self.label_49.setText(QCoreApplication.translate("MainWindow", u"Amplitude", None))
        self.label_41.setText(QCoreApplication.translate("MainWindow", u"DIFFICULTY", None))
        self.facilBt.setText(QCoreApplication.translate("MainWindow", u"Easy", None))
        self.medioBt.setText(QCoreApplication.translate("MainWindow", u"Medium", None))
        self.dificilBt.setText(QCoreApplication.translate("MainWindow", u"Hard", None))
        self.label_45.setText(QCoreApplication.translate("MainWindow", u"GRADE", None))
        self.notaLabel.setText(QCoreApplication.translate("MainWindow", u"10", None))
        self.tutorialBt.setText(QCoreApplication.translate("MainWindow", u"Tutorial", None))
        self.label_40.setText(QCoreApplication.translate("MainWindow", u"YOUR ANSWER", None))
        self.userRespFreqVerif.setText("")
        self.userRespOndaVerif.setText("")
        self.userRespAmpVerif.setText("")
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"Amplitude", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"Frequency", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Waveform", None))
        self.label_46.setText(QCoreApplication.translate("MainWindow", u"INSTRUCTIONS", None))
        self.textBrowser_24.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">How to use</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Using a BNC-BNC cable, connect the CH1 of the signal generator to the CH1 of the oscilloscope;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Turn on the signal generator and the oscilloscope;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0"
                        "; text-indent:0px;\">- Connect the equipment to the computer using the USB ports;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Start the USB communication by clicking on the respective &quot;connect&quot; buttons in the menu &quot;settings&quot;;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Click on the button to shuffle the generator and the oscilloscope (you can choose to shuffle just one of them);</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Fill the fields in &quot;Your answer&quot;;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Click on &quot;answer check&quot;;</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-righ"
                        "t:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Improving your skills:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Do not look at the generator display;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Do not use the auto-set;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- Do not use the measurements menu, try to read the voltage and frequency using the cursors or even better using the scale on the screen;</p></body></html>", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"PARAMETERS", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Frequency:", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"Start:", None))
        self.label_35.setText(QCoreApplication.translate("MainWindow", u"Stop:", None))
        self.label_36.setText(QCoreApplication.translate("MainWindow", u"Samples:", None))
        self.label_37.setText(QCoreApplication.translate("MainWindow", u"PROGRESS", None))
        self.statusFreq.setText(QCoreApplication.translate("MainWindow", u"Holding", None))
        self.startBt_Freq.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.stopBt_Freq.setText(QCoreApplication.translate("MainWindow", u"Stop", None))
        self.novoBt_Freq.setText(QCoreApplication.translate("MainWindow", u"New", None))
        self.salvarBt_Freq.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"INPUT", None))
        self.canalUmCheck.setText(QCoreApplication.translate("MainWindow", u"CH1", None))
        self.canalDoisCheck.setText(QCoreApplication.translate("MainWindow", u"CH2", None))
        self.label_44.setText(QCoreApplication.translate("MainWindow", u"TRIGGER", None))
        self.triggerExt.setText(QCoreApplication.translate("MainWindow", u"External", None))
        self.triggerCh1.setText(QCoreApplication.translate("MainWindow", u"CH1", None))
        self.triggerCh2.setText(QCoreApplication.translate("MainWindow", u"CH2", None))
        self.triggerLinha.setText(QCoreApplication.translate("MainWindow", u"Line (60Hz)", None))
        self.label_43.setText(QCoreApplication.translate("MainWindow", u"ACQUIRE", None))
        self.mediaRBt.setText(QCoreApplication.translate("MainWindow", u"Average", None))
        self.amostraRBt.setText(QCoreApplication.translate("MainWindow", u"Sample", None))
        self.label_42.setText(QCoreApplication.translate("MainWindow", u"PLOT", None))
        self.vtRBt.setText(QCoreApplication.translate("MainWindow", u"V x Time", None))
        self.ivRBt.setText(QCoreApplication.translate("MainWindow", u"Ch1 x Ch2", None))
        self.ch12ch2RBt.setText(QCoreApplication.translate("MainWindow", u"(Ch1 - Ch2) x Ch2", None))
        self.capturarBt_Cap.setText(QCoreApplication.translate("MainWindow", u"Get", None))
        self.salvarBt_Cap.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.label_38.setText(QCoreApplication.translate("MainWindow", u"INPUT", None))
        self.ch1_Fft.setText(QCoreApplication.translate("MainWindow", u"CH1", None))
        self.ch2_Fft.setText(QCoreApplication.translate("MainWindow", u"CH2", None))
        self.startBt_Fft.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.salvarBt_Fft.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.labelBoxBlenderInstalation.setText(QCoreApplication.translate("MainWindow", u"CONNECTIONS", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"SCOPE MODEL", None))
        self.osciloscopioStatus.setText(QCoreApplication.translate("MainWindow", u"DESCONECTADO", None))
        self.conectarBtOsc.setText("")
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"GENERATOR MODEL", None))
        self.geradorStatus.setText(QCoreApplication.translate("MainWindow", u"DESCONECTADO", None))
        self.conectarBtGer.setText("")
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"HELP", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"GENERAL", None))
        self.textBrowser_4.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">The purpose of the exercise is to scramble the oscilloscope settings and wait for the student to set the parameters to make a good reading of the signal.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">Connect the CH1 of the signal generator to the CH1 of the oscilloscope using a BNC-BNC cable. Follow the directions on the screen.</span></p></body></html>", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"INPUT OPTIONS", None))
        self.label_17.setText(QCoreApplication.translate("MainWindow", u"OUTPUT OPTIONS", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"TROUBLESHOOTING", None))
        self.textBrowser_7.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">If you are in no way able to find the signal on the screen, check: </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">if the CH1 of the generator is on; </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">if the oscill"
                        "oscope's CH1 is on; </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">if the cable connecting the equipment is working properly;</span></p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_exerc), QCoreApplication.translate("MainWindow", u"EXERCISES", None))
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"GENERAL", None))
        self.textBrowser_9.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">Routine for signal acquisition in the frequency space. This routine gets the oscilloscope read, calculates the Fast Fourier Transform (FFT) and plot the result.</span></p></body></html>", None))
        self.label_19.setText(QCoreApplication.translate("MainWindow", u"INPUT OPTIONS", None))
        self.textBrowser_8.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">It is possible to select between acquisition of channel 1 or 2 of the oscilloscope.</span></p></body></html>", None))
        self.label_22.setText(QCoreApplication.translate("MainWindow", u"OUTPUT OPTIONS", None))
        self.textBrowser_11.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">The following files will be saved in the folder and with the name selected by the user:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">\u2018_Dados.csv\u2019 - text like file containing the intensities of each frequency;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">"
                        "<span style=\" font-family:'Segoe UI'; font-size:10pt;\">\u2018_Grafico.png\u2019 - image file with the same graphic ploted on the PyLab window;</span></p></body></html>", None))
        self.label_21.setText(QCoreApplication.translate("MainWindow", u"TROUBLESHOOTING", None))
        self.textBrowser_10.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">The time scale on the oscilloscope significantly affects the result of the FFT calculation. If the result is not as expected, check other scales.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">The files will be overwritten if they have the same name.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block"
                        "-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">Unsaved acquisitions will be permanently lost.</span></p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_fft), QCoreApplication.translate("MainWindow", u"FFT", None))
        self.label_24.setText(QCoreApplication.translate("MainWindow", u"GENERAL", None))
        self.textBrowser_13.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Routine for studie the circuit as a function of the frequency. This routine applies different frequencies of sinusoidal voltage signal and gets the oscilloscope reading in channels 1 and 2 (respectively input and output of the circuit), calculates the transmittance and the phase shift between the signals and plots the \u201cBode diagram\u201d (transmittance in db as a function of frequency) and also the phase shift as a frequency function.</p></body></html>", None))
        self.label_23.setText(QCoreApplication.translate("MainWindow", u"INPUT OPTIONS", None))
        self.textBrowser_12.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">It is possible to select the start frequency, the final frequency and number of samples. The generator voltage is not changed by the program and must be configured by the operator.</p></body></html>", None))
        self.label_26.setText(QCoreApplication.translate("MainWindow", u"OUTPUT OPTIONS", None))
        self.textBrowser_15.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The following files will be saved in the folder and name selected by the user:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\u2018_Dados.csv\u2019 - text like file whit the peak-to-peak voltages of the two channels of the oscilloscope, the phase shift between the signals and the db transmittance of each sampled frequency;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\u2018_Grafico.png\u2019 - imag"
                        "e file with the same graphic ploted on the PyLab window;</p></body></html>", None))
        self.label_25.setText(QCoreApplication.translate("MainWindow", u"TROUBLESHOOTING", None))
        self.textBrowser_14.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">PyLab use the channel 1 of the oscilloscope as the circuit  input and channel 2 as the output. Changing this configuration brings some math problems.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The user must use channel 1 of the signal generator. The user must also select the appropriate voltage and manually turn on the generator output.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The files will"
                        " be overwritten if they have the same name.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Unsaved acquisitions will be permanently lost.</p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_freq), QCoreApplication.translate("MainWindow", u"FREQUENCY RESPONSE", None))
        self.label_28.setText(QCoreApplication.translate("MainWindow", u"GENERAL", None))
        self.textBrowser_17.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">Routine to get the oscilloscope read.</span></p></body></html>", None))
        self.label_27.setText(QCoreApplication.translate("MainWindow", u"INPUT OPTIONS", None))
        self.textBrowser_16.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">It is possible to choose between read both channels or just channel 1 or 2. There is possible to acquire by sample or predefined averages. The triger can be taked from any of both channels, externally or using the 60Hz of the power line. The V x Time and Ch1 X Ch2 plots uses the last acquisition and can be switched without needing a new capture.</span></p></body></html>", None))
        self.label_30.setText(QCoreApplication.translate("MainWindow", u"OUTPUT OPTIONS", None))
        self.textBrowser_19.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">The following files will be saved in the folder and with the name selected by the user:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">\u2018_Dados.csv\u2019 - text file containing the voltages as a function of time of the two channels of the oscilloscope;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt"
                        "-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">\u2018_GraficoVT.png\u2019 - image file with the voltage plot by time as the displayed on the PyLab window;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">\u2018_GraficoIV.png\u2019 - image file with the Ch1 X Ch2 plot as the displayed on the PyLab window;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">\u2018_GraficoCh1-Ch2xCh2.png\u2019 - image file with the (Ch1 - Ch2) X Ch2 plot as the displayed on the PyLab window;</span></p></body></html>", None))
        self.label_29.setText(QCoreApplication.translate("MainWindow", u"TROUBLESHOOTING", None))
        self.textBrowser_18.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">PyLab will display the same signal as shown on the oscilloscope screen but with the scale maximized, so the graphics on the oscilloscope screen and the PyLab window may be slightly different.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">The Ch1 X Ch2 plot is done mathematically with the read of the two channels, so the capture must be placed with both c"
                        "hannels enabled.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">The output file with the sample points data has three columns: Time, Voltage at Ch1 and Voltage at Ch2.</span></p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_time), QCoreApplication.translate("MainWindow", u"TIME RESPONSE", None))
        self.label_32.setText(QCoreApplication.translate("MainWindow", u"GENERAL", None))
        self.textBrowser_21.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The settings window establishes communication with the generator and oscilloscope. It also provides additional information about the Pylab program.</p></body></html>", None))
        self.label_31.setText(QCoreApplication.translate("MainWindow", u"INPUT OPTIONS", None))
        self.label_34.setText(QCoreApplication.translate("MainWindow", u"OUTPUT OPTIONS", None))
        self.label_33.setText(QCoreApplication.translate("MainWindow", u"TROUBLESHOOTING", None))
        self.textBrowser_22.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Having problems when establishing communication with the signal generator or the oscilloscope, try one or more of the following items:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- disconnect and reconnect the USB cables;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- restart the equipment;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:"
                        "0; text-indent:0px;\">- restart the Pylab program;</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'MS Shell Dlg 2';\">- restart the computer;</span></p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_set), QCoreApplication.translate("MainWindow", u"SETTINGS", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"AUTHORS", None))
        self.textBrowser.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI Light'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI';\">Developed by Vladimir Gaal and professor Dr. Gustavo S. Wiederhecker. Written and compiled by Vladimir Gaal.</span></p></body></html>", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"ACKNOWLEDGEMENTS", None))
        self.textBrowser_2.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">To icons8.com for providing some icons for free.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">To all professors of F429 and F540 classes for their suggestions and ideas.</p></body></html>", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"REVIEWS", None))
        self.textBrowser_3.setHtml(QCoreApplication.translate("MainWindow", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">0.1 - Initial development</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">0.2 - New screen reading routines implemented</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">0.3 - Usage tips implemented</span></p>\n"
"<"
                        "p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">0.4 - Implemented plot of (Ch1 - Ch2) x Ch2</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">0.5 - Changing the color theme of the screen capture mode and correction of the capture in simple channel</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">0.6 - Initial implementation of the exercises</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">1.0 - Implementation of a new graphical interface</span></p>\n"
"<p style"
                        "=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Segoe UI'; font-size:10pt;\">2.0 - Implementation of a modern layout to graphical interface and upgrade to Qt5</span></p></body></html>", None))
        self.checkBox_2.setText(QCoreApplication.translate("MainWindow", u"CheckBox", None))
        self.radioButton_2.setText(QCoreApplication.translate("MainWindow", u"RadioButton", None))
        self.comboBox_2.setItemText(0, QCoreApplication.translate("MainWindow", u"Test 1", None))
        self.comboBox_2.setItemText(1, QCoreApplication.translate("MainWindow", u"Test 2", None))
        self.comboBox_2.setItemText(2, QCoreApplication.translate("MainWindow", u"Test 3", None))

        self.commandLinkButton_2.setText(QCoreApplication.translate("MainWindow", u"CommandLinkButton", None))
        self.commandLinkButton_2.setDescription(QCoreApplication.translate("MainWindow", u"Open External Link", None))
        self.labelBoxBlenderInstalation_3.setText(QCoreApplication.translate("MainWindow", u"BLENDER INSTALLATION", None))
        self.lineEdit_3.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Your Password", None))
        self.pushButton_3.setText(QCoreApplication.translate("MainWindow", u"Open Blender", None))
        self.labelVersion_5.setText(QCoreApplication.translate("MainWindow", u"Ex: C:Program FilesBlender FoundationBlender 2.82 blender.exe", None))
        self.tabWidget_2.setTabText(self.tabWidget_2.indexOf(self.tab), QCoreApplication.translate("MainWindow", u"Tab 1", None))
        self.tabWidget_2.setTabText(self.tabWidget_2.indexOf(self.tab_2), QCoreApplication.translate("MainWindow", u"Tab 2", None))
        self.label_credits.setText(QCoreApplication.translate("MainWindow", u"Registered by: Wanderson M. Pimenta", None))
        self.label_version.setText(QCoreApplication.translate("MainWindow", u"v1.0.0", None))
    # retranslateUi

