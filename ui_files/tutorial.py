# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'tutorial.ui'
#
# Created: Fri Feb 28 15:14:47 2020
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(551, 750)
        Dialog.setMinimumSize(QtCore.QSize(551, 750))
        Dialog.setMaximumSize(QtCore.QSize(551, 750))
        Dialog.setStyleSheet("QDialog{\n"
"    background-color:White;\n"
"}\n"
"QGroupBox {\n"
"    background-color:transparent;\n"
"    border: 2px solid rgb(0, 0, 0);\n"
"    border-radius: 5px;\n"
"    margin-top: 1ex; \n"
"font-weight: bold;\n"
"font-size: 15px\n"
"}\n"
"\n"
"QGroupBox::title{\n"
"subcontrol-origin: margin;\n"
"subcontrol-position: top left;\n"
"padding: 0 5px;\n"
"color: rgb(0, 0, 0);\n"
"}\n"
"\n"
"QPushButton{\n"
"background-color: Transparent; \n"
"border-style: outset;\n"
"    border-width: 2px;\n"
"    border-radius: 0px;\n"
"    border-color: rgb(127, 127, 127);\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"border-style: inset;\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"background-color:qradialgradient(spread:pad, cx:0.131182, cy:0.108, radius:1.262, fx:0.13, fy:0.142, stop:0.357955 rgba(0, 0, 0, 0), stop:1 rgba(0, 0, 0, 14));\n"
"}\n"
"\n"
"\n"
"QToolButton{\n"
"background-color: Transparent; \n"
"border-style: outset;\n"
"    border-width: 2px;\n"
"    border-radius: 0px;\n"
"    border-color: rgb(127, 127, 127);\n"
"}\n"
"\n"
"QToolButton:pressed{\n"
"border-style: inset;\n"
"}\n"
"\n"
"QToolButton:hover{\n"
"background-color:qradialgradient(spread:pad, cx:0.131182, cy:0.108, radius:1.262, fx:0.13, fy:0.142, stop:0.357955 rgba(0, 0, 0, 0), stop:1 rgba(0, 0, 0, 14));\n"
"}")
        self.okBt = QtGui.QPushButton(Dialog)
        self.okBt.setGeometry(QtCore.QRect(10, 700, 531, 41))
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setWeight(75)
        font.setBold(True)
        self.okBt.setFont(font)
        self.okBt.setObjectName("okBt")
        self.textBrowser = QtGui.QTextBrowser(Dialog)
        self.textBrowser.setGeometry(QtCore.QRect(10, 130, 531, 561))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        self.textBrowser.setFont(font)
        self.textBrowser.setFrameShape(QtGui.QFrame.NoFrame)
        self.textBrowser.setObjectName("textBrowser")
        self.label_3 = QtGui.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(220, 80, 151, 31))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        self.label_3.setFont(font)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, -10, 221, 141))
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap(":/geral/icons/logomenor.png"))
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.label_2 = QtGui.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(220, 20, 151, 51))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(32)
        self.label_2.setFont(font)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.videoBt = QtGui.QToolButton(Dialog)
        self.videoBt.setGeometry(QtCore.QRect(360, 10, 181, 111))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.videoBt.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/geral/icons/youtube.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.videoBt.setIcon(icon)
        self.videoBt.setIconSize(QtCore.QSize(100, 100))
        self.videoBt.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.videoBt.setObjectName("videoBt")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.okBt.setText(QtGui.QApplication.translate("Dialog", "OK", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Tahoma\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:16pt; font-weight:600;\">Check List para inicialização dos osciloscópios</span></p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'MS Shell Dlg 2\'; font-size:16pt; font-weight:600;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">1 - Verifique os cabos e conexões entre a fonte de sinal e o osciloscópio;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">2 - Coloque canal 1 e 2 em &quot;acoplamento: terra&quot; usando o menu do respectivo canal;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">3 - No menu do respectivo canal verifique em </span><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt; font-style:italic;\">sonda</span><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\"> se o sinal não está sendo multiplicado;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">4 - Alterne o botão RUN/STOP para verificar se o osciloscópio está fazendo leitura;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">5 - Usando o botão </span><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt; font-style:italic;\">posição</span><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\"> do respectivo canal coloque CH1 e CH2 no meio da tela (ver seta com respectivo número na esquerda da tela);</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">6 - Coloque o canal de tempo em aproximadamente 10ms/div;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">7 - Usando o botão posição do canal de tempo coloque o sinal no meio da tela(ver seta branca na parte superior da tela);</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">8 - Coloque 5V/div nos canais 1 e 2;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">9 - No menu do respectivo canal coloque CH1 e CH2 em &quot;acoplamento: CC&quot;;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">10 - Desligue o canal que não está sendo usado;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">11 - No menu de Triger use origem: CH1; Modo: auto; Tipo: borda; Acoplamento: CC ;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">12 - Regule a escala de tensão do canal em uso para um valor que maximize o sinal na tela;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">13 - Acerte o </span><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt; font-style:italic;\">nível</span><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\"> do triger até estabilizar o sinal na tela (ver seta na direita da tela);</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">14 - Regule a escala de tempo (seg/div) até obter no mínimo um período do sinal na tela;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:12pt;\">15 - Se o sinal não estabilizar na tela a escala de tempo pode estar em algum múltiplo do sinal amostrado;</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Dialog", "V 1.0", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Dialog", "PyLab", None, QtGui.QApplication.UnicodeUTF8))
        self.videoBt.setText(QtGui.QApplication.translate("Dialog", "Video \n"
"tutorial", None, QtGui.QApplication.UnicodeUTF8))

import imagem_rc
