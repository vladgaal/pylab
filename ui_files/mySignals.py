from PySide.QtGui import *
from PySide.QtCore import *
from PySide import QtCore, QtGui


class MySignal(QObject):
    status = Signal(str)
    falha = Signal(str)
    medida = Signal(float, float, float, float, int)
    terminado = Signal(str)
    terminadoTela = Signal(str)
    terminadoFft = Signal(str)
    tela = Signal(object, object, object)
    fftSignal = Signal(object, object)
    resultado = Signal(int, int, int)