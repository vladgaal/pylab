import numpy as np
from app_modules import *
import visa
import pylef
import time

__modulo__ = 'T'


class MySignal(QObject):
    __nome__ = __modulo__ + '-' + 'MySignal'
    mensagem = 'Setting the signals'
    print('{};{}'.format(__nome__, mensagem))

    status = Signal(str)
    faultSignal = Signal(str)
    measure = Signal(float, float, float, float, int)
    finished = Signal(str)
    finishedTela = Signal(str)
    finishedFft = Signal(str)
    screen = Signal(object, object, object)
    fftSignal = Signal(object, object)
    result = Signal(int, int, int)


class EquipamentosThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        __nome__ = __modulo__ + '-' + 'EquipamentosThread'
        mensagem = 'Setting the Equipment Thread'
        print('{};{}'.format(__nome__, mensagem))

        self.conectarScope = False
        self.conectarGerador = False
        self.desconectarScope = False
        self.desconectarGerador = False
        self.varrerFreq = False
        self.capturarTela = False
        self.scopeConectado = False
        self.geradorConectado = False
        self.interromperFreq = False
        self.fft = False
        self.embaralharGerador = False
        self.embaralharOsciloscopio = False
        self.verificarResposta = False
        self.dificuldade = 0
        self.freqInicial = 0
        self.freqFinal = 0
        self.quantidadePontos = 0
        self.medias = 4
        self.signal = MySignal()
        self.leitura = 0
        self.trigger = 'CH1'
        self.ch1 = True
        self.ch2 = True
        self.tempo = True
        self.tensaoVetNum = [1, 2, 5]
        self.tensaoVetExp = [-1, 0, -2]
        self.freqVetNum = [1, 2, 5]
        self.freqVetExp = [0, 1, 2, 3, 4, 5, 6]
        self.ondaVet = ["SINE", "SQUARE", "RAMP"]
        self.trigVet = ["CH1", "CH2", "AC LINE"]
        self.scalaTensaoVet = [5, 1, 0.1, 0.01, 0.001]
        self.scalaTempoVetNum = [1, 2, 5]
        self.scalaTempoVetExp = [-1, -3, -6, -9, -5, -7]
        self.tensaoMultpVet = [1, 10, 100]
        self.tensaoPosVet = [-5, -1, 0, 1, 5, -0.1, 0.1]
        self.trigPosVet = [-5, -1, 0, 1, 5]
        self.tensaoAtual = 0
        self.freqAtual = 0
        self.ondaAtual = ""
        self.userRespTensao = 0
        self.userRespFreq = 0
        self.userRespOnda = ""

    def run(self):
        __nome__ = __modulo__ + '-' + 'run'

        if self.conectarScope:
            mensagem = 'Trying to connect scope'
            print('{};{}'.format(__nome__, mensagem))
            try:
                self.signal.status.emit('Scope->Connecting')
                visa.ResourceManager().list_resources()
                self.scope = pylef.TektronixTBS1062()
                self.scope.start_acquisition()
                self.scope.set_sample()
                self.scope.ch1.set_probe(1)
                self.scope.ch2.set_probe(1)
                self.scope.ch1.set_position(0)
                self.scope.ch2.set_position(0)
                self.scope.ch1.turn_on()
                self.scope.ch2.turn_on()
                self.scope.ch1.set_scale(2)
                self.scope.ch2.set_scale(2)
                self.scopeConectado = True
                self.signal.status.emit('Scope->Connected')

            except ValueError:
                self.signal.faultSignal.emit('Scope')
                self.scopeConectado = False
                self.signal.status.emit('Scope->Disconnected')
                mensagem = 'Fail to connect Scope'
                print('{};{}'.format(__nome__, mensagem))
            self.conectarScope = False

        if self.desconectarScope:
            if self.scopeConectado:
                mensagem = 'Disconnecting Scope'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.close()
                self.scopeConectado = False
                self.signal.status.emit('Scope->Disconnected')
            self.desconectarScope = False

        if self.conectarGerador:
            try:
                mensagem = 'Trying to connect Generator'
                print('{};{}'.format(__nome__, mensagem))
                self.signal.status.emit('Generator->Connecting')
                visa.ResourceManager().list_resources()
                self.gerador = pylef.BK4052()
                self.geradorConectado = True
                self.signal.status.emit('Generator->Connected')

            except ValueError:
                self.signal.faultSignal.emit('Gerador')
                self.geradorConectado = False
                self.signal.status.emit('Generator->Disconnected')
                mensagem = 'Fail to connect Generator'
                print('{};{}'.format(__nome__, mensagem))
            self.conectarGerador = False

        if self.desconectarGerador:
            if self.geradorConectado:
                mensagem = 'Disconnecting Generator'
                print('{};{}'.format(__nome__, mensagem))
                self.gerador.close()
                self.geradorConectado = False
                self.signal.status.emit('Generator->Disconnected')
            self.desconectarGerador = False

        if self.varrerFreq:
            mensagem = 'Starting frequency sweep'
            print('{};{}'.format(__nome__, mensagem))
            self.freq = np.logspace(np.log10(self.freqInicial), np.log10(self.freqFinal), self.quantidadePontos,
                                    endpoint=True)  # varredura logaritmica
            self.scope.set_average_number(self.medias)  # ajusta o número de médias
            self.scope.set_average()  # turn average ON
            # -----------------
            self.Vpp1 = 0
            self.Vpp2 = 0  # listas para guardar as variáveis
            self.phase = 0  # listas para guardar as variáveis
            ### aquisição de dados no gerador com varredura de frequência
            self.scope.write('MEASUREment:MEAS1:TYPE NONE')
            self.scope.write('MEASUREment:MEAS2:TYPE NONE')
            self.scope.write('MEASUREment:MEAS3:TYPE NONE')
            self.scope.write('MEASUREment:MEAS4:TYPE NONE')
            self.scope.write('MEASUREment:MEAS5:TYPE NONE')
            self.scope.ch1.turn_on()
            self.scope.ch2.turn_on()
            self.scope.ch1.set_ac()
            self.scope.ch2.set_ac()
            self.scope.ch1.set_position(0)
            self.scope.ch2.set_position(0)
            self.gerador.ch1.turn_on()
            #Ligar o gerador
            for m, freqP in enumerate(list(self.freq)):  # loop de aquisição
                mensagem = 'Ponto de medida ' + str(m) + ': ' + str(freqP) + 'Hz'
                print('{};{}'.format(__nome__, mensagem))
                ### ajuste dos instrumentos
                self.gerador.ch1.set_frequency(freqP)  # muda a frequência
                periodP = 1. / freqP  # período da onda
                self.scope.set_horizontal_scale(periodP / 4.)  # escala horizontal = 1/4 período (2.5 oscilações em tela)
                self.scope.ch1.set_smart_scale()  # rescala o canal 1
                self.scope.ch2.set_smart_scale()  # rescala o canal 2
                ### aquisição de dados
                self.Vpp1 = (self.scope.ch1.measure.Vpp())  # acumula a medida do Vpp no canal 1
                self.phase = (-self.scope.ch1.measure.phase())  # acumula a medida da fase no canal 1
                self.Vpp2 = (self.scope.ch2.measure.Vpp())  # acumula a medida do Vpp no canal 2
                self.signal.measure.emit(self.Vpp1, self.Vpp2, self.phase, freqP, m)
                if self.interromperFreq:
                    break
            self.signal.finished.emit('OK')
            self.varrerFreq = False
            mensagem = 'Frequency sweep done'
            print('{};{}'.format(__nome__, mensagem))

        if self.capturarTela:
            mensagem = 'Acquiring scope channels to temporal analysis'
            print('{};{}'.format(__nome__, mensagem))
            canal1 = []
            canal2 = []
            tempo = []

            if self.leitura == 0:
                self.scope.set_sample()
            else:
                self.scope.set_average_number(self.leitura)  # ajusta o número de médias

            self.scope.trigger.set_source(self.trigger)
            self.scope.trigger.set_to_50()

            if self.ch1:
                mensagem = 'Acquiring scope channel 1'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.ch1.turn_on()
                time.sleep(0.5)
                tempo, canal1 = self.scope.ch1.read_channel()
            else:
                canal1 = np.zeros(2048)

            if self.ch2:
                mensagem = 'Acquiring scope channel 2'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.ch2.turn_on()
                time.sleep(0.5)
                tempo, canal2 = self.scope.ch2.read_channel()
            else:
                canal2 = np.zeros(2048)

            if self.ch1 is False and self.ch2 is False:
                tempo = np.zeros(2048)


            self.signal.screen.emit(canal1, canal2, tempo)
            self.capturarTela = False

        if self.fft:
            mensagem = 'Acquiring scope channels to FFT analysis'
            print('{};{}'.format(__nome__, mensagem))

            if self.ch1:
                tempo, canal1 = self.scope.ch1.read_channel()
                escala = self.scope.horizontal_scale()
                tempoReal = np.linspace(0, escala*10, len(tempo))
                self.signal.fftSignal.emit(tempo, canal1)

            if self.ch2:
                tempo, canal2 = self.scope.ch2.read_channel()
                self.signal.fftSignal.emit(tempo, canal2)

            self.fft = False

        if self.embaralharGerador:
            mensagem = 'Shuffling generator'
            print('{};{}'.format(__nome__, mensagem))

            self.tensaoAtual = self.tensaoVetNum[random.randint(0, len(self.tensaoVetNum)-1)] * \
                               10 ** self.tensaoVetExp[random.randint(0, len(self.tensaoVetExp)-1)]
            self.freqAtual = self.freqVetNum[random.randint(0, len(self.freqVetNum)-1)] * \
                               10 ** self.freqVetExp[random.randint(0, len(self.freqVetExp)-1)]
            self.ondaAtual = self.ondaVet[random.randint(0, len(self.ondaVet)-1)]

            if random.randint(0, 1):
                self.gerador.ch1.turn_on()
            else:
                self.gerador.ch1.turn_off()

            self.gerador.ch1.set_frequency(self.freqAtual)
            self.gerador.ch1.set_function(self.ondaAtual)
            self.gerador.ch1.set_Vpp(self.tensaoAtual)

            self.embaralharGerador = False

        if self.embaralharOsciloscopio:
            mensagem = 'Shuffling scope'
            print('{};{}'.format(__nome__, mensagem))

            if random.randint(0, 1):
                self.scope.ch1.turn_on()
            else:
                self.scope.ch1.turn_off()

            self.scope.trigger.set_source(self.trigVet[random.randint(0, len(self.trigVet)-1)])

            self.scope.ch1.set_scale(self.scalaTensaoVet[random.randint(0, len(self.scalaTensaoVet)-1)])

            self.scope.set_horizontal_scale(self.scalaTempoVetNum[random.randint(0, len(self.scalaTempoVetNum) - 1)] *
                                            10 ** self.scalaTempoVetExp[random.randint(0, len(self.scalaTempoVetExp) - 1)])

            if self.dificuldade > 0:
                teste = random.randint(0, 2)
                if teste == 0:
                    self.scope.ch1.set_ac()
                elif teste == 1:
                    self.scope.ch1.set_dc()
                else:
                    self.scope.ch1.set_ground()

            self.scope.ch1.set_position(self.tensaoPosVet[random.randint(0, len(self.tensaoPosVet)-1)])

            if self.dificuldade > 1:
                self.scope.trigger.set_level(self.trigPosVet[random.randint(0, len(self.trigPosVet) - 1)])

            self.embaralharOsciloscopio = False

        if self.verificarResposta:
            mensagem = 'Checking answer'
            print('{};{}'.format(__nome__, mensagem))

            if self.ondaAtual == self.userRespOnda:
                resp1 = 1
            else:
                resp1 = 0

            if self.userRespTensao >= (self.tensaoAtual - self.tensaoAtual * 0.05) and self.userRespTensao <= (self.tensaoAtual + self.tensaoAtual * 0.05):
                resp2 = 1
            else:
                resp2 = 0

            if self.userRespFreq >= (self.freqAtual - self.freqAtual * 0.05) and self.userRespFreq <= (self.freqAtual + self.freqAtual * 0.05):
                resp3 = 1
            else:
                resp3 = 0

            self.signal.result.emit(resp1, resp2, resp3)
            self.verificarResposta = False