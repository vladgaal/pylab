################################################################################
##
## BY: WANDERSON M.PIMENTA
## PROJECT MADE WITH: Qt Designer and PySide2
## V: 1.0.0
##
## This project can be used freely for all uses, as long as they maintain the
## respective credits only in the Python scripts, any information in the visual
## interface (GUI) can be modified without any implication.
##
## There are limitations on Qt licenses if you want to use your products
## commercially, I recommend reading them on the official website:
## https://doc.qt.io/qtforpython/licenses.html
##
################################################################################
import os
import sys
import platform
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import (QCoreApplication, QPropertyAnimation, QDate, QDateTime, QMetaObject, QObject, QPoint, QRect, QSize, QTime, QUrl, Qt, QEvent, QThread, Signal)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont, QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter, QPixmap, QRadialGradient)
from PySide2.QtCore import QFile, QObject, Signal, Slot, QTimer
from PySide2.QtWidgets import QApplication, QVBoxLayout, QWidget
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import *

# GUI FILE
from app_modules import *

__appname__ = "PyLab"
__version__ = "2.0"
__modulo__ = 'M'
__videoURL__ = "https://www.youtube.com/watch?v=FevnHbuhJjI"
control = 0


class Tutorial(QDialog, Ui_Dialog):
    def __init__(self, parent=None):
        super(Tutorial, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)

        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)

        self.shadow = QGraphicsDropShadowEffect(self)
        self.shadow.setBlurRadius(20)
        self.shadow.setXOffset(0)
        self.shadow.setYOffset(0)
        self.shadow.setColor(QColor(0, 0, 0, 60))
        self.frame_main.setGraphicsEffect(self.shadow)

        self.setWindowTitle(__appname__ + " - Tutorial de configuração dos Osciloscópio")
        self.okBt.clicked.connect(self.close)
        self.videoBt.clicked.connect(self.chamaVideo)
        self.label_3.setText("V " + __version__)

    def chamaVideo(self):
        webbrowser.open_new_tab(__videoURL__)


class DSL(QObject):
    dataChanged = Signal(list)

    def __init__(self, parent=None):
        # LOAD HMI
        super().__init__(parent)
        designer_file = QFile("userInterface.ui")
        if designer_file.open(QFile.ReadOnly):
            loader = QUiLoader()
            self.ui = loader.load(designer_file)
            designer_file.close()
            self.ui.show()
        # Data to be visualized
        self.data = []

    def mainLoop(self):
        self.data = []
        for i in range(10):
            self.data.append(random.randint(0, 10))
        # Send data to graph
        self.dataChanged.emit(self.data)
        # LOOP repeater
        QTimer.singleShot(10, self.mainLoop)


class MainWindow(QMainWindow):
    average = 4
    tensao1 = []
    tensao2 = []
    fase = []
    frequencia = []
    transmitancia = []
    transmitanciaDb = []
    ch1Tela = []
    ch2Tela = []
    tempoTela = []
    diretorio = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')

    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        global control

        ########################################################################
        ## START - WINDOW ATTRIBUTES
        ########################################################################

        ## REMOVE ==> STANDARD TITLE BAR
        UIFunctions.removeTitleBar(True)
        ## ==> END ##

        ## SET ==> WINDOW TITLE
        self.setWindowTitle(__appname__ + ' - Python Base')
        UIFunctions.labelTitle(self, __appname__ + ' - Python Base')
        #UIFunctions.labelDescription(self, 'Set text')
        ## ==> END ##

        ## WINDOW SIZE ==> DEFAULT SIZE
        startSize = QSize(1000, 720)
        self.resize(startSize)
        self.setMinimumSize(startSize)
        # UIFunctions.enableMaximumSize(self, 500, 720)
        ## ==> END ##

        ## ==> CREATE MENUS
        ########################################################################

        ## ==> TOGGLE MENU SIZE
        self.ui.btn_toggle_menu.clicked.connect(lambda: UIFunctions.toggleMenu(self, 230, True))
        ## ==> END ##

        ## ==> ADD CUSTOM MENUS
        self.ui.stackedWidget.setMinimumWidth(20)
        UIFunctions.addNewMenu(self, "Exercises", "btn_exercises", "url(:/old/icons/exc_mid.png)", True)
        UIFunctions.addNewMenu(self, "FFT", "btn_fft", "url(:/old/icons/Fft_mid.png)", True)
        UIFunctions.addNewMenu(self, "Frequency Response", "btn_freq_resp", "url(:/old/icons/Plot_mid.png)", True)
        UIFunctions.addNewMenu(self, "Time Response", "btn_time_resp", "url(:/old/icons/Onda_mid.png)", True)
        UIFunctions.addNewMenu(self, "Settings", "btn_settings", "url(:/16x16/icons/16x16/cil-equalizer.png)", False)
        ## ==> END ##


        # START MENU => SELECTION
        UIFunctions.selectStandardMenu(self, "btn_settings")
        ## ==> END ##

        ## ==> START PAGE
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_settings)
        ## ==> END ##

        ## USER ICON ==> SHOW HIDE
        UIFunctions.userIcon(self, "WM", "", False)

        ## CREDIT LABEL
        self.ui.label_credits.setText("<strong>Written and compiled by:</strong> Vladimir Gaal - February 2021")

        ## VERSION LABEL
        self.ui.label_version.setText("v " + __version__)

        ## INITIAL TAB
        self.ui.tabWidget.setCurrentIndex(4)

        ## ==> END ##


        ## ==> MOVE WINDOW / MAXIMIZE / RESTORE
        ########################################################################
        def moveWindow(event):
            # IF MAXIMIZED CHANGE TO NORMAL
            if UIFunctions.returStatus() == 1:
                UIFunctions.maximize_restore(self)

            # MOVE WINDOW
            if event.buttons() == Qt.LeftButton:
                self.move(self.pos() + event.globalPos() - self.dragPos)
                self.dragPos = event.globalPos()
                event.accept()

        # WIDGET TO MOVE
        self.ui.frame_label_top_btns.mouseMoveEvent = moveWindow
        ## ==> END ##

        ## ==> LOAD DEFINITIONS
        ########################################################################
        UIFunctions.uiDefinitions(self)
        ## ==> END ##

        UIFunctions.setIcons(self)

        ########################################################################
        ## END - WINDOW ATTRIBUTES
        ############################## ---/--/--- ##############################




        ########################################################################
        #                                                                      #
        ## START -------------- WIDGETS FUNCTIONS/PARAMETERS ---------------- ##
        #                                                                      #
        ## ==> USER CODES BELLOW                                              ##
        ########################################################################

        ## ==> PLOTS INITIALIZATION
        UIFunctions.setPlots(self)

        ## ==> ZERO FREQ PLOT
        self.blankPlotFreq()

        ## ==> ZERO FFT PLOT
        self.blankPlotFFT()

        ## ==> ZERO TIME PLOT
        self.blankPlotTime()

        ## ==> SETTINGS TAB CONFIGS
        self.ui.conectarBtOsc.setIcon(self.iconConectar)
        self.ui.conectarBtGer.setIcon(self.iconConectar)
        self.ui.conectarBtOsc.clicked.connect(self.conectarScope)
        self.ui.conectarBtGer.clicked.connect(self.conectarGerador)
        self.atualizaStatus("Scope->Disconnected")
        self.atualizaStatus("Generator->Disconnected")

        ## ==> TIME TAB CONFIGS
        self.habilitarCH1()
        self.habilitarCH2()
        self.modoLeitura()
        self.ui.canalUmCheck.toggled.connect(self.habilitarCH1)
        self.ui.canalDoisCheck.toggled.connect(self.habilitarCH2)
        self.ui.amostraRBt.toggled.connect(self.modoLeitura)
        self.ui.mediaRBt.toggled.connect(self.modoLeitura)
        self.ui.ivRBt.toggled.connect(self.atualizaGraficoTela)
        self.ui.vtRBt.toggled.connect(self.atualizaGraficoTela)
        self.ui.ch12ch2RBt.toggled.connect(self.atualizaGraficoTela)
        self.ui.capturarBt_Cap.clicked.connect(self.capturaTela)
        self.ui.salvarBt_Cap.clicked.connect(self.salvarTela)

        ## ==> EXERCISE TAB CONFIGS
        self.ui.embGerBt.clicked.connect(self.embaralharGerador)
        self.ui.embOscBt.clicked.connect(self.embaralharOsciloscopio)
        self.ui.respostaBt.clicked.connect(self.verificaResp)
        self.ui.respostaBt.setEnabled(False)
        self.ui.tutorialBt.clicked.connect(self.telaTutorial)

        ## ==> FREQ TAB CONFIGS
        self.ui.startBt_Freq.clicked.connect(self.varreduraFreq)
        self.ui.stopBt_Freq.clicked.connect(self.interromperFreq)
        self.ui.novoBt_Freq.clicked.connect(self.limparVarreduraFreq)
        self.ui.salvarBt_Freq.clicked.connect(self.saveFreq)

        ## ==> FFT TAB CONFIGS
        self.ui.startBt_Fft.clicked.connect(self.iniciaFft)
        self.ui.salvarBt_Fft.clicked.connect(self.salvarFft)

        ## ==> END ##

        ########################################################################
        #                                                                      #
        ## END --------------- WIDGETS FUNCTIONS/PARAMETERS ----------------- ##
        #                                                                      #
        ############################## ---/--/--- ##############################



        ########################################################################
        #                                                                      #
        ## START ------------------ THREADS SETTINGS ------------------------ ##
        #                                                                      #
        ## ==> USER CODES BELLOW                                              ##
        ########################################################################

        self.equipamentos_thread = threads.EquipamentosThread()
        self.equipamentos_thread.start()
        self.equipamentos_thread.signal.status.connect(self.atualizaStatus)
        self.equipamentos_thread.signal.faultSignal.connect(self.fault)
        self.equipamentos_thread.signal.measure.connect(self.recebendoMedidaFreq)
        self.equipamentos_thread.signal.finished.connect(self.varreduraFreqTerminada)
        self.equipamentos_thread.signal.screen.connect(self.recebendoTela)
        self.equipamentos_thread.signal.fftSignal.connect(self.calculoFft)
        self.equipamentos_thread.signal.result.connect(self.gerarNota)

        ########################################################################
        #                                                                      #
        ## END -------------------- THREADS SETTINGS ------------------------ ##
        #                                                                      #
        ############################## ---/--/--- ##############################


        ## SHOW ==> MAIN WINDOW
        ########################################################################
        self.show()
        control = 2
        ## ==> END ##

    ########################################################################
    ## MENUS ==> DYNAMIC MENUS FUNCTIONS
    ########################################################################
    def Button(self):
        # GET BT CLICKED
        btnWidget = self.sender()

        # PAGE EXERCICIES
        if btnWidget.objectName() == "btn_exercises":
            self.ui.stackedWidget.setCurrentWidget(self.ui.page_exercises)
            UIFunctions.resetStyle(self, "btn_exercises")
            UIFunctions.labelPage(self, "Exercises")
            btnWidget.setStyleSheet(UIFunctions.selectMenu(btnWidget.styleSheet()))

        # PAGE FFT
        if btnWidget.objectName() == "btn_fft":
            self.ui.stackedWidget.setCurrentWidget(self.ui.page_fft)
            UIFunctions.resetStyle(self, "btn_fft")
            UIFunctions.labelPage(self, "FFT")
            btnWidget.setStyleSheet(UIFunctions.selectMenu(btnWidget.styleSheet()))

        # PAGE Frequency Response
        if btnWidget.objectName() == "btn_freq_resp":
            self.ui.stackedWidget.setCurrentWidget(self.ui.page_freq_resp)
            UIFunctions.resetStyle(self, "btn_freq_resp")
            UIFunctions.labelPage(self, "Frequency Response")
            btnWidget.setStyleSheet(UIFunctions.selectMenu(btnWidget.styleSheet()))

        # PAGE Time Response
        if btnWidget.objectName() == "btn_time_resp":
            self.ui.stackedWidget.setCurrentWidget(self.ui.page_time_resp)
            UIFunctions.resetStyle(self, "btn_time_resp")
            UIFunctions.labelPage(self, "Time Response")
            btnWidget.setStyleSheet(UIFunctions.selectMenu(btnWidget.styleSheet()))

        # PAGE SETTINGS
        if btnWidget.objectName() == "btn_settings":
            self.ui.stackedWidget.setCurrentWidget(self.ui.page_settings)
            UIFunctions.resetStyle(self, "btn_settings")
            UIFunctions.labelPage(self, "Settings")
            btnWidget.setStyleSheet(UIFunctions.selectMenu(btnWidget.styleSheet()))


    ## ==> END ##

    ########################################################################
    ## START ==> APP EVENTS
    ########################################################################

    # # EVENT ==> MOUSE DOUBLE CLICK
    # #######################################################################
    # def eventFilter(self, watched, event):
    #     if watched == self.le and event.type() == QtCore.QEvent.MouseButtonDblClick:
    #         print("pos: ", event.pos())
    # ## ==> END ##

    ## EVENT ==> MOUSE CLICK
    ########################################################################
    def mousePressEvent(self, event):
        self.dragPos = event.globalPos()
        # if event.buttons() == Qt.LeftButton:
        #     print('Mouse click: LEFT CLICK')
        # if event.buttons() == Qt.RightButton:
        #     print('Mouse click: RIGHT CLICK')
        # if event.buttons() == Qt.MidButton:
        #     print('Mouse click: MIDDLE BUTTON')
    ## ==> END ##

    # ## EVENT ==> KEY PRESSED
    # ########################################################################
    # def keyPressEvent(self, event):
    #     print('Key: ' + str(event.key()) + ' | Text Press: ' + str(event.text()))
    # ## ==> END ##
    #
    # ## EVENT ==> RESIZE EVENT
    # ########################################################################
    # def resizeEvent(self, event):
    #     self.resizeFunction()
    #     return super(MainWindow, self).resizeEvent(event)
    #
    # def resizeFunction(self):
    #     print('Height: ' + str(self.height()) + ' | Width: ' + str(self.width()))
    # ## ==> END ##

    ########################################################################
    ## END ==> APP EVENTS
    ############################## ---/--/--- ##############################

    def salvarFft(self):
        __nome__ = __modulo__ + '-' + 'salvarFft'
        mensagem = 'Opening window to save FFT'
        print('{};{}'.format(__nome__, mensagem))
        dir = self.diretorio
        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")

        if self.outFile[0] != '':
            self.dadosFft.to_csv(self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Saved file: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))
            ## Suprimido a pedido do professor
            # im = cv2.imread('fft.png')
            # cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png', im)
            # mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png'
            # print('{};{}'.format(__nome__, mensagem))
            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'No file selected'
            print('{};{}'.format(__nome__, mensagem))

    def iniciaFft(self):
        __nome__ = __modulo__ + '-' + 'iniciaFft'
        mensagem = 'Starting acquisition for FFT'
        print('{};{}'.format(__nome__, mensagem))
        if self.equipamentos_thread.scopeConectado:
            if self.ui.ch1_Fft.isChecked():
                self.equipamentos_thread.ch1 = True
                self.equipamentos_thread.ch2 = False

            if self.ui.ch2_Fft.isChecked():
                self.equipamentos_thread.ch1 = False
                self.equipamentos_thread.ch2 = True

            self.equipamentos_thread.fft = True
            self.equipamentos_thread.start()
            self.ui.salvarBt_Fft.setEnabled(True)

        else:
            mensagem = 'Oscilloscope not connected'
            print('{};{}'.format(__nome__, mensagem))
            self.popMsg("Error", "First connect the equipment.", "warning")
            # QMessageBox.warning(self, "Error", "First connect\nthe equipment.", QMessageBox.Ok)

    def salvarTela(self):
        __nome__ = __modulo__ + '-' + 'salvarTela'
        dir = self.diretorio
        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        mensagem = 'Opening window to save temporal acquisition'
        print('{};{}'.format(__nome__, mensagem))

        if self.outFile[0] != '':
            self.dadosTela.to_csv(self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Saved file: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))
            ## Suprimido a pedido do professor
            # im = cv2.imread('telaVT.png')
            # cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoVT.png', im)
            # mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoVT.png'
            # print('{};{}'.format(__nome__, mensagem))
            #
            # im = cv2.imread('telaIV.png')
            # cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoIV.png', im)
            # mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoIV.png'
            # print('{};{}'.format(__nome__, mensagem))
            #
            # im = cv2.imread('telaCh122.png')
            # cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoCh1-Ch2xCh2.png', im)
            # mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_graficoCh1-Ch2xCh2.png'

            print('{};{}'.format(__nome__, mensagem))
            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'No file selected'
            print('{};{}'.format(__nome__, mensagem))

    def capturaTela(self):
        __nome__ = __modulo__ + '-' + 'capturaTela'
        mensagem = 'Starting time acquisition'
        print('{};{}'.format(__nome__, mensagem))
        if self.equipamentos_thread.scopeConectado:
            mensagem = 'Iniciando captura de tela'
            print('{};{}'.format(__nome__, mensagem))

            self.equipamentos_thread.ch1 = self.ui.canalUmCheck.isChecked()
            self.equipamentos_thread.ch2 = self.ui.canalDoisCheck.isChecked()
            # if self.ui.canalUmCheck.isChecked():
            #     self.equipamentos_thread.ch1 = True
            # else:
            #     self.equipamentos_thread.ch1 = False
            #
            # if self.ui.canalDoisCheck.isChecked():
            #     self.equipamentos_thread.ch2 = True
            # else:
            #     self.equipamentos_thread.ch2 = False

            if self.ui.amostraRBt.isChecked():
                self.equipamentos_thread.leitura = 0
            elif self.ui.mediaRBt.isChecked():
                if self.ui.mediaBox.currentIndex() == 0:
                    self.equipamentos_thread.leitura = 4
                if self.ui.mediaBox.currentIndex() == 1:
                    self.equipamentos_thread.leitura = 16
                if self.ui.mediaBox.currentIndex() == 2:
                    self.equipamentos_thread.leitura = 64
                if self.ui.mediaBox.currentIndex() == 3:
                    self.equipamentos_thread.leitura = 128

            if self.ui.triggerCh1.isChecked():
                self.equipamentos_thread.trigger = 'CH1'
            elif self.ui.triggerCh2.isChecked():
                self.equipamentos_thread.trigger = 'CH2'
            elif self.ui.triggerExt.isChecked():
                self.equipamentos_thread.trigger = 'EXT'
            elif self.ui.triggerLinha.isChecked():
                self.equipamentos_thread.trigger = 'AC LINE'

            self.equipamentos_thread.capturarTela = True
            self.equipamentos_thread.start()
            self.ui.salvarBt_Cap.setEnabled(True)
        else:
            self.popMsg("Error", "First connect the equipment.", "warning")
            # QMessageBox.warning(self, "Error", "First connect\nthe equipment.", QMessageBox.Ok)

    def saveFreq(self):
        __nome__ = __modulo__ + '-' + 'saveFreq'
        mensagem = 'Opening frequency save window'
        print('{};{}'.format(__nome__, mensagem))
        dir = self.diretorio
        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")

        if self.outFile[0] != '':
            self.dadosFreq.to_csv(self.outFile[0][:len(self.outFile[0])-4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0])-4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))
            # im = cv2.imread('varreduraFreq.png')
            # cv2.imwrite(self.outFile[0][:len(self.outFile[0])-4]+'_figura.png', im)
            # mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png'
            # print('{};{}'.format(__nome__, mensagem))
            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'No file selected'
            print('{};{}'.format(__nome__, mensagem))

    def limparVarreduraFreq(self):
        __nome__ = __modulo__ + '-' + 'limparVarreduraFreq'
        # result = QMessageBox.question(self, __appname__, "Are you sure you want to delete the data?",
        #                               QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)

        result = self.popMsg("Clear data", "Are you sure you want to delete the data?", "quest")
        if result == QMessageBox.Yes:
            mensagem = 'Clearing frequency sweep data'
            print('{};{}'.format(__nome__, mensagem))
            self.clearPlotFreq()
            self.blankPlotFreq()
            # self.atualizaGraficoFreq()
            self.ui.salvarBt_Freq.setEnabled(False)
            self.ui.novoBt_Freq.setEnabled(False)
            del self.tensao1[:]
            del self.tensao2[:]
            del self.fase[:]
            del self.frequencia[:]
            del self.transmitancia[:]
            del self.transmitanciaDb[:]
            # self.graph_thread.dadosX = copy.deepcopy(self.frequencia)
            # self.graph_thread.dadosY1 = copy.deepcopy(self.transmitanciaDb)
            # self.graph_thread.dadosY2 = copy.deepcopy(self.fase)

    def interromperFreq(self):
        __nome__ = __modulo__ + '-' + 'interromperFreq'
        mensagem = 'Frequency sweep stopped manually'
        print('{};{}'.format(__nome__, mensagem))
        self.equipamentos_thread.varrerFreq = False
        self.equipamentos_thread.interromperFreq = True
        # self.ui.startBt_Freq.setEnabled(True)
        self.ui.stopBt_Freq.setEnabled(False)

    def varreduraFreq(self):
        __nome__ = __modulo__ + '-' + 'varreduraFreq'
        mensagem = 'Starting frequency sweep'
        print('{};{}'.format(__nome__, mensagem))
        self.equipamentos_thread.freqInicial = float(self.ui.freqInicialLine.text())*\
                                               10**(3*self.ui.freqInicialBox.currentIndex())
        self.equipamentos_thread.freqFinal = float(self.ui.freqFinalLine.text())*\
                                             10**(3*self.ui.freqFinalBox.currentIndex())
        self.equipamentos_thread.quantidadePontos = int(self.ui.numeroPontos.text())
        self.equipamentos_thread.interromperFreq = False
        self.ui.progressBar.setValue(0)
        self.clearPlotFreq()
        if self.equipamentos_thread.scopeConectado and self.equipamentos_thread.geradorConectado:
            del self.tensao1[:]
            del self.tensao2[:]
            del self.fase[:]
            del self.frequencia[:]
            del self.transmitancia[:]
            del self.transmitanciaDb[:]
            self.ui.statusFreq.setText("Sweeping...")
            self.ui.statusFreq.setStyleSheet("QLabel#statusFreq {color: red; font: bold 12pt; font-family: Tahoma;}")
            self.ui.startBt_Freq.setEnabled(False)
            self.ui.stopBt_Freq.setEnabled(True)
            # self.graph_thread.varreduraFreq = True
            # self.graph_thread.start()
            self.equipamentos_thread.varrerFreq = True
            self.equipamentos_thread.start()
        else:
            self.popMsg("Error", "First connect the equipment.", "warning")
            # QMessageBox.warning(self, "Error", "First connect\nthe equipment.", QMessageBox.Ok)

    def telaTutorial(self):
        __nome__ = __modulo__ + '-' + 'telaTutorial'
        mensagem = 'Starting tutorial window'
        print('{};{}'.format(__nome__, mensagem))
        prog = Tutorial()
        prog.exec_()

    def conectarScope(self):
        __nome__ = __modulo__ + '-' + 'conectarScope'
        mensagem = 'Trying to connect scope'
        print('{};{}'.format(__nome__, mensagem))

        if self.equipamentos_thread.scopeConectado:
            self.equipamentos_thread.desconectarScope = True
            self.equipamentos_thread.start()
        else:
            if self.ui.modelosOscCombo.currentIndex() == 1:
                self.equipamentos_thread.conectarScope = True
                self.equipamentos_thread.start()
            else:
                self.popMsg("Failed to connect", "Select a valid oscilloscope.", "info")
                # QMessageBox.warning(self, "Failed to connect", "Select a valid oscilloscope.", QMessageBox.Ok)

    def conectarGerador(self):
        __nome__ = __modulo__ + '-' + 'conectarGerador'
        mensagem = 'Trying to connect generator'
        print('{};{}'.format(__nome__, mensagem))

        if self.equipamentos_thread.geradorConectado:
            self.equipamentos_thread.desconectarGerador = True
            self.equipamentos_thread.start()
        else:
            if self.ui.modelosGerCombo.currentIndex() == 1:
                self.equipamentos_thread.conectarGerador = True
                self.equipamentos_thread.start()
            else:
                self.popMsg("Failed to connect", "Select a valid generator.", "info")
                # QMessageBox.warning(self, "Failed to connect", "Select a valid generator.", QMessageBox.Ok)

    def verificaResp(self):
        __nome__ = __modulo__ + '-' + 'verificaResp'
        mensagem = 'Checking answer'
        print('{};{}'.format(__nome__, mensagem))
        if self.ui.userRespOnda.currentIndex() == 0 or float(self.ui.userRespFreqNum.text()) == 0 or float(
                self.ui.userRespAmpNum.text()) == 0:
            self.popMsg("Error", "All fields needto be filled.", "info")
            # QMessageBox.warning(self, "Error", "All fields need\nto be filled.", QMessageBox.Ok)
            return

        if self.ui.userRespOnda.currentIndex() == 1:
            self.equipamentos_thread.userRespOnda = "SINE"
        elif self.ui.userRespOnda.currentIndex() == 2:
            self.equipamentos_thread.userRespOnda = "SQUARE"
        elif self.ui.userRespOnda.currentIndex() == 3:
            self.equipamentos_thread.userRespOnda = "RAMP"

        self.equipamentos_thread.userRespFreq = float(self.ui.userRespFreqNum.text()) * \
                                                10 ** (self.ui.userRespFreqExp.currentIndex() * 3)
        self.equipamentos_thread.userRespTensao = float(self.ui.userRespAmpNum.text()) * \
                                                  10 ** (self.ui.userRespAmpExp.currentIndex() * -3)

        self.equipamentos_thread.verificarResposta = True
        self.equipamentos_thread.start()
        self.ui.respostaBt.setEnabled(False)

    def embaralharOsciloscopio(self):
        __nome__ = __modulo__ + '-' + 'embaralharOsciloscopio'
        mensagem = 'Shuffling scope'
        print('{};{}'.format(__nome__, mensagem))
        if self.equipamentos_thread.scopeConectado and self.equipamentos_thread.geradorConectado:
            if self.ui.facilBt.isChecked():
                self.equipamentos_thread.dificuldade = 0
            elif self.ui.medioBt.isChecked():
                self.equipamentos_thread.dificuldade = 1
            elif self.ui.dificilBt.isChecked():
                self.equipamentos_thread.dificuldade = 2

            self.equipamentos_thread.embaralharOsciloscopio = True
            self.equipamentos_thread.start()
            self.ui.respostaBt.setEnabled(True)
        else:
            self.popMsg("Error", "First connect the equipment.", "warning")
            # QMessageBox.warning(self, "Error", "First connect\nthe equipment.", QMessageBox.Ok)

    def embaralharGerador(self):
        __nome__ = __modulo__ + '-' + 'embaralharGerador'
        mensagem = 'Shuffling generator'
        print('{};{}'.format(__nome__, mensagem))

        if self.equipamentos_thread.scopeConectado and self.equipamentos_thread.geradorConectado:
            self.equipamentos_thread.embaralharGerador = True
            self.equipamentos_thread.start()
            self.ui.respostaBt.setEnabled(True)
            self.ui.respOnda.setText("")
            self.ui.respAmp.setText("")
            self.ui.respFreq.setText("")
        else:
            self.popMsg("Error", "First connect the equipment.", "warning")
            # QMessageBox.warning(self, "Error", "First connect\nthe equipment.", QMessageBox.Ok)

    def atualizaStatus(self, data):
        __nome__ = __modulo__ + '-' + 'atualizaStatus'
        mensagem = 'Updating status ' + data
        print('{};{}'.format(__nome__, mensagem))

        if data == 'Scope->Connecting':
            self.ui.osciloscopioStatus.setText("Connecting...")
            self.ui.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: blue; font: bold 12pt; " +
                                                  "font-family: Segoe UI;}")
        elif data == 'Scope->Connected':
            self.ui.osciloscopioStatus.setText("Connected")
            self.ui.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: green; font: bold 12pt; " +
                                                  "font-family: Segoe UI;}")
            self.ui.conectarBtOsc.setIcon(self.iconDesconectar)
            self.ui.conectarBtOsc.setToolTip("Disconnects the oscilloscope")
            self.ui.scope_ind_label.setText("Scope connected")
            self.ui.scope_ind_light.setPixmap(QPixmap(":/old/icons/green_light.png"))
        elif data == 'Scope->Disconnected':
            self.ui.osciloscopioStatus.setText("Disconnected")
            self.ui.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: red; font: bold 12pt; " +
                                                  "font-family: Segoe UI;}")
            self.ui.conectarBtOsc.setIcon(self.iconConectar)
            self.ui.conectarBtOsc.setToolTip("Connects the oscilloscope")
            self.ui.scope_ind_label.setText("Scope disconnected")
            self.ui.scope_ind_light.setPixmap(QPixmap(":/old/icons/red_light.png"))
        elif data == 'Generator->Connecting':
            self.ui.geradorStatus.setText("Connecting...")
            self.ui.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: blue; font: bold 12pt; " +
                                             "font-family: Segoe UI;}")
        elif data == 'Generator->Connected':
            self.ui.geradorStatus.setText("Connected")
            self.ui.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: green; font: bold 12pt; " +
                                             "font-family: Segoe UI;}")
            self.ui.conectarBtGer.setIcon(self.iconDesconectar)
            self.ui.conectarBtGer.setToolTip("Disconnects the generator")
            self.ui.gen_ind_label.setText("Generator connected")
            self.ui.gen_ind_light.setPixmap(QPixmap(":/old/icons/green_light.png"))
        elif data == 'Generator->Disconnected':
            self.ui.geradorStatus.setText("Disconnected")
            self.ui.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: red; font: bold 12pt; " +
                                             "font-family: Segoe UI;}")
            self.ui.conectarBtGer.setIcon(self.iconConectar)
            self.ui.conectarBtGer.setToolTip("Connects the generator")
            self.ui.gen_ind_label.setText("Generator disconnected")
            self.ui.gen_ind_light.setPixmap(QPixmap(":/old/icons/red_light.png"))

    def fault(self, data):
        __nome__ = __modulo__ + '-' + 'fault'
        mensagem = 'Scope or generator not found'
        print('{};{}'.format(__nome__, mensagem))
        if data == 'Scope':
            self.popMsg("Fail to connect", "Scope not found, check the connections.", "warning")
            # QMessageBox.warning(self, "Fail to connect", "Scope not found, check the connections.",
            #                     QMessageBox.Ok)
        elif data == 'Generator':
            self.popMsg("Fail to connect", "Generator not found, check the connections.", "warning")
            # QMessageBox.warning(self, "Fail to connect", "Generator not found, check the connections.",
            #                     QMessageBox.Ok)

    def recebendoMedidaFreq(self, vpp1, vpp2, phase, freq, point):
        __nome__ = __modulo__ + '-' + 'recebendoMedidaFreq'
        self.ui.progressBar.setValue(100*point/self.equipamentos_thread.quantidadePontos)
        self.tensao1.append(vpp1)
        self.tensao2.append(vpp2)
        self.fase.append(phase)
        self.frequencia.append(freq)
        T = (vpp2 / vpp1) ** 2  # cálculo da transmissão
        T_dB = 10 * np.log10(T)  # transmissão em dB
        self.transmitancia.append(T)
        self.transmitanciaDb.append(T_dB)

        mensagem = 'Received V1:' + str(vpp1) + ' V2:' + str(vpp2) + ' phase:' + str(phase) + ' freq:' + str(freq)
        print('{};{}'.format(__nome__, mensagem))

        # self.graph_thread.dadosX = copy.deepcopy(self.frequencia)
        # self.graph_thread.dadosY1 = copy.deepcopy(self.transmitanciaDb)
        # self.graph_thread.dadosY2 = copy.deepcopy(self.fase)
        # self.graph_thread.varreduraFreq = True
        # self.graph_thread.start()
        self.updatePlotFreq([self.frequencia, self.transmitanciaDb, self.fase])

    def varreduraFreqTerminada(self):
        __nome__ = __modulo__ + '-' + 'varreduraFreqTerminada'
        mensagem = 'Freq scan finished'
        print('{};{}'.format(__nome__, mensagem))
        time.sleep(1)
        self.dadosFreq = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosFreq['frequency (Hz)'] = self.frequencia
        self.dadosFreq['Vpp1 (V)'], self.dadosFreq['Vpp2 (V)'] = self.tensao1, self.tensao2
        self.dadosFreq['phase (Ch2-Ch1) (degrees)'] = self.fase
        self.dadosFreq['T'], self.dadosFreq['T_dB'] = self.transmitancia, self.transmitanciaDb
        mensagem = 'Banco de dados Freq criado'
        print('{};{}'.format(__nome__, mensagem))
        self.ui.progressBar.setValue(100)
        self.ui.salvarBt_Freq.setEnabled(True)
        self.ui.startBt_Freq.setEnabled(True)
        self.ui.stopBt_Freq.setEnabled(False)
        self.ui.novoBt_Freq.setEnabled(True)
        self.ui.statusFreq.setText("Holding")
        self.ui.statusFreq.setStyleSheet("QLabel#statusFreq {color: blue; font: bold 12pt; " +
                                      "font-family: Segoe UI;}")

    def recebendoTela(self, ch1, ch2, time):
        __nome__ = __modulo__ + '-' + 'recebendoTela'
        mensagem = 'Receiving time data'
        print('{};{}'.format(__nome__, mensagem))

        self.ch1Tela = ch1
        self.ch2Tela = ch2
        self.tempoTela = time
        self.dadosTela = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosTela['Time (s)'] = self.tempoTela
        mensagem = 'Banco de dados Tela criado'
        print('{};{}'.format(__nome__, mensagem))

        self.clearPlotTime()

        if self.ui.canalUmCheck.isChecked():
            self.dadosTela['V1 (V)'] = self.ch1Tela

        if self.ui.canalDoisCheck.isChecked():
            self.dadosTela['V2 (V)'] = self.ch2Tela

        if self.ui.canalDoisCheck.isChecked() and self.ui.canalUmCheck.isChecked():
            y1 = np.array(self.ch1Tela)
            y2 = np.array(self.ch2Tela)
            y3 = y1 - y2
            self.dadosTela['V1 - V2 (V)'] = y3

        self.updatePlotTime([self.tempoTela, self.ch1Tela, self.ch2Tela])

    def calculoFft(self, tempo, canal):
        __nome__ = __modulo__ + '-' + 'calculoFft'
        mensagem = 'Starting FFT calculation'
        print('{};{}'.format(__nome__, mensagem))
        delta = tempo[1] - tempo[0]  # time step\n",
        tempo = tempo - tempo[0] + delta
        Ts = float(tempo[0])
        n = len(tempo)  # length of the signal
        k = np.arange(n)
        T = n * Ts
        frq = k / T
        fft = np.fft.fft(canal) / n  # fft computing and normalization

        self.clearPlotFFT()
        self.updatePlotFft([frq[:int(len(frq)/2)], abs(fft[:int(len(fft)/2)])])

        self.dadosFft = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosFft['Frequency (Hz)'] = frq
        self.dadosFft['Amplitude'] = abs(fft)

    def gerarNota(self, resp1, resp2, resp3):
        __nome__ = __modulo__ + '-' + 'gerarNota'
        mensagem = 'Generating grade'
        print('{};{}'.format(__nome__, mensagem))

        self.nota = round(10 * (resp1 + resp2 + resp3) / 3, 1)
        self.ui.notaLabel.setText(str(self.nota))
        if resp1:
            self.ui.userRespOndaVerif.setIcon(self.iconCerto)
        else:
            self.ui.userRespOndaVerif.setIcon(self.iconErrado)
        if resp2:
            self.ui.userRespAmpVerif.setIcon(self.iconCerto)
        else:
            self.ui.userRespAmpVerif.setIcon(self.iconErrado)
        if resp3:
            self.ui.userRespFreqVerif.setIcon(self.iconCerto)
        else:
            self.ui.userRespFreqVerif.setIcon(self.iconErrado)

        if self.nota >= 7:
            self.ui.notaLabel.setStyleSheet("QLabel#notaLabel {color: blue; font: bold 18pt; " +
                                         "font-family: Segoe UI;}")
        else:
            self.ui.notaLabel.setStyleSheet("QLabel#notaLabel {color: red; font: bold 18pt; " +
                                         "font-family: Segoe UI;}")

        if self.equipamentos_thread.ondaAtual == "SINE":
            self.ui.respOnda.setText("Sinus")
        elif self.equipamentos_thread.ondaAtual == "SQUARE":
            self.ui.respOnda.setText("Square")
        elif self.equipamentos_thread.ondaAtual == "RAMP":
            self.ui.respOnda.setText("Ramp")

        self.ui.respAmp.setText(str(self.equipamentos_thread.tensaoAtual) + " V")
        self.ui.respFreq.setText(str(self.equipamentos_thread.freqAtual) + " Hz")

    def habilitarCH1(self):
        __nome__ = __modulo__ + '-' + 'habilitarCH1'
        mensagem = 'Changing acquisition time to CH1'
        print('{};{}'.format(__nome__, mensagem))

        if self.ui.canalUmCheck.isChecked():
            self.ui.canalUmCheck.setToolTip("Channel 1 acquisition enabled")
        else:
            self.ui.canalUmCheck.setToolTip("Channel 1 acquisition disabled")

    def habilitarCH2(self):
        __nome__ = __modulo__ + '-' + 'habilitarCH1'
        mensagem = 'Changing acquisition time to CH2'
        print('{};{}'.format(__nome__, mensagem))

        if self.ui.canalDoisCheck.isChecked():
            self.ui.canalDoisCheck.setToolTip("Channel 2 acquisition enabled")
        else:
            self.ui.canalDoisCheck.setToolTip("Channel 2 acquisition disabled")

    def modoLeitura(self):
        __nome__ = __modulo__ + '-' + 'modoLeitura'
        mensagem = 'Changing acquisition mode'
        print('{};{}'.format(__nome__, mensagem))
        if self.ui.amostraRBt.isChecked():
            self.ui.mediaBox.setEnabled(False)

        elif self.ui.mediaRBt.isChecked():
            self.ui.mediaBox.setEnabled(True)

    def atualizaGraficoTela(self):
        __nome__ = __modulo__ + '-' + 'atualizaGraficoTela'
        mensagem = 'Updating time plot sheet'
        print('{};{}'.format(__nome__, mensagem))
        if self.ui.vtRBt.isChecked():
            self.ui.stacked_plots.setCurrentIndex(0)
        if self.ui.ivRBt.isChecked():
            self.ui.stacked_plots.setCurrentIndex(1)
        if self.ui.ch12ch2RBt.isChecked():
            self.ui.stacked_plots.setCurrentIndex(2)
        # tamanho = self.pixmap.size()
        # self.grafico_Cap.setGeometry(QtCore.QRect(180, 40, tamanho.width(), tamanho.height()))
        # self.grafico_Cap.setPixmap(self.pixmap)

    def blankPlotFreq(self):
        __nome__ = __modulo__ + '-' + 'blankPlotFreq'
        mensagem = 'Ploting blank freq'
        print('{};{}'.format(__nome__, mensagem))

        x = np.logspace(1, 6, num=15)
        y1 = np.zeros(len(x))
        y2 = np.zeros(len(x))
        self.updatePlotFreq(data=[x, y1, y2])

    def blankPlotFFT(self):
        __nome__ = __modulo__ + '-' + 'blankPlotFFT'
        mensagem = 'Ploting blank FFT'
        print('{};{}'.format(__nome__, mensagem))

        x = np.logspace(1, 6, num=1000)
        y1 = np.zeros(len(x))
        self.updatePlotFft(data=[x, y1])

    def blankPlotTime(self):
        __nome__ = __modulo__ + '-' + 'blankPlotTime'
        mensagem = 'Ploting blank time'
        print('{};{}'.format(__nome__, mensagem))

        x = np.arange(1024)
        x = x / 1023
        x = x - max(x) / 2
        y1 = np.zeros(1024)
        y2 = np.zeros(1024)
        self.updatePlotTime(data=[x, y1, y2])

    def updatePlotFreq(self, data):
        __nome__ = __modulo__ + '-' + 'updatePlotFreq'
        mensagem = 'Ploting freq'
        print('{};{}'.format(__nome__, mensagem))

        self.ax1_freq.plot(data[0], data[1], 'o', c=[153/235, 168/235, 209/235])
        self.ax2_freq.plot(data[0], data[2], 'o', c=[153 / 235, 168 / 235, 209 / 235])
        self.ax1_freq.set_xlim(min(data[0]), max(data[0]))
        self.ax2_freq.set_xlim(min(data[0]), max(data[0]))
        self.ax2_freq.set_ylim(-100, 100)
        # self.line_freq.set_data(data[0], data[1])
        self.canvas_freq.draw()
        pass

    def updatePlotFft(self, data):
        __nome__ = __modulo__ + '-' + 'updatePlotFft'
        mensagem = 'Ploting FFT'
        print('{};{}'.format(__nome__, mensagem))

        lines = []
        for i in range(len(data[0])):
            pair = [(data[0][i], 0), (data[0][i], data[1][i])]
            lines.append(pair)

        linecoll = matcoll.LineCollection(lines)
        self.ax_fft.add_collection(linecoll)

        self.ax_fft.semilogx(data[0], data[1], 'o', c=[153/235, 168/235, 209/235])
        self.ax_fft.axis([1e1, max(data[0]), 0, 1])
        self.canvas_fft.draw()
        pass

    def updatePlotTime(self, data):
        __nome__ = __modulo__ + '-' + 'updatePlotTime'
        mensagem = 'Ploting time'
        print('{};{}'.format(__nome__, mensagem))

        self.ax1_time.plot(data[0], data[1], linestyle='-', color='y')
        self.ax2_time.plot(data[0], data[2], linestyle='-', color='cyan')
        self.ax1_time.set_xlim(min(data[0]), max(data[0]))
        self.canvas_time.draw()

        self.ax1_volt.plot(data[1], data[2], 'o', c=[153/235, 168/235, 209/235])
        self.ax1_volt.set_xlim(min(data[1]), max(data[1]))
        self.canvas_volt.draw()

        self.ax1_ch122.plot(data[1], data[1], 'o', c=[153/235, 168/235, 209/235])
        self.ax1_ch122.set_xlim(min(data[1]), max(data[1]))
        self.canvas_ch122.draw()
        pass

    def clearPlotFreq(self):
        color1 = 'white'
        font_mid = {'family': 'Segoe UI', 'size': 16}
        font_big = {'family': 'Segoe UI', 'weight': 'bold', 'size': 22}

        self.ax1_freq.clear()
        self.ax2_freq.clear()

        self.ax1_freq.patch.set_alpha(0)
        for spine in self.ax1_freq.spines.values():
            spine.set_edgecolor(color1)
        self.ax1_freq.xaxis.label.set_color(color1)
        self.ax1_freq.yaxis.label.set_color(color1)
        self.ax1_freq.set_xscale('log')
        self.ax1_freq.grid(True)
        self.ax1_freq.tick_params(which='both', axis='both', colors=color1)
        self.ax1_freq.set_ylabel("Transmittance (dB)", **font_big)

        self.ax2_freq.patch.set_alpha(0)
        for spine in self.ax2_freq.spines.values():
            spine.set_edgecolor(color1)
        self.ax2_freq.set_xscale('log')
        self.ax2_freq.xaxis.label.set_color(color1)
        self.ax2_freq.yaxis.label.set_color(color1)
        self.ax2_freq.grid(True)
        self.ax2_freq.tick_params(which='both', axis='both', colors=color1)
        self.ax2_freq.set_xlabel("Frequency (Hz)", **font_big)
        self.ax2_freq.set_ylabel("Phase (degrees)", **font_big)

    def clearPlotTime(self):
        color1 = 'white'
        font_mid = {'family': 'Segoe UI', 'size': 16}
        font_big = {'family': 'Segoe UI', 'weight': 'bold', 'size': 22}

        self.ax1_time.clear()
        self.ax2_time.clear()
        self.ax1_volt.clear()
        self.ax1_ch122.clear()

        self.ax1_time.patch.set_alpha(0)
        for spine in self.ax1_time.spines.values():
            spine.set_edgecolor(color1)
        self.ax1_time.xaxis.label.set_color(color1)
        self.ax1_time.yaxis.label.set_color(color1)
        self.ax1_time.tick_params(axis='x', colors=color1)
        self.ax1_time.tick_params(axis='y', colors='y')
        self.ax1_time.grid(True)
        self.ax1_time.set_xlabel("Time (s)", **font_big)
        self.ax1_time.set_ylabel("CH1 amplitude (V)", **font_big, color='y')

        self.ax2_time.set_ylabel('CH2 amplitude (V)', **font_big, color='cyan')
        self.ax2_time.tick_params(axis='y', colors='cyan')
        self.ax2_time.grid(True)

        self.ax1_volt.patch.set_alpha(0)
        for spine in self.ax1_volt.spines.values():
            spine.set_edgecolor(color1)
        self.ax1_volt.xaxis.label.set_color(color1)
        self.ax1_volt.yaxis.label.set_color(color1)
        self.ax1_volt.tick_params(axis='x', colors=color1)
        self.ax1_volt.tick_params(axis='y', colors='y')
        self.ax1_volt.grid(True)
        self.ax1_volt.set_xlabel("CH1 amplitude (V)", **font_big)
        self.ax1_volt.set_ylabel("CH2 amplitude (V)", **font_big)

        self.ax1_ch122.patch.set_alpha(0)
        for spine in self.ax1_ch122.spines.values():
            spine.set_edgecolor(color1)
        self.ax1_ch122.xaxis.label.set_color(color1)
        self.ax1_ch122.yaxis.label.set_color(color1)
        self.ax1_ch122.tick_params(axis='x', colors=color1)
        self.ax1_ch122.tick_params(axis='y', colors='y')
        self.ax1_ch122.grid(True)
        self.ax1_ch122.set_xlabel("CH1 amplitude (V)", **font_big)
        self.ax1_ch122.set_ylabel("CH1 - CH2 amplitude (V)", **font_big)

    def clearPlotFFT(self):
        color1 = 'white'
        font_mid = {'family': 'Segoe UI', 'size': 16}
        font_big = {'family': 'Segoe UI', 'weight': 'bold', 'size': 22}

        self.ax_fft.clear()

        self.ax_fft.patch.set_alpha(0)
        for spine in self.ax_fft.spines.values():
            spine.set_edgecolor(color1)
        self.ax_fft.xaxis.label.set_color(color1)
        self.ax_fft.yaxis.label.set_color(color1)
        self.ax_fft.tick_params(which='both', axis='both', colors=color1)
        self.ax_fft.grid(True)
        self.ax_fft.set_xlabel("Frequency (Hz)", **font_big)
        self.ax_fft.set_ylabel("|Y(freq)|", **font_big)

    def popMsg(self, title, msg, type):
        __nome__ = __modulo__ + '-' + 'popMsg'
        mensagem = 'Popping a message'
        print('{};{}'.format(__nome__, mensagem))

        msgBox = QtWidgets.QMessageBox()
        msgBox.setText("<p align='center'>" + title + "</p>")
        msgBox.setInformativeText("<p align='center'>" + msg + "</p>")
        msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok)
        if type == "info":
            msgBox.setIcon(QtWidgets.QMessageBox.Information)
        elif type == "warning":
            msgBox.setIcon(QtWidgets.QMessageBox.Critical)
        elif type == "quest":
            msgBox.setIcon(QtWidgets.QMessageBox.Question)
            msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)

        msgBox.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        msgBox.setStyleSheet("QMessageBox {"
                             "background-color: rgb(41, 45, 56); color: rgb(210, 210, 210);"
                             "border: 3px solid; border-color: rgb(210, 210, 210); border-radius: 6px;}"
                             "QMessageBox QPushButton {	border: 2px solid rgb(52, 59, 72);"
                             "border-radius: 10px; background-color: rgb(32, 35, 43);"
                             "font-family: Segoe UI; font-size: 12px;"
                             "height: 25px;  width: 80px; color: rgb(210, 210, 210);}"
                             "QMessageBox QPushButton:hover { background-color: rgb(57, 65, 80);"
                             "border: 2px solid rgb(61, 70, 86);}"
                             "QMessageBox QPushButton:pressed {	background-color: rgb(35, 40, 49);"
                             "border: 2px solid rgb(43, 50, 61);}"
                             "QMessageBox QLabel{"
                             "color: rgb(210, 210, 210);"
                             "font-family: Segoe UI; font-size: 12px;"
                             "}")
        # msgBox.setAttribute(QtCore.Qt.WA_TranslucentBackground)

        self.shadow = QGraphicsDropShadowEffect(self)
        self.shadow.setBlurRadius(20)
        self.shadow.setXOffset(0)
        self.shadow.setYOffset(0)
        self.shadow.setColor(QColor(0, 0, 0, 60))
        msgBox.setGraphicsEffect(self.shadow)
        return msgBox.exec_()

    def closeEvent(self, event, *args, **kwargs):
        """Ritual de encerramento"""

        # result = QMessageBox.question(self, __appname__, "Tem certeza que deseja sair?",
        #                               QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)

        result = self.popMsg("Exit", "Are you sure you want to quit?", "quest")

        if result == QMessageBox.Yes:
            """Colocar aqui tudo que tiver que ser feito antes de sair"""
            self.equipamentos_thread.desconectarGerador = True
            self.equipamentos_thread.desconectarScope = True
            self.equipamentos_thread.interromperFreq = True
            self.equipamentos_thread.varrerFreq = False
            self.equipamentos_thread.start()
            time.sleep(1)
            event.accept()
        else:
            event.ignore()


# SPLASH SCREEN
class SplashScreen(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_SplashScreen()
        self.ui.setupUi(self)

        # QMainWindow.__init__(self)
        # self.ui = Ui_SplashScreen()
        # self.ui.setupUi(self)

        ## UI ==> INTERFACE CODES
        ########################################################################

        ## REMOVE TITLE BAR
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)


        ## DROP SHADOW EFFECT
        self.shadow = QGraphicsDropShadowEffect(self)
        self.shadow.setBlurRadius(20)
        self.shadow.setXOffset(0)
        self.shadow.setYOffset(0)
        self.shadow.setColor(QColor(0, 0, 0, 60))
        self.ui.dropShadowFrame.setGraphicsEffect(self.shadow)

        ## QTIMER ==> START
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.progress)
        # TIMER IN MILLISECONDS
        self.timer.start(1000)

        self.timer2 = QtCore.QTimer()
        self.timer2.timeout.connect(self.dots)
        # TIMER IN MILLISECONDS
        self.timer2.start(300)

        ## SHOW ==> MAIN WINDOW
        ########################################################################
        self.show()
        ## ==> END ##

    ## ==> APP FUNCTIONS
    ########################################################################

    def runMain(self):
        #pass
        self.dsl = DSL()
        self.dsl.mainLoop()

        self.main = MainWindow()
        self.main.show()

        #self.dsl.dataChanged.connect(self.main.update_plot)

    def dots(self):
        # ANIMATE DOTS
        if self.ui.label_loading.text() == "loading.":
            self.ui.label_loading.setText("loading..")
        elif self.ui.label_loading.text() == "loading..":
            self.ui.label_loading.setText("loading...")
        elif self.ui.label_loading.text() == "loading...":
            self.ui.label_loading.setText("loading.")

    def progress(self):

        global control

        # OPEN APP
        if control == 0:
            #self.run_main_thread.start()
            self.runMain()

        # WAIT COMMAND TO CLOSE SPLASH SCREEN
        if control == 2:
            # STOP TIMER
            self.timer.stop()
            # CLOSE SPLASH SCREEN
            self.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)

    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("IFGW Unicamp")
    QCoreApplication.setOrganizationDomain("portal.ifi.unicamp.br")
    QtGui.QFontDatabase.addApplicationFont('fonts/segoeui.ttf')
    QtGui.QFontDatabase.addApplicationFont('fonts/segoeuib.ttf')



    window = SplashScreen()
    #window = MainWindow()
    sys.exit(app.exec_())
