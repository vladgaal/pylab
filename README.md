# PyLab
> Python based program to run experiments in electronics and electricity teaching lab.

## Clone the repository

> If you don't yet have, [download and install git](https://git-scm.com/downloads). Use the git clone command to clone from: https://bitbucket.org/vladgaal/pylab.git .

---

## Install dependencies
**Windows users**

Download [here](https://www.python.org/downloads/windows/) and install the latest version of Python 3.7.
Go to the cloned folder ".\Dependencies" and execute the "install_all.bat" (just for x64 computers) to install everything python related that you need to run.
Go to the NI [page](https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html#346210), download and install the NI-VISA drivers.
 
**Linux users**

Install/update the following libraries:

> ```
> apt-get update && apt-get install -y --no-install-recommends \
>       "python3-matplotlib=2.1.1-2ubuntu3" \
>       "python3-numpy=1:1.13.3-2ubuntu1" \
>       "python3-Pyvisa==1.8" \
>       "python3-pandas=0.22.0-4" \
>       "python3-IPython" \
>       "python3-pylef"  
> ```

> ```
> sudo -H pip3 install pyside2
> ```

Go to the NI [page](https://www.ni.com/en-us/support/documentation/supplemental/18/downloading-and-installing-ni-driver-software-on-linux-desktop.html), download and install the NI-VISA drivers. We had issues with NI-VISA drivers on Linux distros, due to the **port access authorizations**. Manjaro seems to be the only versions that can handle this problem.

---
## Run

Just execute "PyLab.bat" or from the command line (on the cloned folder):

> ```
> python main.py
> ```


If you want to run an older version just use the respective "main_antigox.py", like:

> ```
> python main_antigo4.py
> ```

You can also right-click in the 'PyLab.bat' file and 'Send to' -> 'Desktop' to create a shortcut.

---

## Important tips

Based on user experiences, we outlined a set of tips on how to get better results with PyLab:

1 - The first screen have the HELP section that contains an explanation of all the acquisition modes.

2 - For now PyLab can connect just with "TBS1062" and "BK 4052", the equipments that we have.

3 - Windows often has problems with USB ports. If the USB cable is OK and the equipment is working well try to change the USB ports or even restart the computer.

---

## Release control

0.1 -> Initial development

0.2 -> New screen reading routines implemented

0.3 -> Usage tips implemented

0.4 -> Implemented plot of (Ch1 - Ch2) x Ch2

0.5 -> Changing the color theme of the screen capture mode and correction of the capture in simple channel

0.6 -> Initial implementation of the exercises

1.0 -> Implementation of a new graphical interface

2.0 -> Implementation of a modern layout to graphical interface and upgrade to Qt5