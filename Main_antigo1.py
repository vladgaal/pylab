import sys
#from win32com.shell import shell, shellcon
import cv2
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from PySide.QtGui import *
from PySide.QtCore import *
from PySide import QtCore, QtGui
import mainDialogV1 as mainDialog
import sobre
import pylef    # pylef para controlar instrumentos
import visa
import numpy as np   # Numpy para lidar com matrizes
import pandas as pd
import os   # module for general OS manipulation
import time
import copy
import csv


__appname__ = "PyLab"
__version__ = "0.1"
__modulo__ = 'M'


class Oprograma(QDialog, sobre.Ui_Dialog):

    def __init__(self, parent=None):
        super(Oprograma, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)
        self.setWindowTitle(__appname__ + " - Sobre o programa")
        self.okBt.clicked.connect(self.close)


class MySignal(QObject):
    status = Signal(str)
    falha = Signal(str)
    medida = Signal(float, float, float, float, int)
    terminado = Signal(str)
    terminadoTela = Signal(str)
    terminadoFft = Signal(str)
    tela = Signal(object, object, object)
    fftSignal = Signal(object, object)


class GraphThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.tempo = 1500
        self.dadosX = []
        self.dadosY1 = []
        self.dadosY2 = []
        self.signal = MySignal()
        self.graficoBranco = False
        self.telaBranca = False
        self.varreduraFreq = False
        self.tela = False
        self.fft = False
        self.fftBranco = False
        self.escalaCH1 = 1
        self.escalaCH2 = 1
    def run(self):
        __nome__ = __modulo__ + '07'

        if self.varreduraFreq:
            mensagem = 'Gerando grafico Freq'
            print('{};{}'.format(__nome__, mensagem))
            if len(self.dadosY1) >= 1:
                npt = len(self.dadosY1)
                fig, ax = plt.subplots(2, sharex=True, figsize=(7.5, 4.2))

                ax[0] = plt.subplot(211)  # define um eixo
                ax[0].plot(self.dadosX[0:npt], self.dadosY1[0:npt], 'go')  # plota a transmissão
                ax[0].set_xscale('log')
                ax[0].set_ylabel('Transmissão (dB)')  # seta escala do eixo y
                ax[0].grid(True)
                ax[0].axhline(y=-3, c="R", linewidth=1)
                ax[0].set_ylim((min(self.dadosY1)-0.2*abs(min(self.dadosY1)), max(self.dadosY1)+0.2*abs(max(self.dadosY1))))
                ax[0].set_xlim((min(self.dadosX), max(self.dadosX)))

                ax[1] = plt.subplot(212)  # define um eixo
                ax[1].plot(self.dadosX[0:npt], self.dadosY2[0:npt], 'bo')  # plota a transmissão
                ax[1].set_xscale('log')  # seta a escala de x para logaritmica
                # Por que não usamos escala log no eixo y também?
                ax[1].set_xlabel('frequência (Hz)')  # seta escala do eixo x
                ax[1].set_ylabel('Fase (graus)')  # seta escala do eixo y
                ax[1].grid(True)
                ax[1].axhline(y=90, c="R", linewidth=1)
                ax[1].axhline(y=-90, c="R", linewidth=1)
                ax[1].axhline(y=0, c="R", linewidth=1)
                ax[1].axhline(y=-180, c="R", linewidth=1)
                ax[1].axhline(y=180, c="R", linewidth=1)
                ax[1].set_ylim((min(self.dadosY2)-0.2*abs(min(self.dadosY2)), max(self.dadosY2)+0.2*abs(max(self.dadosY2))))
                ax[1].set_xlim((min(self.dadosX), max(self.dadosX)))

                fig.tight_layout()
                fig.savefig('varreduraFreq.png', format='png', bbox_inches='tight')
                plt.clf()
                plt.close()

                self.signal.terminado.emit('ok')

            time.sleep(0.001 * self.tempo)
            self.varreduraFreq = False

        if self.graficoBranco:
            mensagem = 'Gerando grafico branco'
            print('{};{}'.format(__nome__, mensagem))

            fig, ax = plt.subplots(2, sharex=True, figsize=(8, 4.2))

            ax[0] = plt.subplot(211)  # define um eixo
            ax[0].plot([1], [1], 'ro')
            ax[0].set_xscale('log')
            ax[0].set_ylabel('Transmissão (dB)')  # seta escala do eixo y
            ax[0].grid(True)
            ax[0].set_ylim(-5., 1.)
            ax[0].set_xlim(10., 100000.)

            ax[1] = plt.subplot(212)  # define um eixo
            ax[1].plot([1], [360], 'bo')
            ax[1].set_xscale('log')  # seta a escala de x para logaritmica
            # Por que não usamos escala log no eixo y também?
            ax[1].set_xlabel('frequência (Hz)')  # seta escala do eixo x
            ax[1].set_ylabel('Fase (graus)')  # seta escala do eixo y
            ax[1].grid(True)
            ax[1].set_ylim(-180, 180)
            ax[1].set_xlim(10, 100000)

            fig.savefig('varreduraFreq.png', bbox_inches='tight')
            plt.clf()
            plt.close()

            self.signal.terminado.emit('ok')
            self.graficoBranco = False

        if self.tela:
            mensagem = 'Gerando grafico tela'
            print('{};{}'.format(__nome__, mensagem))
            x = np.array(self.dadosX)
            if len(self.dadosY1) > 0:
                y1 = np.array(self.dadosY1)
            else:
                y1 = np.zeros(len(self.dadosX))
            if len(self.dadosY2) > 0:
                y2 = np.array(self.dadosY2)
            else:
                y2 = np.zeros(len(self.dadosX))

            fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
            ax1.plot(x, y1, 'b-')
            ax1.set_xlabel('tempo (s)')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('tensão CH1 (V)', color='b')
            ax1.tick_params('y', colors='b')
            ax1.set_ylim(-1*self.escalaCH1*4, self.escalaCH1*4)
            ax1.axis([float(min(x)), float(max(x)), float(min(y1)), float(max(y1))])
            ax1.grid(True)


            ax2 = ax1.twinx()
            ax2.plot(x, y2, 'r-')
            ax2.set_ylabel('tensão CH2 (V)', color='r')
            ax2.tick_params('y', colors='r')
            ax2.set_ylim(-1*self.escalaCH2*4, self.escalaCH2*4)
            ax2.axis([min(x), max(x), min(y2), max(y2)])
            ax2.grid(True)


            fig.tight_layout()
            fig.savefig('tela.png', bbox_inches='tight')
            plt.clf()
            plt.close()

            self.signal.terminadoTela.emit('ok')
            self.tela = False

        if self.telaBranca:
            mensagem = 'Gerando Tela branco'
            print('{};{}'.format(__nome__, mensagem))
            x = np.arange(1024)
            x = x / 1023
            x = x - max(x) / 2
            y1 = np.zeros(1024)
            y2 = np.zeros(1024)

            fig, ax1 = plt.subplots(figsize=(7.6, 4.2))
            ax1.plot(x, y1, 'b-')
            ax1.set_xlabel('tempo (s)')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('tensão CH1 (V)', color='b')
            ax1.tick_params('y', colors='b')
            ax1.grid(True)
            ax1.set_ylim(-1, 1)
            ax1.set_xlim(min(x), max(x))

            ax2 = ax1.twinx()
            ax2.plot(x, y2, 'r-')
            ax2.set_ylabel('tensão CH2 (V)', color='r')
            ax2.tick_params('y', colors='r')
            ax2.grid(True)
            ax2.set_ylim(-1, 1)

            fig.tight_layout()
            fig.savefig('tela.png', bbox_inches='tight')
            plt.show()

            self.signal.terminadoTela.emit('ok')
            self.telaBranca = False

        if self.fft:
            mensagem = 'Gerando grafico FFT'
            print('{};{}'.format(__nome__, mensagem))
            x = np.array(self.dadosX)
            if len(self.dadosY1) > 0:
                y1 = np.array(self.dadosY1)
            else:
                y1 = np.zeros(len(self.dadosX))

            fig, ax1 = plt.subplots(figsize=(8.5, 4.2))
            #ax1.plot(x, y1, 'r-')
            ax1.semilogx(x, y1, 'r')  # plotting the spectrum
            ax1.set_xlabel('Frequência (Hz)')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('|Y(freq)|')
            ax1.axis([1e1, max(x), 0, max(y1)])
            ax1.grid(True)

            fig.tight_layout()
            fig.savefig('fft.png', bbox_inches='tight')
            plt.show()

            self.signal.terminadoFft.emit('ok')
            self.fft = False

        if self.fftBranco:
            mensagem = 'Gerando grafico FFT em branco'
            print('{};{}'.format(__nome__, mensagem))
            x = np.linspace(10, 1e6, 1000)
            y1 = np.zeros(len(x))

            fig, ax1 = plt.subplots(figsize=(8.5, 4.2))
            ax1.semilogx(x, y1, 'r')  # plotting the spectrum
            ax1.set_xlabel('Frequência (Hz)')
            ax1.set_ylabel('|Y(freq)|')
            ax1.axis([1e1, max(x), 0, 1])
            ax1.grid(True)

            fig.tight_layout()
            fig.savefig('fft.png', bbox_inches='tight')
            plt.show()
            self.fftBranco = False
            self.signal.terminadoFft.emit('ok')


class EquipamentosThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.conectarScope = False
        self.conectarGerador = False
        self.desconectarScope = False
        self.desconectarGerador = False
        self.varrerFreq = False
        self.capturarTela = False
        self.scopeConectado = False
        self.geradorConectado = False
        self.interromperFreq = False
        self.fft = False
        self.freqInicial = 0
        self.freqFinal = 0
        self.quantidadePontos = 0
        self.medias = 4
        self.signal = MySignal()
        self.escalaCh1 = 1
        self.escalaCh2 = 1
        self.acopCh1 = 'AC'
        self.acopCh2 = 'AC'
        self.offSetCh1 = 0
        self.offSetCh2 = 0
        self.escalaTempo = 0.001
        self.ch1 = True
        self.ch2 = True
        self.tempo = True

    def run(self):
        __nome__ = __modulo__ + '04'
        if self.conectarScope:
            try:
                mensagem = 'Tentando conectar Scope'
                print('{};{}'.format(__nome__, mensagem))
                self.signal.status.emit('Conectando Scope')
                visa.ResourceManager().list_resources()
                self.scope = pylef.TektronixTBS1062()
                self.scope.start_acquisition()
                self.scope.set_sample()
                self.scope.ch1.set_probe(1)
                self.scope.ch2.set_probe(1)
                self.scope.ch1.set_position(0)
                self.scope.ch2.set_position(0)
                self.scope.ch1.turn_on()
                self.scope.ch2.turn_on()
                self.scope.ch1.set_scale(2)
                self.scope.ch2.set_scale(2)
                self.scopeConectado = True
                self.signal.status.emit('Scope conectado')

            except ValueError:
                self.signal.falha.emit('Scope')
                self.scopeConectado = False
                self.signal.status.emit('Scope desconectado')
                mensagem = 'Problema ao conectar Scope'
                print('{};{}'.format(__nome__, mensagem))
            self.conectarScope = False

        if self.desconectarScope:
            if self.scopeConectado:
                mensagem = 'Desconectar Scope'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.close()
                self.scopeConectado = False
                self.signal.status.emit('Scope desconectado')
            self.desconectarScope = False

        if self.conectarGerador:
            try:
                mensagem = 'Tentando conectar Gerador'
                print('{};{}'.format(__nome__, mensagem))
                self.signal.status.emit('Conectando Gerador')
                visa.ResourceManager().list_resources()
                self.gerador = pylef.BK4052()
                self.geradorConectado = True
                self.signal.status.emit('Gerador conectado')

            except ValueError:
                self.signal.falha.emit('Gerador')
                self.geradorConectado = False
                self.signal.status.emit('Gerador desconectado')
                mensagem = 'Problema ao conectar Gerador'
                print('{};{}'.format(__nome__, mensagem))
            self.conectarGerador = False

        if self.desconectarGerador:
            if self.geradorConectado:
                mensagem = 'Desconectar Gerador'
                print('{};{}'.format(__nome__, mensagem))
                self.gerador.close()
                self.geradorConectado = False
                self.signal.status.emit('Gerador desconectado')
            self.desconectarGerador = False

        if self.varrerFreq:
            self.freq = np.logspace(np.log10(self.freqInicial), np.log10(self.freqFinal), self.quantidadePontos,
                                    endpoint=True)  # varredura logaritmica
            self.scope.set_average_number(self.medias)  # ajusta o número de médias
            self.scope.set_average()  # turn average ON
            # -----------------
            self.Vpp1 = 0
            self.Vpp2 = 0  # listas para guardar as variáveis
            self.phase = 0  # listas para guardar as variáveis
            ### aquisição de dados no gerador com varredura de frequência
            self.scope.write('MEASUREment:MEAS1:TYPE NONE')
            self.scope.write('MEASUREment:MEAS2:TYPE NONE')
            self.scope.write('MEASUREment:MEAS3:TYPE NONE')
            self.scope.write('MEASUREment:MEAS4:TYPE NONE')
            self.scope.write('MEASUREment:MEAS5:TYPE NONE')
            self.scope.ch1.turn_on()
            self.scope.ch2.turn_on()
            self.scope.ch1.set_ac()
            self.scope.ch2.set_ac()
            self.scope.ch1.set_position(0)
            self.scope.ch2.set_position(0)
            self.gerador.ch1.turn_on()
            #Ligar o gerador
            for m, freqP in enumerate(list(self.freq)):  # loop de aquisição
                mensagem = 'Ponto de medida ' + str(m) + ': ' + str(freqP) + 'Hz'
                print('{};{}'.format(__nome__, mensagem))
                ### ajuste dos instrumentos
                self.gerador.ch1.set_frequency(freqP)  # muda a frequência
                periodP = 1. / freqP  # período da onda
                self.scope.set_horizontal_scale(periodP / 4.)  # escala horizontal = 1/4 período (2.5 oscilações em tela)
                self.scope.ch1.set_smart_scale()  # rescala o canal 1
                self.scope.ch2.set_smart_scale()  # rescala o canal 2
                ### aquisição de dados
                self.Vpp1 = (self.scope.ch1.measure.Vpp())  # acumula a medida do Vpp no canal 1
                self.phase = (-self.scope.ch1.measure.phase())  # acumula a medida da fase no canal 1
                self.Vpp2 = (self.scope.ch2.measure.Vpp())  # acumula a medida do Vpp no canal 2
                self.signal.medida.emit(self.Vpp1, self.Vpp2, self.phase, freqP, m)
                if self.interromperFreq:
                    break
            self.signal.terminado.emit('OK')
            self.varrerFreq = False

        if self.capturarTela:
            mensagem = 'Lendo canais'
            print('{};{}'.format(__nome__, mensagem))
            canal1 = []
            canal2 = []
            tempo = []

            if self.tempo:
                self.scope.set_average_number(4)  # ajusta o número de médias
                self.scope.set_horizontal_scale(self.escalaTempo)

            if self.ch1:
                mensagem = 'Lendo canal 1'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.ch1.turn_on()
                if self.acopCh1 == 'AC':
                    self.scope.ch1.set_ac()
                elif self.acopCh1 == 'DC':
                    self.scope.ch1.set_dc()
                self.scope.ch1.set_scale(self.escalaCh1)
                self.scope.ch1.set_position(self.offSetCh1)
                time.sleep(0.5)
                tempo, canal1 = self.scope.ch1.read_channel()
            else:
                self.scope.ch1.turn_on()
                time.sleep(0.5)
                tempo, canal1 = self.scope.ch1.read_channel()

            if self.ch2:
                mensagem = 'Lendo canal 2'
                print('{};{}'.format(__nome__, mensagem))
                self.scope.ch2.turn_on()
                if self.acopCh2 == 'AC':
                    self.scope.ch2.set_ac()
                elif self.acopCh2 == 'DC':
                    self.scope.ch2.set_dc()
                self.scope.ch2.set_scale(self.escalaCh2)
                self.scope.ch2.set_position(self.offSetCh2)
                time.sleep(0.5)
                tempo, canal2 = self.scope.ch2.read_channel()
            else:
                self.scope.ch2.turn_on()
                time.sleep(0.5)
                tempo, canal2 = self.scope.ch2.read_channel()



            self.signal.tela.emit(canal1, canal2, tempo)
            self.capturarTela = False

        if self.fft:
            mensagem = 'Lendo canal'
            print('{};{}'.format(__nome__, mensagem))

            if self.ch1:
                tempo, canal1 = self.scope.ch1.read_channel()
                escala = self.scope.horizontal_scale()
                tempoReal = np.linspace(0, escala*10, len(tempo))
                self.signal.fftSignal.emit(tempo, canal1)

            if self.ch2:
                tempo, canal2 = self.scope.ch2.read_channel()
                self.signal.fftSignal.emit(tempo, canal2)

            self.fft = False


class MainDialog(QDialog, mainDialog.Ui_Dialog):

    #scopeConectado = False
    geradorConectado = False
    average = 4
    tensao1 = []
    tensao2 = []
    fase = []
    frequencia = []
    transmitancia = []
    transmitanciaDb = []
    ch1Tela = []
    ch2Tela = []
    tempoTela = []
    #diretorio = shell.SHGetFolderPath(0, shellcon.CSIDL_PERSONAL, None, 0)
    diretorio = '*'

    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)

        self.iconConectar = QtGui.QIcon()
        self.iconConectar.addPixmap(QtGui.QPixmap(":/Main/icons/conectar.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.conectarBtOsc.setIcon(self.iconConectar)
        self.conectarBtGer.setIcon(self.iconConectar)

        self.iconDesconectar = QtGui.QIcon()
        self.iconDesconectar.addPixmap(QtGui.QPixmap(":/Main/icons/desconectar.png"), QtGui.QIcon.Normal,
                                       QtGui.QIcon.Off)

        self.setWindowTitle(__appname__ + ' V' + __version__)
        self.stackedWidget.setCurrentIndex(1)
        self.modelosGerCombo.addItems(['', 'BK 4052'])
        self.modelosOscCombo.addItems(['', 'TBS1062'])
        self.modelosOscCombo.setCurrentIndex(1)
        self.modelosGerCombo.setCurrentIndex(1)
        self.freqInicialBox.addItems(['Hz', 'KHz', 'MHz'])
        self.freqFinalBox.addItems(['Hz', 'KHz', 'MHz'])
        self.escalaBox_CH1.addItems(['V/Div', 'mV/Div'])
        self.escalaBox_CH2.addItems(['V/Div', 'mV/Div'])
        self.offsetBox_CH1.addItems(['V', 'mV'])
        self.offsetBox_CH2.addItems(['V', 'mV'])
        self.msRB_T.setChecked(1)
        self.progressBar.setRange(0, 100)
        self.progressBar.setValue(0)

        self.graph_thread = GraphThread()
        self.graph_thread.signal.terminado.connect(self.atualizaGraficoFreq)
        self.graph_thread.signal.terminadoTela.connect(self.atualizaGraficoTela)
        self.graph_thread.signal.terminadoFft.connect(self.atualizaGraficoFft)
        self.equipamentos_thread = EquipamentosThread()
        self.equipamentos_thread.start()
        self.equipamentos_thread.signal.status.connect(self.atualizaStatus)
        self.equipamentos_thread.signal.falha.connect(self.falha)
        self.equipamentos_thread.signal.medida.connect(self.recebendoMedidaFreq)
        self.equipamentos_thread.signal.terminado.connect(self.varreduraFreqTerminada)
        self.equipamentos_thread.signal.tela.connect(self.recebendoTela)
        self.equipamentos_thread.signal.fftSignal.connect(self.calculoFft)

        #self.timer = QTimer()
        #self.timer.timeout.connect(self.atualizaStatus)
        #self.timer.start(500)

        self.habilitarCH1()
        self.habilitarCH2()
        self.habilitarTempo()
        self.canalUmCheck.toggled.connect(self.habilitarCH1)
        self.canalDoisCheck.toggled.connect(self.habilitarCH2)
        self.canalTempoCheck.toggled.connect(self.habilitarTempo)

        self.freqInicialLine.setInputMask('9999')
        self.freqInicialLine.setText('10')
        self.freqFinalLine.setInputMask('9999')
        self.freqFinalLine.setText('100')
        self.freqInicialBox.setCurrentIndex(0)
        self.freqFinalBox.setCurrentIndex(1)
        self.numeroPontos.setInputMask('9999')
        self.numeroPontos.setText('10')

        self.escalaLine_CH1.setInputMask('999')
        self.escalaLine_CH1.setText('1')
        self.escalaLine_CH2.setInputMask('999')
        self.escalaLine_CH2.setText('1')
        self.escalaBox_CH1.setCurrentIndex(0)
        self.escalaBox_CH2.setCurrentIndex(0)
        self.offsetLine_CH1.setInputMask('999')
        self.offsetLine_CH1.setText('0')
        self.offsetLine_CH2.setInputMask('999')
        self.offsetLine_CH2.setText('0')
        self.offsetBox_CH1.setCurrentIndex(1)
        self.offsetBox_CH2.setCurrentIndex(1)
        self.escalaLine_T.setInputMask('999')
        self.escalaLine_T.setText('1')

        self.exercicio1Bt.clicked.connect(self.telaExecercicio1)
        self.fftBt.clicked.connect(self.telaFft)
        self.freqBt.clicked.connect(self.teleFrequencia)
        self.ondaBt.clicked.connect(self.telaOnda)

        self.sairBt.clicked.connect(self.close)
        self.sobreBt.clicked.connect(self.telaDuvida)
        self.conectarBtOsc.clicked.connect(self.conectarScope)
        self.conectarBtGer.clicked.connect(self.conectarGerador)


        self.startBt_Freq.clicked.connect(self.varreduraFreq)
        self.stopBt_Freq.clicked.connect(self.interromperFreq)
        self.novoBt_Freq.clicked.connect(self.limparVarreduraFreq)
        self.salvarBt_Freq.clicked.connect(self.salvarFreq2)

        self.capturarBt_Cap.clicked.connect(self.capturaTela)
        self.acoplamentoCh1Bt.clicked.connect(self.acoplamentoCh1)
        self.acoplamentoCh2Bt.clicked.connect(self.acoplamentoCh2)
        self.salvarBt_Cap.clicked.connect(self.salvarTela)

        self.startBt_Fft.clicked.connect(self.iniciaFft)
        self.salvarBt_Fft.clicked.connect(self.salvarFft)

        self.atualizaStatus('Scope desconectado')
        self.atualizaStatus('Gerador desconectado')
        self.stopBt_Freq.setEnabled(False)
        self.salvarBt_Freq.setEnabled(False)
        self.novoBt_Freq.setEnabled(False)
        self.salvarBt_Cap.setEnabled(False)
        self.salvarBt_Fft.setEnabled(False)
        self.statusFreq.setText("Em espera")
        self.statusFreq.setStyleSheet("QLabel#statusFreq {color: blue; font: bold 12pt; " +
                                     "font-family: Tahoma;}")

        self.graficoBranco()
        self.graficoBrancoTela()
        self.graficoBrancoFft()
        self.acoplamentoCh1()
        self.acoplamentoCh2()

    def acoplamentoCh1(self):
        if self.acoplamentoCh1Bt.text() == 'AC':
            self.acoplamentoCh1Bt.setText('DC')
            self.acoplamentoCh1Bt.setToolTip(QtGui.QApplication.translate("Dialog", "Muda o acoplamento para AC", None,
                                                                          QtGui.QApplication.UnicodeUTF8))

        elif self.acoplamentoCh1Bt.text() == 'DC':
            self.acoplamentoCh1Bt.setText('AC')
            self.acoplamentoCh1Bt.setToolTip(QtGui.QApplication.translate("Dialog", "Muda o acoplamento para DC", None,
                                                                          QtGui.QApplication.UnicodeUTF8))

    def acoplamentoCh2(self):
        if self.acoplamentoCh2Bt.text() == 'AC':
            self.acoplamentoCh2Bt.setText('DC')
            self.acoplamentoCh2Bt.setToolTip(QtGui.QApplication.translate("Dialog", "Muda o acoplamento para AC", None,
                                                                          QtGui.QApplication.UnicodeUTF8))

        elif self.acoplamentoCh2Bt.text() == 'DC':
            self.acoplamentoCh2Bt.setText('AC')
            self.acoplamentoCh2Bt.setToolTip(QtGui.QApplication.translate("Dialog", "Muda o acoplamento para DC", None,
                                                                          QtGui.QApplication.UnicodeUTF8))

    def telaExecercicio1(self):
        self.stackedWidget.setCurrentIndex(0)

    def teleFrequencia(self):
        self.stackedWidget.setCurrentIndex(1)

    def telaOnda(self):
        self.stackedWidget.setCurrentIndex(2)

    def telaFft(self):
        self.stackedWidget.setCurrentIndex(3)

    def limparVarreduraFreq(self):
        __nome__ = __modulo__ + '06'
        result = QMessageBox.question(self, __appname__, "Tem certeza que deseja apagar os dados?",
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        if result == QMessageBox.Yes:
            mensagem = 'Limpando dados de varredura em Freq'
            print('{};{}'.format(__nome__, mensagem))
            self.graficoBranco()
            self.atualizaGraficoFreq()
            self.salvarBt_Freq.setEnabled(False)
            self.novoBt_Freq.setEnabled(False)
            del self.tensao1[:]
            del self.tensao2[:]
            del self.fase[:]
            del self.frequencia[:]
            del self.transmitancia[:]
            del self.transmitanciaDb[:]
            self.graph_thread.dadosX = copy.deepcopy(self.frequencia)
            self.graph_thread.dadosY1 = copy.deepcopy(self.transmitanciaDb)
            self.graph_thread.dadosY2 = copy.deepcopy(self.fase)

    def graficoBranco(self):
        __nome__ = __modulo__ + '05'
        mensagem = 'Gerando grafico em branco'
        #print('{};{}'.format(__nome__, mensagem))
        self.graph_thread.graficoBranco = True
        self.graph_thread.start()

    def graficoBrancoTela(self):
        __nome__ = __modulo__ + '12'
        mensagem = 'Gerando Tela em branco'
        #print('{};{}'.format(__nome__, mensagem))
        self.graph_thread.telaBranca = True
        self.graph_thread.start()

    def graficoBrancoFft(self):
        __nome__ = __modulo__ + '18'
        mensagem = 'Gerando FFT em branco'
        #print('{};{}'.format(__nome__, mensagem))
        self.graph_thread.fftBranco = True
        self.graph_thread.start()

    def interromperFreq(self):
        __nome__ = __modulo__ + '02'
        mensagem = 'Varredura em freq interrompida manualmente'
        print('{};{}'.format(__nome__, mensagem))
        self.equipamentos_thread.varrerFreq = False
        self.equipamentos_thread.interromperFreq = True

    def varreduraFreqTerminada(self):
        time.sleep(1)
        __nome__ = __modulo__ + '03'
        mensagem = 'Varredura em freq terminada'
        print('{};{}'.format(__nome__, mensagem))
        self.dadosFreq = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosFreq['frequencia (Hz)'] = self.frequencia
        self.dadosFreq['Vpp1 (V)'], self.dadosFreq['Vpp2 (V)'] = self.tensao1, self.tensao2
        self.dadosFreq['fase (Ch2-Ch1) (graus)'] = self.fase
        self.dadosFreq['T'], self.dadosFreq['T_dB'] = self.transmitancia, self.transmitanciaDb
        mensagem = 'Banco de dados Freq criado'
        print('{};{}'.format(__nome__, mensagem))
        self.progressBar.setValue(100)
        self.salvarBt_Freq.setEnabled(True)
        self.startBt_Freq.setEnabled(True)
        self.stopBt_Freq.setEnabled(False)
        self.novoBt_Freq.setEnabled(True)
        self.statusFreq.setText("Em espera")
        self.statusFreq.setStyleSheet("QLabel#statusFreq {color: blue; font: bold 12pt; " +
                                      "font-family: Tahoma;}")

    def recebendoMedidaFreq(self, vpp1, vpp2, phase, freq, ponto):
        __nome__ = __modulo__ + '03'
        self.progressBar.setValue(100*ponto/self.equipamentos_thread.quantidadePontos)
        self.tensao1.append(vpp1)
        self.tensao2.append(vpp2)
        self.fase.append(phase)
        self.frequencia.append(freq)
        T = (vpp2 / vpp1) ** 2  # cálculo da transmissão
        T_dB = 10 * np.log10(T)  # transmissão em dB
        self.transmitancia.append(T)
        self.transmitanciaDb.append(T_dB)
        mensagem = 'Recebido V1:' + str(vpp1) + ' V2:' + str(vpp2) + ' fase:' + str(phase) + ' freq:' + str(freq)
        print('{};{}'.format(__nome__, mensagem))

        self.graph_thread.dadosX = copy.deepcopy(self.frequencia)
        self.graph_thread.dadosY1 = copy.deepcopy(self.transmitanciaDb)
        self.graph_thread.dadosY2 = copy.deepcopy(self.fase)

        self.graph_thread.varreduraFreq = True
        self.graph_thread.start()

    def varreduraFreq(self):
        __nome__ = __modulo__ + '01'
        mensagem = 'Iniciando varredura em freq'
        print('{};{}'.format(__nome__, mensagem))
        self.equipamentos_thread.freqInicial = float(self.freqInicialLine.text())*10**(3*self.freqInicialBox.currentIndex())
        self.equipamentos_thread.freqFinal = float(self.freqFinalLine.text())*10**(3*self.freqFinalBox.currentIndex())
        self.equipamentos_thread.quantidadePontos = int(self.numeroPontos.text())
        self.equipamentos_thread.interromperFreq = False
        self.progressBar.setValue(0)
        self.graficoBranco()
        if self.equipamentos_thread.scopeConectado and self.equipamentos_thread.geradorConectado:
            del self.tensao1[:]
            del self.tensao2[:]
            del self.fase[:]
            del self.frequencia[:]
            del self.transmitancia[:]
            del self.transmitanciaDb[:]
            self.statusFreq.setText("Varrendo...")
            self.statusFreq.setStyleSheet("QLabel#statusFreq {color: red; font: bold 12pt; " +
                                          "font-family: Tahoma;}")
            self.startBt_Freq.setEnabled(False)
            self.stopBt_Freq.setEnabled(True)
            self.graph_thread.varreduraFreq = True
            self.equipamentos_thread.varrerFreq = True
            self.equipamentos_thread.start()
            self.graph_thread.start()
        else:
            QMessageBox.warning(self, "Falha", "Conecte os equipamentos "
                                               "\npara iniciar as medidas.", QMessageBox.Ok)

    def salvarFreq2(self):
        __nome__ = __modulo__ + '09'
        dir = self.diretorio
        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        mensagem = 'Janela de salvar Freq aberta'
        print('{};{}'.format(__nome__, mensagem))

        if self.outFile[0] != '':
            self.dadosFreq.to_csv(self.outFile[0][:len(self.outFile[0])-4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0])-4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))

            im = cv2.imread('varreduraFreq.png')
            cv2.imwrite(self.outFile[0][:len(self.outFile[0])-4]+'_figura.png', im)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png'
            print('{};{}'.format(__nome__, mensagem))

            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'Nenhum arquivo selecionado'
            print('{};{}'.format(__nome__, mensagem))

    def capturaTela(self):
        __nome__ = __modulo__ + '10'
        if self.equipamentos_thread.scopeConectado and self.equipamentos_thread.geradorConectado:
            mensagem = 'Iniciando captura de tela'
            print('{};{}'.format(__nome__, mensagem))

            if not self.canalUmCheck.isChecked():
                self.equipamentos_thread.ch1 = True
                self.equipamentos_thread.acopCh1 = self.acoplamentoCh1Bt.text()
                if self.escalaBox_CH1.currentIndex() == 0:
                    self.equipamentos_thread.escalaCh1 = float(self.escalaLine_CH1.text())
                if self.escalaBox_CH1.currentIndex() == 1:
                    self.equipamentos_thread.escalaCh1 = float(self.escalaLine_CH1.text())*0.001
                if self.offsetBox_CH1.currentIndex() == 0:
                    self.equipamentos_thread.offSetCh1 = float(self.offsetLine_CH1.text())
                if self.offsetBox_CH1.currentIndex() == 1:
                    self.equipamentos_thread.offSetCh1 = float(self.offsetLine_CH1.text())*0.001
            else:
                self.equipamentos_thread.ch1 = False

            if not self.canalDoisCheck.isChecked():
                self.equipamentos_thread.ch2 = True
                self.equipamentos_thread.acopCh2 = self.acoplamentoCh2Bt.text()
                if self.escalaBox_CH2.currentIndex() == 0:
                    self.equipamentos_thread.escalaCh2 = float(self.escalaLine_CH2.text())
                if self.escalaBox_CH2.currentIndex() == 1:
                    self.equipamentos_thread.escalaCh2 = float(self.escalaLine_CH2.text()) * 0.001
                if self.offsetBox_CH2.currentIndex() == 0:
                    self.equipamentos_thread.offSetCh2 = float(self.offsetLine_CH2.text())
                if self.offsetBox_CH2.currentIndex() == 1:
                    self.equipamentos_thread.offSetCh2 = float(self.offsetLine_CH2.text()) * 0.001
            else:
                self.equipamentos_thread.ch2 = False

            if not self.canalTempoCheck.isChecked():
                self.equipamentos_thread.tempo = True
                if self.sRB_T.isChecked():
                    self.equipamentos_thread.escalaTempo = self.escalaLine_T
                elif self.msRB_T.isChecked():
                    self.equipamentos_thread.escalaTempo = float(self.escalaLine_T.text()) * 10 ** (-3)
                elif self.usRB_T.isChecked():
                    self.equipamentos_thread.escalaTempo = float(self.escalaLine_T.text()) * 10 ** (-6)
                elif self.nsRB_T.isChecked():
                    self.equipamentos_thread.escalaTempo = float(self.escalaLine_T.text()) * 10 ** (-9)
            else:
                self.equipamentos_thread.tempo = False

            self.equipamentos_thread.capturarTela = True
            self.equipamentos_thread.start()
            self.salvarBt_Cap.setEnabled(True)
        else:
            QMessageBox.warning(self, "Falha", "Conecte os equipamentos "
                                               "\npara iniciar as medidas.", QMessageBox.Ok)

    def recebendoTela(self, ch1, ch2, tempo):
        __nome__ = __modulo__ + '13'
        self.ch1Tela = ch1
        self.ch2Tela = ch2
        self.tempoTela = tempo
        self.graph_thread.tela = True
        self.graph_thread.dadosX = copy.deepcopy(self.tempoTela)
        self.graph_thread.dadosY1 = copy.deepcopy(self.ch1Tela)
        self.graph_thread.dadosY2 = copy.deepcopy(self.ch2Tela)
        self.graph_thread.escalaCH1 = float(self.escalaLine_CH1.text()) * 10 ** (-3 * self.escalaBox_CH1.currentIndex())
        self.graph_thread.escalaCH2 = float(self.escalaLine_CH2.text()) * 10 ** (-3 * self.escalaBox_CH2.currentIndex())
        self.graph_thread.start()
        self.dadosTela = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosTela['Tempo (s)'] = self.tempoTela
        self.dadosTela['V1 (V)'], self.dadosTela['V2 (V)'] = self.ch1Tela, self.ch2Tela
        mensagem = 'Banco de dados Tela criado'
        print('{};{}'.format(__nome__, mensagem))

    def salvarTela(self):
        __nome__ = __modulo__ + '14'
        dir = self.diretorio

        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        mensagem = 'Janela de salvar Tela aberta'
        print('{};{}'.format(__nome__, mensagem))

        if self.outFile[0] != '':
            self.dadosTela.to_csv(self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))

            im = cv2.imread('tela.png')
            cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png', im)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png'
            print('{};{}'.format(__nome__, mensagem))

            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'Nenhum arquivo selecionado'
            print('{};{}'.format(__nome__, mensagem))

    def iniciaFft(self):
        __nome__ = __modulo__ + '15'
        mensagem = 'Iniciando leitura para FFT'
        print('{};{}'.format(__nome__, mensagem))
        if self.equipamentos_thread.scopeConectado:
            if self.CH1_Fft.isChecked():
                self.equipamentos_thread.ch1 = True
                self.equipamentos_thread.ch2 = False

            if self.CH2_Fft.isChecked():
                self.equipamentos_thread.ch1 = False
                self.equipamentos_thread.ch2 = True

            self.equipamentos_thread.fft = True
            self.equipamentos_thread.start()
            self.salvarBt_Fft.setEnabled(True)

        else:
            mensagem = 'Osciloscópio não conectado'
            print('{};{}'.format(__nome__, mensagem))
            QMessageBox.warning(self, "Falha", "Conecte o osciloscópio"
                                               "\npara iniciar as medidas.", QMessageBox.Ok)

    def calculoFft(self, tempo, canal):
        __nome__ = __modulo__ + '16'
        mensagem = 'Iniciando calculo do FFT'
        print('{};{}'.format(__nome__, mensagem))
        delta = tempo[1] - tempo[0]  # time step\n",
        tempo = tempo - tempo[0] + delta
        Ts = float(tempo[0])
        n = len(tempo)  # length of the signal
        k = np.arange(n)
        T = n * Ts
        frq = k / T
        fft = np.fft.fft(canal) / n  # fft computing and normalization

        self.graph_thread.fft = True
        self.graph_thread.dadosX = copy.deepcopy(frq)
        self.graph_thread.dadosY1 = copy.deepcopy(abs(fft))
        self.graph_thread.start()
        self.dadosFft = pd.DataFrame()  # inicializa um dataframe do pandas
        self.dadosFft['Frequencia (Hz)'] = frq
        self.dadosFft['Amplitude'] = abs(fft)

    def salvarFft(self):
        __nome__ = __modulo__ + '19'
        dir = self.diretorio

        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        mensagem = 'Janela de salvar FFT aberta'
        print('{};{}'.format(__nome__, mensagem))

        if self.outFile[0] != '':
            self.dadosFft.to_csv(self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv', sep=',', index=False)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_dados.csv'
            print('{};{}'.format(__nome__, mensagem))

            im = cv2.imread('fft.png')
            cv2.imwrite(self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png', im)
            mensagem = 'Arquivo salvo: ' + self.outFile[0][:len(self.outFile[0]) - 4] + '_grafico.png'
            print('{};{}'.format(__nome__, mensagem))

            self.diretorio = os.path.dirname(os.path.abspath(self.outFile[0]))
        else:
            mensagem = 'Nenhum arquivo selecionado'
            print('{};{}'.format(__nome__, mensagem))

    def atualizaGraficoFreq(self):
        __nome__ = __modulo__ + '08'
        mensagem = 'Atualizando grafico Freq'
        print('{};{}'.format(__nome__, mensagem))
        self.pixmap = QPixmap("varreduraFreq.png")
        tamanho = self.pixmap.size()
        self.grafico_Freq.setGeometry(QtCore.QRect(209, 50, tamanho.width(), tamanho.height()))
        self.grafico_Freq.setPixmap(self.pixmap)

    def atualizaGraficoTela(self):
        __nome__ = __modulo__ + '11'
        mensagem = 'Atualizando grafico Tela'
        print('{};{}'.format(__nome__, mensagem))

        self.pixmap = QPixmap("tela.png")
        tamanho = self.pixmap.size()
        self.grafico_Cap.setGeometry(QtCore.QRect(200, 40, tamanho.width(), tamanho.height()))
        self.grafico_Cap.setPixmap(self.pixmap)

    def atualizaGraficoFft(self):
        __nome__ = __modulo__ + '17'
        mensagem = 'Atualizando grafico FFT'
        print('{};{}'.format(__nome__, mensagem))
        self.pixmap = QPixmap("fft.png")
        tamanho = self.pixmap.size()
        self.grafico_Fft.setGeometry(QtCore.QRect(110, 50, tamanho.width(), tamanho.height()))
        self.grafico_Fft.setPixmap(self.pixmap)

    def habilitarCH1(self):
        if not self.canalUmCheck.isChecked():
            self.escalaLine_CH1.setEnabled(True)
            self.escalaBox_CH1.setEnabled(True)
            self.offsetBox_CH1.setEnabled(True)
            self.offsetLine_CH1.setEnabled(True)
            self.acoplamentoCh1Bt.setEnabled(True)
            self.canalUmCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Usar as configurações do osciloscópio para o canal 1", None,
                                             QtGui.QApplication.UnicodeUTF8))

        else:
            self.escalaLine_CH1.setEnabled(False)
            self.escalaBox_CH1.setEnabled(False)
            self.offsetBox_CH1.setEnabled(False)
            self.offsetLine_CH1.setEnabled(False)
            self.acoplamentoCh1Bt.setEnabled(False)
            self.canalUmCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Usar as configurações do programa para o canal 1", None,
                                             QtGui.QApplication.UnicodeUTF8))

    def habilitarCH2(self):
        if not self.canalDoisCheck.isChecked():
            self.escalaLine_CH2.setEnabled(True)
            self.escalaBox_CH2.setEnabled(True)
            self.offsetBox_CH2.setEnabled(True)
            self.offsetLine_CH2.setEnabled(True)
            self.acoplamentoCh2Bt.setEnabled(True)
            self.canalDoisCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Usar as configurações do osciloscópio para o canal 2", None,
                                             QtGui.QApplication.UnicodeUTF8))

        else:
            self.escalaLine_CH2.setEnabled(False)
            self.escalaBox_CH2.setEnabled(False)
            self.offsetBox_CH2.setEnabled(False)
            self.offsetLine_CH2.setEnabled(False)
            self.acoplamentoCh2Bt.setEnabled(False)
            self.canalDoisCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Usar as configurações do programa para o canal 2", None,
                                             QtGui.QApplication.UnicodeUTF8))

    def habilitarTempo(self):
        if not self.canalTempoCheck.isChecked():
            self.escalaLine_T.setEnabled(True)
            self.sRB_T.setEnabled(True)
            self.msRB_T.setEnabled(True)
            self.nsRB_T.setEnabled(True)
            self.usRB_T.setEnabled(True)
            self.canalTempoCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Usar as configurações do osciloscópio para o tempo", None,
                                             QtGui.QApplication.UnicodeUTF8))

        else:
            self.escalaLine_T.setEnabled(False)
            self.sRB_T.setEnabled(False)
            self.msRB_T.setEnabled(False)
            self.nsRB_T.setEnabled(False)
            self.usRB_T.setEnabled(False)
            self.canalTempoCheck.setToolTip(
                QtGui.QApplication.translate("Dialog", "Usar as configurações do programa para o tempo", None,
                                             QtGui.QApplication.UnicodeUTF8))

    def telaDuvida(self):
        prog = Oprograma()
        prog.exec_()

    def conectarScope(self):
        if self.equipamentos_thread.scopeConectado:
            self.equipamentos_thread.desconectarScope = True
            self.equipamentos_thread.start()
        else:
            if self.modelosOscCombo.currentIndex() == 1:
                self.equipamentos_thread.conectarScope = True
                self.equipamentos_thread.start()
            else:
                QMessageBox.warning(self, "Falha ao conectar", "Selecione um osciloscópio válido.", QMessageBox.Ok)

    def conectarGerador(self):
        if self.equipamentos_thread.geradorConectado:
            self.equipamentos_thread.desconectarGerador = True
            self.equipamentos_thread.start()
        else:
            if self.modelosGerCombo.currentIndex() == 1:
                self.equipamentos_thread.conectarGerador = True
                self.equipamentos_thread.start()
            else:
                QMessageBox.warning(self, "Falha ao conectar", "Selecione um gerador válido.", QMessageBox.Ok)

    def atualizaStatus(self, data):

        if data == 'Conectando Scope':
            self.osciloscopioStatus.setText("Conectando...")
            self.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: blue; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
        elif data == 'Scope conectado':
            self.osciloscopioStatus.setText("Conectado")
            self.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: green; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
            self.conectarBtOsc.setIcon(self.iconDesconectar)
            self.conectarBtOsc.setToolTip(
                QtGui.QApplication.translate("Dialog", "Desconecta o osciloscópio", None,
                                             QtGui.QApplication.UnicodeUTF8))


        elif data == 'Scope desconectado':
            self.osciloscopioStatus.setText("Desconectado")
            self.osciloscopioStatus.setStyleSheet("QLabel#osciloscopioStatus {color: red; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
            self.conectarBtOsc.setIcon(self.iconConectar)
            self.conectarBtOsc.setToolTip(
                QtGui.QApplication.translate("Dialog", "Conecta o osciloscópio", None,
                                             QtGui.QApplication.UnicodeUTF8))

        elif data == 'Conectando Gerador':
            self.geradorStatus.setText("Conectando...")
            self.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: blue; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
        elif data == 'Gerador conectado':
            self.geradorStatus.setText("Conectado")
            self.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: green; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
            self.conectarBtGer.setIcon(self.iconDesconectar)
            self.conectarBtGer.setToolTip(
                QtGui.QApplication.translate("Dialog", "Desconecta o gerador", None,
                                             QtGui.QApplication.UnicodeUTF8))


        elif data == 'Gerador desconectado':
            self.geradorStatus.setText("Desconectado")
            self.geradorStatus.setStyleSheet("QLabel#geradorStatus {color: red; font: bold 12pt; " +
                                                  "font-family: Tahoma;}")
            self.conectarBtGer.setIcon(self.iconConectar)
            self.conectarBtGer.setToolTip(
                QtGui.QApplication.translate("Dialog", "Conecta o gerador", None,
                                             QtGui.QApplication.UnicodeUTF8))

    def falha(self, data):
        if data == 'Scope':
            QMessageBox.warning(self, "Falha ao conectar", "Osciloscópio não encontrado, verifique as conexões.",
                                QMessageBox.Ok)
        if data == 'Gerador':
            QMessageBox.warning(self, "Falha ao conectar", "Gerador não encontrado, verifique as conexões.",
                                QMessageBox.Ok)

    def salvarFreq(self):
        dir = "."
        self.outFile = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        self.dadosFreq.to_csv(self.outFile[0] + '_dados.csv', sep='\t', index=False)
        im = cv2.imread('varreduraFreq.png')
        cv2.imwrite(self.outFile[0]+'_grafico.png', im)

    def closeEvent(self, event, *args, **kwargs):
        """Ritual de encerramento"""

        result = QMessageBox.question(self, __appname__, "Tem certeza que deseja sair?",
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)

        if result == QMessageBox.Yes:
            """Colocar aqui tudo que tiver que ser feito antes de sair"""
            self.equipamentos_thread.desconectarGerador = True
            self.equipamentos_thread.desconectarScope = True
            self.equipamentos_thread.interromperFreq = True
            self.equipamentos_thread.varrerFreq = False
            self.equipamentos_thread.start()
            time.sleep(1)
            event.accept()
        else:
            event.ignore()


def escreve_dicionario():
    __nome__ = __modulo__ + '10'
    mensagem = 'Escrevendo dicionario'
    print('{};{}'.format(__nome__, mensagem))

    with open('Dicionario.csv', 'w', newline='') as g:
        spamwriter = csv.writer(g, delimiter=';', dialect='excel')
        spamwriter.writerow(['M00', 'Main', 'Main'])
        spamwriter.writerow(['M01', 'MainDialog', 'varreduraFreq'])
        spamwriter.writerow(['M02', 'MainDialog', 'interromperFreq'])
        spamwriter.writerow(['M03', 'MainDialog', 'varreduraFreqTerminada'])
        spamwriter.writerow(['M04', 'EquipamentosThread', 'run'])
        spamwriter.writerow(['M05', 'MainDialog', 'graficoBranco'])
        spamwriter.writerow(['M06', 'MainDialog', 'limparVarreduraFreq'])
        spamwriter.writerow(['M07', 'GraphThread', 'run'])
        spamwriter.writerow(['M08', 'MainDialog', 'atualizaGraficoFreq'])
        spamwriter.writerow(['M09', 'MainDialog', 'salvarFreq'])
        spamwriter.writerow(['M10', 'MainDialog', 'capturaTela'])
        spamwriter.writerow(['M11', 'MainDialog', 'atualizaGraficoTela'])
        spamwriter.writerow(['M12', 'MainDialog', 'graficoBrancoTela'])
        spamwriter.writerow(['M13', 'MainDialog', 'recebendoTela'])
        spamwriter.writerow(['M14', 'MainDialog', 'salvarTela'])
        spamwriter.writerow(['M15', 'MainDialog', 'FFT'])
        spamwriter.writerow(['M16', 'MainDialog', 'Cálculo FFT'])
        spamwriter.writerow(['M17', 'MainDialog', 'atualizaGraficoFft'])
        spamwriter.writerow(['M18', 'MainDialog', 'graficoBrancoFft'])
        spamwriter.writerow(['M19', 'MainDialog', 'salvarFft'])


if __name__ == '__main__':
    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("IFGW Unicamp")
    QCoreApplication.setOrganizationDomain("portal.ifi.unicamp.br")

    app = QApplication(sys.argv)
    form = MainDialog()
    form.show()
    app.exec_()
